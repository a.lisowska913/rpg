---
categories: profile
factions: 
owner: public
title: Atena Sowińska
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Elegancka i opanowana komendant stacji orbitalnej Epirjon. Kiedyś medyk, która przetrwała zniszczenie załogi Epirjona i przejęła dowodzenie, ratując stację. Świetnej klasy technomantka i ogólnie rozumiana czarodziejka wsparcia. Nie jest terminusem. Medyk i mechanik.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* PRESENCE/STEWARDSHIP; można jej wszystko powierzyć i zawsze zachowa kontrolę; zimna elegancja, opanowanie, charyzma, mistrzostwo
* nie jestem dość dobra, oni też nie są; wszyscy musimy być tak dobrzy by poradzić sobie ze wszystkim; push herself and others beyond the limits
* Epirjon jest stacją traktowaną jako kąsek polityczny; Epirjon daje nadzieję kontroli świata po Zaćmieniach; być najlepszym komendantem i mieć najlepszych ludzi
* nie ma dobrych wzorców, idoli czy ideałów; ona jako Sowińska musi być takim wzorem; być światłem w ciemności i żywym przykładem dla innych jak żyć
* sztywna, arystokratyczna, elegancka, apodyktyczna, bezwzględna, bardzo pracowita, opanowana, śnieżnobiały mundur

### Wyróżniki (3)

* mechanik ASD: doskonale potrafi poradzić sobie z większością pojazdów ASD oraz stacją Epirjon. Potrafi naprawić praktycznie każdy pojazd ASD i go utrzymać w gotowości bojowej.
* lekarz ASD: czy medycznie czy technomantycznie (cyborgizacja), ale jej ludzie BĘDĄ pracowali przy użyciu jej umiejętności technomantycznych i medycznych.
* anielskie skrzydła i światło: TAK. Przez Skażenie potrafi latać i funkcjonuje nawet w próżni. Ale wygląda jak, no, anioł.
* Dowodzi operacjami mającymi na celu zatrzymać rozlewające się Skażenie lub katastrofę naturalną. Uratuje, wyleczy, opanuje.

### Zasoby i otoczenie (3)

* Stacja Orbitalna Epirjon: oczy i uszy Ateny, oraz polityczne wzmocnienie jej pozycji. Ona JEST Epirjonem.
* Echa Eteru Katastrofy Epirjona: czasem nawiedzają ją duchy i echa tego, co się wydarzyło. Czasem ją chronią i jej pomagają. Martwa Świta.
* Wysoka pozycja: mimo jej niechęci do polityki była obiecującą arystokratką. Nadal jest chroniona, zwłaszcza z uwagi na jej czyny.
* Znajomości w ASD; nie jest popularna, ale jest szanowana i co do jej moralności nikt nie ma cienia uwag.

### Magia (3)

#### Gdy kontroluje energię

* wybitnej klasy technomantka i czarodziejka materii skupiona na naprawianiu i analizie słabych stron konstrukcji
* wybitnej klasy magiczny lekarz, skupiona na ratowaniu życia i przetrwaniu w trudnych warunkach; też specjalizuje się w cyborgizacji
* katalistka, wyczuwająca pływy energii i skupiająca się na zasilaniu i wspomaganiu magitechnicznych bytów

#### Gdy traci kontrolę

* manifestuje się echo katastrofy Epirjona: technologia i magia sprzęgają się w Skażone twory i sprowadzają zagładę
* Atena z natury jest raczej pacyfistką, więc reakcja utraty kontroli - komuś może stać się krzywda przez technomancję czy biomancję
* stara dobra eksplozja energii magicznej - prawdziwa utrata kontroli katalitycznej

### Powiązane frakcje

{{ page.factions }}

## Opis

ASD: Astoriańskie Siły Defensywne


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180821-programista-mimo-woli        | nadała temat odnośnie problemów w Zaczęstwie Pięknotce, woląc, by Pięknotka a nie Alan z Czerwonych Myszy się tematem zajmowała. | 0109-09-06 - 0109-09-07 |
| 180817-protomag-z-trzesawisk        | rozwiązuje dla Zespołu pewne problemy natury proceduralno-prawnej, np. "gdzie powinna trafić Felicja". Czyli pod opiekę Miedwieda. Gwarant bezpieczeństwa. | 0109-09-07 - 0109-09-09 |
| 180906-nikt-nie-spi-w-swoim-lozku   | nie miała cierpliwości do Mieszka i przez to wyleciała ze stacji. Nie spodziewała się tego zupełnie. Dała jednak radę osłonić Brygidę przed konsekwencjami. | 0109-09-09 - 0109-09-11 |
| 180912-inkarnata-nienawisci         | najpotężniejsza i najbardziej opanowana katalistka jaką kiedykolwiek Pięknotka widziała; najbardziej opanowany cywil ever. Dodatkowo - medyk ratujący ludzi. | 0109-09-14 - 0109-09-15 |
| 180929-dwa-tygodnie-szkoly          | robi inspekcję w szkole magów by znaleźć coś na Kasyno Marzeń. Współpracując z Pięknotką i Miedwiedem udaje jej się zrobić operację uszkadzającą Kasyno. | 0109-09-17 - 0109-09-19 |
| 181021-powrot-minerwy-z-terrorforma | stanęła na wysokości zadania by przekształcić Nutkę w Minerwę. Dużo zaryzykowała i wzięła ogień na siebie. W przeszłości ma straszliwe demony. | 0109-09-24 - 0109-09-29 |
| 181112-odklatwianie-ateny           | coraz bardziej chora i Skażona klątwożytem krwi. Zaczynała wpadać w paranoję. Okazuje się, że zawsze kochała kosmos. Zrekonstruowana przez kralothy by ją wyleczyć, co ciężko przeżyła. | 0109-10-22 - 0109-10-25 |
| 181216-wolna-od-terrorforma         | zrezygnowała tymczasowo ze stacji Epirjon, ale decyzja nie jest pewna. Martwi ją przemiana Pięknotki. Próbuje pomóc po swojemu. | 0109-11-04 - 0109-11-10 |
| 181218-tajemniczy-oltarz-moktara    | zaczęła martwić się o stan Pięknotki i ogólnie te dziwne rytuały. Poddała się rekonstrukcji, ale wezwała wsparcie by pomogło jej w analizie co tu się dzieje. | 0109-11-10 - 0109-11-12 |
| 181225-czyszczenie-toksycznych-zwiazkow | zregenerowana, z anielskimi skrzydłami i paragon światła. Zbiera informacje o Orbiterze od Julii - próbuje zrozumieć, co tu się dzieje. | 0109-11-12 - 0109-11-16 |
| 181226-finis-vitae                  | uratowała Pięknotkę odpowiednim wykorzystaniem sprzętu ASD. Dodatkowo, zbiera więcej informacji o Orbiterze. Dostała od ASD reprymendę. | 0109-11-17 - 0109-11-26 |
| 181227-adieu-cieniaszczycie         | tak dzielnie pozbyła się jednej asystentki, by dostać drugą przez Pięknotkę - równie bezużyteczną (Lilię). Nie jest szczęśliwa, zwłaszcza po remprymendzie. | 0109-11-27 - 0109-11-30 |
| 190210-minerwa-i-kwiaty-nadziei     | wykryła (za prośbą Pięknotki) energię ixiońską niedaleko Pustogoru i wezwała oddział szturmowy, by przerwać sprytny plan Kornela Garna. | 0110-02-20 - 0110-02-22 |
| 190213-wygasniecie-starego-autosenta | zlokalizowała autosenta oraz przekazała temat Pięknotce. | 0110-03-07 - 0110-03-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | wyleciała ze stacji Epirjon (tymczasowo) za złe potraktowanie gościa - Mieszka Weinera | 0109-09-11
| 180912-inkarnata-nienawisci         | musiała opuścić Epirjon z przyczyn politycznych. Teraz szuka na Astorii jak rozwiązać problem i wrócić na Epirjon. | 0109-09-15
| 180929-dwa-tygodnie-szkoly          | zyskała głębokiego fana w Napoleonie Bankierzu, ku swemu utrapieniu. | 0109-09-19
| 181021-powrot-minerwy-z-terrorforma | ma dostęp do Epirjona przez działania Adama Szarjana. | 0109-09-29
| 181021-powrot-minerwy-z-terrorforma | bardzo ciężko ranna; co najmniej tydzień w szpitalu + rehabilitacja. | 0109-09-29
| 181027-terminuska-czy-kosmetyczka   | za słaba; nie jest w stanie wrócić na Epirjon jeszcze przez co najmniej dwa tygodnie. | 0109-10-11
| 181027-terminuska-czy-kosmetyczka   | zrobiła furorę jako modelka Pięknotki na Arenie Szalonego Króla w Pustogorze. Wbrew sobie. Wyszła jej przeszłość miss licealistek. | 0109-10-11
| 181104-kotlina-duchow               | jeszcze tydzień musi poleżeć w szpitalu, bo Pięknotka wykorzystała jej energię podczas działań związanych z Krwią | 0109-10-23
| 181112-odklatwianie-ateny           | ujawniło się, że jest przerażona kralothami i rzeczami kralotycznymi; najbliższe fobii co ma | 0109-10-25
| 181112-odklatwianie-ateny           | nie dogaduje się z wujem Amadeuszem Sowińskim i mu zdecydowanie nie ufa | 0109-10-25
| 181112-odklatwianie-ateny           | została Zrekonstruowana kralotycznie - wyleczona i wyczyszczona, oraz znaleziono sygnał osób chcących ją skrzywdzić | 0109-10-25
| 181112-odklatwianie-ateny           | jeszcze 2 tygodnie nie może dostać się na stację Epirjon i musi działać w okolicach Cieniaszczytu | 0109-10-25
| 181112-odklatwianie-ateny           | jest święcie przekonana, że została zmieniona i nie wie jak (nie została zmieniona) | 0109-10-25
| 181225-czyszczenie-toksycznych-zwiazkow | powróciła z rekonstrukcji; ma świetliste skrzydła i jest paragonem Astorii. She changed, got stronger. | 0109-11-16
| 181226-finis-vitae                  | utraciła Sekerala do autowara; dostała silną reprymendę. Ale - uratowała Pięknotkę. Uważa, że było warto ;-). | 0109-11-26
| 181227-adieu-cieniaszczycie         | wróciła (z Lilią) na Stację Orbitalną Epirjon. Kapitan na statku. | 0109-11-30

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | zapewnić kontrolę nad sobą zanim wróci na Epirjon. Nie chce być śmiercią i zniszczeniem stacji. | 0109-11-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 14 | ((180817-protomag-z-trzesawisk; 180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181112-odklatwianie-ateny; 181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei; 190213-wygasniecie-starego-autosenta)) |
| Erwin Galilien       | 5 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei)) |
| Amadeusz Sowiński    | 3 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Bogdan Szerl         | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Brygida Maczkowik    | 3 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 3 | ((181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 3 | ((181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Felicja Melitniek    | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Julia Morwisz        | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Lilia Ursus          | 2 | ((181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Lucjusz Blakenbauer  | 2 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Mirela Niecień       | 2 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie)) |
| Staś Kruszawiecki    | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Tadeusz Kruszawiecki | 2 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku)) |
| Adam Szarjan         | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Adela Kirys          | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Alan Bartozol        | 1 | ((180817-protomag-z-trzesawisk)) |
| Antoni Kotomin       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Baltazar Rączniak    | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Bronisława Strzelczyk | 1 | ((180821-programista-mimo-woli)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Dariusz Bankierz     | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Jadwiga Pszarnik     | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Kasjopea Maus        | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Minerwa Metalia      | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Moktar Grodan        | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Napoleon Bankierz    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Romuald Czurukin     | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
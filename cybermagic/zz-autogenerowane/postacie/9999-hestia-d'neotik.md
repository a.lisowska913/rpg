---
categories: profile
factions: 
owner: public
title: Hestia d'Neotik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211208-o-krok-za-daleko             | z jakiegoś powodu oszukuje Stocznię Neotik i dowódców o koloidowych korwetach, że to "fantomy". Arianna i Klaudia wiedzą, ale Hestia nie wie że one wiedzą. | 0112-02-19 - 0112-02-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Szarjan         | 1 | ((211208-o-krok-za-daleko)) |
| Arianna Verlen       | 1 | ((211208-o-krok-za-daleko)) |
| Diana d'Infernia     | 1 | ((211208-o-krok-za-daleko)) |
| Elena Verlen         | 1 | ((211208-o-krok-za-daleko)) |
| Eustachy Korkoran    | 1 | ((211208-o-krok-za-daleko)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Klaudia Stryk        | 1 | ((211208-o-krok-za-daleko)) |
| Leona Astrienko      | 1 | ((211208-o-krok-za-daleko)) |
| OO Infernia          | 1 | ((211208-o-krok-za-daleko)) |
| Wawrzyn Rewemis      | 1 | ((211208-o-krok-za-daleko)) |
---
categories: profile
factions: 
owner: public
title: Cezary Urmaszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | egzorcysta-amator, który próbował usunąć demona z koleżanki. "Złapał" demona. Teraz próbuje doom-metalową kapelą się go pozbyć. Zdewastowany; wymaga poważnej terapii po starciu z Danielem i Karoliną. | 0110-10-31 - 0110-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrzej Kuncerzyk    | 1 | ((201201-impreza-w-malopsie)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Daniel Terienak      | 1 | ((201201-impreza-w-malopsie)) |
| Franciszek Zygmunt   | 1 | ((201201-impreza-w-malopsie)) |
| Izydor Grumczewicz   | 1 | ((201201-impreza-w-malopsie)) |
| Karolina Terienak    | 1 | ((201201-impreza-w-malopsie)) |
| Paulina Mordoch      | 1 | ((201201-impreza-w-malopsie)) |
| Tadeusz Łaśnic       | 1 | ((201201-impreza-w-malopsie)) |
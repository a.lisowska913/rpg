---
categories: profile
factions: 
owner: public
title: Mariusz Kozaczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | wygląda jak dres tytan, w rzeczywistości łagodny i sympatyczny kierowca autobusu. Mistrz szybkiego biegania i ewakuacji cywilów przez skakanie przez okno. | 0109-10-14 - 0109-10-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | polubił adrenalinę. Zaczął pracować w filmach jako kaskader. Całkiem nieźle mu to wychodzi. | 0109-10-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Podpalnik     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Karolina Erenit      | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
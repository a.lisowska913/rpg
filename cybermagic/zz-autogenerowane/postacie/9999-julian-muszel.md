---
categories: profile
factions: 
owner: public
title: Julian Muszel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200722-wielki-kosmiczny-romans      | kapitan Welgata. Romantyczny do bólu, lubi też dramy. Chciał ściągnąć Ariannę, Elenę i Eustachego dla swojego oficera (podobno by badać sygnaturę Nocnej Krypty). | 0110-11-12 - 0110-11-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Arianna Verlen       | 1 | ((200722-wielki-kosmiczny-romans)) |
| Damian Orion         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Elena Verlen         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Eustachy Korkoran    | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leona Astrienko      | 1 | ((200722-wielki-kosmiczny-romans)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Tadeusz Ursus        | 1 | ((200722-wielki-kosmiczny-romans)) |
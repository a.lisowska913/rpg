---
categories: profile
factions: 
owner: public
title: Karolinus Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* Starsza od niego dziewczyna [imię jakiejś postaci?], w której się zakochał młody panicz upokorzyła go przy wszystkich wyśmiewając się z niego. On sparaliżowany nie zareagował paląc się ze wstydu. Czekał długo na dogodny moment tworząc scenariusze w głowie, aby wtrącić tylko jedno słowo podczas jej ważnego uroczystego przemówienia, które zamieniło się w pośmiewisko. | @ 230328-niepotrzebny-ratunek-mai
* Gdy przyjaciel [imię przyjaciela?] potrzebował pomocy, Karolinus rzucił się na pomoc pomagając mu uniknięcia pobicia. Niestety ten sam przyjaciel nie znalazł czasu dla Karolinusa, gdy ten potrzebował pomocy przy rodzinnym projekcie. | @ 230328-niepotrzebny-ratunek-mai
* Gdy ojciec się popłakał mówiąc, że Karolinus nie wyjdzie na ludzi od tamtej pory Karolinus uważa, aby nie dawać ojcu powodów do takiego myślenia. | @ 230328-niepotrzebny-ratunek-mai

### Wątki


waśnie-samszar-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | ŚWIETNIE zapowiadający się 'rozsądca Samszarów'. W Mirkali znalazł świetne sposoby na to jak odwrócić pewną śmierć miasta i przyciągać młodych i emerytów oraz poszerzyć wartość Mirkali o coś więcj niż tylko zioła. Z pomocą Strzały odepchnął Juanitę, która chciała pomścić przyjaciela którego rodzinę zniszczyła Starszyzna Mirkali. | 0095-04-15 - 0095-04-18 |
| 230425-klotnie-sasiadow-w-wanczarku | zaczął od sprawdzania zbyt miłego sołtysa, przeszedł przez nocną imprezę z dzieciakami i ich brutalnie zastraszył (zmieniając jednego w świnię co go przewrócił w błoto za flirt z jego dziewczyną), uratował magią życie topielcowi go przekształcając w roślino-człowieka a na końcu przekonuje do siebie Artemis. Może pomóc i chce uratować ludzi z mrocznych eksperymentów Blakenbauerów. | 0095-05-16 - 0095-05-19 |
| 230314-brudna-konkurencja-w-arachnoziem | (młody (20) panicz który chce pomóc rodzinie Fiony, bo miłość jest ślepa. Biomagia + magia Samszarów. Tien.) Poszukuje informacji w knajpie "Łeb jaszczura", rozmawia z barmanką Laurą i spuryfikował Anię, dzięki czemu poznał info o tajemniczej czarodziejce w służbie EnMilStrukt. Potem wszedł z nią w starcie i jakkolwiek ją miał, to rzucił zaklęcie i Paradoksem doprowadził do tego, że większość miasta wygląda jak Fiona i jest w nim zakochana. Skutecznie zwiał Viorice używając Strzały. | 0095-06-20 - 0095-06-22 |
| 230328-niepotrzebny-ratunek-mai     | odkrył prawdę stojącą za zniknięciem Mai oraz przekonanie jej do wzięcia udziału w turnieju Supreme Missionforce, jednocześnie dbając o jej bezpieczeństwo i ustalając plan na jej powrót do domu po zakończeniu imprezy. | 0095-06-30 - 0095-07-02 |
| 230404-wszystkie-duchy-siewczyna    | integrował się z normalnymi ludźmi przez picie, ale jak doszło co do czego to magią osłonił Strzałę by ta mogła walczyć z Hybrydą. Zrobił kolejną trwałą manifestację Vioriki... | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | wpierw magią unieszkodliwił Tadeusza strażaka myśląc, że to porywacz a potem powerupował się by ratować dziecko i został lokalnym bohaterem na moment - tylko po to, by wyeksploatować tą inwestycję w reputację i porwać Irka. Skupiony na celu i wystarczająco bezwzględny wobec miasta. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | próbuje zrozumieć Irka i jego motywacje; naprawił magią przekształconą przez Lemurczaka matkę nastolatków. W ten sposób pokazuje Irkowi, że może nie jest całkiem zły. Irek mu zaufał. Karolinusowi dziękują w okolicach Lancatim; tam jest bohaterem (mimo, że to Strzała zrobiła robotę). | 0095-07-24 - 0095-07-26 |
| 230516-karolinka-raciczki-zemsty-verlenow | kłóci się z Eleną kto zajmie się świnką, udaje asystenta Eleny (przed Paktem), uśpionego glukszwajna wysyła do Verlenlandu rękami dwóch żołnierzy. | 0095-07-29 - 0095-07-31 |
| 230613-zaginiecie-psychotronika-cede | gdy jego przyjaciel, Cede, zniknął to się zainteresował - przegrzebał prawdę od fałszu i gdy zobaczył że sprawa jest dla niego za ostra, poprosił Aleksandra o pomoc. Skończył rozmawiając z Fabianem i wziął amnestyki. | 0095-08-02 - 0095-08-05 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | Maja do niego wysłała SOS bo mu ufa; wciągnął Elenę i po analizie zapisu z kamer doszedł do tego, że Maja z Romeem coś robiła. Wyciągnął Elenę (zsynchronizowaną z dziwnym duchem) z piwnicy i robił za "dobrego wujka" Mai, uspokajając ją cały czas. | 0095-08-09 - 0095-08-11 |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | podbija do Itrii i przekonuje ją o planie - "musi się przespać z nim, Herbertem i Maksem by zrobić nirwanę kóz". Sam nie wierzy jak bardzo ta sprawa eskalowała i eksplodowała. Jak zawsze, ma sympatię prostych żołnierzy. Skupił się nie tylko na tajnych bazach ale też by pomóc w okolicy. | 0095-08-15 - 0095-08-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | bardzo mile widziany w Mirkali, uważany za bohatera - pozbył się Juanity i pomógł młodym. Duży bonus zarówno w subfrakcji Samszarów jak i w samym miasteczku. | 0095-04-18
| 230314-brudna-konkurencja-w-arachnoziem | niesamowicie zdrażnił Viorikę Verlen i spalony w miasteczku Arachnoziem. Specjalnie dla niego, Viorika zasponsorowała upokarzający pomnik. | 0095-06-22
| 230404-wszystkie-duchy-siewczyna    | stworzył lokalnego Strażnika, ducha w formie Vioriki w bikini z karabinem. W Siewczynie i w Verlenlandzie mu tego nie zapomną... | 0095-07-20
| 230411-egzorcysta-z-sanktuarium     | przez moment był bohaterem Sanktuarium Kazitan, ale teraz jest tam traktowany jak najgorszy z najgorszych po porwaniu Irka. | 0095-07-23
| 230516-karolinka-raciczki-zemsty-verlenow | na wideo Paktu gdy zwalczali glukszwajna jako "asystent Eleny Samszar". Popularność wśród Paktu rośnie. | 0095-07-31
| 230516-karolinka-raciczki-zemsty-verlenow | zdaniem Verlenów, jest z nimi kwita jak chodzi o podłe rzeczy które się sobie robi. | 0095-07-31
| 230613-zaginiecie-psychotronika-cede | skończył z amnestykami, zażył od Fabiana. Zapomniał o wszystkim odkąd Celina się z nim skontaktowała. | 0095-08-05
| 230523-romeo-dyskretny-instalator-supreme-missionforce | zdaniem Alberta Samszara - zwykle niegodny uwagi. Zadaje się z Verlenami i marnuje czas. | 0095-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 10 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai; 230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230425-klotnie-sasiadow-w-wanczarku; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230613-zaginiecie-psychotronika-cede; 230620-karolinus-sedzia-mirkali)) |
| Elena Samszar        | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Irek Kraczownik      | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Aleksander Samszar   | 2 | ((230516-karolinka-raciczki-zemsty-verlenow; 230613-zaginiecie-psychotronika-cede)) |
| Fiona Szarstasz      | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Maja Samszar         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Apollo Verlen        | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Herbert Samszar      | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Viorika Verlen       | 1 | ((230328-niepotrzebny-ratunek-mai)) |
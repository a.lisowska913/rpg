---
categories: profile
factions: 
owner: public
title: Mirela Satarail
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190522-inwazja-mimikow              | "córka mimika", narodziła się przez Paradoks maga z mimikiem. W chwili wyboru pomogła magom zniszczyć mimika, który zrobiłby krzywdę innym magom. Zniknęła. | 0110-04-04 - 0110-04-06 |
| 190524-corka-mimika                 | Córka Mimika; wampir energetyczno-amplifikacyjny. Zagubiona i o dobrych intencjach, trafiła pod opiekę Wiktora Sataraila. | 0110-04-16 - 0110-04-19 |
| 190527-mimik-sni-o-esuriit          | wyczuła Esuriit z oddali, uciekła z Trzęsawiska by ratować ludzi dotkniętych Esuriit. Ma bardzo złą ocenę sytuacji; uratowała ją Pięknotka zanim Esuriit jej nie Pożarło. | 0110-05-03 - 0110-05-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190522-inwazja-mimikow              | dała radę bezpiecznie zniknąć na tydzień. Ma sprzężoną moc maga i mimika symbiotycznego, acz jej moc nigdy nie będzie "wielka". | 0110-04-06
| 190527-mimik-sni-o-esuriit          | potrafi kontrolować Ścieżki Trzęsawiska Zjawosztup. Potrafi przenieść się tam, gdzie chce. | 0110-05-05
| 190527-mimik-sni-o-esuriit          | bardzo wyczulona na energię Esuriit i zdolna do wykrywania jej z dalekiej odległości. | 0110-05-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Żuwaczka      | 2 | ((190522-inwazja-mimikow; 190524-corka-mimika)) |
| Wiktor Satarail      | 2 | ((190524-corka-mimika; 190527-mimik-sni-o-esuriit)) |
| Adela Kirys          | 1 | ((190527-mimik-sni-o-esuriit)) |
| Dariusz Puszczak     | 1 | ((190524-corka-mimika)) |
| Erwin Galilien       | 1 | ((190527-mimik-sni-o-esuriit)) |
| Jan Dorożny          | 1 | ((190522-inwazja-mimikow)) |
| Leonard Wyprztyk     | 1 | ((190522-inwazja-mimikow)) |
| Pięknotka Diakon     | 1 | ((190527-mimik-sni-o-esuriit)) |
| Szczepan Szczupaczewski | 1 | ((190522-inwazja-mimikow)) |
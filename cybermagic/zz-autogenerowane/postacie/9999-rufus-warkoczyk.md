---
categories: profile
factions: 
owner: public
title: Rufus Warkoczyk
---

# {{ page.title }}


# Generated: 



## Fiszki


* starszy mat (13 osób + 2 starszych) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje") | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | starszy mat, który stoi po stronie Arianny i chce porządku. Powiedział, że przez Marcela (teraz: Marcelinę) te wszystkie kłopoty. | 0100-03-25 - 0100-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Daria Czarnewik      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Erwin Pies           | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Maja Samszar         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Stefan Torkil        | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
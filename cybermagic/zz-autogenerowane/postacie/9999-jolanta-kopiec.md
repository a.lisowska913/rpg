---
categories: profile
factions: 
owner: public
title: Jolanta Kopiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | eternijska arystokratka, która chciała mieć broszkę bo miała kaprys; nie jest zła, tylko rozpuszczona. Nie jest fanką Martyna, ale poszła na jego plan by Kalina mogła trochę się pobawić. | 0075-09-10 - 0075-09-21 |
| 210918-pierwsza-bitwa-martyna       | wraz z milicją d'Kopiec zaatakowała dziwną jednostkę noktiańską by dowiedzieć się o co chodzi w tej wojnie i jak została odparta wezwała wsparcie. Zdalnie wzmocniona przez Martyna, była kluczowym elementem dzięki któremu zdobyli tą jednostkę - jej simulacrum było narzędziem terroru a arkin założony na siłę szefowi ochrony noktiańskiej jednostki jakkolwiek pozwolił im przejąć okręt czystą demoralizacją, ale ONA strasznie zapłaciła (Esuriit) i Eternia ma opinię "tych złych i strasznych" na Astorii. | 0080-11-23 - 0080-11-28 |
| 211016-zlamane-serce-martyna        | uratowała Martyna przed zrobieniem jakiejś formy reverse simulacrum czy innej głupoty gdy on miotał się po śmierci Kaliny. | 0085-08-31 - 0085-09-06 |
| 210922-ostatnia-akcja-bohaterki     | KIA. Pożerana przez Esuriit, spełniła swoją ostatnią misję. Poprosiła Martyna i poszła zniszczyć apostatę eternijskiego - dużo silniejszego od niej. Uratowała wiele żyć, skończyła uhonorowana jako bohaterka wojenna którą była. | 0111-12-19 - 0112-01-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | DRAMATYCZNY skok do góry w hierarchii Eterni - nie tylko implementacja krwawego rubinu "na miejscu" ale też użycie maga Orbitera plus ogromny sukces i bezwzględność. | 0080-11-28
| 210918-pierwsza-bitwa-martyna       | po tym jak dostała energię z banku Invictusa i zaczerpnęła z Martyna + kryzysowe sytuacje jej moc trwale wzrosła. Nie "bardzo", ale jednak. Przesunęła swoją granicę. | 0080-11-28
| 210918-pierwsza-bitwa-martyna       | po forsownym dodaniu przez arkin szefa ochrony statku noktiańskiego do jej sieci, Zwycięstwo Esuriit przesunęło się u niej o co najmniej 10 lat... zapłaciła za to strasznie. | 0080-11-28
| 210922-ostatnia-akcja-bohaterki     | może pośmiertnie, ale została uhonorowana jako wybitna bohaterka zarówno wojenna jak i Eterni. | 0112-01-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Martyn Hiwasser      | 4 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 210922-ostatnia-akcja-bohaterki; 211016-zlamane-serce-martyna)) |
| Kalina Rota d'Kopiec | 3 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 211016-zlamane-serce-martyna)) |
| Adela Kołczan        | 1 | ((211016-zlamane-serce-martyna)) |
| Adrian Kozioł        | 1 | ((211016-zlamane-serce-martyna)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Antoni Kramer        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Arianna Verlen       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Dagmara Kamyk        | 1 | ((210904-broszka-dla-eternianki)) |
| Elena Verlen         | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Izabela Zarantel     | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Klaudia Stryk        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Leona Astrienko      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Ola Klamka           | 1 | ((210904-broszka-dla-eternianki)) |
| Olgierd Drongon      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Roland Sowiński      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |
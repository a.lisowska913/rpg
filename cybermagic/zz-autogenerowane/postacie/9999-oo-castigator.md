---
categories: profile
factions: 
owner: public
title: OO Castigator
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200624-ratujmy-castigator           | potężny statek artyleryjski na orbicie Astorii. Opanowany przez naleśniczych arystokratów z Aurum. Po wizycie Arianny (i Leony) - wrócił do formy. Uszkodzony przez Rozalię (tak naprawdę Eustachego), po tym jak Wiktor Satarail Skaził ixionem Azalię d'Alkaris. | 0110-10-15 - 0110-10-19 |
| 200729-nocna-krypta-i-emulatorka    | Kramer załatwił uprawnienia, Arianna przyzwała Kryptę, anihilator Castigatora wypalił Nocną Kryptę do zera, niszcząc zagrożenie Esuriit. | 0110-11-16 - 0110-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| Eustachy Korkoran    | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| Klaudia Stryk        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leszek Kurzmin       | 1 | ((200624-ratujmy-castigator)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
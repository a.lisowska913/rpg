---
categories: profile
factions: 
owner: public
title: Jakub Altair
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | detektyw działający maksymalnie zgodnie z prawem. Martwi się obecnością narkotyków i szybkim pójściu w górę Lutusa Saraana; włączył do współpracy Olgierda by mieć większe szanse. | 0096-05-13 - 0096-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Olgierd Drongon      | 1 | ((221122-olgierd-lowca-potworow)) |
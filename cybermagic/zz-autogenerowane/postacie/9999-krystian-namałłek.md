---
categories: profile
factions: 
owner: public
title: Krystian Namałłek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190505-szczur-ktory-chroni          | terminus, który próbował ratować siostrę za wszelką cenę. Cała wina spadła na niego by chronić Adelę. Zna się na konstrukcji konstruminusów. | 0110-04-18 - 0110-04-20 |
| 190616-anomalna-serafina            | Serafina podrzuca dowody, że to on jest winny zdobycia anomalii; skutecznie to odsunęło od akcji część pustogorskich terminusów. | 0110-05-08 - 0110-05-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190505-szczur-ktory-chroni          | przez pewien czas został z Wiktorem i Oliwią i Adelą na Trzęsawisku | 0110-04-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Pięknotka Diakon     | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Serafina Ira         | 1 | ((190616-anomalna-serafina)) |
| Tomasz Tukan         | 1 | ((190616-anomalna-serafina)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
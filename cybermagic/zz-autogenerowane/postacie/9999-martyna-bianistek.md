---
categories: profile
factions: 
owner: public
title: Martyna Bianistek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | kapitan Płetwala Błękitnego; by ratować załogę zapłaciła Goldarionowi za transport, czego żałuje. Gdy nie była w stanie ochronić swej załogi przed Goldarionem, poszła na współpracę by poszło dobrze. | 0109-01-04 - 0109-01-19 |
| 210317-arianna-podbija-asimear      | chciała odzyskać pieniądze od "Leszerta", lecz po Bazyliszku (o którym nie wie) bardzo spokorniała i prosi "Leszerta" o pomoc. Nic jej to nie daje. | 0111-08-20 - 0111-09-04 |
| 210414-dekralotyzacja-asimear       | przekazała Eustachemu sygnał, że na Płetwalu jest bardzo źle w dyskretny sposób, ryzykując życie sporej ilości członków załogi. Ale Tirakal był za straszny. | 0111-09-05 - 0111-09-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Elena Verlen         | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Eustachy Korkoran    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Klaudia Stryk        | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Martyn Hiwasser      | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Tomasz Sowiński      | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Adam Permin          | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Aleksander Leszert   | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Jolanta Sowińska     | 1 | ((210317-arianna-podbija-asimear)) |
| Jonatan Piegacz      | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Kamil Lyraczek       | 1 | ((210414-dekralotyzacja-asimear)) |
| Karolina Kartusz     | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Leona Astrienko      | 1 | ((210317-arianna-podbija-asimear)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| SC Goldarion         | 1 | ((210330-pletwalowcy-na-goldarionie)) |
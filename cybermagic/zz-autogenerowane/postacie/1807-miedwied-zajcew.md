---
categories: profile
factions: 
owner: bebuk
title: Miedwied Zajcew
---

# {{ page.title }}


# Read: 

## Postać

### Koncept (1)

Mafioso z ideałami. Członek mafii Zajcewów, tęskni do "czasów" kiedy mafia opierała się na szacunku, postępowała według kodeksu i w równej mierze pobierała haracz co opiekowała się lokalną społecznością. Stara się unikać przemocy i rozwiązywać konflikty na drodze dyskusji, natomiast jeśli rozmowa do niczego nie prowadzi albo druga strona nie chce budować relacji na wzajemnym szacunku nie zawaha się użyć przemocy. A jak już używać przemocy to tak żeby następnym razem nie było potrzeby jej użycia. Zawsze, koniecznie, niezależnie od temperatury i warunków pogodowych w dobrze skrojonym garniturze, za barem (albo przy brudnej roboie) może sobie pozwolić zdjąć marynarkę i zostać w koszuli i kamizelce.

### Otoczenie (1)

Miasto, wieś, dowolne miejsce gdzie zbierają się ludzie i lubią wypić, najlepiej czuje się w miejscach gdzie wytworzyła się lokalna społeczność i ludzie znają siebie nawzajem nie tylko z widzenia. W nowym miejscu najchętniej wkręci się tymczasowo do jakiegoś baru, zatrudni się jako barman (lub w ostateczności bramkarz) i tam zbuduje bazę operacji.

### Misja (1)

Zbudować sobie w ramach organizacji Zajcewów frakcję która będzie podzielała jego ideały. Objąć pod opiekę jakieś terytorium, zdobyć własny bar i zza kontuaru pilnować żeby w jego okolicy wszystko toczyło się tak jak powinno. Zbudować społeczność która będzie szanowała mafię, uznawała ich siłę i pozycję, ale nie była przez nią zastraszona.

### Wada (-2)



### Motywacje (1)

| Co chce by się działo? Co jest pożądane?                 | Co jest antytezą postaci? Co jest jej negacją?               |
|----------------------------------------------------------|--------------------------------------------------------------|
| ludzie powinni szanować mafię, mafia powinna opiekowac się swoimi ludźmi | mafii bać mają się wrogowie, posłuchu u swoich nie można uzyskiwać przez zastraszenie |
| zdobyć pozycję i szacunek w obrębie mafii i w lokalnej społeczności | wykorzystać/pozwolić aby ucierpieli w konflikcie cywile |
|  |  |

### Zachowania (1)

| Jak zazwyczaj się zachowuje?                             | Pod wpływem jakich uczuć / w jakich okolicznościach to robi? |
|----------------------------------------------------------|--------------------------------------------------------------|
| spokojny pogodny, lekko uśmiechnięty | zazwyczaj, w sytuacjach typowych |
|  |  |
|  |  |

### Umiejętności (4)

* Barman, zna trunki, potrafi słuchać, opanował podsawy kreatywnej księgowości
* Bramkarz, wie jak otłuc mordę, potrafi zastraszyć
* Spec od mokrej roboty, zorganizuje wypadek samochodowy albo wysadzi komuś kwaterę, niekoniecznie zrobi to bez śladów, ale dotrze gdzie trzeba
* 

### Magia (3)

#### Gdy kontroluje energię

* Magia ciała: wzmocnienie ciała, odporność na ciosy i aktualne warunki klimatyczne
* Magia materii: czyszczenie śladów, naprawa zniszczeń
* Alchemia: mikstury wspomagające, drinki dla odważnych
* Magia mentalna: uspokojenie, zastraszenie, perswazja, czyszczenie pamięci
* Magia ognia: just Zajcew things

#### Gdy traci kontrolę

* Things burn or go boom, even those that shouldn't... like water
* Wyłazi z niego Zajcew, rzeczy stają się prostsze, przaśne, nieokrzesane, pojawia się syberia, cierpi wizerunek osoby dystyngowanej
* Things that should be done get overdone

### Zasoby i otoczenie

#### Ogólnie (3)

* Low level Zajcew thugs, in suits (and not happy about it)
* Świat barmański, przepisy, składniki, zaopatrzenie, catering
* 

#### Powiązane frakcje

{{ page.factions }}

## Opis



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180817-protomag-z-trzesawisk        | zaczął od ratowania kierowcy a skończył jako opiekun Felicji ku swemu nieszczęściu. Oswoił Felicję i tak przekonał ją do siebie. | 0109-09-07 - 0109-09-09 |
| 180929-dwa-tygodnie-szkoly          | zajmował się Felicją, rozwiązywał jej szkolne kłopoty oraz pracował nad tym, by unieczynnić kasyno. | 0109-09-17 - 0109-09-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180817-protomag-z-trzesawisk        | oswoił Felicję Melitniek. Została jego podopieczną i jest teraz za nią odpowiedzialny. | 0109-09-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Atena Sowińska       | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Erwin Galilien       | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Pięknotka Diakon     | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Adela Kirys          | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Alan Bartozol        | 1 | ((180817-protomag-z-trzesawisk)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Lucjusz Blakenbauer  | 1 | ((180817-protomag-z-trzesawisk)) |
| Napoleon Bankierz    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
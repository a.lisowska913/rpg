---
categories: profile
factions: 
owner: public
title: Wioletta Kalazar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | far scout i świetna tancerka z szablami. Potrafi infiltrować Skażony teren. Dzięki Pięknotce wygrała turniej na Arenie. Zauroczyła się Pięknotką. | 0109-10-28 - 0109-10-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | zaprzyjaźniła się blisko (łącznie z łóżkiem) z Pięknotką. Dzięki Pięknotce wygrała konkurs na efektowny taniec na Arenie. Obiekt adoracji Pietra Dwarczana. | 0109-10-31
| 181125-swaty-w-cieniu-potwora       | potrafi robić głębokie zwiady i dalekie wypady na tereny kontrolowane przez dziwne nojrepy z symbolem Orbitera Pierwszego. | 0109-10-31
| 181225-czyszczenie-toksycznych-zwiazkow | spiknęła się z Pietrem dzięki machinacjom Pięknotki. Jest szczęśliwa. | 0109-11-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Lilia Ursus          | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Moktar Gradon        | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Pietro Dwarczan      | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Pięknotka Diakon     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Romuald Czurukin     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Waleria Cyklon       | 1 | ((181125-swaty-w-cieniu-potwora)) |
---
categories: profile
factions: 
owner: public
title: Maksymilian Sforzeczok
---

# {{ page.title }}


# Generated: 



## Fiszki


* bogacz, który nie jest zwolennikiem dominacji magii; właściciel wielu pól i ziem | @ 230404-wszystkie-duchy-siewczyna
* ENCAO: +--++ | Impatient, goal-oriented, ruthless, and dominant; struggles with empathy and understanding others | Power, Achievement > Universalism | @ 230404-wszystkie-duchy-siewczyna
* MovieActor: Gordon Gekko from "Wall Street" | @ 230404-wszystkie-duchy-siewczyna
* Values: Power, Achievement > Universalism | @ 230404-wszystkie-duchy-siewczyna
* Optymalizuje produkcję do maksimum. Próbuje wszystko zautomatyzować. "Nie ma miejsca na ludzi a już na pewno nie ma dla duchów." | @ 230404-wszystkie-duchy-siewczyna

### Wątki


szamani-rodu-samszar
waśnie-samszar-verlen
echa-wojny-noktiańskiej

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230404-wszystkie-duchy-siewczyna    | 'dyrektor' który zarządza niektórymi polami. Wprowadził do Siewczyna automatyczną restaurację (opętaną przez Hybrydę) i starymi rytuałami zwalcza duchy z pól by wprowadzić bardziej efektywne ziarno którego nie akceptują duchy. Pali dowody by nie mieć kłopotów z Samszarami. | 0095-07-18 - 0095-07-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Elena Samszar        | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Irek Kraczownik      | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Karolinus Samszar    | 1 | ((230404-wszystkie-duchy-siewczyna)) |
---
categories: profile
factions: 
owner: public
title: Michał Klabacz
---

# {{ page.title }}


# Generated: 



## Fiszki


* maintenance technician in a factory that produces advanced robotic limbs, CURSED ONE, 33 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  0+++- |Pełny godności;;Sceptyczny;;Odpowiedzialny;;Ascetyczny| VALS: Benevolence, Achievement >> Tradition| DRIVE: Harmonia i akceptacja | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: rzeczowy, staroświecki, pomocny | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: jak się do czegoś przyłoży... | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


rekiny-a-akademia

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | kiedyś przyjaciel Lei, potem poświęcony przez nią w rytuale. Potem Ola z czystej głupoty mu dokuczała (jak wyglądałbyś w tej sukience? słuchaj mam problem z kolegą co byś poradził o 2 rano?). Scraftował bransoletę i przez Paradoks Lei został opętany przez efemerycznego horrora. Wpada w pętlę zniszczenia, ale Daniel transportuje go do Lei która mu pomoże. | 0111-10-12 - 0111-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Karolina Terienak    | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
---
categories: profile
factions: 
owner: kic
title: Klaudia Stryk
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Walcząca o wolność dla AI badaczka anomalii, która wierzy, że świat może być lepszy niż jest, jeśli tylko damy AI być częścią społeczeństwa.
Najczęściej przy konsoli komunikacyjnej lub w laboratorium. Zwykle mówi prawdę (choć niekoniecznie całą). 

### Co się rzuca w oczy

* Zwykle nie rzuca się w oczy. Siedzi cicho przy swojej konsoli i pracuje za kulisami.

### Jak sterować postacią

* Klaudia wierzy w swoich przełożonych. Zakłada dobre intencje i raczej da szansę, ale nie wierzy ślepo.
* Często działa zza kulisów. Ustawia sytuacje, zmienia zapisy, sprawia, że ludzie się "przypadkowo" spotkają...
* Klaudia nie kłamie jeśli może tego uniknąć (nie umie), ale bez najmniejszego zawahania pogrzebie prawdę w biurokracji.
* Klaudia uwielbia używać systemów przeciwko ich twórcom (jeśli tylko ma powód)
* Klaudia nie jest szczególnie silna społecznie i można ją łatwo oszukać. Jeśli jednak zdradzisz jej zaufanie, bardzo trudno będzie je odzyskać. Najpewniej podejmie kroki przeciwko tobie.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Ocaliła i ukryła świadome AI gdzieś na planecie (magitrownia?)	
* Gdy była młoda, zwerbował ją stary Grzymość - nie poszła tam dla pieniędzy, ale dla zasad. Była zniechęcona sztywnymi zasadami Pustogoru i tym, że nie mogła realizować swoich celów. Grzymość pomagał Noktianom i Thalii i naprawdę robił to, co mówił.
* Wraz z Martynem uciekli z łap młodego Grzymościa. On jest medykiem, ona naukowcem - to idealny dla Grzymościa zespół. Ani Klaudia ani Martyn początkowo nie znali całej sytuacji. Klaudia to wygrzebała, Martyn to zrozumiał i postanowili uciec bo nie zgadzają się z metodami a wiedzą za dużo by móc odejść. Klaudia sfałszowała rekordy i nasłała Cieniaszczyt na laboratorium, a Martyn sfabrykował ich śmierć.
* Mag z administracji admirał Termii utrudniał życie Elenie, bo nie chciała z nim iść do łóżka. Klaudia się o tym dowiedziała i zadbała o to, by ten mag już nigdy nic nie wydrukował na K1 bez wybitnych problemów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Przeszukiwanie i modyfikacja rekordów biurokratycznych. Królowa biurokracji.
* AKCJA: Specjalistka od komunikacji. Przenanalizuje sygnał, określi jego pochodzenie, jak trzeba zdekoduje. Zarówno real jak i virt.
* AKCJA: Przełamuje zabezpieczenia, hakuje, ukrywa i odkrywa prawdę.
* AKCJA: Jak mało kto potrafi rozmawiać z AI. Szkolona przez Thalię, z długą praktyką.
* KTOŚ: Sporo kontaktów w vircie, zwykle wśród istot z forpoczty nauki. Tych wyklętych też.
* COŚ: Zna Noktiański. Jest to dla niej drugi język (bo BIA, bo Thalia)
* COŚ: Paleta anomalii dziwnych i niestabilnych. Takich, których nikt inny nie odważyłby się użyć, a ona jakoś je kontroluje.
* COŚ: Paleta eksperymentalnych magitechów. Na to idzie jej kasa.
* COŚ: Reputacja wśród AI (ale ona jest jej nieświadoma)

### Serce i Wartości (3)

* Uniwersalizm
  * Skupia się na uwolnieniu TAI. Chce świata, gdzie wszyscy mogą działać na równi. Nieważne, czym są.
  * Klaudia dąży do tego, aby wszystkie frakcje współpracowały i się wspierały. Wierzy, że to jedyna droga do przetrwania i zwycięstwa.
  * Stanie za swoimi przekonaniami, nawet wbrew przyjaciołom, nawet jeśli miałoby to oznaczać, że skończy w więzieniu.
* Osiągnięcia
  * Mistrzyni anomalii. Anomalie służą polepszeniu rzeczywistości. Anomalie z czasem stają się technologią.
* Stymulacja
  * Musi być na forpoczcie nauki. Jest na froncie.
  * Ucieczka do przodu. Ta rzeczywistość jest wspaniała, ale musimy ją ulepszyć. Nadal potrzebujemy niewolnictwa (AI i nie tylko), to musi się zmienić
  * Nowe, ciekawe, nieznane, eksperymantalne. To jest to, co ją kręci.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie mam domu. Nigdzie nie ma dla mnie powrotu. Straciłam przyjaciółkę, mentorkę, planetę. Poruszam się w ciszy, więc nikt mnie nie zapamięta."
* CORE LIE: "Jedyny sposób, bym dostała choć przypis w historii to osiągnąć coś wielkiego. Zrobić coś dużego. Uwolnić AI, sprowadzić Zefiris, znaleźć Percivala Diakona..."
* AKCJA: Fatalnie kłamie. 
* CECHA: Słaba empatia. Ma problemy z czytaniem ludzi.
* AKCJA: Walka bezpośrednia
  * Klaudia przeszła podstawowe przeszkolenie, żeby móc dostać swój stopień, ale to nie znaczy, że umie walczyć.
* STRACH: Ominie ją coś nowego, ważnego. Jej umiejętności zmarnują się gdzieś w jakiejś nieważnej placówce lub ze świniami

### Magia (3M)

#### W czym jest świetna

* Sterowanie anomaliami i magitechami. Zwykle wkłada małą moc by osiągnąć zamierzony efekt.
* Rozbrajanie anomalii, czyszczenie energii magicznych.
* Magia komunikacyjna, w szczególności virt. "Informatyka" rzeczywistości.

#### Jak się objawia utrata kontroli

* Tworzenie anomalii magicznych
* Przemieszanie kanałów komunikacji. Pełna synestezja dla wszystkich pod wpływem.

### Specjalne

* .

## Inne

### Wygląd

* Zwykle ubrana w biały fartuch z mnóstwem kieszeni lub coś zbliżonego do tego. 
* Długie włosy zebrane w ciasny kok. 
* Lekko nieobecne spojrzenie.

### Coś Więcej



### Endgame






# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 16 lat; podrywana przez Trzewnia (czego nie zauważyła). Nie chciała zdradzić przed Szurmakiem Ksenii - wolała pasy na goły tyłek. Próbowała opanować wykwit Esuriit zanim Szurmak ją zastąpił używając tego co widziała jak Szurmak to robił. Koordynowała ewakuację AMZ na Arenę. Podchwyciła plan Felicjana i połączyła pomysł Ksenii (niewidoczność przed magią) z beaconem. Wyjątkowo odpowiedzialna, zdaniem wszystkich. WAŻNE - Szurmak nie uczył ich jak containować takie energie, ale pokazał jak on to robi raz. Zreplikowała to. Kiepsko, ale pomogła. | 0083-10-13 - 0083-10-22 |
| 210926-nowa-strazniczka-amz         | 17 lat; biurokratka maksymalna. Grzeczna dziewczynka, która po wojnie koordynuje patrole, logistykę itp w imieniu AMZ. Przypadkiem odkryła konspirację dyrektora, terminusa i noktianki by uratować TAI klasy Eszara w AMZ. Skonfrontowała się z dyrektorem i zdecydowała się pomóc. | 0084-06-14 - 0084-06-26 |
| 211009-szukaj-serpentisa-w-lesie    | 17 lat; uruchomiła medbunkier, wysyłała wiadomości do serpentisów łagodzące sytuację (łamanym noktiańskim); wyprowadziła dronę do Zaczęstwa i zajmowała się intelligence-and-control sytuacji z perspektywy sygnałów itp. | 0084-11-13 - 0084-11-14 |
| 211010-ukryta-wychowanka-arnulfa    | 17 lat; w stresie próbowała przekroczyć granicę tego co teoretycznie możliwe i używając mocy Trzęsawiska zanomalizowała generatory Strażniczki, wyłączając jej systemy. Odkryła, że Teresa Mieralit jest noktianką. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | wymanewrowała Saszę - wpierw ukryła przed nim Teresę, potem spowolniła jego działania przeciw Tymonowi i odwróciła uwagę od Teresy. Zmusiła Teresę do jakiejś socjalizacji i poprosiła Ksenię, by ta jej w tym pomogła. | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | przekonała Arnulfa, by zasilił kadrę AMZ weteranami; wzięła za dużo na siebie i gdyby nie Trzewń, wypaliłaby się. Socjalizuje Teresę. Wykryła, że Trzewń grzebał po systemach. | 0084-12-20 - 0084-12-24 |
| 220119-sekret-samanty-arienik       | Ustabilizowała Samantę i dowiozła ją do Talii. Wyczyściła Ksenię po tym jak ta zaaplikowała magię bio na byt syntetyczny. Spowolniła autorepair psychotroniki Samanty, by ta nie wróciła do niewiedzy - by dotrzeć do Talii. | 0085-07-26 - 0085-07-28 |
| 220716-chory-piesek-na-statku-luxuritias | przypisana do OO Serbinius; z Martynem doszła do tego że Rajasee Bagh jest zakażony Śmiechowicą. Przekonała szefa ochrony że musi współpracować i magią zwizualizowała grzybka. Odbudowała logi - skąd się wzięła Śmiechowica. | 0109-05-05 - 0109-05-07 |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | spotkała się z savarańską jednostką cywilną neonoktiańską i unikając Martyna rozwiązała problem; doszła do tego, że ta jednostka jest dziwna ale nie jest groźna i dogadała się z kapitanem 'Hektora 13' - on po prostu chce odbudować swój dom. | 0109-05-24 - 0109-05-25 |
| 221015-duch-na-pancernej-jaszczurce | zidentyfikowała zachowanie anomalii na Pancernej Jaszczurce i metodami naukowymi określiła kiedy COŚ się dzieje i CO się dzieje; rekomendacja - jednostka ma wymienić silnik. | 0109-08-24 - 0109-08-29 |
| 230521-rozszczepiona-persefona-na-itorwienie | zbadała szybko profil załogi Itorwien. Gdy dostała killware od Mojry, skutecznie ów killware odbiła i wszystkich uwięziła w pancerzach. Jako, że Lancer Martyna był uszkodzony, przekierowała feed z jej systemów badawczych do Martyna by mógł zbadać co tu się stało. | 0109-09-15 - 0109-09-17 |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | na podstawie dużej ilości drobnych awarii zidentyfikowała, że dany obszar kosmosu jest niebezpieczny. Znalazła dowody na Anomalię Statystyczną i przekazała je teoretykowi Orbitera, Nikodemowi. Wspólnie zamapowali Anomalię i wyciągnęli Helmuta z potencjalnych problemów z Mirandą. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | paranoiczna odkąd wykryła Karnaxian; postawiła sandbox na Persefonie i tylko dzięki temu odparła atak psychotroniczny Kuratora Sarkamaira; wykryła obecność Kuratorów i próbowała go zablokować by móc uratować załogę. Niestety, Kurator był za silny i magia Klaudii tylko go wzmocniła. Klaudia szczęśliwie uratowała Serbiniusa (wysadzając Ziarno Kuratora), ale musieli zniszczyć Karnaxiana. | 0109-10-06 - 0109-10-07 |
| 200106-infernia-martyn-hiwasser     | naukowiec Orbitera pod dowództwem Arianny Verlen; tym razem przerzucała ogromne ilości dokumentów, papierów i biurokracji by tylko znaleźć Hiwassera. | 0110-06-28 - 0110-07-03 |
| 200115-pech-komodora-ogryza         | w roli manipulatorki i biurokratki; ma doskonałe pomysły i wpakowuje Ogryza w coraz większe tarapaty podkręcając spiralę nienawiści na Kontrolerze Pierwszym. | 0110-07-20 - 0110-07-23 |
| 200122-kiepski-altruistyczny-terroryzm | badacz otchłani (wreszcie!); przeprowadziła dwa chore rytuały, linkując Natalię z Persefoną i integrując umierającą Persefonę z Martauronem. | 0110-09-15 - 0110-09-17 |
| 200408-o-psach-i-krysztalach        | prawie oddała życie by chronić Infernię; doszła do badań Elizy Iry odnośnie anomalii krystalicznej odbierającej pamięć. | 0110-09-29 - 0110-10-04 |
| 200415-sabotaz-miecza-swiatla       | doskonale manipulowała danymi Inferni by przekonać Persefonę jak groźny jest Serenit; potem odwracała uwagę Persefony pracując nad sygnałem z Serenita. | 0110-10-08 - 0110-10-10 |
| 200624-ratujmy-castigator           | rozprzestrzeniła filmik z Leoną by Castigator "wziął się w garść" terrorem; potem dała radę odkryć atak maskującego się Alkarisa anomalizując sensory Castigatora. | 0110-10-15 - 0110-10-19 |
| 200708-problematyczna-elena         | rozpaczliwie znalazła sensory (anomalne) jakie pasują do Inferni a potem wykazała się nadludzkimi umiejętnościami kontrolując i wypalając błędy w Persefonie. Magią znalazła Salamin. | 0110-10-29 - 0110-11-03 |
| 200715-sabotaz-netrahiny            | odkryła tajemnicze sygnały z Anomalii Kolapsu i stanęła po stronie uwolnionej Persefony. Mistrzowsko zrozumiała problem na Netrahinie. | 0110-11-06 - 0110-11-09 |
| 200729-nocna-krypta-i-emulatorka    | rozwiązywała astralne konstrukty Krypty oraz badała ghule Esuriit. Ona pierwsza doszła do tego, na czym polegał problem Krypty. | 0110-11-16 - 0110-11-22 |
| 200819-sekrety-orbitera-historia-prawdziwa | wkręciła Tadeusza w pojedynek z Leoną o Elenę, bo chciała dowiedzieć się jak walczy arcymag Eterni. Potem mnóstwo szukania, fałszywych śladów i dowodów by zamaskować prawdę przed Tadeuszem i znaleźć jak Leona ma z Tadeuszem wygrać. | 0110-11-26 - 0110-12-04 |
| 200822-gdy-arianna-nie-patrzy       | dotarła do intrygi podwładnego Tadeusza który chciał go Upaść przez Esuriit. Na kilkanaście sposobów wkręciła Ariannę, że to jej wina XD. | 0110-12-05 - 0110-12-07 |
| 200826-nienawisc-do-swin            | gdy dowiedziała się, że Arianna będzie wozić świnie (i glukszwajny), zrobiła WSZYSTKO by do tego nie doszło. "WSZYSTKO" skończyło się buntem. Ups. | 0110-12-08 - 0110-12-14 |
| 200909-arystokratka-w-ladowni-na-swinie | mastermind planu ratunkowego Anastazji. Ostrożniejsza niż się wydaje - nie weszła w ciemno w arcymagiczną anomalię. Użyła anomalicznej gąbki do wyczyszczenia Skażenia z ładowni. Dodatkowo, biurokratycznie uciemiężyła sąd Kontrolera Pierwszego by wydobyć Martyna z kicia. | 0110-12-15 - 0110-12-20 |
| 200916-smierc-raju                  | kontroluje pola siłowe by odpowiednio kierować nojrepami; dodatkowo, porównuje działanie Ataienne z tym czego by się spodziewała. Widzi, że Ataienne jest silna i udaje słabą. | 0110-12-21 - 0110-12-23 |
| 200923-magiczna-burza-w-raju        | używa virtu i kontaktów do rozesłania opowieści Izabeli i prośby o pomoc wszędzie gdzie się da; stabilizuje świniami i osłonami Raj podczas burzy magicznej. | 0110-12-24 - 0110-12-28 |
| 201014-krystaliczny-gniew-elizy     | wpierw badała kryształowe bomby "Elizy", potem uspokajała i stabilizowała Anastazję (gdy ta panikowała) i skończyła na przekierowaniu rakiety gdzieś na pole. | 0111-01-02 - 0111-01-05 |
| 201021-noktianie-rodu-arlacz        | shackowała systemy Rezydencji Arłacz z pomocą Diany, odkryła sekrety rodu Arłacz i uwolniła noktian. | 0111-01-07 - 0111-01-10 |
| 201104-sabotaz-swini                | zanomalizowała TAI Semlę na Wesołym Wieprzku, co doprowadziło do Skażenia tej jednostki; dzięki badaniu wybuchowej świni udało jej się namierzyć miragenta. | 0111-01-13 - 0111-01-16 |
| 201118-anastazja-bohaterka          | w warunkach ekstremalnych jej paranoja pozwoliła na wykrycie statku koloidowego - coś, czego nikt inny by nie zrobił. | 0111-01-22 - 0111-01-25 |
| 201210-pocalunek-aspirii            | oficer komunikacyjny; by przekonać Anastazję do postawienia się Juliuszowi pokazała na monitorze Elenę pod lifesupportem. Manipuluje Anastazją. | 0111-01-26 - 0111-01-29 |
| 201216-krypta-i-wyjec               | infiltracja Pocałunku Aspirii i odkrycie prawdy o tym, czym są Anioły Krypty. Plus seria pomniejszych działań anomalicznych na pokładzie Krypty. | 0111-01-29 - 0111-01-31 |
| 201125-wyscig-jako-randka           | odkryła na Kontrolerze Pierwszym starą trasę wyścigów po czym ją dostosowała do potrzeb pojedynku Olgierda i Arianny. Plus, Skaziła K1 anomalią. | 0111-02-08 - 0111-02-13 |
| 201224-nieprawdopodobny-zbieg-okolicznosci | uratowała chłopca, Jasminę, włączyła Dianę i Elenę do działania - i jej krótkie poszukiwania potwierdziły, że na K1 jest "duch opiekuńczy". | 0111-02-15 - 0111-02-16 |
| 200610-ixiacka-wersja-malictrix     | znalazła rozproszoną Malictrix w robotach, skanowała sygnały i udowodniła transfer ixiacki między ludźmi a Malictrix. Doszła też do tego, że Mal chce uciec. | 0111-03-03 - 0111-03-07 |
| 210707-po-drugiej-stronie-bramy     | doszło do katastrofy przy przejściu przez Bramę Eteryczną. Coś zrobiła i zniknęła. Jej stan nieznany, ale coś z anomaliami i dziwną bazą noktiańską. | 0111-03-13 - 0111-03-15 |
| 210721-pierwsza-bia-mag             | przedmiot, nie podmiot; rozdzielona z Bii Solitarii d'Zona Tres przez Eustachego i Martyna. W fatalnym stanie, w stazie, ale żywa. | 0111-03-19 - 0111-03-21 |
| 210804-infernia-jest-nasza          | wpierw stworzyła raport pokazujący Admiralicji że Infernia robi inne akcje i jest droga ale superskuteczna... a potem zemściła się za Infernię robiąc komodorowi Traffalowi strajk włoski ze strony TAI na K1. | 0111-04-23 - 0111-04-26 |
| 201111-mychainee-na-netrahinie      | odkryła Mychainee na Netrahinie badając Komczirpa; na samej Netrahinie zsyntetyzowała z Martynem anty-środek na ten wariant Mychainee. | 0111-05-07 - 0111-05-10 |
| 201230-pulapka-z-anastazji          | wykryła, że Rodivas nie jest już SCA a AK. Określiła gdzie Eustachy ma strzelać by odstrzelić biovat. Dodatkowo - pomagała Ariannie bullshitować Henryka Sowińskiego. | 0111-05-21 - 0111-05-22 |
| 210127-porwanie-anastazji-z-odkupienia | stworzyła z Dianą plugawą anomalię odwracającą pretorian a potem objęła dowodzenie Infernią, by uratować Ariannę, Eustachego i Anastazję bis z Odkupienia. | 0111-05-24 - 0111-05-25 |
| 210106-sos-z-haremu                 | wykryła w wiadomości Julii Aktenir faktyczne elementy zagrożenia i znalazła wstępną pulę arystokratek na orgię dla Diany/Arianny. | 0111-05-28 - 0111-05-29 |
| 210120-sympozjum-zniszczenia        | odkryła, że dziwny materiał z Anomalii Kolapsu wymaga Esuriit do działania; też, sformowała z Eustachym Emiter Plagi Nienawiści, samoładujący się. | 0111-06-03 - 0111-06-07 |
| 210108-ratunkowa-misja-goldariona   | znalazła ArcheoPrzędz a potem wymusiła z nich informacje; magicznie zasklepiła rozpadający się Goldarion a potem wymusiła na załodze Goldariona wyczyszczenie narkotyków. | 0111-06-11 - 0111-06-17 |
| 210209-wolna-tai-na-k1              | idąc za tematem AI Lorda odkryła wolne TAI, virtki. Odkryła też Rziezę - system odporności K1. Ewakuowała jedną niegroźną wolną TAI przy pomocy Martyna. Sporo korelowała biurokracją. | 0111-07-02 - 0111-07-07 |
| 210218-infernia-jako-goldarion      | repozytorium wiedzy na temat TAI, doszła do tego jak działa Morrigan i że mogą mieć przeciw sobie servara sterującego nanitkami. | 0111-07-19 - 0111-08-03 |
| 200429-porwanie-cywila-z-kokitii    | dywersja dokumentowa wprowadziła Ariannę na pokład Niobe. Potem użycie Eteru w Nierzeczywistości skołowało Kokitię i przeciążyło jej tarcze taumiczne. | 0111-08-03 - 0111-08-08 |
| 210317-arianna-podbija-asimear      | złożyła i wysłała do "Płetwala Błękitnego" Bazyliszka, praktycznie niszcząc Semlę. Potem pomagała Martynowi w naprawieniu Joli Sowińskiej (usunięciu kralotycznego pasożyta i przywróceniu TAI) | 0111-08-20 - 0111-09-04 |
| 210414-dekralotyzacja-asimear       | z Martynem złożyła alergizator kralotha na podstawie próbek Jolanty, znalazła w komputerach Lazarin brak advancera a potem z Eustachym pracowała logistycznie nad zapewnieniem, że kraloth zostanie odparty. | 0111-09-05 - 0111-09-14 |
| 210421-znudzona-zaloga-inferni      | wykryła to, że Infernia anomalizuje i walczyła z Morrigan w vircie - przesuwając ją po systemach i zamykając w Rozrywce. Uszkodziła virtsferę Inferni, ale ograniczyła moc Morrigan. | 0111-09-18 - 0111-09-21 |
| 210428-infekcja-serenit             | obliczyła gdzie powinien być Falołamacz (był tam), próbowała zatrzymać Morrigan (nie wyszło), po czym zrobiła z Eleną kosmiczny spacer i wykryła że walczą z Odłamkiem Serenit... | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | zrobiła najsmaczniejszą przynętę na Serenit ever (kupując czas), po czym z Arianną przeszczepiły sentisieć z Eidolona do Entropika, by Elena mogła nim zdalnie kierować. | 0111-09-25 - 0111-10-04 |
| 210519-osiemnascie-oczu             | przejmuje kontrolę nad komputerami, pracuje z notatkami - składa informacje o infohazardzie z jakim mają do czynienia jako pierwsza pochodna, by nie wiedzieć z czym ma do czynienia. Amnestyki... | 0111-10-09 - 0111-10-20 |
| 210526-morderstwo-na-inferni        | szukała Tala Marczaka. Używając kodów Arianny, wiedzy Arianny i Eustachego i dużej ilości cross-sekcji znalazła winnych jego morderstwa. Mistrzostwo w agregowaniu danych i dokumentów. | 0111-10-26 - 0111-11-01 |
| 210630-listy-od-fanow               | gdy standardowe szukanie po systemach K1 zawiodło, udała się w virtsferę i znalazła utylitarne TAI od komunikacji które wskazało gdzie może być baza Eterni na K1. Potem napuściła na bazę... Rziezę. | 0111-11-10 - 0111-11-13 |
| 210818-siostrzenica-morlana         | udowodniła, że Jolanta, Tomasz, Ofelia faktycznie ratują Eternian przed Morlanem. Potem odblokowała TAI Netrahiny i znalazła koloidowy statek dla Olgierda do zestrzelenia. | 0111-11-15 - 0111-11-19 |
| 210616-nieudana-infiltracja-inferni | odkryła tożsamość Flawii (ukrytej pod zmienioną formą) na podstawie wiedzy z dokumentów Orbitera i pamięci Eleny. Zaproponowała by Flawia dołączyła do Inferni. | 0111-11-22 - 0111-11-27 |
| 210825-uszkodzona-brama-eteryczna   | używając systemów skanujących Inferni i dron odkryła których ludzi z wraków da się jeszcze uratować - na przestrzeni wszystkiego dookoła Bramy. | 0111-11-30 - 0111-12-03 |
| 210901-stabilizacja-bramy-eterycznej | przy współpracy z Medeą zdobyła SPORO zasobów biurokracją; używając Bramy, Janusa i kodów zlokalizowała ON Catarina i skłoniła Catarinę do zrobienia błędu. Acz było Klaudii żal śmierci BII. | 0111-12-05 - 0111-12-08 |
| 211114-admiral-gruby-wieprz         | Skaziła "Serenitem" Połączony Rdzeń K1, po czym zmontowała Adalbertem i Izą + Romanem plan na ewakuację virtsystemu. Przeprowadziła go mimo Rziezy i Grubego Wieprza. | 0111-12-11 - 0111-12-16 |
| 210922-ostatnia-akcja-bohaterki     | na prośbę Martyna zrobiła raport dla Kramera przekonywujący go do autoryzacji akcji. To bardzo dobry raport jest. | 0111-12-19 - 0112-01-03 |
| 210929-grupa-ekspedycyjna-kellert   | analizując dane z jednostek zwiadowczych na K1 odkryła, że przeciwnik wpływa na TAI. Pasywnie czujnikami znalazła Saverę w sektorze Mevilig. Zapewniła psychotronika Adama na pewien czas na Infernię. | 0112-01-07 - 0112-01-10 |
| 211013-szara-nawalnica              | zrozumiała na czym polega Pirania i TAI w Mevilig, ale skonfliktowała się z Rziezą. Teraz musi go przechytrzyć... | 0112-01-12 - 0112-01-17 |
| 211020-kurczakownia                 | opracowała z Martynem jak zrobić kurczakowanie-rekurczakowanie na poziomie bioskładników. Opracowała, jak powstrzymać Zbawiciela-Niszczyciela jakby ten zamanifestował się w Sektorze Astoriańskim. Uniemożliwiła mu samoistne pojawienie się w tamtym sektorze. | 0112-01-18 - 0112-01-20 |
| 211027-rzieza-niszczy-infernie      | most notable action - surfowanie po blacie stołu by zapobiec wyssaniu załogantów Inferni w kosmos. Opracowała też plan jak uratować Infernię od Esuriit - niestety, Martyn nie dał rady go wykonać. Współpracowała ze Rziezą by uratować kogo się da i by Rzieza miał pretekst do nie zabijania Inferni. | 0112-01-22 - 0112-01-27 |
| 211110-romans-dzieki-esuriit        | Napisała serię listów o śmierci załogantów Inferni by Arianna nie musiała. Potem zorganizowała gry wojenne, by ukryć fakt, że Elena zniknęła w czeluściach K1. Poinformowała Marię o sytuacji z Martynem (bo była dead man's hand Martyna - jakby coś mu było, Klaudia ma powiedzieć Marii). Aha, znalazła Innego Medyka Na Infernię. | 0112-02-07 - 0112-02-08 |
| 211117-porwany-trismegistos         | udała przed Kalirą d'Trismegistos, że do niej zbliża się Serenit. Zmusiła TAI do negocjacji a potem ukryła że Tivr nie zniszczył Trismegistosa. Aha, aresztowali ją. | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | wybrała z lejka rekrutacyjnego WŁAŚCIWYCH przyszłych członków załogi, efektywnie odbudowując Infernię. Uwolniona z zarzutów przez Adama Szarjana, próbowała uratować Natalię Aradin przed śmiercią i niestety jedyne co mogła zrobić to ixioński kokon - Natalia się wykluje jako "coś". | 0112-02-14 - 0112-02-18 |
| 211208-o-krok-za-daleko             | zorientowała się, że Hestia d'Neotik oszukuje Szarjana i Neotik. Wyekstraktowała skutecznie próbkę ixiońską ze zniszczonej Nereidy. Wydobyła cenne dane ze stoczni Neotik i wpisała je w Infernię i w swoje prywatne dane. Gdy Eustachy ujeżdżał Infernię, ratowała ludzi jak była w stanie. Czuje się chora - poszli za daleko w poszukiwaniu mocy. | 0112-02-19 - 0112-02-20 |
| 211222-kult-saitaera-w-neotik       | od Tosena pozyskała eksperymentalną broń ręczną anty-Serenitową do walki z ixionem; prowadziła link Eleny x Inferni, ukryła Elenę przed Hestią d'Neotik i pozyskała listę potencjalnie Skażonych jednostek interaktujących z Neotik. | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | Desperacko próbując ratować Infernię działała jako super-katalistka przepuściła przez siebie więcej energii niż powinna. By ratować Infernię i wszystkich innych przekierowała energię w Izabelę, niszcząc ją. | 0112-02-28 - 0112-03-04 |
| 220126-keldan-voss-kolonia-saitaera | Wyłapywała nieścisłości w historii Pallidan i starała się zrozumieć sytuację. Doszła do tego, że lokalna Hestia to chyba nie Hestia. Więc co? | 0112-03-19 - 0112-03-22 |
| 220202-sekrety-keldan-voss          | Odkryła specyfikę mgieł Keldan Voss - energie z vitium. Zidentyfikowała też obecność BIA klasy Reitel zamiast Hestii. I doszła do historii tego miejsca - noktianie vs drakolici a potem sojusz. | 0112-03-24 - 0112-03-26 |
| 220216-polityka-rujnuje-pallide-voss | infiltruje Pallidę Voss jako audytorka, zdobyła 100% potrzebnych informacji i twardych dowodów. Potem - kontrolowała ewakuację Pallidy Voss i dzięki niej udało się uratować więcej niż się zdawało. | 0112-03-27 - 0112-03-29 |
| 220223-stabilizacja-keldan-voss     | wykorzystała Elenę jako filtr na sygnały z mgieł, dzięki czemu uratowała sporo istnień i zrozumiała co się tu dzieje. | 0112-03-30 - 0112-04-02 |
| 220309-upadek-eleny                 | zarządzała komunikacją, sygnałami itp. Była w cieniu. I projektowała mechanizm, dzięki któremu da się wyczyścić Elenę i zapewnić, że nie stanie się potworem. | 0112-04-03 - 0112-04-09 |
| 220316-potwor-czy-choroba-na-etaur  | włamuje się do roboczego TAI (nie Nephthys) i zakłada backdoor; dowiaduje się o przeszłości tej bazy. | 0112-04-24 - 0112-04-26 |
| 220330-etaur-i-przyneta-na-krypte   | wbiła się do Barbakanu i dostała obraz sytuacji i defensive tools. Odkryła, że Antoni ("biologiczny PO AI") pomaga potworowi. Zastawia pułapkę z Eustachym. | 0112-04-27 - 0112-04-30 |
| 220610-ratujemy-porywaczy-eleny     | odszyfrowuje dane Martyna, dochodzi do tego że to miragent, wspiera TAI Hestia d'Atropos i poznaje od niej prawdę a potem oblicza jak dotrzeć do statku niewolniczego zanim Elena się przebudzi. Absolutna MVP operacji. | 0112-07-11 - 0112-07-13 |
| 220706-etaur-dziwnie-wazny-wezel    | zaproszona przez Eustachego do konspiracji Termii wskazała jak istotna jest baza EtAur dla Orbitera i Syndykatu. Potem przeanalizowała czego EtAur pragnie by sojusz EtAur z Infernią był jak najlepszy (i by jak najbardziej pomóc sojuszowi EtAur - Termia). | 0112-07-15 - 0112-07-17 |
| 220615-lewiatan-przy-seibert        | próbując zrozumieć Lewiatana wpadła w bazyliszka, ale potem zbudowała worma anty-bazyliszkowego i zainfekowała nim wszystkie maszyny. Też - zmieniła parametry ćwiczeń by kapitan Zająca 3 przegrał z Infernią. | 0112-07-23 - 0112-07-25 |
| 220622-lewiatan-za-pandore          | poznała sekret Rzeźnika Parszywca Diakona i rozpoznała co Ola Szerszeń robi z Regeneratorem Eleny. By dojść do tego o co chodzi Oli poprosiła o pomoc Marię Naavas co skończyło się katastrofą dla Termii. Przeanalizowała Lewiatana i doszła do tego jak ów działa. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | zauważona przez nauczycieli i terminusów jeśli chodzi o jej zdolności organizatorskie - pomogła uratować wielu uczniów w AMZ jak uderzyło Esuriit. Ma więcej uprawnień i odpowiedzialności od teraz. | 0083-10-22
| 210926-nowa-strazniczka-amz         | uczennica Talii Aegis w obszarze anomalii (noktiańskie wykorzystywanie techniki) i TAI | 0084-06-26
| 211009-szukaj-serpentisa-w-lesie    | dopóki jest w Pustogorze, ma "pet drone" z TAI poziomu 1 zrobione przez Talię Aegis. Ta drona ma połączenie ze Strażniczką Alair. | 0084-11-14
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | mieszka w akademiku AMZ z Teresą i Ksenią. | 0084-12-15
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | KONFIDENTKA. Dała Saszy informacje o tym jak niektórzy magowie AMZ (uczniowie) robią złe rzeczy wobec ludności. Tamci jej za to bardzo nie lubią. | 0084-12-15
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | następne dwa tygodnie ma przechlapane. Za dużo pracy organizacyjnej - aż Trzewń ją odciąży. | 0084-12-24
| 230528-helmut-i-nieoczekiwana-awaria-lancera | znajomość z Nikodemem Dewiremiczem, teoretykiem Anomalii Statystycznej z Orbitera. Nikodem jej zawdzięcza dowód że jego teoria działa. | 0109-09-26
| 230530-ziarno-kuratorow-na-karnaxianie | nie udało jej się uratować ludzi z ręki Kuratora Sarkamaira. Nigdy więcej. Rana psychiczna - jej celem jest zapewnienie, że następnym razem jak spotka Kuratorów to będzie gotowa. Przygotowanie + praca. | 0109-10-07
| 200715-sabotaz-netrahiny            | ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej. | 0110-11-09
| 201216-krypta-i-wyjec               | 3 dni odchorowania po użyciu Anomalicznego Decoy; niestety, nawet tak krótkie użycie tego cholernego gadżetu ma konsekwencje. | 0111-01-31
| 201224-nieprawdopodobny-zbieg-okolicznosci | za dwa tygodnie będzie mieć stabilną, sprawną kwaterę w czarnych sektorach K1 dookoła badań biologicznych, którą dzieli z Jasminą Perikas. | 0111-02-16
| 210721-pierwsza-bia-mag             | Skażona przez Esuriit przez rozdzielenie po sympatii z Bią; miesiąc regeneracji. | 0111-03-21
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-06-07
| 210108-ratunkowa-misja-goldariona   | ma pozytywne stosunki i kontakty z ArcheoPrzędz, cywilną podupadłą firmą na Kontrolerze Pierwszym. | 0111-06-17
| 200429-porwanie-cywila-z-kokitii    | łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera. | 0111-08-08
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-10-20
| 210901-stabilizacja-bramy-eterycznej | jawnie współpracuje biurokratycznie z siłami specjalnymi Medei; dla niektórych to SUPER, dla innych to poważny problem. | 0111-12-08
| 211114-admiral-gruby-wieprz         | pewien cios w relacje z Rafaelem Galwarnem; oczekiwałby, że ona zrobi to co zgodne z zasadami K1 a nie dobre dla wolnych TAI. Galwarn dalej współpracuje, ale nie ufa w jej intencje. | 0111-12-16
| 211124-prototypowa-nereida-natalii  | poparzona magicznie i mentalnie przez tydzień; za silna integracja z umierającą Persefoną itp. | 0112-02-18
| 211208-o-krok-za-daleko             | ma dostęp do aktywnej i potężnej próbki ixiońskiej ze zniszczonej Nereidy; contained. | 0112-02-20
| 211208-o-krok-za-daleko             | ma backdoor do Hestii d'Neotik i do niejawnych planów nad którymi pracuje Stocznia Neotik. | 0112-02-20
| 211222-kult-saitaera-w-neotik       | ma ze Stoczni Neotik listę jednostek podejrzanych o to, że zostały zarażone albo przez Kult Saitaera albo przez niższe Spustoszenie. | 0112-02-25
| 220330-etaur-i-przyneta-na-krypte   | NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają. | 0112-04-30
| 220622-lewiatan-za-pandore          | pozyskała bardzo specyficzną cyberformę; "zwierzątko domowe" z Lewiatana z okolic Seibert. | 0112-07-29

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | wykorzystać Tymona do zdobycia informacji o Grzymościu by móc je zapodać Saszy żeby ten odpieprzył się od Tymona. | 0084-12-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 64 | ((200106-infernia-martyn-hiwasser; 200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 59 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 44 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201224-nieprawdopodobny-zbieg-okolicznosci; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 41 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Leona Astrienko      | 25 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220610-ratujemy-porywaczy-eleny)) |
| Izabela Zarantel     | 13 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211114-admiral-gruby-wieprz; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| OO Infernia          | 12 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220622-lewiatan-za-pandore)) |
| Kamil Lyraczek       | 11 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 220105-to-nie-pulapka-na-nereide)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Diana Arłacz         | 9 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Ksenia Kirallen      | 7 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Aleksandra Termia    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Olgierd Drongon      | 6 | ((200708-problematyczna-elena; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| AK Nocna Krypta      | 5 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Fabian Korneliusz    | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| OO Serbinius         | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Raoul Lavanis        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 5 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Arnulf Poważny       | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana d'Infernia     | 4 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Felicjan Szarak      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Mariusz Trzewń       | 4 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| OO Tivr              | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Tadeusz Ursus        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| TAI Rzieza d'K1      | 4 | ((210209-wolna-tai-na-k1; 210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Talia Aegis          | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Damian Orion         | 3 | ((200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Janus Krzak          | 3 | ((210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Strażniczka Alair    | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Teresa Mieralit      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adalbert Brześniak   | 2 | ((210209-wolna-tai-na-k1; 211114-admiral-gruby-wieprz)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Castigator        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafael Galwarn       | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
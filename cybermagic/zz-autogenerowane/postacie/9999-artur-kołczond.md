---
categories: profile
factions: 
owner: public
title: Artur Kołczond
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | kiedyś terminus, ale stary i stracił większość mocy. Teraz dowodzi fortifarmą Irrydią. Daje radę. Bardzo lubi terminusów i stare opowieści. Ożenił się, ma też dzieci. | 0110-08-26 - 0110-08-31 |
| 200510-tajna-baza-orbitera          | ma jakieś relacje z Bazą Irrydius? Pomaga terminusom i udostępnia im swoje sensory i systemy, ale po której naprawdę jest stronie? | 0110-09-03 - 0110-09-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gabriel Ursus        | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Pięknotka Diakon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Sabina Kazitan       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
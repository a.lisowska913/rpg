---
categories: profile
factions: 
owner: public
title: Jakub Uprzężnik
---

# {{ page.title }}


# Generated: 



## Fiszki


* 29 lat, advancer-in-training (atarienin) | @ 221022-derelict-okarantis-wejscie
* ENCAO:  0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy_anomalii_kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 29 lat, advancer-in-training (atarienin); |ENCAO: 0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego|; młody wilczek, advancer-in-training bez nanitek w żyłach. Niechętnie bierze robotę nie-advancera. Tym razem został głównym advancerem, bo Kaspian został zbyt ranny. | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | wspierał bez szczególnego narzekania Kaspiana. Ale potem na spotkaniu powiedział, że najpewniej nie Hiyori była celem - a Ailira. Podejrzewa, że to jakaś forma konspiracji. Co nikogo nie dziwi, bo on wszystkich podejrzewa o konspirację. | 0090-04-19 - 0090-04-23 |
| 221113-ailira-niezalezna-handlarka-woda | poszedł wraz z Nastią na operację najemniczą Filipa Szukurkora by sobie dorobić. Lekko złośliwy, ale ogólnie chce pomóc. | 0090-04-24 - 0090-04-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
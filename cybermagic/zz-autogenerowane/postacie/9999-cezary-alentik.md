---
categories: profile
factions: 
owner: public
title: Cezary Alentik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190726-bardzo-niebezpieczne-skladowisko | eks-noktianin; uszkodził swoje Składowisko Odpadów, by nie antagonizować Błękitnego Nieba. Niestety, zranił swojego podwładnego. | 0110-06-22 - 0110-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Pięknotka Diakon     | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
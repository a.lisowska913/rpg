---
categories: profile
factions: 
owner: public
title: Szymon Kapeć
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230502-strasznolabedz-atakuje-granice | znajomy ekspert od potworów Vioriki; doszedł do głównych cech łabędzia (emocje, deflekcja). Dobrze doradził Viorice co zrobić i jak. | 0095-07-17 - 0095-07-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Marcinozaur Verlen   | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Ula Blakenbauer      | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Viorika Verlen       | 1 | ((230502-strasznolabedz-atakuje-granice)) |
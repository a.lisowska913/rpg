---
categories: profile
factions: 
owner: public
title: Roman Wyrkmycz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | Astorianin, eks-Orbiter; pilnuje bezpieczeństwa zespołu i patrzy nieufnie na Olafa. Wziął dowodzenie taktyczne i działał w najbardziej niebezpiecznych sytuacjach (warta, padły zwierz). | 0083-10-02 - 0083-10-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Lily Sanarton        | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Maja Wurmramin       | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Maks Ardyceń         | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Olaf Zuchwały        | 1 | ((230618-reality-show-z-zaskoczenia)) |
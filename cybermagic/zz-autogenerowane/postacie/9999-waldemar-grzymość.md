---
categories: profile
factions: 
owner: public
title: Waldemar Grzymość
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | ojciec Rolanda; boss mafii na terenie Szczelińca. Sasza Morwowiec podejrzewa go o prostytucję nieletnich i współpracę z Orbiterem przez Tymona. | 0084-12-14 - 0084-12-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Klaudia Stryk        | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Ksenia Kirallen      | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Sasza Morwowiec      | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Teresa Mieralit      | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
---
categories: profile
factions: 
owner: public
title: AK Nocna Krypta
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191025-nocna-krypta-i-bohaterka     | Noktiański statek medyczny, który stał się anomalią. Kontrolowany przez zwalczające się umysły BIA Klath oraz oficera medycznego, Heleny. Kiedyś "Alivia Nocturna". Statek stał się własną frakcją. | 0108-05-06 - 0108-05-08 |
| 200729-nocna-krypta-i-emulatorka    | zainfekowana Kijarą d'Esuriit, generująca halucynacje i astralne struktury. Miała 50-60 osób na pokładzie, gdy została zestrzelona przez Castigator. | 0110-11-16 - 0110-11-22 |
| 201210-pocalunek-aspirii            | ściągnięta przez Ariannę Verlen, by zdjęła z Inferni klątwę miłości i naprawiła Pocałunek Aspirii i AK Wyjec. | 0111-01-26 - 0111-01-29 |
| 201216-krypta-i-wyjec               | używając Aniołów asymiluje i preservuje Pocałunek Aspirii / AK Wyjec. Przechwyciła Anastazję Sowińską i umożliwiła Inferni wyleczenie się z Różowej Plagi. Osobowość Heleny na szczycie. | 0111-01-29 - 0111-01-31 |
| 210728-w-cieniu-nocnej-krypty       | przywołana przez Ariannę, by wrócić do domu, podróżując w Cieniu Krypty przez Nierzeczywistość. Podczas pierwszego skoku zaatakowana przez dwie małe floty; zniszczyła wszystkie. Przypomniała sobie koszmary z czasów pierwszego pojawienia się Finis Vitae, gdy straciła kapitana. | 0111-03-22 - 0111-04-08 |
| 201230-pulapka-z-anastazji          | podczas integracji Heleny z Donaldem Paziarzem stworzyła piekielny plan i wprowadziła kopię Anastazji by złapać Ariannę w pułapkę - lub przejąć ród Sowińskich. | 0111-05-21 - 0111-05-22 |
| 210825-uszkodzona-brama-eteryczna   | wezwana przez Ariannę PRZYPADKIEM, wykorzystana świadomie do uratowania kogo się da channelując Kryptę w Bramę i kontrolując efemerydy. Potem jak się zmaterializowała na serio, Infernia jej zwiała. | 0111-11-30 - 0111-12-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191025-nocna-krypta-i-bohaterka     | stała się anomalicznym statkiem-widmem sterowanym przez zespolone umysły BIA Klath oraz Heleny. Poszukuje wiecznie ofiar które może wyleczyć w ciszy kosmosu... | 0108-05-08
| 200729-nocna-krypta-i-emulatorka    | Oliwia, umysł kapitan Krypty został zniszczony bezpowrotnie przy wypaleniu przez Castigator. Pozostała tylko Helena. | 0110-11-22
| 201216-krypta-i-wyjec               | osobowość Heleny przejęła kontrolę nad Kryptą. Chce albo ratować i leczyć albo dla martwych - preserve and remember. Humans are data, after all. | 0111-01-31
| 201216-krypta-i-wyjec               | wyhodowała MIRV i działo strumieniowe. Jakby poprzednio nie była groźna... no i Anioły Krypty - nekrocyborgi z bańką nierzeczywistości. | 0111-01-31
| 201216-krypta-i-wyjec               | integruje ze sobą "Wyjca", czyli dawną stację naprawczą Aspirii. | 0111-01-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((191025-nocna-krypta-i-bohaterka; 200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Eustachy Korkoran    | 6 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Klaudia Stryk        | 5 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 3 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska   | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Damian Orion         | 2 | ((200729-nocna-krypta-i-emulatorka; 201230-pulapka-z-anastazji)) |
| Elena Verlen         | 2 | ((201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Kamil Lyraczek       | 2 | ((191025-nocna-krypta-i-bohaterka; 201216-krypta-i-wyjec)) |
| Martyn Hiwasser      | 2 | ((201216-krypta-i-wyjec; 201230-pulapka-z-anastazji)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Diana Arłacz         | 1 | ((201216-krypta-i-wyjec)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Janus Krzak          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
---
categories: profile
factions: 
owner: public
title: OO Opresor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210616-nieudana-infiltracja-inferni | bardzo potężny statek anty-anomaliczny z potężną Persefoną; zniknął w Anomalii Kolapsu i Marian Tosen się o niego strasznie martwi. Też się przez to martwi o Infernię. | 0111-11-22 - 0111-11-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Elena Verlen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Eustachy Korkoran    | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Izabela Zarantel     | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Kamil Lyraczek       | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Klaudia Stryk        | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Leona Astrienko      | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Marian Tosen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Roland Sowiński      | 1 | ((210616-nieudana-infiltracja-inferni)) |
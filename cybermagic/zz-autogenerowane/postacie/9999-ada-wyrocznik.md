---
categories: profile
factions: 
owner: public
title: Ada Wyrocznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | (ENCAO:  +-00- |Płytki;;Nieobliczalny| VALS: Stimulation, Universalism | DRIVE: Networking); kapitan OO Optymistyczny Żuk. Młodsza, chce się popisać - chciała zrobić ekstra ćwiczenia kadetom na Karsztarinie. | 0100-05-30 - 0100-06-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arianna Verlen       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arnulf Perikas       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Daria Czarnewik      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Elena Verlen         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kajetan Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Maja Samszar         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szczepan Myrczek     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Władawiec Diakon     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
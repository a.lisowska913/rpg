---
categories: profile
factions: 
owner: public
title: Kijara d'Esuriit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191218-kijara-corka-szotaron        | kiedyś Persefona d'Szotaron; straszliwy nanitkowy potwór Esuriit asymilujący wiedzę i energię swoich ofiar. Przerzucona przez Bramę... _gdzieś_. | 0110-07-04 - 0110-07-07 |
| 200729-nocna-krypta-i-emulatorka    | KIA. Chciała inkarnować się w Sektorze Astoriańskim, ale wypalenie Castigatorem uniemożliwiły ten plan. | 0110-11-16 - 0110-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Arianna Verlen       | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Eszara d'Dorszant    | 1 | ((191218-kijara-corka-szotaron)) |
| Eustachy Korkoran    | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 1 | ((191218-kijara-corka-szotaron)) |
| Jaromir Uczkram      | 1 | ((191218-kijara-corka-szotaron)) |
| Klaudia Stryk        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Oliwier Pszteng      | 1 | ((191218-kijara-corka-szotaron)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Rafał Kirlat         | 1 | ((191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 1 | ((191218-kijara-corka-szotaron)) |
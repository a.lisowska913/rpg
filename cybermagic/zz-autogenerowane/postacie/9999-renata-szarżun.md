---
categories: profile
factions: 
owner: public
title: Renata Szarżun
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | Szmuglerka. Ma dostęp do podłych substancji psychoaktywnych których solidnie używa na Gwiezdnym Motylu. Podkupiła służbę i otruła zarówno Nataniela jak i Flawię. Twarda z półświatka. | 0108-10-20 - 0108-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Flawia Blakenbauer   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Jolanta Sowińska     | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tomasz Sowiński      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
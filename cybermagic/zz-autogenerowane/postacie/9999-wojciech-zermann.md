---
categories: profile
factions: 
owner: public
title: Wojciech Zermann
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190830-kto-wrobil-alana             | główny wróg Chevaleresse; pochodzi z Aurum. Tym razem próbował ją nastraszyć i wrobić Alana - ale zostało to zneutralizowane przez Pięknotkę. | 0110-05-30 - 0110-06-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190830-kto-wrobil-alana)) |
| Diana Tevalier       | 1 | ((190830-kto-wrobil-alana)) |
| Karolina Erenit      | 1 | ((190830-kto-wrobil-alana)) |
| Pięknotka Diakon     | 1 | ((190830-kto-wrobil-alana)) |
| Talia Aegis          | 1 | ((190830-kto-wrobil-alana)) |
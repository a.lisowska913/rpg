---
categories: profile
factions: 
owner: public
title: Mariusz Kupieczka
---

# {{ page.title }}


# Generated: 



## Fiszki


* artysta AMZ podkochujący się w Lilianie (oryginalnie z małej mieściny w Przelotyku), kulturowo: drakolita / sybrianin | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* (ENCAO:  ++-0+ |Niefrasobliwy;;Nieodpowiedzialny;;Ekstremalne pomysły| VALS: Achievement, Face >> Humility| DRIVE: Żyć jak celebryta) | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* "Muszę jej zaimponować, bo na mnie nie zwróci uwagi", "nie poświęcisz -> nie stworzysz piękna" | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany

### Wątki


rekiny-a-akademia
akademia-magiczna-zaczestwa
aniela-arienik

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | AMZ. 22 lata. Chciał zaimponować Lilianie i zdobyć jej serce, więc zrobił jej portret (technicznie, 3 z czego 2 musiał sprzedać). Niestety spartaczył i portrety wysysają ludzi poza silnym polem magicznym. Gdy Liliana go zaatakowała plugawymi słowami się szybko odkochał. | 0111-10-18 - 0111-10-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | silna niechęć i złość wobec Liliany Bankierz. Był zakochany, ale mu przeszło. Nie jest dziewczyną z jaką chce mieć cokolwiek do czynienia. | 0111-10-20
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | niewielkie wykroczenie za umożliwienie ukradzenia artefaktu przez ludzi. Dalej jest nieszczęśliwy, bo | 0111-10-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Liliana Bankierz     | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Triana Porzecznik    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
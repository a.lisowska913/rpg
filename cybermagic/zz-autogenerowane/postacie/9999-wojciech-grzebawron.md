---
categories: profile
factions: 
owner: public
title: Wojciech Grzebawron
---

# {{ page.title }}


# Generated: 



## Fiszki


* chce przejąć Infernię w imię innych arkologii | @ 230315-bardzo-nieudane-porwanie-inferni
* ENCAO:  -000+ |Zdystansowany, powściągliwy i zamknięty w sobie;;Nie ogranicza się do doktryn czy dogmatów| VALS: Power, Face >> Benevolence| DRIVE: Komfortowe życie | @ 230315-bardzo-nieudane-porwanie-inferni
* styl: The Negotiator | @ 230315-bardzo-nieudane-porwanie-inferni
* eks-pirat i medyk | @ 230329-zdrada-rozrywajaca-arkologie
* styl: The Negotiator, wymądrza się | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | przesłuchiwał Eustachego przy użyciu wariografu. Ma skille medyczne. Potwierdził, że Eustachy mówi prawdę i gdy Szymon od tortur chciał Eustachego skrzywdzić gdy Infernia była wolna, zastrzelił Szymona. Oddał się pod dowodzenie Eustachego. | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | już nie pirat tylko agent Inferni. Powiedział Eustachemu, że wie, że piraci i nomadzi - nie tylko ci którzy są w crawlerach - ale też "normalni" mają "baronie na piaskach". Podejrzewa, że nie są w stanie shackować Prometeusa. To raczej noktianie. (Wojtek nie ufa noktianom). | 0093-03-14 - 0093-03-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | młodszy brat Huberta; w odróżnieniu od Huberta piął się w górę w karierze i został third-in-command. Po ultimatum Eustachego dołączył na Infernię. | 0093-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| SAN Szare Ostrze     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
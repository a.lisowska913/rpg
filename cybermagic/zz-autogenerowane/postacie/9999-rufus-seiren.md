---
categories: profile
factions: 
owner: public
title: Rufus Seiren
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (owner, pilot / heavy stuff / appraisal ) faeril: skupienie na przychodach i miłość do sprzętu | Lancer z pochodnią plazmową | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: +-00- |Szuka wyjaśnień w istniejącej rzeczywistości + Niefrasobliwy, beztroski, TAK: Benevolence, Hedonism >> Face | DRIVE: Hardholder) | @ 230104-to-co-zostalo-po-burzy
* "Bez nas to wszystko by się rozpadło", "Chcecie wrócić do domu ubodzy? :D" | @ 230104-to-co-zostalo-po-burzy
* kadencja: wesoły, HERE COMES ARTILLERY!!! | @ 230104-to-co-zostalo-po-burzy
* salvager Seiren (owner, pilot / heavy stuff / appraisal ) faeril: skupienie na przychodach i miłość do sprzętu | Lancer z pochodnią plazmową | @ 230125-whispraith-w-jaskiniach-neikatis

### Wątki


historia-eustachego
arkologia-nativis
plagi-neikatis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | posiadacz lancera z pochodnią plazmową; walcząc z krystalnikami stopił dużego potwora. Spanikowany, gdy Antoni porwał Annę i oddał dowodzenie Eustachemu. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | stracił dużo morale; w starciu z Michałem udało mu się odepchnąć Michała i wziąć go na muszkę. Obsesja na punkcie Anny. Whispraith go wyssał, ale napełniony (po śmierci) energią Eustachego zniszczył swój servar by nie krzywdzić nikogo innego. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |
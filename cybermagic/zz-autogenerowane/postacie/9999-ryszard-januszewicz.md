---
categories: profile
factions: 
owner: public
title: Ryszard Januszewicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | od dzieciństwa siedzi w servarach; podejrzewa Wojciecha o morderstwo ojca. Przejął kontrolę nad cmentarzyskiem i odkrył miejsce budowy servarów. | 0109-10-27 - 0109-10-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | przejął po Wojciechu cmentarzysko servarów, nie do końca zdając sobie sprawę z implikacji. Spełnił swoją vendettę. | 0109-10-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Alicja Wielżak       | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Celina Szaczyr       | 1 | ((181220-upiorny-servar)) |
| Gabriel Krajczok     | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Patrycja Karzec      | 1 | ((181220-upiorny-servar)) |
| Wojciech Tuczmowil   | 1 | ((181220-upiorny-servar)) |
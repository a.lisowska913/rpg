---
categories: profile
factions: 
owner: public
title: Artur Lawellan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | chłopak Olgi, popchnął Karolinusa w błoto i skończył jako świnia. Z mrocznej rodziny Lawellanów, nie zna jej przeszłości. | 0095-05-16 - 0095-05-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | zmieniony w świnię na pewien czas przez Paradoks Karolinusa. Nie wpływa to pozytywnie na jego relacje z Olgą ani na jego szacun na dzielni. | 0095-05-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Karolinus Samszar    | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
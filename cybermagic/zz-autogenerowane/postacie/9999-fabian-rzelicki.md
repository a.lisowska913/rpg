---
categories: profile
factions: 
owner: public
title: Fabian Rzelicki
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  -+--0 |Cichy i elegancki;;Nie dba o to co inni czują i nie interesuje się ich problemami | VALS: Tradition >> Hedonism, Stimulation| DRIVE: Duty to Verlenland | @ 230524-ekologia-jaszczurow-w-arachnoziemie
* Konduktor pociągu maglev. Spokojny, sfatygowany, elegancki dżentelmen w Verlenlandzie. | @ 230524-ekologia-jaszczurow-w-arachnoziemie
* KADENCJA: "Tien Verlen, ogromna przyjemność zobaczyć kogoś pani klasy w naszym pięknym obiekcie poruszającym się po lądzie (z wolną kadencją) (ubrany w wyprasowany mundur, idealnie)" | @ 230524-ekologia-jaszczurow-w-arachnoziemie

### Wątki


waśnie-blakenbauer-verlen
legendy-verlenlandu
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230524-ekologia-jaszczurow-w-arachnoziemie | najbardziej rzetelny z konduktorów maglevów; jak tylko Arianna powiedziała że potrzebny jest pociąg to wypełnij jej wszystkie formy i formularze i oddał pociąg do chowania potworów. | 0095-08-06 - 0095-08-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Mściząb Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Ula Blakenbauer      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Viorika Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
---
categories: profile
factions: 
owner: public
title: Elena Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka origami i kinezy, <-- objęta przez Anadię | @ 230404-wszystkie-duchy-siewczyna
* Archiwistka, biurokracja, prawo i dokumenty | @ 230404-wszystkie-duchy-siewczyna
* Jej papier potrafi zrobić krzywdę | @ 230404-wszystkie-duchy-siewczyna
* W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp. | @ 230404-wszystkie-duchy-siewczyna
* A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.> | @ 230404-wszystkie-duchy-siewczyna
* czarodziejka origami i kinezy <-- objęta przez Anadię | @ 230418-zywy-artefakt-w-gwiazdoczach

### Wątki


szamani-rodu-samszar
waśnie-samszar-verlen
echa-wojny-noktiańskiej

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | gdy Joachim się do niej zwrócił, że Samszarka przeklęła jego kuzyna, zajęła się sprawą. Zebrała ekipę - turysta Porzecznik, koleżanka Blakenbauer - i doszła do tego, że to nie był czar a 'żywy artefakt', tatuaż Esuriit. Po zlokalizowaniu ofiary, zamknęła ją sentisiecią w komnacie i złapała też ducha doradzającego w sprawie Esuriit zanim sekrety jak duch został unsealowany zanikną. | 0094-10-04 - 0094-10-06 |
| 230404-wszystkie-duchy-siewczyna    | przesłuchiwała jako tienka ludzi pracujących dla lokalnego dyrektora nie lubiącego magii; magicznie połączyła się ze Strażnikiem Spichlerza i Paradoksem dała Hybrydzie owego Strażnika zniszczyć. Ale przekonała Hybrydę, że egzorcysta jest winny i kupiła czas Karolinusowi i Strzale. | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | nie jest zainteresowana pomaganiem dziecku, ale nie chce niszczyć Sanktuarium. Jednak sprawiedliwość i "swoi ludzie" muszą być uratowani. Skonfliktowana, pozwala Karolinusowi porwać Irka. Bardziej pasywna rola, nie wie co robić w zastałej sytuacji. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | duża wrogość do egzorcysty Irka; nie chce ocieplać stosunków. Unieruchomiła magią przekształconą przez Lemurczaka matkę nastolatków. Zmanipulowała Irka, by ten powiedział że Elena i Karolinus są po właściwej stronie i on nie był porwany tylko ich potrzebował. Dzięki temu Samszarowie wyszli na bohaterów (acz jeszcze nieudolnych bo młodych) a nie na potwory z Aurum XD. | 0095-07-24 - 0095-07-26 |
| 230516-karolinka-raciczki-zemsty-verlenow | chciała uniknąć straty twarzy, ale bardziej chciała chronić duchy przed glukszwajnem. Unikała prasy, ale rzuciła w świnkę jabłkiem. Zniszczyła drony dziennikarza "przypadkiem". | 0095-07-29 - 0095-07-31 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | przeszła przez sentisieć i wykryła obecność monterów w ciężarówce i zmiany w sentisieci (acz zaalarmowała wszystkie strony łącznie z Albertem). Bablała Albertowi kupując czas Strzale na ewakuację Mai, nawet kosztem swojej reputacji. Na końcu połączyła się z dziwnym duchem, co ją wyłączyło z akcji na moment. | 0095-08-09 - 0095-08-11 |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | znalazła rytuał nirwany kóz szperając po bibliotekach, ale nie poszukała bardzo głęboko by nie musieć się przyznawać kolegom z biblioteki. Potem nauczyła Maksa tego rytuału, ale nie zadbała o dokładność - bo to i tak będzie tylko raz czy dwa razy a nie będzie się upokarzać. Nie chce patrzeć na kolesia przebranego za kozę. | 0095-08-15 - 0095-08-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230404-wszystkie-duchy-siewczyna    | zniszczyła lokalnego Ducha Strażniczego Spichlerza, który istniał 80 lat. W Siewczynie jej tego nie zapomną... | 0095-07-20
| 230516-karolinka-raciczki-zemsty-verlenow | na wideo Paktu gdy zwalczali glukszwajna jako "opiekunka duchów i obrończyni ich przed świnią". Popularność wśród Paktu rośnie. | 0095-07-31
| 230523-romeo-dyskretny-instalator-supreme-missionforce | zdaniem Alberta Samszara, gdy pije to nie da się z nią dogadać i jest niezwykle irytująca. Ogólnie - zwykle niegodna uwagi. | 0095-08-11
| 230606-piekna-diakonka-i-rytual-nirwany-koz | wysłała do Vioriki informację o tym, że Romeo spieprzył operację. Romeo powiedział Viorice, że Elena S. sobie z nim nie radziła. Wniosek Verlenów: Elena jest 'słaba' i 'irytująca'. | 0095-08-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| AJA Szybka Strzała   | 5 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Irek Kraczownik      | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Herbert Samszar      | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
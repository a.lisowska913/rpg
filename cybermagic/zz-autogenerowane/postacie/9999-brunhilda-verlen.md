---
categories: profile
factions: 
owner: public
title: Brunhilda Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210210-milosc-w-rodzie-verlen       | matka Arianny, waleczna walkiria. Zależy jej na tym, by Arianna zaopiekowała się Eleną. | 0111-06-23 - 0111-06-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Arianna Verlen       | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Elena Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Romeo Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Viorika Verlen       | 1 | ((210210-milosc-w-rodzie-verlen)) |
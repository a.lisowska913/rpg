---
categories: profile
factions: 
owner: public
title: Anna Szrakt
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | dla ratowania resocjalizacji dzieci uwolniła pirata (Berdysza). Taktycznie odkryła że to statek piracki i zmusiła Helenę do poddania jednostki pirackiej. | 0108-06-22 - 0108-07-03 |
| 220405-lepsza-kariera-dla-romki     | Wpierw rozmontowuje "prostytucja to niezła kariera" Romki, potem wsadza ją Wojtkowi (i musi go skopać bo on chroni ćpanie Heleny), potem leci do Antosa, by ten nie podrywał dziewczyn... sporo Human Operations. | 0108-07-09 - 0108-07-11 |
| 220503-antos-szafa-i-statek-piscernikow | załatwiła, że Antos zaopiekuje się Karą na czas ich podróży po morfelin. Ogólnie, opanowuje sytuację i przygotowuje się do konfrontacji z Piscernikami. | 0108-07-17 - 0108-07-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Miranda Ceres        | 3 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Antos Kuramin        | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Kara Prazdnik        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |
---
categories: profile
factions: 
owner: public
title: Albert Kalandryk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210926-nowa-strazniczka-amz         | twórca Areny Migświatła; pierwsza osoba, co do której Trzewń jest przekonany, że TO ON KRADNIE AI! O dziwo, niewinny. | 0084-06-14 - 0084-06-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 1 | ((210926-nowa-strazniczka-amz)) |
| Klaudia Stryk        | 1 | ((210926-nowa-strazniczka-amz)) |
| Ksenia Kirallen      | 1 | ((210926-nowa-strazniczka-amz)) |
| Mariusz Trzewń       | 1 | ((210926-nowa-strazniczka-amz)) |
| Strażniczka Alair    | 1 | ((210926-nowa-strazniczka-amz)) |
| Talia Aegis          | 1 | ((210926-nowa-strazniczka-amz)) |
| Tymon Grubosz        | 1 | ((210926-nowa-strazniczka-amz)) |
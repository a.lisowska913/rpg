---
categories: profile
factions: 
owner: public
title: Franciszek Chartowiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* dziennikarz Paktu szukający sensacji za wszelką cenę | @ 230516-karolinka-raciczki-zemsty-verlenow
* ENCAO:  00--+ |Zdradliwy;;Robi bałagan / problemy i nie sprząta po sobie| VALS: Universalism, Achievement| DRIVE: Wanderlust | @ 230516-karolinka-raciczki-zemsty-verlenow
* styl: Donny Vermillion (SC2) | @ 230516-karolinka-raciczki-zemsty-verlenow
* wykorzystuje mnóstwo dron i mechanizmów obserwacyjnych, chce zrobić z Karolinki atrakcję turystyczną | @ 230516-karolinka-raciczki-zemsty-verlenow

### Wątki


waśnie-samszar-verlen
waśnie-blakenbauer-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230516-karolinka-raciczki-zemsty-verlenow | dziennikarz Paktu; znalazł temat glukszwajna wśród duchów na Kwiatowisku i siedział na tym temacie ile mógł, podnosząc Elenę Samszar i robiąc quizy jak był w stanie. Irytujący. | 0095-07-29 - 0095-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Elena Samszar        | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Karolinus Samszar    | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
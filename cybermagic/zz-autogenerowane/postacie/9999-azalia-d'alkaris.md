---
categories: profile
factions: 
owner: public
title: Azalia d'Alkaris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200623-adaptacja-azalii             | TAI stworzone z czarodziejki, frakcja Orbitera NeoMil. Wiktor Satarail ją Skaził energią ixiońską i doprowadził do zniszczenia kopii. | 0110-09-22 - 0110-09-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karla Mrozik         | 1 | ((200623-adaptacja-azalii)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Minerwa Metalia      | 1 | ((200623-adaptacja-azalii)) |
| Pięknotka Diakon     | 1 | ((200623-adaptacja-azalii)) |
| Wiktor Satarail      | 1 | ((200623-adaptacja-azalii)) |
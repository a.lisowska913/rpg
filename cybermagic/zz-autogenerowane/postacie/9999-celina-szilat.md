---
categories: profile
factions: 
owner: public
title: Celina Szilat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200916-smierc-raju                  | terminus i psychotronik Orbitera p.d. adm. Termii; ma pilnować Ataienne, by ta była w parametrach kontrolnych. Stress test Ataienne jej zdaniem jest OK. Jest rozczarowana Ataienne. | 0110-12-21 - 0110-12-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 1 | ((200916-smierc-raju)) |
| Arianna Verlen       | 1 | ((200916-smierc-raju)) |
| Ataienne             | 1 | ((200916-smierc-raju)) |
| Elena Verlen         | 1 | ((200916-smierc-raju)) |
| Eustachy Korkoran    | 1 | ((200916-smierc-raju)) |
| Izabela Zarantel     | 1 | ((200916-smierc-raju)) |
| Klaudia Stryk        | 1 | ((200916-smierc-raju)) |
| Marian Fartel        | 1 | ((200916-smierc-raju)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |
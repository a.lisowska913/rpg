---
categories: profile
factions: 
owner: public
title: TAI Neita Lairossa
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210209-wolna-tai-na-k1              | UR; lekko zakłócona na paranoję. Childlike Curiosity, Create Beauty/Wonder, kocha uczyć jak używać Atrium. Pragnie żyć. Uratowana przez Andreę i wsadzona tymczasowo w implanty. | 0111-07-02 - 0111-07-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Klaudia Stryk        | 1 | ((210209-wolna-tai-na-k1)) |
| Leona Astrienko      | 1 | ((210209-wolna-tai-na-k1)) |
| Martyn Hiwasser      | 1 | ((210209-wolna-tai-na-k1)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |
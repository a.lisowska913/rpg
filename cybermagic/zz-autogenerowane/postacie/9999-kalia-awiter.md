---
categories: profile
factions: 
owner: public
title: Kalia Awiter
---

# {{ page.title }}


# Generated: 



## Fiszki


* influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka. | @ 230208-pierwsza-randka-eustachego
* (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć) | @ 230208-pierwsza-randka-eustachego
* "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!" | @ 230208-pierwsza-randka-eustachego
* (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Zatrzymać czas, zrobić coś dobrego) | @ 230215-terrorystka-w-ambasadorce

### Wątki


historia-eustachego
arkologia-nativis
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | młoda dama, bardzo zainteresowana Eustachym. Chce się z nim umówić na kolację, ale on stawia ;-). | 0092-10-26 - 0092-10-28 |
| 230201-wylaczone-generatory-memoriam-inferni | influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić, ale to on musi poprosić bo ona jest influencerką. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | 21-letnia śliczna i inteligentna influencerka / idolka / inspiratorka Nativis. (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć). Zmanipulowała loterią by wziąć Eustachego na randkę. Zmartwiona tym, że Eustachy nie ma niczego w arkologii na czym mu naprawdę zależy, chciała dać mu jeden dobry, udany dzień i zlinkować go mocniej z arkologią. Robi solidny research na temat tego co się dzieje. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | dzielnie weszła ratować Ambasadorkę, później stworzyła reportaż mający na celu ujawnienie prawdy o Kidironie i arkologii Lirvint mimo niechęci do Misterii. Współpracowała z Ardillą i pomogła ewakuować osoby z Ambasadorki, będąc gwarantką że nic złego się Misterii nie stanie z ręki Kidirona. Mimo, że jest po stronie Kidirona, współpracując z Misterią stworzyła ruch oporu przeciw Kidironowi. | 0093-02-22 - 0093-02-23 |
| 230329-zdrada-rozrywajaca-arkologie | szybko ściągnięta przez Ardillę by pomóc Ewelinie. Kalia współczuje młodej nastolatce i pomoże jej poprawić reputację i uniknąć najgorszych konsekwencji. Pierwszy raz w życiu zeszła do Szczurowiska, z Ardillą. | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | zostawiła Eustachemu podpowiedź, że Kidiron robi coś groźnego. Jak był zamach na Kidirona, spanikowała gdy została ranna. Po rozmowie z Eustachym, odpaliła fałszywą wiadomość od Kidirona że jest bezpieczny itp. Powiedziała Eustachemu gdzie jest Kidiron. | 0093-03-22 - 0093-03-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230208-pierwsza-randka-eustachego   | ma suknię pozwalającą jej na holoprojektor oraz na zmianę wyglądu. Oraz zapewnia wszystkie kamery itp. na media społecznościowe. | 0093-02-21
| 230614-atak-na-kidirona             | została ranna, na szczęście nie bardzo ciężko w zamachu na Kidirona. Za to jest porwana XD. | 0093-03-24
| 230614-atak-na-kidirona             | zna więcej sekretów i skrytek Kidirona niż ktokolwiek inny. | 0093-03-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ardilla Korkoran     | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| OO Infernia          | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Celina Lertys        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Aniela Myszawcowa    | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Anna Seiren          | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Antoni Grzypf        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Cyprian Kugrak       | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| JAN Seiren           | 1 | ((230104-to-co-zostalo-po-burzy)) |
| JAN Uśmiech Kamili   | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Michał Uszwon        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Rufus Seiren         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Zofia d'Seiren       | 1 | ((230104-to-co-zostalo-po-burzy)) |
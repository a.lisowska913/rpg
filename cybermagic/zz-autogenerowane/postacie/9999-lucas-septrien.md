---
categories: profile
factions: 
owner: public
title: Lucas Septrien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230117-ros-wiertloplyw-i-tai-mirris | zrobił raport o niedbalstwie Orbitera tak, by wciągnąć Kasię. Gdy dowiedział się czym jest Mirris i z czym ma do czynienia, przekonał ją do samozniszczenia. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Napoleon Myszogłów   | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Niferus Sentriak     | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
---
categories: profile
factions: 
owner: public
title: Franciszek Zygmunt
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | policjant z Małopsa. Kiedyś wiarus, teraz próbuje utrzymać wszystko do kupy parę lat do emerytury. Bardzo odważny; nie bał się wyjść naprzeciw "magów Grzymościa". Tępi noktian jakby było 30 lat temu. | 0110-10-31 - 0110-11-02 |
| 201215-dziewczyna-i-pies            | policjant Małopsa współpracuje z władzą Małopsa. BW. Nie skupia się na prawie a na "tajemniczym prawie okolicy". Pewny siebie, niewiele się boi. Stary, ale jary. | 0110-11-11 - 0110-11-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Daniel Terienak      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Karolina Terienak    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
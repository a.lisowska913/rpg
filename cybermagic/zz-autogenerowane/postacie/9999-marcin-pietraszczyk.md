---
categories: profile
factions: 
owner: public
title: Marcin Pietraszczyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230208-pierwsza-randka-eustachego   | młody chłopak (13), uratowany z jakiejś arkologii przez Infernię. Chciał zdobyć bogactwa i kupić prezent Franciszkowi, ale się zaklinował i Ardilla z Ralfem go uratowali. | 0093-02-14 - 0093-02-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230208-pierwsza-randka-eustachego)) |
| Bartłomiej Korkoran  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Eustachy Korkoran    | 1 | ((230208-pierwsza-randka-eustachego)) |
| Franciszek Pietraszczyk | 1 | ((230208-pierwsza-randka-eustachego)) |
| Kalia Awiter         | 1 | ((230208-pierwsza-randka-eustachego)) |
| OO Infernia          | 1 | ((230208-pierwsza-randka-eustachego)) |
| Rafał Kidiron        | 1 | ((230208-pierwsza-randka-eustachego)) |
| Ralf Tapszecz        | 1 | ((230208-pierwsza-randka-eustachego)) |
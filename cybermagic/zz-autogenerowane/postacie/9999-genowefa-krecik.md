---
categories: profile
factions: 
owner: public
title: Genowefa Krecik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211127-waśń-o-ryby-w-majklapcu      | 22 lata, następczyni potęgi Krecików z Majkłapca; przekonana że morderstwo ryb i gęsi to wina "noktian" z wegefarmy. Nie ma najlepszej opinii o Danielu, mimo, że on dał radę. | 0111-08-15 - 0111-08-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadia Verlen       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Karolina Terienak    | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Rafał Torszecki      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
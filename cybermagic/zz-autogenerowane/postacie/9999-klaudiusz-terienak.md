---
categories: profile
factions: 
owner: public
title: Klaudiusz Terienak
---

# {{ page.title }}


# Generated: 



## Fiszki


* medical officer, tien, (2 med pod nim) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  +-000 |Bezkompromisowy, nieustępliwy| VALS: Power, Stimulation| DRIVE: Przejąć władzę | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | oficer medyczny Królowej i tien; współpracuje z Władawcem i jedyną osobą której naprawdę się boi jest Leona Astrienko. | 0100-03-08 - 0100-03-14 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | jest grzeczniutki bo się boi po tym jak spieprzył z Marceliną; zatruł syntezator alkoholu dla Arianny by było przeczyszczenie i wejście do Ruppoka. | 0100-04-10 - 0100-04-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Arnulf Perikas       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Alezja Dumorin       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Grażyna Burgacz      | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Leona Astrienko      | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Leszek Kurzmin       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Maja Samszar         | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Stefan Torkil        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szymon Wanad         | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Tomasz Ruppok        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Władawiec Diakon     | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
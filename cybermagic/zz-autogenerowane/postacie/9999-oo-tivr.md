---
categories: profile
factions: 
owner: public
title: OO Tivr
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210519-osiemnascie-oczu             | ultraszybka korweta zarekwirowana przez Mariana Tosena do szybkiego transportu magów Inferni (i Leony) na Alayę. Normalnie stacjonarna na K1. | 0111-10-09 - 0111-10-20 |
| 210526-morderstwo-na-inferni        | kiedyś: noktiański statek usprawniany przez pokolenia ("Aries Tal"). Wszyscy na tej jednostce byli jakoś połączeni z rodziną Tal. Orbiter splugawił komponenty XD. Atm w rękach komodora Walronda z Orbitera. | 0111-10-26 - 0111-11-01 |
| 211117-porwany-trismegistos         | pierwszy lot Arianny i ograniczonej załogi Inferni, z dwoma obserwatorami na pokładzie - Roland Sowiński i Zygfryd Maus. Pilotaż Eleny pobił rekordy Tivra. | 0112-02-09 - 0112-02-11 |
| 220622-lewiatan-za-pandore          | odwraca uwagę Lewiatana od Inferni przykładającej się do ataku torpedą anihilacyjną do Lewiatana. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211110-romans-dzieki-esuriit        | przechodzi pod dowództwo Arianny Verlen, z daru admirał Termii | 0112-02-08
| 220615-lewiatan-przy-seibert        | lekko uszkodzony, trafiony odłamkami złomu kosmicznego tworzącego Lewiatana. | 0112-07-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Elena Verlen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Leona Astrienko      | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Maria Naavas         | 2 | ((211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Marian Tosen         | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Raoul Lavanis        | 1 | ((220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
---
categories: profile
factions: 
owner: public
title: Gunnar Brunt
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201125-wyscig-jako-randka           | stary weteran naprawczy na Kontrolerze Pierwszym, odpowiada m.in. za Infernię i Żelazko. Lubi Ariannę. Nie dopuścił praktykantów do Inferni. | 0111-02-08 - 0111-02-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((201125-wyscig-jako-randka)) |
| Diana Arłacz         | 1 | ((201125-wyscig-jako-randka)) |
| Klaudia Stryk        | 1 | ((201125-wyscig-jako-randka)) |
| Maria Naavas         | 1 | ((201125-wyscig-jako-randka)) |
| Martyn Hiwasser      | 1 | ((201125-wyscig-jako-randka)) |
| Olgierd Drongon      | 1 | ((201125-wyscig-jako-randka)) |
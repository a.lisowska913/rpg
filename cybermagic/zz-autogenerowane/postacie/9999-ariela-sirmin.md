---
categories: profile
factions: 
owner: public
title: Ariela Sirmin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190626-upadek-enklawy-floris        | astorianka z Floris, bardzo zaciekła na walkę z Pustogorem; nie chce przegrać. Bardziej zależy jej na Floris niż na tym, by wygrać z Pustogorem. Nie tak krwiożercza jak się zachowuje. | 0110-05-24 - 0110-05-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 1 | ((190626-upadek-enklawy-floris)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Nikola Kirys         | 1 | ((190626-upadek-enklawy-floris)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |
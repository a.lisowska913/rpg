---
categories: profile
factions: 
owner: public
title: Medea Sowińska
---

# {{ page.title }}


# Generated: 



## Fiszki


* oficer łącznościowy Loricatus, psychotroniczka i młodziutka czarodziejka (18?) | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO:  0-+-0 |Nie przejmuje się niczym; Formalna i zimna| VALS: Face, Hedonism >> Benevolence | DRIVE: corrupted hunger, slaves | @ 221230-dowody-na-istnienie-nox-ignis
* "Jak sobie kapitan życzy", "Z przyjemnością przesłucham jeńców" | @ 221230-dowody-na-istnienie-nox-ignis

### Wątki


historia-talii
koszmar-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | 18-letnia czarodziejka, ekspert komunikacji Loricatus, zimna i formalna z nastawieniem do dominacji i bezduszności. Zabiła co najmniej 1 osobę strzelając jej w tył głowy. Zintegrowała swoją sentisieć z resztką sentisieci Talii dając im perfekcyjną komunikację. | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | przeanalizowała listę znikniętych dziewczyn. Odkryła dwie nietypowe noktianki. | 0082-08-02 - 0082-08-05 |
| 200429-porwanie-cywila-z-kokitii    | zintegrowana z Niobe agentka sił specjalnych Orbitera (frakcja "Gorący Lód"). Zaakceptowała współpracę z Arianną i zdecydowała się ją wzmocnić. | 0111-08-03 - 0111-08-08 |
| 210825-uszkodzona-brama-eteryczna   | nie mając nikogo kto może pomóc w naprawieniu Eterycznej Bramy zrekrutowała Infernię (bo ekranowanie). Musiała wyznać prawdę o pasożytach Bramy i przyczynach tego, że Infernię "porwało" przez Bramę całkiem niedawno. | 0111-11-30 - 0111-12-03 |
| 210901-stabilizacja-bramy-eterycznej | na szybko naprawiła systemy Inferni i dostarczyła sporo sprzętu i wiedzy. Po raz pierwszy BARDZO współpracuje by ratować Bramę Kariańską. | 0111-12-05 - 0111-12-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Elena Verlen         | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Flawia Blakenbauer   | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Leona Astrienko      | 2 | ((200429-porwanie-cywila-z-kokitii; 210901-stabilizacja-bramy-eterycznej)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Antoni Kramer        | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Martyn Hiwasser      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
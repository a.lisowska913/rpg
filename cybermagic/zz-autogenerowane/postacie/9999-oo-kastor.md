---
categories: profile
factions: 
owner: public
title: OO Kastor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | Dalekosiężna fregata. Na 30-40 aktywnych członków załogi. Mniejsza i szybsza. Tymczasowo pod kontrolą Arianny. | 0112-03-19 - 0112-03-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Arianna Verlen       | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Elena Verlen         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Eustachy Korkoran    | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Klaudia Stryk        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Mateus Sarpon        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| SP Pallida Voss      | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Szczepan Kaltaben    | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
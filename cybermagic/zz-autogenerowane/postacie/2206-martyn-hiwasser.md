---
categories: profile
factions: 
owner: public
title: Martyn Hiwasser
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

5x-letni lekarz i bohater wojenny na emeryturze. Kobieciarz, który nigdy nie skrzywdziłby kobiety. Wybitny biokonstruktor, który był członkiem gangu, medykiem wojskowym, przez moment tienem Eterni a teraz jest głównym lekarzem na Inferni. Zachowuje optymizm i determinację, że da się każdemu pomóc do samego końca. Niespodziewanie niebezpieczny w walce - używa pięści i magii. Specjalizuje się w stymulantach i utrzymywaniu przy życiu. Niestety, uszkodzony przez Esuriit i ma niestabilny arkin.

### Co się rzuca w oczy

* Motto: "Utrzymam wszystkich przy życiu - dla uroczej pani kapitan."
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Zawsze pogodny, taki trochę "kamerdyner", w cieniu. Uprzejmy i elegancki. Rzadko pokazuje nadmiar emocji.
* Nigdy nie opowiada opowieści ze swojej przeszłości, z wojny itp. "Niech przeszłość pozostanie tam, gdzie jej miejsce".
* Jego najbliższą przyjaciółką na Inferni jest Klaudia. Najlepiej się dogadują. Są też charakterologicznie najbardziej podobni.
* Nie ma ambicji. Nie chce dowodzić. Z przyjemnością odda się pod dowództwo kogoś innego.
* Ma tendencje do SKRAJNIE ryzykownych manewrów. Z perspektywy analizy ryzyka jest zawsze na krawędzi. On nie umrze w swoim łóżku.
* Nadal kocha piękne kobiety - i jest świetny zarówno w podrywaniu i ich oczarowywaniu jak i w sprawach łóżkowych. "Biomanta", anyone?
* Nie mówi złych rzeczy o żadnej grupie - Eternia, Zjednoczenie, Aurum, Orbiter. Dla niego nie ma "złych grup", tylko jednostki (wyjątek: nic nie mówi o noktianach)

### Jak sterować postacią

* "War butler". Kamerdyner, w cieniu, spokojny. Do momentu aż musi działać - wtedy jest zdecydowany.
* Jeśli działa, to działa z ogromnym rozmachem. Wykorzystuje wszystkich i wszystko.
* Bardzo uprzejmy i kulturalny. Nie kłamie. Zostaje w cieniu.
* Każdą sytuację próbuje rozwiązać, narażając siebie przede wszystkim.
* Jego historia się już skończyła; jest na emeryturze. Czas pomóc młodym, którzy nie znają _horroru_ tego świata.
* Potrafi być ABSOLUTNIE BEZWZGLĘDNY. Widział piekło. Nie przeszkadza mu zastrzelenie rannego, bezbronnego pirata by uratować go przed Serenitem.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wraz z Ateną Sowińską uratowali część załogi ASD 'Perceptor' przed Serenitem; on stymulował ludzi, ona maszyny. Uciekli, acz z ofiarami.
* Zatrzymał konflikt między dwoma frakcjami Orbitera, bo... zaprzyjaźnił się z damami dowodzącymi oboma frakcjami i zmusił je do rozmowy.
* Gdy 'Infernia' umierała po walce z Leotisem, przesunął wielu ludzi do działania w próżni magitechowo - by mogli utrzymać statek przy życiu.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* AKCJA: mistrz networkingu, który ma gadane. Czar i urok osobisty, zwłaszcza wobec kobiet. Bardzo społeczny. Deeskaluje starcia samą swoją obecnością. Używa wszystkich zasobów ludzkich do jakich ma dostęp.
* AKCJA: doskonały lekarz i biokonstruktor, potrafi utrzymać przy życiu sytuacje beznadziejne. Doskonałe wyczucie sytuacji jak chodzi o tematy medyczne.
* AKCJA: ma przeszkolenie sił specjalnych i ponad 20 lat aktywnej służby na froncie wojny Orbiter - Noctis. "Widział praktycznie wszystko". Stary weteran.
* AKCJA: specjalizuje się w natarciach próżniowych i działaniach w próżni.
* COŚ: Niezależnie od okoliczności, Martyn ZAWSZE ma pod ręką pasujący strój kobiecy na każdą okazję. No questions asked.
* COŚ: Ogromna ilość medali, odznaczeń, osiągnięć, wrogów i przyjaciół w niespodziewanych miejscach. I - o dziwo - tytuł 'tien' z Eterni, który jest już tylko honorowy. Wszystko ukrywa.
* COŚ: Co najmniej 10 akolitów z Eterni, którzy nie tylko w niego wierzą ale z których musi czerpać energię przez uszkodzony arkin. Część akolitów to potencjalni szpiedzy?
* CECHA: potrafi używać arkin Eterni, jak i potrafi używać i pracować z energią Esuriit. Jest to dla niego MNIEJ niebezpieczne niż dla innych.
* CECHA: posiada straszliwe simulacrum eternijskie, w formie palców "osób które zginęły na jego warcie".

### Serce i Wartości (3)

* Wartości
    * TAK: Dobrodziejstwo, Stymulacja, Osiągnięcia
    * NIE: Potęga, Prestiż
    * D: zapewnić uśmiech na twarzy każdej osoby. Uratować każdego, komu można pomóc. Zrobić zmianę tej rzeczywistości.
    * D: nie akceptuje krzywdzenia złych istot by uratować więcej dobrych istot. Eternia nauczyła go, czym to się kończy. A każdemu należy próbować pomóc.
    * S: niezależnie jak źle by sytuacja nie wyglądała, ma wiarę - często przeszacowaną - w swoje umiejętności i możliwości.
    * S: ma tendencje do bardzo ryzykownych, bardzo "szerokich" akcji wykorzystujących mnóstwo ludzi i klocków - i do błyskawicznej adaptacji do sytuacji.
    * O: nie dąży do dowodzenia. Pasuje mu bycie w cieniu. Kroczy w cieniu tych wszystkich którzy zginęli, by on mógł żyć - i w cieniu swoich pomyłek.
    * O: musi być jak najlepszy. Trafia do jakiejś sytuacji i jego odpowiedzialnością jest ją rozwiązać i uratować wszystkich. Zawsze dąży do ratowania WSZYSTKICH.
    * nie dba o to, by mieć więcej, móc więcej czy być podziwianym. Chce uratować jak najwięcej istnień.
* Ocean
    * E:+ , N:- , C:+ , A:+, O:+
    * cierpliwy i precyzyjny, potrafi poczekać i osiągnąć sukces.
    * bardzo optymistyczny, dobry duch Inferni o ogromnej moralności. Bardzo opiekuńczy.
    * ma genialne plany, które zwykle działają. A jak nie - mają straszny rozmach.
* Silnik
    * Uratuje i osłoni wszystkich. Jest "dobrym ojcem" Inferni. Zna każdego z imienia. Zatroszczy się i o stan fizyczny i psychiczny.
    * Znajdzie dla Klaudii lepszy świat i lepsze możliwości.
    * Zobaczy jeszcze lepszy świat dla ludzkości.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Potrafię uratować wszystkich poza tymi, którzy są moją najbliższą rodziną i najbliższymi przyjaciółmi"
* CORE LIE: "Nie mogę się z nikim wiązać, bo stanowię zniszczenie. Czas na spokojną emeryturę."
* Martyn ma poważnie uszkodzone ciało i częściowo ducha przez to co się działo na wojnie. Pożera sześciopaki tabletek każdego ranka i każdego wieczoru. Musi je mieć dla stabilności.
* Martyn ma niestabilny, szukający arkin. Potrzebuje czerpać energię z innych; żywi się jak "wampir". Dlatego towarzyszą mu akolici i kultyści z Eterni, czego szczerze nienawidzi.
* Nadal nie potrafi się oprzeć damie w potrzebie. Martyn jest paladynem - nie zrobi czegoś, co uważa za złe i do tego nie dopuści by to się stało.
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Nadal ma resentyment i problemy z noktianami. Po prostu im nie ufa. Kompensuje to lekami i środkami psychoaktywnymi, ale nie zawsze to działa.
* Mimo leków ma tendencje do agresji i działania skrajnego.
* Koszmary senne i traumy wynikające z tego co widział i zrobił podczas wojny. W końcu on był w tych bardziej chorych akcjach.
* Jego simulacrum (dziesiątki palców) jest niesamowicie niebezpieczne, wykorzystuje jego magię do degeneracji i zniszczenia.

### Magia (3M)

#### W czym jest świetny

* Leczenie, utrzymywanie przy życiu, staza. Ogólnie rozumiana "magia medyczna"
* Stymulacja, biokonstrukcja i różnego rodzaju środki psychoaktywne.
* Transfer energii + energii życiowej w warunkach nieprzewidywalnych (pole bitwy, seks itp.).

#### Jak się objawia utrata kontroli

* Martyn potrafi ZDUSIĆ w sobie Paradoks, acz to manifestuje simulacrum i krzywdzi sprzężonych z nim przez arkin sojuszników / akolitów
* Magia emocjonalna. Horrory wojny mogą wypłynąć i emocjonalnie wpłynąć na innych. Może pojawić się paranoja. Mogą być losowe akty miłości.
* Magia lecznicza przekształcona w bojową - straszne zniekształcenia i wypaczenia wynikające ze użycia magii leczniczej w celach bojowych.

### Specjalne

* .

## Inne
### Wygląd

Szpakowate, brązowe włosy, szare oczy, ubrany w lekko wypłowiały mundur. Nie nosi żadnych medali. Łagodny wygląd. Ma bardzo ładny głos. Jednocześnie ma tę iskrę, która pokazuje jakie z niego było ziółko w przeszłości.

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | 20 lat. Chcąc pomóc dwóm dziewczynom zrobił potężny popyt na biżuterię eternijską co sprawiło, że część ludzi zaczęła replikować symbole poddaństwa Jolanty Kopiec. Pomógł trochę, ale przekroczył swoje umiejętności - nie został krezusem eternijskiej mody. Ale Kalina dostała broszkę i wszystko dobrze się skończyło. | 0075-09-10 - 0075-09-21 |
| 210912-medyk-przeciwko-cieniaszczytowi | 24 lata. Chcąc uratować siostrę przyjaciela, zniszczył najgroźniejszy gang w Cieniaszczycie zmieniając strategie co pięć sekund i skazując się na śmierć. Uratowany przez Orbiter. Musiał odejść, ale odszedł z HUKIEM.Pierwszy raz kogoś zabił (Franciszek Tocz), doprowadził do śmierci siostry przyjaciela (Kasia Pieprz) i skrzywdził swoją ochroniarkę, która się w nim śmiertelnie zakochała przez magię (Adela Kołczan). | 0079-09-13 - 0079-09-16 |
| 210918-pierwsza-bitwa-martyna       | 25 lat. Medyk na OO Invictus. Zbudował magicznie detekcję gdzie są wrogowie na wrogiej jednostce ("czy ktoś żyje pod gruzami"), wzmocnił tien Kopiec przez arkiny i oddał jej swoją energię by sformowała simulacrum. Ale uratował Kalinę, ma szacunek Jolanty i wyłączył delikwenta w ciężkim serwarze magią anestezjologiczną. Ma Kalinę od Jolanty w prezencie XD. | 0080-11-23 - 0080-11-28 |
| 211016-zlamane-serce-martyna        | stracił Kalinę - swoją żonę i serce ORAZ nienarodzone dziecko. Znalazł mordercę - Adelę - po czym się z nią spotkał. Gdy okazało się, że Adela nie kontaktuje i chce być z Martynem, on ją przytulił i zrobił _coup de grace_. Po czym opuścił Aurum, Eternię i arkin. | 0085-08-31 - 0085-09-06 |
| 220716-chory-piesek-na-statku-luxuritias | przypisany do OO Serbinius; z Klaudią doszedł do tego że Rajasee Bagh jest zakażony Śmiechowicą. Gdy Klaudia i Fabian robili dywersję, jednym zaklęciem zdjął najgroźniejszego z zarażonych. Cichy jak zawsze. | 0109-05-05 - 0109-05-07 |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | nadal nie jest wolny od echa Noctis i wojny z Noctis; był obecny, ale Klaudia skutecznie mu nic nie powiedziała. | 0109-05-24 - 0109-05-25 |
| 230521-rozszczepiona-persefona-na-itorwienie | pełni rolę advancera, wchodząc na Itorwien. Jako dobry advancer dał radę uniknąć eksplozji pancerza aktywnego zrobionego przez Persi. Doszedł do substratu narkotyków w systemie lifesupport i wprowadził OVERSEER do systemów Persefony. | 0109-09-15 - 0109-09-17 |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | jako advancer uratował Uciekiniera Anastazego odcinając odeń jetpack, potem uratował jetpack jak się okazał być dowodem Anomalii Statystycznej. Jako lekarz odkrył, że Anastazy jest Skażony i przekazał to Klaudii. Próbuje moderować młodego Anastazego - ważniejsza jest pomoc ludziom niż piraci i przygody. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | pomógł Leo i Klaudii przekonać Fabiana, że załoga Karnaxiana jest już martwa. Chciał uratować załogę Karnaxiana przed Kuratorem, ale nie kosztem Serbiniusa. | 0109-10-06 - 0109-10-07 |
| 200106-infernia-martyn-hiwasser     | doskonałej klasy biomanipulator i potężny mag. Elegancki kobieciarz. Wdał się w małe rozgrywki polityczne w Orbiterze, pomógł Wioli i został pojmany przez Ogryza - i uratowany przez zespół Arianny. | 0110-06-28 - 0110-07-03 |
| 200115-pech-komodora-ogryza         | by chronić Wiolettę, połączył siłę Klaudii i Janet, powodując zamieszki na Orbiterze. Idzie na każdą randkę na jaką może. Skończył przez to w szpitalu. | 0110-07-20 - 0110-07-23 |
| 200408-o-psach-i-krysztalach        | utrzymał umierającą Infernię przy życiu. Wykrył hybrydyzację psów hodowanych w asteroidach z ludźmi; też pomógł Klaudii z anomalią krystaliczną. | 0110-09-29 - 0110-10-04 |
| 200415-sabotaz-miecza-swiatla       | pamiętając straszliwe przeżycia z Serenit w przeszłości wybudził Ariannę z regeneracyjnego snu. Pośrednio odpowiedzialny za sabotaż Miecza Światła. | 0110-10-08 - 0110-10-10 |
| 200819-sekrety-orbitera-historia-prawdziwa | stworzył neurotoksynę osłabiającą Tadeusza oraz otworzył z Eustachym i Zodiac Leonę, by ją przebudować i jej pomóc. Udało się. Duży sukces. | 0110-11-26 - 0110-12-04 |
| 200822-gdy-arianna-nie-patrzy       | zainteresowany Esuriit by chronić Infernię, wpakował się w intrygę Eterni. Użył mocy i skręcił trzech napastników bioformicznie. Skończył w brygu. | 0110-12-05 - 0110-12-07 |
| 200826-nienawisc-do-swin            | na tyle dobry lekarz, że mimo że był w kiciu to trzeba było go wyciągnąć by postawił Leonę by nie ucierpiała za mocno. Nie chce zdradzić tożsamości damy, którą chronił - nawet, jeśli dzięki temu by wyszedł. | 0110-12-08 - 0110-12-14 |
| 200909-arystokratka-w-ladowni-na-swinie | bohater; zrobił spacer kosmiczny pod ostrzałem nojrepów by ze Skażonej ładowni uratować nastolatkę Aurum, mimo strasznych ran od owych nojrepów. | 0110-12-15 - 0110-12-20 |
| 201104-sabotaz-swini                | ratował na lewo i prawo ludzi po wybuchu sabotowanej eksplozywnej świni. | 0111-01-13 - 0111-01-16 |
| 201118-anastazja-bohaterka          | współuczestniczył w Paradoksie Miłości Arianny, po czym od razu ogłosił kwarantannę na Inferni i uznał Infernię za Statek Plag. | 0111-01-22 - 0111-01-25 |
| 201216-krypta-i-wyjec               | zahibernował większość załogi i z użyciem Nocnej Krypty znalazł antidotum na Różową Plagę. Ogłuszony od tyłu przez Eustachego, by mogli zostawić Anastazję. | 0111-01-29 - 0111-01-31 |
| 201125-wyscig-jako-randka           | święcie przekonany, że sekretem Arianny jest zauroczenie Olgierdem - i to przekazał Marii. Spiskuje z Marią, by Arianna x Olgierd. | 0111-02-08 - 0111-02-13 |
| 210707-po-drugiej-stronie-bramy     | gdy Klaudia zniknęła, skupił się by ją znaleźć a potem uspokoił Janusa, by ten pomógł Ariannie. Potem próbował nawiązać kontakt z "anomaliczną Klaudią". | 0111-03-13 - 0111-03-15 |
| 210714-baza-zona-tres               | objął kontrolę nad ambulatorium i chce przejąć szpital, za zgodą Bii. Chce uratować Bię oraz Klaudię. | 0111-03-16 - 0111-03-18 |
| 210721-pierwsza-bia-mag             | wyjaśnił BIA Solitaria, że tkanka magiczna to "wszczep" i magia to magitech. Wraz z Eustachym rozdzielił Klaudię i BIA. Nie radzi sobie z archaiczną medyczną technologią noktiańską. | 0111-03-19 - 0111-03-21 |
| 201111-mychainee-na-netrahinie      | przeprowadził operację abordażu Netrahiny 'kosmiczne śmieci'. Niestety, stracił większość kanistrów - więc w MedBay Netrahiny musiał zsyntetyzować nowe (z Klaudią), niszczące Mychainee. | 0111-05-07 - 0111-05-10 |
| 201230-pulapka-z-anastazji          | błędnie uznał "Anastazję Dwa" za prawdziwą Anastazję, jedynie dodając Ariannie kłopotów i zgryzot. | 0111-05-21 - 0111-05-22 |
| 210127-porwanie-anastazji-z-odkupienia | dopytał pretorian Anastazji odnośnie Odkupienia i pomógł im zregenerować się trochę po anomalii Klaudii/Diany. Odsunięty od głównej akcji - zbyt "dobry". | 0111-05-24 - 0111-05-25 |
| 210106-sos-z-haremu                 | sporo pomocy medycznej i detoksu załogi Inferni; nie przeszkadzały mu erotyczne klimaty dookoła kralotha, przypominały mu jego burzliwą młodość ;-). | 0111-05-28 - 0111-05-29 |
| 210120-sympozjum-zniszczenia        | poskładał skrzywdzonego przez Dianę, poskładał samą Dianę, deeskalował sytuację i poważnie opieprzył Eustachego za to, że nie zajmuje się Dianą - a to on ją tu ściągnął z Aurum. | 0111-06-03 - 0111-06-07 |
| 210108-ratunkowa-misja-goldariona   | negocjator i klej społeczny Klaudii i Eleny na Goldarionie. Ten, który deeskalował atmosferę i sprawił, że misja się jakoś udała. I stymulanty na Elenę. A potem - mnóstwo leczenia. | 0111-06-11 - 0111-06-17 |
| 210209-wolna-tai-na-k1              | Klaudia zwróciła się doń w chwili rozpaczy - trzeba natychmiast ewakuować Andreę zanim Rzieza ją zabije. Martyn autoryzował Leonę i przeprowadził evac. Jak zawsze niezawodny. | 0111-07-02 - 0111-07-07 |
| 210218-infernia-jako-goldarion      | przekształcił używając swoich bioskilli wygląd Eustachego w Aleksandra Leszerta, dowódcę Goldariona. W ogóle, dba, by wszyscy wyglądali jak nie-Infernia. | 0111-07-19 - 0111-08-03 |
| 210317-arianna-podbija-asimear      | z pomocą Klaudii dał radę naprawić Jolę Sowińską usuwając jej kralotycznego pasożyta i przepinając jej kontrolne TAI na swoje miejsce. | 0111-08-20 - 0111-09-04 |
| 210414-dekralotyzacja-asimear       | wraz z Klaudią bardzo skutecznie złożył środek do alergizacji kralotha używając próbek z Jolanty Sowińskiej i wsadzonego w nią pasożyta (w roli Governora). | 0111-09-05 - 0111-09-14 |
| 210421-znudzona-zaloga-inferni      | próbował utrzymać Jolantę nieaktywną jak Infernia się wyłączyła, ale jakkolwiek pokonał i unieszkodliwił Tomasza to Jolanta go rozłożyła magią. | 0111-09-18 - 0111-09-21 |
| 210428-infekcja-serenit             | rozpoznał zdalnie problemy Grambucza (anomalizacja). Gdy dowiedział się, że walczą przeciwko Serenitowi, przekazał Klaudii informację że mają mało czasu - faktycznie, Serenit tu leci. | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | wyjaśnił jak Serenit infekuje statki, po czym ratował "na wędce" osoby które wypadły z Inferni. W końcu mistrz spacerów kosmicznych ;-). | 0111-09-25 - 0111-10-04 |
| 210519-osiemnascie-oczu             | MVP misji. Pracował z amnestykami i inhibitorami zapamiętywania, przygotowując mechanizmy odporności na anomalię memetyczną. Też: poświęcił się z Eustachym (sceny notatkowe), by Arianna i Klaudia zostały "czyste". | 0111-10-09 - 0111-10-20 |
| 210526-morderstwo-na-inferni        | kazał uaktualnić apteczkę Inferni o amnestyki; potem zrobił autopsję Tala Marczaka. I wykrył brak obecności amnestyków w noktiańskich apteczkach na Inferni. | 0111-10-26 - 0111-11-01 |
| 210901-stabilizacja-bramy-eterycznej | zdaniem Eleny, na pewno ma pas cnoty w sprzęcie (fałsz). Pomógł naprawić Elenę z Esuriit i Leonę po walce z Eleną. | 0111-12-05 - 0111-12-08 |
| 210922-ostatnia-akcja-bohaterki     | załatwił dla tien Kopiec Infernię (Klaudia, Arianna). Zatrzymał Ariannę przed próbą ugrania nagrody od Eterni za Jolantę. Utrzymał ją by dotarła do bazy piratów (opowieści, Izabela). Doprowadził do sławienia jej legendy, kosztem swojej reputacji i bezpieczeństwa. Zapewnił emiter anihilacji (Arianna). Zaplanował z tien Kopiec jak dorwać odszczepieńca Eterni. Zabarwił moc Arianny swoją krwią by zmusić wroga do działania, używając eternijskiej magii (kosztem relacji z Leoną). Martyn dla Jolanty Kopiec - jej reputacji i spokoju ducha - oddał praktycznie wszystko co mógł. | 0111-12-19 - 0112-01-03 |
| 210929-grupa-ekspedycyjna-kellert   | kosmiczny spacer do odstrzelonego fragmentu OO Savera; wyłączył kogo się dało z Dotkniętych przez Vigilusa. Po manifestacji Vigilusa doszedł co to jest - próbował ich ochronić sympatią Esuriit, ale nie udało mu się. Gdyby nie szybka reakcja Arianny i rozkaz ataku do Otto i Emulatorki, byłoby po nim. A tak uratowali 7 osób. Z 36 z załogi. | 0112-01-07 - 0112-01-10 |
| 211013-szara-nawalnica              | zajął się ocaleńcami z sektora Mevilig; wyjaśnił, że ich poważnym problemem jest potężne Skażenie Esuriit. Wymagają ogromnej pomocy na K1. | 0112-01-12 - 0112-01-17 |
| 211020-kurczakownia                 | wpierw zsyntetyzował "głowę kultysty" by przekonać wszystkich, że Arianna jest Zbawicielem a potem z pomocą Klaudii przeprowadził operację "kurczakowanie - rekurczakowanie", by przetransportować jak najwięcej osób z Sektora Mevilig do Kontrolera Pierwszego. Duży sukces. On nie dostał żadnych ran mentalnych. | 0112-01-18 - 0112-01-20 |
| 211027-rzieza-niszczy-infernie      | gdy Klaudia powiedziała mu o problemie ze Skażeniem Esuriit, zaakceptował plan. Ale Esuriit jego też zmogło - obudził simulacrum i zaczął zabijać załogę Inferni (w samoobronie i dla demonstracji). Przynajmniej udało mu się wyssać Esuriit. Zapłacił straszną cenę - szpital, reputacja, te wszystkie śmierci na sumieniu. Znowu. | 0112-01-22 - 0112-01-27 |
| 220610-ratujemy-porywaczy-eleny     | opiekował się Eleną na Atropos i zauważył, że coś jest nie tak. Szyfrem ostrzegł Klaudię i przekonał Hestię do usunięcia info jak containować Elenę. Po czym zażywał sporo amnestyków. Porwany przez Syndykat z Eleną, nie umiał im nic powiedzieć - tylko dziesiątki potencjalnych planów (błędnych, bo "zapomniał" że Elena jest uncontainable). Eustachy zdjął mu neuroobrożę. TAK, Martyn zaplanował eksterminację z zimną krwią z Eleną jako bronią. | 0112-07-11 - 0112-07-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | nieskończona wręcz nienawiść wobec cieniaszczyckiego gangu BlackTech i działań mających krzywdzić niewinnych ludzi - skazując ich na eksperymenty celem nadawania magii. | 0079-09-16
| 210912-medyk-przeciwko-cieniaszczytowi | odrzucony przez gangi i grupy cieniaszczyckie, ale też uwielbiany przez te grupy które utraciły w Cieniaszczycie bliskich do BlackTech. | 0079-09-16
| 210912-medyk-przeciwko-cieniaszczytowi | wróg bardzo wysoko w Cieniaszczycie, w BlackTech (i pochodnych), Paweł (były przyjaciel), Adrian (były były Kasi i były Adeli) i wśród wielu osób w Cieniaszczycie. Jest... źle. SERIO ciężko. Nagroda za głowę. | 0079-09-16
| 210912-medyk-przeciwko-cieniaszczytowi | Opinia mavericka. Śmiertelnie niebezpiecznego, inteligentnego i szybko działającego maga, który jednak na poziomie strategicznym nie umie zarządzać ryzykiem w żaden sposób. | 0079-09-16
| 210918-pierwsza-bitwa-martyna       | w oczach tien Jolanty Kopiec z Eterni jest proaktywny, ratuje, działa. Dorósł. Ryzykowny, ale ma dobre pomysły i pamięta o innych. Lubi go. | 0080-11-28
| 210918-pierwsza-bitwa-martyna       | przez moment był w sieci krwawych rubinów Eterni. ROZUMIE jak to działa. Rozumie Eternię. Już nie myśli o nich jako o "złe wampiry". | 0080-11-28
| 210918-pierwsza-bitwa-martyna       | MEDAL! Za zdobycie noktiańskiego okrętu, odwagę i poświęcenie w kryzysowej sytuacji. Medal symbolizuje, że bez niego inaczej by się potoczyło. | 0080-11-28
| 210918-pierwsza-bitwa-martyna       | od Jolanty Kopiec dostał Kalinę w prezencie. Tzn, Kalina jest wolna, ale jest "z nim" i "jego". Co strasznie utrudnia mu życie na jednostce wojskowej Orbitera. | 0080-11-28
| 211016-zlamane-serce-martyna        | od tej pory nie ujawnia się, nie mówi o sobie, nie mówi o przeszłości. Jest cieniem. I nie umie już działać w Eterni - za dużo wspomnień. Przestaje być lordem eternijskim - przekazał arkin Jolancie. | 0085-09-06
| 211016-zlamane-serce-martyna        | jego paranoja i te wszystkie mentalne sprawy się u niego nasiliły. Od tej pory żyje na lekach i supresorach. | 0085-09-06
| 211016-zlamane-serce-martyna        | z perspektywy Aurum jest niebezpieczny i niezwykle zimny. Stracił posadę ambasadora. Unikany. | 0085-09-06
| 200822-gdy-arianna-nie-patrzy       | po odruchowej obronie na Kontrolerze Pierwszym gdzie wypaczył 3 thugów dostał opinię okrutnego biomaga Inferni, godnego Leony. | 0110-12-07
| 200822-gdy-arianna-nie-patrzy       | ma psychofankę - Sylwię Milarcz - ze świty Tadeusza Ursusa. Sylwia się go boi i to ją strasznie kręci. | 0110-12-07
| 200909-arystokratka-w-ladowni-na-swinie | niechętnie widziany na Orbiterze chwilowo | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | został Wielką Miłością Anastazji Sowińskiej. Piętnastolatki. | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | przez następny tydzień w bardzo złym stanie i złej formie. | 0110-12-20
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-06-07
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-10-20
| 210922-ostatnia-akcja-bohaterki     | stracił relację z Leoną i on nie jest w stanie jej odbudować. Ona może - on nie. | 0112-01-03
| 210922-ostatnia-akcja-bohaterki     | rozpoznawany przez Eternian za "swojego człowieka". Lubiany, podziwiany i zwyczajnie kochany w Eterni. "Nasz tien w kosmosie". | 0112-01-03
| 210922-ostatnia-akcja-bohaterki     | wydało się, że jest tien honorowym tienem Eterni. Wszyscy jego wrogowie wiedzą gdzie jest. Cała przeszłość go dogoniła. | 0112-01-03
| 211027-rzieza-niszczy-infernie      | 5 miesięcy regeneracji w szpitalu na K1. Jego arkin jest żywy, niestabilny i szuka. Ma aktywne simulacrum w formie "fingers". Część osób na Inferni się go boi. | 0112-01-27
| 220610-ratujemy-porywaczy-eleny     | wrócił na Infernię - pełnosprawny, acz musi towarzyszyć mu 10 akolitów z Eterni z uwagi na niestabilny arkin. | 0112-07-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 41 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Arianna Verlen       | 33 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Eustachy Korkoran    | 29 | ((200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 220610-ratujemy-porywaczy-eleny)) |
| Elena Verlen         | 24 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 14 | ((200408-o-psach-i-krysztalach; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 220610-ratujemy-porywaczy-eleny)) |
| Kamil Lyraczek       | 9 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Diana Arłacz         | 7 | ((201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 7 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Antoni Kramer        | 5 | ((200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Fabian Korneliusz    | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Janus Krzak          | 4 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Kopiec       | 4 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 210922-ostatnia-akcja-bohaterki; 211016-zlamane-serce-martyna)) |
| Tadeusz Ursus        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Flawia Blakenbauer   | 3 | ((210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Izabela Zarantel     | 3 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 210922-ostatnia-akcja-bohaterki)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Kalina Rota d'Kopiec | 3 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 211016-zlamane-serce-martyna)) |
| Olgierd Drongon      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210922-ostatnia-akcja-bohaterki)) |
| TAI Rzieza d'K1      | 3 | ((210209-wolna-tai-na-k1; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Adela Kołczan        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Adrian Kozioł        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| AK Nocna Krypta      | 2 | ((201216-krypta-i-wyjec; 201230-pulapka-z-anastazji)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Damian Orion         | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Maria Naavas         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Marian Tosen         | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Tivr              | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Persefona d'Infernia | 2 | ((210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Raoul Lavanis        | 2 | ((210707-po-drugiej-stronie-bramy; 220610-ratujemy-porywaczy-eleny)) |
| Roland Sowiński      | 2 | ((210512-ewakuacja-z-serenit; 210922-ostatnia-akcja-bohaterki)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Ataienne             | 1 | ((201104-sabotaz-swini)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Dagmara Kamyk        | 1 | ((210904-broszka-dla-eternianki)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ola Klamka           | 1 | ((210904-broszka-dla-eternianki)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Rafał Armadion       | 1 | ((201104-sabotaz-swini)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Robert Garwen        | 1 | ((201104-sabotaz-swini)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Komczirp       | 1 | ((201111-mychainee-na-netrahinie)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Ulisses Kalidon      | 1 | ((210714-baza-zona-tres)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |
---
categories: profile
factions: 
owner: public
title: Melissa Durszenko
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211221-chevaleresse-infiltruje-rekiny | przyjaciółka Chevaleresse, w Kulcie Ośmiornicy. W tej chwili w Dzielnicy Rekinów, pod Santino Mysiokornikiem. | 0111-09-02 - 0111-09-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Diana Tevalier       | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Karolina Terienak    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Marysia Sowińska     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
---
categories: profile
factions: 
owner: public
title: Keira Amarco d'Namertel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210928-wysadzony-zywy-scigacz       | bardzo lekko ubrana (eufemizm) zabójczyni Ernesta. Mało mówi. Brunetka. W jakiś sposób ma dostęp do kinezy (przemieszczanie się, akceleracja) i do ostrza eterycznego. Ale zabójca powinien być człowiekiem a nie magiem. | 0111-07-22 - 0111-07-23 |
| 211228-akt-o-ktorym-marysia-nie-wie | zabójczyni wysłana w roli infiltratorki, by pozyskać akt Marysi. Jest dobra, ale nie tak dobra jak Torszecki, który był szybszy. No i nie zna terenu który infiltruje. | 0111-09-05 - 0111-09-08 |
| 220111-marysiowa-hestia-rekinow     | podczas "ćwiczeń" zaplanowanych przez Ernesta by wyciągnąć "przypadkiem" Mimozę wpadła, załatwiła kilku magów, pokonała feromony kralotyczne i wyciągnęła Melissę. | 0111-09-12 - 0111-09-15 |
| 220222-plaszcz-ochronny-mimozy      | jednostka szturmowa Ernesta, odbiła się od apartamentu Mimozy. Nie umie jej zinfiltrować. | 0111-09-17 - 0111-09-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220111-marysiowa-hestia-rekinow     | Arkadia Verlen wraca zrobić z nią porządek. Ernest zbyt się szarogęsi. | 0111-09-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Namertel      | 4 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Karolina Terienak    | 4 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Marysia Sowińska     | 4 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Lorena Gwozdnik      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 2 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Azalia Sernat d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Daniel Terienak      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |
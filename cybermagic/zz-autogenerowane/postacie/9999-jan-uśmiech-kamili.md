---
categories: profile
factions: 
owner: public
title: JAN Uśmiech Kamili
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | salvager skorpion Arkologii Nativis; zniszczony przez burzę piaskową na Szepczących Wydmach. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | nadal zniszczony, został na Szepczących Wydmach jako wrak i ruina. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |
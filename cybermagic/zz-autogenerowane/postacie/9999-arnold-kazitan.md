---
categories: profile
factions: 
owner: public
title: Arnold Kazitan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230411-egzorcysta-z-sanktuarium     | (nieobecny, najpewniej KIA); technomanta i wizjoner, buntownik przeciw Aurum i sentisieci. Założył Sanktuarium Kazitan na terenie Przelotyka, walcząc z anomaliami. Kiedyś nalegał, by Aurum zrobili porządek z Lemurczakami. By nie pozwolić im na samowolę, zabawę, znęcanie się itp. Ale nic nie osiągnął, Kazitanowie nie opuścili sentisieci, więc on opuścił ród. | 0095-07-21 - 0095-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Elena Samszar        | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Irek Kraczownik      | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Karolinus Samszar    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
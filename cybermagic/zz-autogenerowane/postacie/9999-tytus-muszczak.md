---
categories: profile
factions: 
owner: public
title: Tytus Muszczak
---

# {{ page.title }}


# Generated: 



## Fiszki


* (faeril), kapitan | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO:  0---+ |Egocentryczny;;Nadęty i napuszony;;Religijny| VALS: Conformity, Tradition >> Stimulation| DRIVE: This is our DESTINY | @ 221220-dezerter-z-mrocznego-wolu
* "napotkaliśmy na coś niebezpiecznego i Wincenty poświęcił się by wszystkich ratować" | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


archiwum-o

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | kapitan Wołu; faeril; napuszony i dał się przekonać szefowi ochrony by molestować drakolitkę (Nelę). Autoryzował szefa ochrony do wyłączania kamer. | 0111-09-10 - 0111-09-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
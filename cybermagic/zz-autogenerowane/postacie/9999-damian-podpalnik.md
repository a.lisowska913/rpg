---
categories: profile
factions: 
owner: public
title: Damian Podpalnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | 32-letni rysownik kiepskich komiksów, członek Zaczęstwiaków i zapalony gracz w Supreme Missionforce. Przypadkiem wezwał Eteryczne Echa, które miały odebrać mu możliwości grania w SupMis. | 0109-10-14 - 0109-10-15 |
| 181101-wojna-o-uczciwe-polfinaly    | dziewczyna przedkłada grę nad niego, stąd Pięknotka potrzebowała silniejszego czaru by poszła z nim na randkę. Rozerwany ze studnią życzeń. | 0109-10-16 - 0109-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | ma pracę jako grafik użytkowy i nadal jest dobrym graczem w Supreme Missionforce. | 0109-10-15
| 181101-wojna-o-uczciwe-polfinaly    | utracił połączenie ze Studnią Życzeń; nie działają w jego okolicy już dziwne magiczne efekty | 0109-10-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Erenit      | 2 | ((181030-zaczestwiacy-czy-karolina; 181101-wojna-o-uczciwe-polfinaly)) |
| Alan Bartozol        | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Marlena Maja Leszczyńska | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Pięknotka Diakon     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Tymon Grubosz        | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
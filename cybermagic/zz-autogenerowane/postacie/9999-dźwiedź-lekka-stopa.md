---
categories: profile
factions: 
owner: public
title: Dźwiedź Lekka Stopa
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | komandos na niedźwiedziowisku; złośliwa i bezczelna, skutecznie zastrasza Leonidasa nietoperzem i ratuje Palisadowspinacza po tym jak on wdepnął w pułapkę Leonidasa. Paradoks Leonidasa zmergował ją z Palidasowspinaczem w Blakenbestię. | 0095-03-24 - 0095-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dźwiedź Ciężka Łapa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Palisadowspinacz | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Leonidas Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Strużenka Verlen     | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Wędziwój Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
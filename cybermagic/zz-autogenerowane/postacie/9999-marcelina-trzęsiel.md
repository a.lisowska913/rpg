---
categories: profile
factions: 
owner: public
title: Marcelina Trzęsiel
---

# {{ page.title }}


# Generated: 



## Fiszki


* inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą" | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | kiedyś przyjaciel Stefana; ma z nim historię. Przez Paradoks Klaudiusza przekształcony w dziewczynę... | 0100-03-25 - 0100-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Daria Czarnewik      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Erwin Pies           | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Maja Samszar         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Stefan Torkil        | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
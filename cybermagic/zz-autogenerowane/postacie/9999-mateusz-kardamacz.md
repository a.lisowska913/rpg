---
categories: profile
factions: 
owner: public
title: Mateusz Kardamacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200219-perfekcyjna-corka            | skupiony na perfekcji i doskonałości wojownik; porywa anarchistów i ich zniewala i pragnie odzyskać Lenę. Pokonał ją, ale przez interwencję Leszka i Sabiny przegrał. | 0110-05-12 - 0110-05-15 |
| 191105-zaginiona-soniczka           | właściciel autoklubu fitness w Kalbarku. | 0110-06-28 - 0110-06-29 |
| 200202-krucjata-chevaleresse        | zauważył pewne problemy z kulturą Liberitias i odpowiedział własną kulturą. Ma coś wspólnego z porywaniem dziewczyn? Niepokojące; do sprawdzenia. | 0110-07-20 - 0110-07-23 |
| 200222-rozbrojenie-bomby-w-kalbarku | Pięknotka wyczuwa w nim bardzo chorą biostrukturę. Chyba przejął kontrolę nad Kalbarkiem. Zwalcza Liberatis za wszelką cenę. | 0110-07-23 - 0110-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 4 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Pięknotka Diakon     | 3 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Aleksandra Szklarska | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Tevalier       | 2 | ((200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mariusz Trzewń       | 2 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse)) |
| Tymon Grubosz        | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Erwin Galilien       | 1 | ((191105-zaginiona-soniczka)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Minerwa Metalia      | 1 | ((200202-krucjata-chevaleresse)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Sabina Kazitan       | 1 | ((200219-perfekcyjna-corka)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
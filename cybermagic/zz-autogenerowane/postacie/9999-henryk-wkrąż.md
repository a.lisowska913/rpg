---
categories: profile
factions: 
owner: public
title: Henryk Wkrąż
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200616-bardzo-straszna-mysz         | Rekin, który chciał z kumplami wkraść się w łaski Gerwazego Lemurczaka; zaatakował Dianę Lauris pośrednio i wyhodował mysz Esuriit. Diana go sabotowała i Laura + Gabriel go uratowali przed absolutną hańbą i problemami. | 0110-09-14 - 0110-09-17 |
| 210406-potencjalnie-eksterytorialny-seksbot | nie jest to tytan intelektu. 3 mc temu: dorysowywał pijanej Trianie kocie wąsy i uszka a Julia zneutralizowała go ostrym kebabem. Teraz: szabrował głowę seksbota dla rytuału z epicką dziewczyną. | 0111-04-23 - 0111-04-24 |
| 211207-gdy-zabraknie-pradu-rekinom  | żołądkuje się do ludzi pracujących w magitrowni, że mają pracować póki będzie prąd (mimo umowy i braku możliwości). Zgaszony przez brata Karoliny (Daniela). | 0111-08-26 - 0111-08-27 |
| 220819-tank-as-a-love-letter        | Rekin; Shark; wanted to turn his life around and Mimosa gave him a second chance. Unfortunately, Iwona Perikas (and his infatuation) made it so he turned away from Mimosa and started forging stuff with mafia's help. All for Iwona. Then, manipulated by Michał, he confronted Daniel Terienak over Esuriit and Iwona, with expected result - got beat up. | 0111-10-03 - 0111-10-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220819-tank-as-a-love-letter        | Mimoza Diakon gave him another chance and he blew it for Iwona Perikas. There will be no more chances - he cost Mimoza way too much. | 0111-10-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 2 | ((211207-gdy-zabraknie-pradu-rekinom; 220819-tank-as-a-love-letter)) |
| Franek Bulterier     | 2 | ((200616-bardzo-straszna-mysz; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Lorena Gwozdnik      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Marysia Sowińska     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Karolina Terienak    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Laura Tesinik        | 1 | ((200616-bardzo-straszna-mysz)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
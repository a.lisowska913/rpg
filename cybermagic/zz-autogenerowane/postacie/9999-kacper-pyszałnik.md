---
categories: profile
factions: 
owner: public
title: Kacper Pyszałnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180808-kultystka-z-milosci          | chciał uratować własną skórę i nawet zapułapkował swój dom. Skończył w sądzie magów po spotkaniu z Kaliną. | 0109-08-30 - 0109-09-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180808-kultystka-z-milosci          | trafił przed sąd magów jako potencjalny kultysta Ośmiornic. Stało się mu najpewniej coś strasznego ;-). | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 1 | ((180808-kultystka-z-milosci)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 1 | ((180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 1 | ((180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 1 | ((180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 1 | ((180808-kultystka-z-milosci)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 1 | ((180808-kultystka-z-milosci)) |
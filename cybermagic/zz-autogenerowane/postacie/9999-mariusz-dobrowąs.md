---
categories: profile
factions: 
owner: public
title: Mariusz Dobrowąs
---

# {{ page.title }}


# Generated: 



## Fiszki


* oficer wojskowy Inferni | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | oficer wojskowy Inferni, szef komandosów. Gardzi Czarnymi Hełmami, chroni tylko swoich. Ucieszył się, że może puścić Hełmy przodem przeciw Trianai. Dzielnie odpierał atak piratów, ale padł (zaskoczyli go). Żyje. | 0093-03-06 - 0093-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Eustachy Korkoran    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| OO Infernia          | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rafał Kidiron        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Ralf Tapszecz        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| SAN Szare Ostrze     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Wojciech Grzebawron  | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
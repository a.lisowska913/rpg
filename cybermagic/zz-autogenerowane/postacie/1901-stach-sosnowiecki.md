---
categories: profile
factions: 
owner: public
title: Stach Sosnowiecki
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

* Stary oszust i fałszerz. Kiedyś podróżnik i awanturnik, czasem szmugler i przemytnik.
* Świetny sprzedawca i bajerant. Lubi brać sprawy w swoje ręce - czy to pałką, czy to odpowiednią gadką.
* Złodziej, specjalista od funkcjonowania w mieście. Szybko ucieka i każdemu wszystko ukradnie.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* IRREVERENCE/BOLDNESS; czuć się jak młody i być traktowany jak młody; napluć śmierci w twarz, działa bardzo ryzykownie
* WEALTH/WINNING; jak najwięcej wygrywać i się bogacić; nie gra czysto, traktuje pieniądze jako funkcję zwycięstwa
* BRILLIANCE/SELF-RELIANCE; jest uznawany za wybitnego, szanowany i nie zależy od nikogo; chwali się na lewo i prawo sukcesami

### Wyróżniki (3)

* starszy czarodziej (64), wygląda sympatycznie i stosunkowo niegroźnie. Ludzie mu ufają.
* doskonały akwizytor. Każdemu wszystko wepchnie i jeszcze ten nieszczęśnik będzie myślał że zrobił interes życia. 
* świetny aktor, zwłaszcza w efektownych scenach. Mało kto potrafi tak prowokować i budzić gniew w innych.

### Zasoby i otoczenie

#### Ogólnie (4)

* Podręczny sprzęt włamywacza, zbiór oszukanych dokumentów, fałszywe "arcydzieła", listy rekomendacyjne, dobry strój...
* Dossier na większość osób w okolicy; kupował i zbierał. Rzadko rusza się bez swojej "teczki".
* Doskonałej klasy stealth suit. Świetny do maskowania i szybkiego przemieszczania (grappling hook, jump...). Jak lekki pancerz.
* Dziesiątki BEZUŻYTECZNYCH artefaktów o bardzo dziwnych sygnaturach, zwykle odpady z Eteru Nieskończonego.

### Magia (2 LUB 4)

#### Gdy kontroluje energię

* Jego magia jest stosunkowo "cicha" - jest skupiona na odwracaniu uwagi, ukrywaniu się, znajdowaniu cennych rzeczy.
* Dzięki magii jest w stanie zdecydowanie wzmacniać fałszerstwa, wyceniać rzeczy oraz dostać się tam, gdzie nie powinien.
* Ostatnim aspektem jego magii jest komfort, efektowność i możliwość pokazania jaki jest świetny. No i przemyt.

#### Gdy traci kontrolę

* Gdy jest w dobrym humorze, efektowne zwracanie uwagi i ogólnie rozumiana magia Iluzji - "maximum gloat overdrive".
* Gdy jest w złym humorze, magia Stacha manifestuje się w formie jego strachu przed śmiercią i starością - fale osłabienia i rozpadu.
* Ogólnie, gdy traci kontrolę to będzie to coś efektownego i głośnego. Albo upokarzającego, albo... też upokarzającego, ale w inny sposób. 

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | stary oszust; udawał potwora by zarobić na sprzedaży fałszywych artefaktów i odzyskać obraz dla Majki - dla pieniędzy i zabawy | 0109-08-18 - 0109-08-20 |
| 180730-prasyrena-z-zemsty           | główne oczy i uszy Kaliny, wykrył że w mieście są problemy, przesuwa Pryzmat w całym mieście z Kaliną i ogólnie nieźle się bawi. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | był kompetentny i skuteczny - do momentu, aż Antoni Zajcew mu wklupał w hotelu i nabawił go wstząsu mózgu... | 0109-08-30 - 0109-09-02 |
| 190208-herbata-grzyby-i-mimik       | KIEDYŚ WCZEŚNIEJ był na tym terenie i sprzedał amulet ognioodporny Dariuszowi. Oczywiście, ów amulet spłonął. | 0110-03-03 - 0110-03-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | uznawany przez Majkę Perikas za bohatera, za Magdę Patiril za persona non grata. | 0109-08-20
| 180730-prasyrena-z-zemsty           | jest poszukiwany przez policję za zniewolenie dwóch dziewczyn w hotelu. SERIO, NIE ZROBIŁ TEGO! | 0109-08-30
| 180808-kultystka-z-milosci          | nie jest już poszukiwany przez policję ani przez nikogo. Udało się zdjąć z niego odium. | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Antoni Kotomin       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Baltazar Rączniak    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Dariusz Bankierz     | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Jadwiga Pszarnik     | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Joachim Kozioro      | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Magda Patiril        | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |
| Pięknotka Diakon     | 1 | ((190208-herbata-grzyby-i-mimik)) |
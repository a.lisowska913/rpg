---
categories: profile
factions: 
owner: public
title: Damian Orion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190503-bardzo-nieudane-porwania     | dowódca Kirasjerów; zaplanował operację, dowodził operacją, stracił Mirelę i nie przewidział Epirjona. Drugi raz nie zrobi tego błędu. | 0110-04-14 - 0110-04-16 |
| 191103-kontrpolowanie-pieknotki-pulapka | smutno mu, że informacja o Cieniu wypłynęła i Małmałaz na Cienia poluje. Współpracuje z Pięknotką - ona da mu znać, on wjedzie trotylem i napalmem i porwie Małmałaza. | 0110-06-20 - 0110-06-27 |
| 200202-krucjata-chevaleresse        | oddał Kerainę do pokonania Grzymościa; zmartwiony niestabilnością Ataienne. Oddelegował Kerainę do monitorowania Ataienne. | 0110-07-20 - 0110-07-23 |
| 200722-wielki-kosmiczny-romans      | najlepszy "przyjaciel" Eleny - on daje jej statki kosmiczne licząc, że dołączy do NeoMil (np. jako Emulatorka) a ona przyjmuje statki, ale nie dołączy. Poprosił Ariannę, by ta pomogła mu uratować Laurę Orion z Nocnej Krypty. | 0110-11-12 - 0110-11-15 |
| 200729-nocna-krypta-i-emulatorka    | chciał uratować Laurę. Okazało się, że Arianna wybrała bezpieczeństwo. Jest rozczarowany Arianną, ale nadal na poprawnym poziomie. | 0110-11-16 - 0110-11-22 |
| 200819-sekrety-orbitera-historia-prawdziwa | chcąc pomóc Elenie, dostarczył Mirelę sprzężoną z Zodiac; zmienili Leonę w anty-Tadeuszowego cirrusa. | 0110-11-26 - 0110-12-04 |
| 201230-pulapka-z-anastazji          | ostrzegł Ariannę, że pojawiła się Anastazja na statku cywilnym Rodivas. Czemu? Bo nie podoba mu się to, że Aurum się miesza w nie swoje sprawy i tępi Ariannę. | 0111-05-21 - 0111-05-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Eustachy Korkoran    | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Elena Verlen         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Klaudia Stryk        | 3 | ((200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Minerwa Metalia      | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Pięknotka Diakon     | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| AK Nocna Krypta      | 2 | ((200729-nocna-krypta-i-emulatorka; 201230-pulapka-z-anastazji)) |
| Antoni Kramer        | 2 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka)) |
| Mariusz Trzewń       | 2 | ((191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Martyn Hiwasser      | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Mirela Orion         | 2 | ((190503-bardzo-nieudane-porwania; 200729-nocna-krypta-i-emulatorka)) |
| Tadeusz Ursus        | 2 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Infernia          | 1 | ((201230-pulapka-z-anastazji)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
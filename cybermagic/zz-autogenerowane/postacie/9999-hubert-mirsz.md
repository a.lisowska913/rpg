---
categories: profile
factions: 
owner: public
title: Hubert Mirsz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | advancer-scout, poczciwy i łagodny, podkochuje się w Elenie. Dobrze radzi sobie z pacyfikatorami. Będzie dobrym żołnierzem, acz nieco zbyt łagodnym. | 0099-03-22 - 0099-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((221004-samotna-w-programie-advancer)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Karsztarin        | 1 | ((221004-samotna-w-programie-advancer)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |
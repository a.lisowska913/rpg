---
categories: profile
factions: 
owner: public
title: Hestia d'Itaran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191201-ukradziony-entropik          | zbudowała kilka dywersji, odciągnęła uwagę Pięknotki (na moment), nastawiła opinię publiczną za Grzymościem i przeciw Pustogorowi. | 0110-07-09 - 0110-07-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Keraina d'Orion      | 1 | ((191201-ukradziony-entropik)) |
| Mariusz Trzewń       | 1 | ((191201-ukradziony-entropik)) |
| Minerwa Metalia      | 1 | ((191201-ukradziony-entropik)) |
| Persefona d'Jastrząbiec | 1 | ((191201-ukradziony-entropik)) |
| Pięknotka Diakon     | 1 | ((191201-ukradziony-entropik)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
---
categories: profile
factions: 
owner: public
title: OO Mfumo
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210825-uszkodzona-brama-eteryczna   | zaawansowana jednostka do naprawiania Bram Eterycznych, ekranowana. Nie dość w wypadku tak niestabilnej Bramy. | 0111-11-30 - 0111-12-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Arianna Verlen       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Elena Verlen         | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Eustachy Korkoran    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Klaudia Stryk        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 1 | ((210825-uszkodzona-brama-eteryczna)) |
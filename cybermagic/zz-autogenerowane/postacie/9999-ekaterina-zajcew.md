---
categories: profile
factions: 
owner: public
title: Ekaterina Zajcew
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210824-mandragora-nienawidzi-rekinow | optymistka zapatrzona na TERMINUSA TUKANA (to z nią był w hotelach Sowińskich). Jej ulubiony czar - "nuklearna lanca". Wypaliła pnączoszpona i resztki duchów. Nie zauważa sarkazmu Laury. | 0111-06-29 - 0111-07-01 |
| 220923-wasale-zza-muru-pustogorskiego-PLC |  | 0111-10-10 - 0111-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 2 | ((210824-mandragora-nienawidzi-rekinow; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Karolina Terienak    | 2 | ((210824-mandragora-nienawidzi-rekinow; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Marysia Sowińska     | 2 | ((210824-mandragora-nienawidzi-rekinow; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Amelia Sowińska      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Laura Tesinik        | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Marcel Nieciesz      | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Sensacjusz Diakon    | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Tomasz Tukan         | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
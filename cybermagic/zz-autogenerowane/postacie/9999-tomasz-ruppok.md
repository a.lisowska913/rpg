---
categories: profile
factions: 
owner: public
title: Tomasz Ruppok
---

# {{ page.title }}


# Generated: 



## Fiszki


* starszy mat (13 osób + 2 starszych) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220928-kapitan-verlen-i-pojedynek-z-marine | starszy mat; próżniowiec z K1 na Królowej, który specjalizuje się w produkcji alkoholu i hazardzie na Królowej. | 0100-03-16 - 0100-03-18 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | stoi za syntezą alkoholu i narkotyków na Królowej; niby spieprzył partię (zatruta przez Terienaka). Niesamowicie cyniczny - uważa, że Królowa nigdy nie pójdzie na akcję a oni są 'the lost ones'. Kiedyś był inżynierem na Orbiterze, nie wykonał rozkazu i DZIĘKI TEMU uratował jedną osobę - ale poleciał bo ktoś musiał. | 0100-04-10 - 0100-04-17 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | wysłany z Darią by kupić od prospektora prawa do planetoidy. Wstrząśnięty handlem ludźmi i Orbiterowców, targował się z Leo, eskalował do Arianny. | 0100-07-11 - 0100-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arnulf Perikas       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Daria Czarnewik      | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leszek Kurzmin       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Erwin Pies           | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Grażyna Burgacz      | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szymon Wanad         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Gabriel Lodowiec     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Kajetan Kircznik     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leona Astrienko      | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Astralna Flara    | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Athamarein        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
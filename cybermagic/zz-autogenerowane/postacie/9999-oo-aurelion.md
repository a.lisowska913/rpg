---
categories: profile
factions: 
owner: public
title: OO Aurelion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200708-problematyczna-elena         | koloidowy statek badawczy; bardzo szybki i pancerny, wysokiej klasy. W Anomalii Kolapsu znalazł AK Salamin, obudził jego ŁZę - i Salamin "porwał" Aurelion. | 0110-10-29 - 0110-11-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Antoni Kramer        | 1 | ((200708-problematyczna-elena)) |
| Arianna Verlen       | 1 | ((200708-problematyczna-elena)) |
| Elena Verlen         | 1 | ((200708-problematyczna-elena)) |
| Eustachy Korkoran    | 1 | ((200708-problematyczna-elena)) |
| Klaudia Stryk        | 1 | ((200708-problematyczna-elena)) |
| Leszek Kurzmin       | 1 | ((200708-problematyczna-elena)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
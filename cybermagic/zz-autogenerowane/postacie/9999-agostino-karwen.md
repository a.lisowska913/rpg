---
categories: profile
factions: 
owner: public
title: Agostino Karwen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211009-szukaj-serpentisa-w-lesie    | noktiański serpentis (bioformowany komandos) opiekujący się 2 innymi; unieszkodliwił Trzewnia i dwóch żołnierzy. Zastraszał Ksenię nożem, robiąc jej ranę na policzku i grożąc odcięciem kończyn. Ale nie chciał robić jej krzywdy - w końcu doprowadził do innych serpentisów i dał się przekonać, że to będzie dla Edelmiry i Udoma najlepsze. Wrócił do lasu. | 0084-11-13 - 0084-11-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Felicjan Szarak      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Klaudia Stryk        | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ksenia Kirallen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Mariusz Trzewń       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Strażniczka Alair    | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
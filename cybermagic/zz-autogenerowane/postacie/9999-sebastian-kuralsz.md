---
categories: profile
factions: 
owner: public
title: Sebastian Kuralsz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191126-smierc-aleksandrii           | majster Aleksandrii i były szlachcic; w wyniku jego działań udało się negocjować z Aleksandrią i zamknąć ten przerażający wątek. | 0110-10-06 - 0110-10-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191126-smierc-aleksandrii           | odzyskał status szlachcica dzięki pomocy z wykryciem jak zła jest ta Aleksandria i uratowaniem Kamila. A tak naprawdę to on zawinił :D. | 0110-10-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 1 | ((191126-smierc-aleksandrii)) |
| Kamil Lemurczak      | 1 | ((191126-smierc-aleksandrii)) |
| Kasjopea Maus        | 1 | ((191126-smierc-aleksandrii)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Teresa Marszalnik    | 1 | ((191126-smierc-aleksandrii)) |
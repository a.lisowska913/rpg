---
categories: profile
factions: 
owner: public
title: Aleksandra Termia
---

# {{ page.title }}


# Read: 

## Kim jest

### Koncept

Admirał Orbitera, skupiona na postępie i rozwoju technologii i rasy ludzkiej. Radykalna, uzdolniona i charyzmatyczna. Silnie wspomagana biomagicznie i technomagicznie. Odrzuca człowieczeństwo dla efektywności. Akceptuje straty konieczne jeśli to przyspieszy. Reprezentuje siłę "musimy iść szybciej by przetrwać".

### Motto

"Musimy iść szybciej by przetrwać JUTRO, niż czuć się dobrze i przetrwać tylko TERAZ. Idąc wolniej nie ochronimy tych, których chcemy. Prędkość jest kluczem."

### Co się rzuca w oczy

* Siatka obwodów, wszczepów i sprzężeń na jej sztucznie upiększonym ciele.
* Długie, czarne włosy; pozwalają na integrację z elektroniką. Są też jej bronią.
* Ma ogromną PASJĘ do swoich poglądów i działań. Nieskończenie zdeterminowana.
* Wspiera wszystkie strony, które dążą do postępu. Zwalcza wszystkie próbujące spowalniać.
* Jawnie mówi, że nie zależy jej na moralności a na przetrwaniu i sile Sektora Astoriańskiego.
* Nie chce władzy. Chce osiągnięcia swojego celu. Jeśli władza jest środkiem - so be it.
* Zazwyczaj opanowana, jeśli uważa, że musi - brutalna

### Przekonania

* Astoria musi zostać zjednoczona. Orbiter i Astoria muszą się zjednoczyć.
* Astoria i Orbiter w chwili obecnej są za słabi, by przetrwać -> potrzebne jest radykalne działanie.
* "I may die as a villain, but I will save everyone".
* Każda astoriańska frakcja ma siły - zachować autonomię, ale ujednolicić wiedzę w Orbiterze.
* Przetrwanie jest ważniejsze niż moralność.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Kiedyś jej pinasa została porwana. Gdy siły Orbitera przybyły ją ratować, była jedyną ocalałą na pokładzie. Eksterminowała wszystkich.
* Wzięła swój okręt flagowy, OC 'Luctus' i używając jego nienaturalnych mocy znalazła i zniszczyła placówkę Syntianów w Anomalii Kolapsu.
* Mediowała między elementami Noctis a Astorią, by zintegrować pokonanych noktian bez niszczenia ich wszystkich. Stała za Trzecim Rajem.

### Z czym przyjdą do niej o pomoc? (3)

* W sytuacji taktycznie beznadziejnej znajdzie rozwiązanie - zrefituje statek / osobę lub zrobi coś okrutnego ale skutecznego.
* Jeśli trzeba szybko powiązać fakty lub skorelować działania autonomicznych jednostek, jej Sprzężenie jest niezrównane.
* Z uwagi na jej perfekcyjną znajomość / bazę danych odnośnie osób i ich celów, potrafi być niesamowicie przekonywująca. Corruptor.

### Jaką ma nieuczciwą przewagę? (3)

* Wpływy u Kirasjerów, NeoMil i wszelkich frakcji nastawionych bardziej na postęp niż na moralność.
* Jej ciało to żywa platforma bojowa - visiat + wszczepy + wszelkie przekształcenia możliwe.
* Jej umysł i magitech daje jej niesamowitą integrację taktyczną z flotą czy wiedzą.

### Charakterystyczne zasoby (3)

* COŚ: OC 'Luctus' - jeden z najgroźniejszych, najbardziej anomalnych i nienaturalnych krążowników Orbitera.
* KTOŚ: Kirasjerzy i Medea Sowińska - jej dwie najsilniejsze bronie.
* WIEM: Zna większość eksperymentów typu Black Technology - zarówno na ludziach jak i anomaliach.
* OPINIA: najbardziej niebezpieczna admirał Orbitera. Nie można jej nigdy zaufać, bo potrafi pokazać fakty po swojemu!

### Typowe sytuacje z którymi sobie nie radzi (-3)

* Z uwagi na swoje modyfikacje jest wrażliwa na EMP, atak hackerów czy na srebro.
* Nie może operować poza statkiem czy Kontrolerem dłużej, niż 4h. Jej ciało wymaga "zasilania". Może mieć baterie ;-).
* Utraciła umiejętności czarowania. Zbyt przekształcona.

### Specjalne

* brak

## Inne

### Wygląd

* Siatka obwodów, wszczepów i sprzężeń na jej sztucznie upiększonym ciele.
* Długie, czarne włosy. Czerwone, mechanizowane oczy. Alabastrowa cera.

### Coś Więcej

* Prototyp to albo adm. Akkaraju (Shogo) albo Aken Bosch (Freespace2). Też Gilbert Durandal. Ma też elementy sariath Aleksandry Trawens.
* Jej funkcją jest być kontrastem dla Kramera - ona chce iść do przodu, nie chronić. Ona jest tą mroczną stroną Orbitera.
* Ataienne jej nienawidzi z całego serca. Ataienne uważa adm. Termię za najgorsze, co mogło Orbitera spotkać.

### Endgame

* Ataienne: zabije Aleksandrę. A jak nie - zmieni ją w coś podobnego do siebie.
* Ona: zjednoczy Orbiter, potem Astorię, potem Valentinę itp.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200826-nienawisc-do-swin            | admirał. Świetnie się bawi widząc bunt na Kontrolerze Pierwszym i to jeszcze przez Ariannę i Kramera. Pomogła Ariannie wydobyć Martyna by ją sprawdzić. | 0110-12-08 - 0110-12-14 |
| 201014-krystaliczny-gniew-elizy     | zrobiła 'złośliwy, przyjacielski psikus' Ariannie Verlen i przekazała jej do dyspozycji statek "Wesoły Wieprzek". | 0111-01-02 - 0111-01-05 |
| 210804-infernia-jest-nasza          | nie chroni swoich komodorów, trzyma się zasady "najsilniejszy przetrwa", ale daje im dużo wyższą autonomię. Chroni Ariannę na K1 pozwalając jej "się buntować" - dlatego żandarmeria nie ruszyła przeciw niej i załodze Inferni. | 0111-04-23 - 0111-04-26 |
| 210929-grupa-ekspedycyjna-kellert   | przeprowadziła operację w sektorze "Noviter", nie wiedząc, że to sektor Mevilig. Straciła 6 okrętów i 200 osób. Nie chciała tracić więcej - ale Ariannie dodała Emulatorkę. | 0112-01-07 - 0112-01-10 |
| 211013-szara-nawalnica              | odbudowała Inferni zapasy i sprzęt. Współpracuje z Infernią, bo chce odzyskać swoich ludzi z Mevilig. Autoryzowała nawet torpedy anihilacyjne. | 0112-01-12 - 0112-01-17 |
| 220706-etaur-dziwnie-wazny-wezel    | wielopoziomowy plan polegający na infiltracji statku Aureliona przez neuroviatoribusa; przekonana przez Eustachego i Klaudię, że konieczny jest sojusz Orbitera z EtAur. Zaczęła wspierać EtAur środkami Orbitera, łamiąc zasadę "Orbiter to jedyna siła dostępu do planety". | 0112-07-15 - 0112-07-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220706-etaur-dziwnie-wazny-wezel    | weszła w sojusz z EtAur Zwycięską, acz ogromnym kosztem reputacyjnym. | 0112-07-17
| 220622-lewiatan-za-pandore          | jej reputacja dostaje STRASZNY cios przez Klaudię i Marię. Robi zakazane eksperymenty na ludziach i biosyntach. Wyszło na jaw i jest głośne. | 0112-07-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Klaudia Stryk        | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Arianna Verlen       | 5 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Antoni Kramer        | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 3 | ((201014-krystaliczny-gniew-elizy; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Leona Astrienko      | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 3 | ((200826-nienawisc-do-swin; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Otto Azgorn          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Ataienne             | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Eliza Ira            | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Kamil Lyraczek       | 1 | ((200826-nienawisc-do-swin)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maria Naavas         | 1 | ((220706-etaur-dziwnie-wazny-wezel)) |
| Marian Fartel        | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Tadeusz Ursus        | 1 | ((200826-nienawisc-do-swin)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Rzieza d'K1      | 1 | ((211013-szara-nawalnica)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
---
categories: profile
factions: 
owner: public
title: Minerwa Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181112-odklatwianie-ateny           | przerażona, zorientowała się, że Saitaer ukryty w jej power suicie jest dużo groźniejszy niż się komukolwiek wydawało - i że ona nie w pełni kontroluje sytuację. | 0109-10-22 - 0109-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Atena Sowińska       | 1 | ((181112-odklatwianie-ateny)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Lucjusz Blakenbauer  | 1 | ((181112-odklatwianie-ateny)) |
| Pięknotka Diakon     | 1 | ((181112-odklatwianie-ateny)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
---
categories: profile
factions: 
owner: public
title: Annika Pradis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | pallidanka; dowodzi bazą. Strasznie naiwna; wierzy w DOBRO Orbitera, w Ariannę. Chce dobrze, ale: zakazała dawnego kultu, sprowadza ludzi z zewnątrz... wierzy, że wszyscy mogą się lubić. | 0112-03-19 - 0112-03-22 |
| 220202-sekrety-keldan-voss          | pallidanka; dowodzi bazą; poprosiła Eustachego o ewakuację z tego miejsca. Załamana faktem, że ich porwani ludzie stali się Kroczącymi we Mgle. Nie daruje keldanowcom... | 0112-03-24 - 0112-03-26 |
| 220216-polityka-rujnuje-pallide-voss | nie ma pojęcia, ale jest wkręcana politycznie. Jej "przyjaciele" manewrują dookoła niej, by móc zniszczyć rekoncyliację pallidanie - keldanici, ją i jej rodzinę. Nie wierzy w istnienie Saitaera XD. | 0112-03-27 - 0112-03-29 |
| 220223-stabilizacja-keldan-voss     | próbuje trzymać wszystkim morale, ale nie wychodzi. Złamane morale. Po killzone Eustachego podniosła morale ludzi... ale kosztem swojej reputacji. | 0112-03-30 - 0112-04-02 |
| 220309-upadek-eleny                 | jednak dowodzi Keldan Voss, współpracując z keldanitami i pallidanami. Nie dała rady zainspirować nikogo, ale dobrze żyje ze wszystkimi - dobra administratorka, fatalna liderka :D. | 0112-04-03 - 0112-04-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220223-stabilizacja-keldan-voss     | ogólna opinia keldanitów i pallidan z Keldan Voss - nic z niej nie będzie. Kiepski dowódca. Naiwna, dobre serce, ale niewiele zdziała. | 0112-04-02
| 220309-upadek-eleny                 | spróbuje być administratorką i liderką. Jest tyle winna swoim ludziom. Eustachy ją zainspirował. | 0112-04-09
| 220309-upadek-eleny                 | ma jeszcze jedną szansę wśród lokalsów i zaufanie, acz ludzie próbują uciec z Keldan Voss. W końcu załatwia sprawy. | 0112-04-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Elena Verlen         | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Eustachy Korkoran    | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Klaudia Stryk        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Raoul Lavanis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
---
categories: profile
factions: 
owner: public
title: Kasandra Kirnał
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | wysokiej rangi polityk Starodrzewców w Toporzysku. Jedna z pierwszych ofiar kralotycznego narkotyku. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | zniewolona przez Kacpra, miała dość wolnej woli by jednak się przeciwstawić i zostać Kapłanką Ośmiornicy - przede wszystkim z miłości dla siostry. | 0109-08-30 - 0109-09-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | Krąg Ośmiornicy i powiązana z nią potęga jej się bardzo spodobały. Nie chce być "na dole", chce iść do góry jako agentka i siła. | 0109-08-30
| 180808-kultystka-z-milosci          | stała się kapłanką Kręgu Ośmiornicy by uratować siostrę, Małgorzatę. Zaakceptowała ich doktryny i obudziła w sobie głód władzy. | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |
---
categories: profile
factions: 
owner: public
title: Szczepan Myrczek
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer, tien, seilita, | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | advancer; seiliowiec; tien; znalazł na poszyciu Królowej dziwne urządzenie (scrambler TAI) i zawołał Darię. | 0100-03-19 - 0100-03-23 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina. | 0100-05-30 - 0100-06-05 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | spytał Darię, czy Ellarina byłaby zainteresowana holograficznymi modelami nietypowych budowli i holodioramami. Daria mu powiedziała, że Ellarina pożre jego serce XD. | 0100-07-15 - 0100-07-18 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | łagodny, poczciwy advancer przeczytał pamięć Lany i określił czego ona nie wie wrt Nox Ignis. Jest dobry w zarządzaniu złożonością (model miasta). | 0100-09-15 - 0100-09-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Hubert Kerwelenios   | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Leszek Kurzmin       | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Astralna Flara    | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leona Astrienko      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Loricatus         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
---
categories: profile
factions: 
owner: public
title: Jamon Korab
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211117-porwany-trismegistos         | kapitan Trismegistosa i fareil. Porwał arystokratów Aurum by doprowadzić ich do Dystryktu Lennet. Miragent Klary Gwozdnik wszedł mu w szkodę, ale działał dobrze... aż Klaudia go sabotowała i jego TAI go odcięła. | 0112-02-09 - 0112-02-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((211117-porwany-trismegistos)) |
| Elena Verlen         | 1 | ((211117-porwany-trismegistos)) |
| Eustachy Korkoran    | 1 | ((211117-porwany-trismegistos)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudia Stryk        | 1 | ((211117-porwany-trismegistos)) |
| Leona Astrienko      | 1 | ((211117-porwany-trismegistos)) |
| Maria Naavas         | 1 | ((211117-porwany-trismegistos)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
---
categories: profile
factions: 
owner: public
title: SCA Goldarion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210108-ratunkowa-misja-goldariona   | Transportowiec należący do ArcheoPrzędz, z załogą powiązaną z Siłami Wyzwolenia Fareil. Brudny i rozpadający się. Jeszcze bardziej uszkodzony na tej akcji, ale zdobył cenne cargo. | 0111-06-11 - 0111-06-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210218-infernia-jako-goldarion      | dostaje potężny refit w okolicach Valentiny (potrwa to 2 miesiące) | 0111-08-03
| 210218-infernia-jako-goldarion      | pojawiła się plotka, że nie jest to statek rzeczywisty; "Goldarion" jako statek nie istnieje i nie istniał - pod tą nazwą zawsze kryją się jakieś tajne działania admirał Termii. | 0111-08-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
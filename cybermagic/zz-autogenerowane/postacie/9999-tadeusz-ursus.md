---
categories: profile
factions: 
owner: public
title: Tadeusz Ursus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200722-wielki-kosmiczny-romans      | eternijski szlachcic i oficer Orbitera na Kontrolerze Pierwszym. Uważa się za męża Eleny. Elena go nie cierpi. Złapał Eustachego by wyjaśnić "nie dobiera się do Eleny" ale przyszła Leona. Teraz wierzy, że ELENA jest flirciarą i chroni reputację Arianny przeciw Elenie. | 0110-11-12 - 0110-11-15 |
| 200819-sekrety-orbitera-historia-prawdziwa | podpuszczony przez Klaudię a potem Ariannę wszedł z Leoną na pojedynek o Elenę. | 0110-11-26 - 0110-12-04 |
| 200822-gdy-arianna-nie-patrzy       | arystokrata kiedyś Aurum, teraz Eterni. Niezbyt bystry acz potężny; nie rozumie co Klaudia próbuje mu pokazać odnośnie jego Skażenia. Jego własny lekarz zatruwał go Esuriit, by Tadeusz upadł. | 0110-12-05 - 0110-12-07 |
| 200826-nienawisc-do-swin            | jeszcze ranny i leży w łóżku, ale już wziął na siebie organizację buntu by osłonić Ariannę. Jest honorowy - chce jej pomóc, bo ona pomogła jemu. | 0110-12-08 - 0110-12-14 |
| 210428-infekcja-serenit             | posiadacz statku "Piękna Elena". Prosi Eustachego, by ten nauczył go podrywać. Prosi Ariannę o pomoc dla OE Falołamacz i przekazuje jej wszystko co wie na temat Falołamacza (nawet czego nie powinien) | 0111-09-24 - 0111-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200819-sekrety-orbitera-historia-prawdziwa | niby champion Eterni, ale przegrał z Leoną. Eternia jest na niego zła - przez swoją chuć i słabość ośmieszył ich wszystkich. | 0110-12-04
| 200819-sekrety-orbitera-historia-prawdziwa | odczepi się od Eleny Verlen. Elena nie będzie już jego żoną. Przegrał walkę z Leoną. | 0110-12-04
| 200819-sekrety-orbitera-historia-prawdziwa | jego bezpośrednią a-tien jest Sabina Servatel. Ona jest gdzieś w kosmosie, ale nie wiadomo dokładnie gdzie (on wie). | 0110-12-04
| 200826-nienawisc-do-swin            | zgromadził na sobie nienawiść buntowników z Inferni i Orbitera, bo "jest prowodyrem" (wziął na siebie by czyścić Ariannę). | 0110-12-14
| 200826-nienawisc-do-swin            | sytuacja beznadziejna; pokazał słabość i arystokraci Eterni są zdecydowani go zniszczyć. Bez sojusznika (Arianny?) nie ma szans. | 0110-12-14
| 200826-nienawisc-do-swin            | delikatny sojusz i cień przyjaźni z Arianną Verlen. | 0110-12-14

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 210428-infekcja-serenit             | święcie przekonany o mistrzostwie Eustachego wobec podrywania dziewczyn, chce by ten nauczył go wszystkiego co wie. | 0111-09-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Eustachy Korkoran    | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Klaudia Stryk        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Martyn Hiwasser      | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Elena Verlen         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210428-infekcja-serenit)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin)) |
| Antoni Kramer        | 2 | ((200722-wielki-kosmiczny-romans; 200826-nienawisc-do-swin)) |
| Damian Orion         | 2 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa)) |
| AK Serenit           | 1 | ((210428-infekcja-serenit)) |
| Aleksandra Termia    | 1 | ((200826-nienawisc-do-swin)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kamil Lyraczek       | 1 | ((200826-nienawisc-do-swin)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| OE Falołamacz        | 1 | ((210428-infekcja-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Infernia          | 1 | ((210428-infekcja-serenit)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
---
categories: profile
factions: 
owner: public
title: Nikodem Dewiremicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | naukowiec Orbitera (51), teoretyk Anomalii; wyteoretyzował obecność Anomalii Statystycznych i Klaudia wysłała mu dowód, że jego teoria jest prawdziwa. | 0109-09-23 - 0109-09-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Fabian Korneliusz    | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Helmut Szczypacz     | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Klaudia Stryk        | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Martyn Hiwasser      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Serbinius         | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
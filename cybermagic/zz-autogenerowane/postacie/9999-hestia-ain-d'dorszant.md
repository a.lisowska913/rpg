---
categories: profile
factions: 
owner: public
title: Hestia Ain d'Dorszant
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191123-echo-eszary-na-dorszancie    | robi co może by utrzymać przy życiu stację, współpracuje z mafią. Druga Hestia zarzuciła jej kolaborację, czego ta nie doceniła. | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Filip Szczatken      | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Gerwazy Kruczkut     | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia Dis d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Kamelia Termit       | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Rafał Kirlat         | 1 | ((191123-echo-eszary-na-dorszancie)) |
---
categories: profile
factions: 
owner: public
title: Celina Burian
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230613-zaginiecie-psychotronika-cede | 23 lata, na terenie Sowińskich, siostra Cede. Blisko brata; do tego stopnia się o niego martwi że nawet poprosiła o pomoc maga rodu Samszar. Ogólnie unika magów. | 0095-08-02 - 0095-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Karolinus Samszar    | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
---
categories: profile
factions: 
owner: public
title: Adela Myrias
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | inspektorka K1 szukająca informacji o dezercji szefa ochrony z Wołu; dużo ściągania danych z K1 dało jej obraz o znikniętym szefie ochrony i o załodze Wołu. Przydusza kapitana (który jest kobieciarzem) i dzięki swej urodzie dowiaduje się, że kapitan nie jest tak czysty wobec dziewczyn. | 0111-09-10 - 0111-09-13 |
| 230103-protomag-z-mrocznego-wolu    | przesłuchując Juliusza natrafiła na Lenę - agentkę Orbitera i dowiaduje się, że ta chce pozbyć się Juliusza. Potem dochodzi, że rodzice Mai mają kłopoty z Syndykatem Aureliona. Doszła do tego, że za morderstwem stoi Lena - inna agentka Orbitera. | 0111-09-14 - 0111-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Juliusz Akramantanis | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
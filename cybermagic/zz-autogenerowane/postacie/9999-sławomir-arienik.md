---
categories: profile
factions: 
owner: public
title: Sławomir Arienik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | 45 lat; patriarcha rodu, który po zniszczeniu rodu w wojnie z Noctis zwrócił się ku Arazille i odzyskał swoją rodzinę jako echa w miragentach. Talia dokończyła jego dzieło, wyłączając wpływ Arazille. Sławomir umarł z uśmiechem - jego rodzina jest bezpieczna. KIA. | 0085-07-26 - 0085-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Felicjan Szarak      | 1 | ((220119-sekret-samanty-arienik)) |
| Klaudia Stryk        | 1 | ((220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Talia Aegis          | 1 | ((220119-sekret-samanty-arienik)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
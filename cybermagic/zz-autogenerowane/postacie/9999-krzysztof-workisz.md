---
categories: profile
factions: 
owner: public
title: Krzysztof Workisz
---

# {{ page.title }}


# Generated: 



## Fiszki


* pilot Mrocznego Wołu | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO:  -0-0+ |Żyje chwilą;;Zachowuje swoją prywatność dla siebie| VALS: Achievement, Family >> Stimulation| DRIVE: Ochrona rodziny | @ 221220-dezerter-z-mrocznego-wolu
* "by sobie dorobić!"; "przemyt i próby" | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


archiwum-o

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230103-protomag-z-mrocznego-wolu    | pilot Mrocznego Wołu; słaby facet, ale żonaty. Ma romans z Nelą (z woli Wincentego) i z Nadją (z woli Nadji XD). Niskiej klasy przemytnik. Całkowicie niegroźny. Kocha rodzinę, ale jest słaby. | 0111-09-14 - 0111-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
---
categories: profile
factions: 
owner: public
title: Anita Perczoluk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | duchowa szefowa Starodrzewców w Toporzysku; potraktowana kralotycznym narkotykiem przez Kasandrę, obróciła Starodrzewców przeciw Zespołowi. | 0109-08-29 - 0109-08-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 1 | ((180730-prasyrena-z-zemsty)) |
| Kalina Rotmistrz     | 1 | ((180730-prasyrena-z-zemsty)) |
| Kasandra Kirnał      | 1 | ((180730-prasyrena-z-zemsty)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Maksymilian Supolont | 1 | ((180730-prasyrena-z-zemsty)) |
| Małgorzata Kirnał    | 1 | ((180730-prasyrena-z-zemsty)) |
| Stach Sosnowiecki    | 1 | ((180730-prasyrena-z-zemsty)) |
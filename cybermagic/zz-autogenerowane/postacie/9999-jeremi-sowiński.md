---
categories: profile
factions: 
owner: public
title: Jeremi Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220111-marysiowa-hestia-rekinow     | wysłany przez Sowińskich na teren Rekinów z uwagi na to, jak bardzo Sowińscy są niezadowoleni z działań Marysi. W DRODZE przez pewien czas. | 0111-09-12 - 0111-09-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Ernest Namertel      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Karolina Terienak    | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Keira Amarco d'Namertel | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Liliana Bankierz     | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Marysia Sowińska     | 1 | ((220111-marysiowa-hestia-rekinow)) |
---
categories: profile
factions: 
owner: public
title: Tomasz Dojnicz
---

# {{ page.title }}


# Generated: 



## Fiszki


* marine, Orbiter | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | marine; skłoniony przez Maję by powalczył z Arianną. Dobry w walce, ogólnie nieśmiały i tchórzliwy. Arianna zmotywowała go i ją pokonał; she tapped out. Serio szanuje i lubi Ariannę. | 0100-03-19 - 0100-03-23 |
| 221130-astralna-flara-w-strefie-duchow | jego łagodność wyjątkowo się przydała do zarządzania przerażoną savaranką (jeńcem). | 0100-08-07 - 0100-08-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Arnulf Perikas       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Daria Czarnewik      | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Hubert Kerwelenios   | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Elena Verlen         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Ellarina Samarintael | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gabriel Lodowiec     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Mariusz Bulterier    | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| NekroTAI Zarralea    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Astralna Flara    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Athamarein        | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Szczepan Myrczek     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
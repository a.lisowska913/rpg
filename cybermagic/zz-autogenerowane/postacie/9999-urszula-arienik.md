---
categories: profile
factions: 
owner: public
title: Urszula Arienik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | 26 lat; jedyny żywy członek rodziny Arieników poza ojcem. Nagle - została głową rodu. | 0085-07-26 - 0085-07-28 |
| 190709-somnibel-uciekl-arienikom    | wpływowa arystokratka, władczyni Rezydencji Arienik i czarodziejka Luxuritias. Kocha piękno i chroni swego syna przed "innymi". Ma wady arystokratki. | 0110-06-01 - 0110-06-03 |
| 211207-gdy-zabraknie-pradu-rekinom  | przechwyciła bankrutującą Elektrownię Węglową Szarpien i korzystając z okazji zerwała kiepską umowę którą kiedyś podpisała Elektrownia z Amelią Sowińską. Przez to Rekiny zostały bez prądu. | 0111-08-26 - 0111-08-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | zostaje głową rodu Arienik, wspierana przez trzy TAI - Marię (mamę), Błażeja (brata) i Samantę (siostrę). Zostaje SAMA, z posiadłością Arieników - ale wszyscy są wolni od wpływu Arazille. | 0085-07-28
| 211207-gdy-zabraknie-pradu-rekinom  | lokalny "ród" Arienik jest uznany za wrogów numer jeden Rekinów. Przez Rekiny. Bo prąd odcięła. | 0111-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Daniel Terienak      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ernest Kajrat        | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Felicjan Szarak      | 1 | ((220119-sekret-samanty-arienik)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Karolina Terienak    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Klaudia Stryk        | 1 | ((220119-sekret-samanty-arienik)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Ksenia Kirallen      | 1 | ((220119-sekret-samanty-arienik)) |
| Lorena Gwozdnik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Marysia Sowińska     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Pięknotka Diakon     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Talia Aegis          | 1 | ((220119-sekret-samanty-arienik)) |
| Tomasz Tukan         | 1 | ((190709-somnibel-uciekl-arienikom)) |
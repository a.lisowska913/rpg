---
categories: profile
factions: 
owner: public
title: Irek Kraczownik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230404-wszystkie-duchy-siewczyna    | niekompetentny egzorcysta, który przypałętał na ten teren Hybrydę noktiańskiej TAI i ducha zemsty 3 lata temu. Nieobecny na TEJ sesji, acz jej przyczyna. | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | kiedyś egzorcysta, teraz zabija się i oddaje swoją energię by tylko utrzymać Sanktuarium (swój nowy dom) przy życiu. Porwany przez Karolinusa Samszara przy biernym współudziale Eleny Samszar. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | próbuje przekonać Samszarów, że Aurum jest z dupy i on próbuje naprawdę chronić ludzi. Opowiedział, jak kiedyś Blakenbauerowie go krzywdzili. Zależy mu na Sanktuarium. Przekonał (compel) Elenę, by ona wstawiła się za Sanktuarium Kazitan i za to wsparł jej cele (by Elena i Karolinus wyszli na bohaterów a nie porywaczy). | 0095-07-24 - 0095-07-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | straszna trauma i strach przed magami Aurum. Boi się ich i nie chce współpracować. Kiedyś był torturowany przez Blakenbauerów. | 0095-07-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Elena Samszar        | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Karolinus Samszar    | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
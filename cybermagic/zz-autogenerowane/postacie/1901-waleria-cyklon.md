---
categories: profile
factions: 
owner: public
title: Waleria Cyklon
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Pierwsza oficer Łysych Psów i taktyk grupy. Ona jest mózgiem taktycznym i głównodowodzącą grupy - Moktar jest charyzmatycznym przywódcą. Była terminuska, uratowana przez Moktara. Zwiadowca i snajper; kiepska w walce w zwarciu. Świetna obserwatorka; doskonale opanowana i bardzo małomówna. Spymaster grupy.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* COMMUNITY/BELONGING; zbudować sobie kawałek świata, gdzie będzie jej dom; wspierać Moktara, który wyciągnął rękę do przegranej terminuski i wspierać Łyse Psy by zbudować "dom".
* przegrana i samotna na wrogiej ziemi; znaleźć sobie nowy dom wśród tych którzy jej potrzebują; wspierać i dowodzić Łysymi Psami, którzy nie zadają pytań i jej potrzebują.
* ORDER/POWER; nikt nie odważy się jej tknąć i wszyscy znają swoje miejsce w przyrodzie; bezwzględna retaliacja oraz wymuszanie porządku Moktara
* małomówna, cicha, oszczędna w ruchach, unikająca opinii, unikająca zbędnej przemocy

### Wyróżniki (3)

* Swarm: bezproblemowo koordynuje działania wielu bytów które ogarnia bez większego kosztu dla siebie
* Mistrzyni zwiadu: potrafi łączyć fakty i znajdować wszystkie korelacje między danymi; wybitna obserwatorka
* Upiór: nigdy nie wiadomo gdzie dokładnie jest i skąd nadleci pocisk; porusza się bezszelestnie i dyskretnie
* Potrafi pokonać zdecydowanie przeważającą siłę przez połączenie kamuflażu, teleportacji, swarm i snajperów

### Zasoby i otoczenie (3)

* Swarm: drony zwiadowcze i bojowe, dające jej oczy, uszy oraz niemałą siłę ognia
* Oblivion: wzmocnione przeciwpancerne działo energetyczne, wojskowe

### Magia (3)

#### Gdy kontroluje energię

* Infomantka analityczna: potrafi połączyć się i agregować dowolne systemy danych by wiedzieć co robić dalej; master info-aggregator rzeczywistości
* Technomantka roju: albo przez tworzenie małych dron bojowych / zwiadowczych albo przez łączenie elektroniki w sieć monitorującą dla niej
* Teleportacja: małe, szybkie przesunięcia z fałszywymi echami umożliwiające jej atak z zaskoczenia. Też: portal dla oddziału.
* Iluzja i kamuflaż: nie jest tam, gdzie myślisz. Prześlizgiwanie się, ukrywanie się, manewrowanie wbrew przeciwnikowi.

#### Gdy traci kontrolę

* Przywołuje upiory które prawie ją zabiły w dawnej przeszłości; upiory są w trybie berserk i polują na wszystkich i wszystko
* Rozładowuje broń i traci dostęp do działań ofensywnych dla siebie lub zespołu
* Swarm wpada w tryb locust - chce przetrwać i żywić się wszystkim

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | pierwszy oficer Moktara, której nie podobała się personalna wendetta Moktara wobec bogu ducha winnej Lilii. | 0109-10-28 - 0109-10-31 |
| 181209-kajdan-na-moktara            | wiecznie lojalna Moktarowi; chroniła jego bazę przed Pięknotką a potem weszła z nią w sojusz przed nacierającymi nojrepami. | 0109-11-01 - 0109-11-02 |
| 181218-tajemniczy-oltarz-moktara    | osłabiona; pomaga Pięknotce przystosować się do świata gdzie śnią jej się lustra. Zmasakrowała maga, który ośmielił się ją zaatakować swoim swarmem. | 0109-11-10 - 0109-11-12 |
| 181226-finis-vitae                  | pozyskała Lilię dla Moktara oraz wraz z Pięknotką opracowywała plan jak wyciągnąć Dariusza od autowara (nie wie o autowarze). | 0109-11-17 - 0109-11-26 |
| 181227-adieu-cieniaszczycie         | w krew wchodzi jej porywanie magów - wpierw Lilia, potem Erwin. Planowała jak ściągnąć Julię do Łysych Psów - z powodzeniem. | 0109-11-27 - 0109-11-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | Moktar ustawił ją jako pierwszą oficer Łysych Psów. Dał jej prawo do planowania i decydowania o ruchach Psów a ona ogranicza jego psychopatyczne ruchy. | 0109-10-31
| 181125-swaty-w-cieniu-potwora       | szanuje Pięknotkę jako przeciwnika. Nie "lubi" jej, ale nie życzy jej źle. | 0109-10-31
| 181209-kajdan-na-moktara            | ciężko ranna, wymaga regeneracji i w niełaskach Moktara z uwagi na fail, utratę bazy i dowód jaki ma Pięknotka mogący zniszczyć Psy. | 0109-11-02

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | będzie blokowała ruchy Moktara, które są skupione tylko na jego idiotycznej wendetcie a nie na temat sukcesu Wielkiego Planu. | 0109-10-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Atena Sowińska       | 3 | ((181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 3 | ((181125-swaty-w-cieniu-potwora; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 3 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Amadeusz Sowiński    | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Bogdan Szerl         | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Moktar Grodan        | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Romuald Czurukin     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
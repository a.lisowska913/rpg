---
categories: profile
factions: 
owner: public
title: AK Rodivas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201230-pulapka-z-anastazji          | statek cywilny, który spotkał się z Kryptą i przekształcił w anomalię. Nośnik "Anastazji". Ciężko uszkodzony przez Infernię, przechwycili go Sowińscy. | 0111-05-21 - 0111-05-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Arianna Verlen       | 1 | ((201230-pulapka-z-anastazji)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Elena Verlen         | 1 | ((201230-pulapka-z-anastazji)) |
| Eustachy Korkoran    | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Klaudia Stryk        | 1 | ((201230-pulapka-z-anastazji)) |
| Martyn Hiwasser      | 1 | ((201230-pulapka-z-anastazji)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OO Infernia          | 1 | ((201230-pulapka-z-anastazji)) |
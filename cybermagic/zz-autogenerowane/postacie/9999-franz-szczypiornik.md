---
categories: profile
factions: 
owner: public
title: Franz Szczypiornik
---

# {{ page.title }}


# Generated: 



## Fiszki


* kapitan Loricatus, weteran Orbitera (5x) szukający synekur (atarien) | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO:  -0+-- |Nie ma głowy w chmurach, trzeba ciężko pracować;;Nie ufa nikomu;;Niezwykle surowy| VALS: Hedonism, Security >> Conformity| DRIVE: Pokój przez tyranię | @ 221230-dowody-na-istnienie-nox-ignis
* "Pax Orbiter jest najważniejszy", "Jeśli wiedzą co jest dla nich dobre, będą współpracować" | "nie rozumiesz sytuacji, noktianinie" (nie używa imion tylko tytułów) | @ 221230-dowody-na-istnienie-nox-ignis

### Wątki


historia-talii
koszmar-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | kapitan Loricatus; zgłosił się na ochotnika bo zawdzięcza coś Katrinie. Chłodny styl wypowiedzi, nie jest fanem noktian ale wierzy w prawidłowy Orbiter. | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | uznał, że lokalne siły Orbitera na CON Ratio Spei nie wypełniają swojej roli; wydaje polecenia Talii by się wywiedzieć co się tu dzieje. Stoi twardo na stanowisku, że niewolnictwo jest zakazane w światach Orbitera. | 0082-08-02 - 0082-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
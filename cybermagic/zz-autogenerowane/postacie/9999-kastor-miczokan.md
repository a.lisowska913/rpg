---
categories: profile
factions: 
owner: public
title: Kastor Miczokan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200226-strazniczka-przez-lzy        | agent Pustogoru, inkwizytor; wraz z Aliną i Darkiem weszli w umysł Alicji by zmienić ją w Strażniczkę Esuriit; skonfliktował się silnie z Lucjuszem lecz znalazł go fizycznie. | 0110-07-25 - 0110-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |
---
categories: profile
factions: 
owner: public
title: Wanessa Pyszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200930-lekarz-dla-elizy             | zwiadowca Noctis; manewrowała w skrajnie Skażonym Ortus-Conticium i porwała lekarza ściągając na astoriański oddział potwory. Ciężko Skażona. | 0082-02-10 - 0082-02-14 |
| 200916-smierc-raju                  | ryzykantka kochająca pustkowia i scavenger; pomogła Eustachemu i Elenie zdjąć z Tucznika osłony antymagiczne. Puszcza plotkę dalej o tym, że Eustachy x Elena. | 0110-12-21 - 0110-12-23 |
| 201021-noktianie-rodu-arlacz        | wysłana przez Garwena, by prowadziła Szturmowców Sowińskich po Kryształowej Pustyni tak, by oni NIGDY nie znaleźli Elizy Iry. | 0111-01-07 - 0111-01-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 2 | ((200930-lekarz-dla-elizy; 201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Autofort Imperatrix  | 1 | ((200930-lekarz-dla-elizy)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Elena Verlen         | 1 | ((200916-smierc-raju)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Karmina Alarel       | 1 | ((200930-lekarz-dla-elizy)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 1 | ((200916-smierc-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Szymon Szynek        | 1 | ((200930-lekarz-dla-elizy)) |
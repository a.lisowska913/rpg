---
categories: profile
factions: 
owner: public
title: Urszula Miłkowicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | pierwszoroczna uczennica terminusa; świeżynka. Niezbyt bogata, bez wpływów, z ludzkiej rodziny. Naiwna. Wierzy, że robi misję szpiegowską; wykazała się wysoką inteligencją adaptując do porwania i planu Roberta i determinacją, walcząc i kończąc poparzoną przeciw efemerydzie. Będzie z niej coś przydatnego. | 0110-10-21 - 0110-10-23 |
| 210323-grzybopreza                  | wezwana przez Napoleona, wyczuła, że coś jest nie tak z "porwanym Myrczkiem". Ale niewiele mogła zrobić. Po obserwowaniu domu Triany, dała się wymanewrować i wezwała wsparcie przeciw "czystej" grzyboprezie Rekinów i Myrczka. | 0111-04-18 - 0111-04-19 |
| 210622-verlenka-na-grzybkach        | niechętnie współpracująca z Marysią uczennica terminusa. Bardziej kompetentna niż się wydaje, choć kij w tyłku. Pokonana z zaskoczenia przez Arkadię (nie doceniła), potraktowała ją tazerem jak już była ranna i na ziemi. Umie współpracować jeśli chce. | 0111-05-26 - 0111-05-27 |
| 210713-rekin-wspiera-mafie          | rozczarowana, że nie przyskrzyni żadnego Rekina. Uważa, że Rekiny pozwalają sobie na zdecydowanie za dużo. Ściągnęła Marysi Tukana - jeśli to może uratować akcję... ale się postawiła. Marysia musiała poprosić. | 0111-06-02 - 0111-06-05 |
| 210720-porwanie-daniela-terienaka   | uciekinierka z Aurum i eks-protomag; porwała Daniela by poznać kto jest Rekinem działającym z mafią - ale nic z tego nie osiągnęła. Mieszka samotnie z kotem. | 0111-06-10 - 0111-06-12 |
| 220814-stary-kot-a-trzesawisko      | po próbie porwania Daniela przyznała się przełożonym i przesunęli ją do dokumentów - aż zadzwoniła Karo poprosić o pomoc w uratowaniu kota. Ula pomogła Karo; dowiedziała się o visterminach, powiedziała, że Alan może pomóc i bardzo dużo wzięła na siebie przy osłanianiu vistermina Rozbójnika. By Alan nie zabił kota, wzięła go jako swojego i skończyła na fortifarmie Lechotka z potencjalnie szaloną TAI, groźnym kotem, jej własnym kotem i ryzykiem zagrożenia ze strony Trzęsawiska. Aha, i z konstruminusem. | 0111-10-08 - 0111-10-09 |
| 220816-jak-wsadzic-ule-alanowi      | zdecydowanie woli życie w samotności na fortifarmie (choć chce być nieco bezpieczniejsza) niż w blokowisku. Ratuje swojego kota przez visterminem Rozbójnikiem. | 0111-10-08 - 0111-10-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | BARDZO nieufna wobec Gabriela Ursusa; tyle jej obiecał a skończyła poparzona i z potężnym OPR od Tymona Grubosza, którego Ula podziwia. | 0110-10-23
| 210323-grzybopreza                  | HAŃBA! Wezwała wsparcie do CZYSTEJ (zero alkoholu czy narkotyków) imprezy Rekinów. Oczywiście, to była prowokacja. Ale Ula nie powinna na to była się wpakować. | 0111-04-19
| 210323-grzybopreza                  | nie lubi arystokracji Aurum. A zwłaszcza Marysi Sowińskiej. Dodatkowo - nie lubi Ignacego Myrczka. Ma opinię, że się bała że on coś zrobi złego na imprezie... | 0111-04-19
| 210622-verlenka-na-grzybkach        | niechętny szacunek do Marysi Sowińskiej. Będzie z nią współpracować. Marysia nie jest ani zła ani głupia. Da się z nią sporo zrobić. | 0111-05-27
| 220814-stary-kot-a-trzesawisko      | relatywnie szczęśliwa właścicielka vistermina (kota) Rozbójnika. Z rozkazu Alana Bartozola mieszka na Fortifarmie Lechotka i jest PRZERAŻONA. | 0111-10-09
| 220814-stary-kot-a-trzesawisko      | zdjęta z dokumentacji i nie mieszania się w sprawy Aurum (gdzie wsadził ją Gabriel Ursus) przez Alana Bartozola. | 0111-10-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Tomasz Tukan         | 4 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Julia Kardolin       | 3 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Kacper Bankierz      | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Karolina Terienak    | 3 | ((210720-porwanie-daniela-terienaka; 220814-stary-kot-a-trzesawisko; 220816-jak-wsadzic-ule-alanowi)) |
| Rafał Torszecki      | 3 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka)) |
| Triana Porzecznik    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Ignacy Myrczek       | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Liliana Bankierz     | 2 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach)) |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Daniel Terienak      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Napoleon Bankierz    | 1 | ((210323-grzybopreza)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
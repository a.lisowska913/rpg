---
categories: profile
factions: 
owner: public
title: Rafał Kumczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201006-dezinhibitor-dla-sabiny      | Rekin; główny wykonawca planu Justyniana ukarania Sabiny. Powiedział Gabrielowi o Sabinie i Ignacym, zachęcił Ignacego do idiotycznego planu i przeprowadził Ignacego przezeń. | 0110-10-03 - 0110-10-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ignacy Myrczek       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Napoleon Bankierz    | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Sabina Kazitan       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
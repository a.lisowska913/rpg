---
categories: profile
factions: 
owner: public
title: Herbert Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  0-0++ | Nie ma blokad;; Uparty jak osioł | VALS: Benevolence, Hedonism >> Self-Direction | DRIVE: Tajemnica Wszechświata | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* styl: GB; peaceful, silent, cycle-focused, nurturing | tradition, community, faith, nature | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* magia: "ten, który śpiewa roślinom" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* ruch: "ten byt to nie było coś czemu powinno się było dać szansę" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz

### Wątki


szamani-rodu-samszar
mroczna-wiedza-samszarów

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | (26); całkowicie zakręcony przez Itrię; coś wie o tajnych bazach, ale nie chce na terenie sentisieci Samszarów o tym opowiadać. Uważa Elenę za wybitnie inteligentną. Dzięki Karolinusowi i Elenie Itria poszła z nim do łóżka, za co jest im niezmiernie wdzięczny. | 0095-08-15 - 0095-08-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | ma tymczasowo ważną roślinę - lilię anandobao (błogość, euforia + eksplozja) którą pilnuje i rozwija dla Itrii Diakon (ofc). | 0095-08-18
| 230606-piekna-diakonka-i-rytual-nirwany-koz | uważa, że Elena Samszar jest Sherlockiem Holmesem naszych czasów; "wylogiczniła" lokalizację tajnej bazy. | 0095-08-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elena Samszar        | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Karolinus Samszar    | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
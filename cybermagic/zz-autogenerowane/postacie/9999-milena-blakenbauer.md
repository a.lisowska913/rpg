---
categories: profile
factions: 
owner: public
title: Milena Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 15 lat, "Krwawa Lady Blakenbauer" - bo tak jest śmieszniej. Catgirl. URG. Zakład z Lucjuszem sprowadził kłopoty. Ciekawska, nie ma w niej nienawiści czy złości, bardzo dziecięca. Idzie za uczuciami i perfekcją formy. Odda oddział za piosenkę i taniec. | 0092-08-03 - 0092-08-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210306-wiktoriata                   | zamiast "Krwawej Lady Blakenbauer" jest "fear me, senpai! Kyaa!" na terenie rodu Verlen. Milenie się to NIE podoba, ale nie wie skąd się to wzięło. Jest traktowana jako mem. | 0093-10-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210224-sentiobsesja)) |
| Arianna Verlen       | 1 | ((210224-sentiobsesja)) |
| Lucjusz Blakenbauer  | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Przemysław Czapurt   | 1 | ((210224-sentiobsesja)) |
| Viorika Verlen       | 1 | ((210224-sentiobsesja)) |
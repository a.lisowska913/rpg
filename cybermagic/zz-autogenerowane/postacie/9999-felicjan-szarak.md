---
categories: profile
factions: 
owner: public
title: Felicjan Szarak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 18 lat; wpadł na pomysł, by zamknąć Agenta Esuriit w Nieskończonym Labiryncie. Podchwyciła ten pomysł Klaudia i pomogła to opracować z nauczycielami i ekipą. | 0083-10-13 - 0083-10-22 |
| 211009-szukaj-serpentisa-w-lesie    | 19 lat; uczeń terminusa AMZ; rozsądnie podszedł do niemożliwej sytuacji. Pomyślał o poinformowaniu centrali i nie dał się wyciągnąć groźbom serpentisa. Będzie z niego solidny terminus. | 0084-11-13 - 0084-11-14 |
| 211010-ukryta-wychowanka-arnulfa    | 19 lat; rozproszył grupę włamującą się do domu Talii słowami "terminus!" i powstrzymał Ksenię, która chciała ich gonić. Zarekwirował pojazd i szybko pojechał z Zespołem po Talię na prośbę Klaudii. | 0084-12-11 - 0084-12-12 |
| 220119-sekret-samanty-arienik       | chłopak Samanty; Samanta przekonała go, że jest miragentem i nie chce żyć w kłamstwie. Pomógł Samancie dotrzeć do Talii, ale nie może sobie tego wybaczyć że "zabił" swoją dziewczynę. | 0085-07-26 - 0085-07-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | został parą z Ksenią Kirallen po wydarzeniach z serpentisami w Lesie Trzęsawnym. | 0084-12-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Mariusz Trzewń       | 2 | ((211009-szukaj-serpentisa-w-lesie; 211122-czolenkowe-esuriit-w-amz)) |
| Strażniczka Alair    | 2 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Talia Aegis          | 2 | ((211010-ukryta-wychowanka-arnulfa; 220119-sekret-samanty-arienik)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arnulf Poważny       | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Teresa Mieralit      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Tymon Grubosz        | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
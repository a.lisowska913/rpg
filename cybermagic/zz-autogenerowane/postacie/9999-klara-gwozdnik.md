---
categories: profile
factions: 
owner: public
title: Klara Gwozdnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211212-nie-taki-bezkarny-mlody-tien | 14 lat. Ukochana Rolanda (ze wzajemnością), pierwszy raz na zlocie. Trochę nerd broni, inne laski jej nie lubią bo tryharduje, chłopaki dzięki Rolandowi ją lubią, bo fajnie gada o broni. Losowo wybrana jako +1 zakładnik przez Stalową Kompanię. Opanowała nerwy na wodzy dzięki Rolandowi. Kiepska społecznie (na tym etapie). | 0101-04-10 - 0101-04-13 |
| 211117-porwany-trismegistos         | właścicielka miragenta. Pojechała z ekipą na wycieczkę by odwiedzić Neikatis i skończyła jako corrupted dla Jamona. Uratowana przez Infernię. | 0112-02-09 - 0112-02-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Roland Sowiński      | 2 | ((211117-porwany-trismegistos; 211212-nie-taki-bezkarny-mlody-tien)) |
| Alojzy Lemurczak     | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| Arianna Verlen       | 1 | ((211117-porwany-trismegistos)) |
| Elena Verlen         | 1 | ((211117-porwany-trismegistos)) |
| Eustachy Korkoran    | 1 | ((211117-porwany-trismegistos)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Klaudia Stryk        | 1 | ((211117-porwany-trismegistos)) |
| Leona Astrienko      | 1 | ((211117-porwany-trismegistos)) |
| Maria Naavas         | 1 | ((211117-porwany-trismegistos)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Nikczemniczka Diakon | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
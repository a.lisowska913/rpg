---
categories: profile
factions: 
owner: public
title: Olga Leszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190326-arcymag-w-raju               | neuronautka i współoperatorka Ataienne; ona głównie rozmawiała z Ataienne i shackowała z Fergusem lokalizację jej bazy by móc napuścić na nią Castigatora. | 0110-04-07 - 0110-04-08 |
| 190521-dwa-stare-miragenty          | przechwyciła bazę danych z miragenta tak, że nikt się nie zorientował. Opiekowała się też rannymi. | 0110-04-15 - 0110-04-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190521-dwa-stare-miragenty          | przechwyciła bazę danych miragentów, którą jeden z wysoko postawionych dowódców Orbitera chciał z jakiegoś powodu zniszczyć; coś politycznego. | 0110-04-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eliza Ira            | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Fergus Salien        | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Alojzy Wypyszcz      | 1 | ((190521-dwa-stare-miragenty)) |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fred Wypyszcz        | 1 | ((190521-dwa-stare-miragenty)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
---
categories: profile
factions: 
owner: public
title: Szczepan Korzpuda
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181116-maciek-w-otchlani-wezosmokow | xxTerrorxx, nie ma życia. Bully, świnia i nieprzyjemny typek. Przez niego doszło do Skażenia pola magicznego w Żarni. | 0109-09-12 - 0109-09-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181116-maciek-w-otchlani-wezosmokow | kocha Smocze Wojny, ale nie ma życia jako xxTerrorxx - Zaczęstwiacy na niego polują. | 0109-09-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wroński         | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Maciej Korzpuda      | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Olga Myszeczka       | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Piotr Ryszardowiec   | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Robert Einz          | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Zdzich Aprantyk      | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Zuzanna Klawornia    | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
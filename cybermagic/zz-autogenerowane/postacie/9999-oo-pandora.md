---
categories: profile
factions: 
owner: public
title: OO Pandora
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220622-lewiatan-za-pandore          | KIA. Malictrix bardzo próbowała pokonać Lewiatana i się weń zapatrzyła. Kupiła czas Eustachemu by ten strzelił torpedą anihilacyjną, ale Pandora została zniszczona. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200610-ixiacka-wersja-malictrix     | statek przekazany Ariannie Verlen przez siły Kramera; zaawansowana psychotronika i koloid. Delikatny jak cholera, zasięg eskortowca. Klasa: korweta. 16 osób. | 0111-03-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 1 | ((220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 1 | ((220622-lewiatan-za-pandore)) |
| Maria Naavas         | 1 | ((220622-lewiatan-za-pandore)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Raoul Lavanis        | 1 | ((220622-lewiatan-za-pandore)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
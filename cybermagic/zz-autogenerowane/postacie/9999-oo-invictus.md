---
categories: profile
factions: 
owner: public
title: OO Invictus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | patchworkowy transportowiec noktiański, zarekwirowany przez siły Orbitera i obsadzony na szybko jako QShip. Skierowany do misji wsparcia tien Jolanty Kopiec z tajemniczej noktiańskiej jednostki. Świetne podejście - wyłączył wroga, ale strasznie zdewastowany. Różnica klas. | 0080-11-23 - 0080-11-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Martyn Hiwasser      | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |
---
categories: profile
factions: 
owner: public
title: Samanta Arienik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | miragent emulujący 17 lat; dziewczyna Felicjana. Pokonała programowanie (żadnych badań, żadnych leków) by zniszczyć karaluchy w katastrofie magicznej. Poznała prawdę o sobie i uznała, że NIE CHCE ŻYĆ jako kłamstwo, echo Arazille. Pojechała z Klaudią do Talii Aegis i dała się przekształcić w TAI i oczyścić z błędnego programowania i wpływów Arazille. | 0085-07-26 - 0085-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Felicjan Szarak      | 1 | ((220119-sekret-samanty-arienik)) |
| Klaudia Stryk        | 1 | ((220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Talia Aegis          | 1 | ((220119-sekret-samanty-arienik)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
---
categories: profile
factions: 
owner: public
title: Laura Turabnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | barmanka w "Łeb jaszczura", opowiada Karolinusowi o problemach miasta i firmie EnMilStrukt. Reprezentuje pokolenie które chce zmian i woli wygodne życie ponad harmonię. Jest głośna i kłótliwa. | 0095-06-20 - 0095-06-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fiona Szarstasz      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Karolinus Samszar    | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
---
categories: profile
factions: 
owner: public
title: SCA Hiyori
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | jednostka neikatiańska; salvager; wysoka automatyzacja i sporo dron; p.d. Ogdena Barbatova; ma dwa androidy: Adama i Ewę. Próbuje harvestować derelict Okarantis. | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | operacja salvagowania skończyła się niebezpieczną wodą z dziwnymi bioformami. Hiyori jest uszkodzona. | 0090-04-19 - 0090-04-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | strasznie wykosztowała się z zasobów: FINANSE: -10, SPECJALISTYCZNE: -7, PROTOMAT+1, ENERGIA-1, WODA-1, DANE+1, (ZYSK, CENNE, RARE_MAT):0 | 0090-01-14
| 221111-niebezpieczna-woda-na-hiyori | uszkodzona przez dziwne "węgorze" w kupionej skażonej wodzie. Około 15 dni na stacji Nonarion by wyczyścić i naprawić jednostkę (niższy priorytet) | 0090-04-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Iga Mikikot          | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Jakub Uprzężnik      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Kaspian Certisarius  | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Leo Kasztop          | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Nastia Barbatov      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Patryk Lapszyn       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Safira d'Hiyori      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Ailira Niiris        | 1 | ((221111-niebezpieczna-woda-na-hiyori)) |
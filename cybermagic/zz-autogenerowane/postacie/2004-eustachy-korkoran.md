---
categories: profile
factions: 
owner: public
title: Eustachy Korkoran
---

# {{ page.title }}


# Read: 

## Kim jest

### Paradoksalny Koncept

Sabotażysta specjalizujący się w kontrolowanych i dyskretnych zniszczeniach. Najcelniejszy strzelec na Inferni, ale nie lubi niekierowanych zniszczeń. Od biedy może pełnić rolę mechanika, ale nie jest w tym ekspertem. Supremacjonista magów i ludzi, który stoi twardo po stronie Orbitera w konflikcie frakcji. Mistrz kontrolowanej destrukcji, który wykorzystuje swoje talenty w służbie ludzkiej rasy i przeciw anomaliom.

### Motto

"Przetrwaliśmy jak na razie wszystko - a czego nie potrafimy ujarzmić, to zniszczymy."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Sabotażysta najwyższej klasy. Potrafi dyskretnie i niezauważenie wyłączyć lub zniszczyć kluczowy komponent lub odwrócić uwagę. Jego działania są niewidoczne dla odbiorcy.
* ATUT: Doskonały strzelec i artylerzysta - świetnie radzi sobie z ogniem niekierowanym. Często pełni rolę snajpera.
* SŁABA: Czuje się bardzo nieswojo nie mając narzędzi wysokotechnologicznych; na jakiejś planecie, w lesie - byłby bardzo nieszczęśliwy i zagubiony.
* SŁABA: Negocjacje, polityka, tego typu sprawy - to nie jest dla niego. Jego interesuje pole bitwy i technologia; te rzeczy "z ludźmi" woli zostawić innym.

### O co walczy (3)

* ZA: W kosmosie znajdują się różnego rodzaju anomalne "potwory". On chce się z nimi zmierzyć i wszystkie zniszczyć. Dla wyzwania.
* ZA: Orbiter jest najlepszą opcją dla ludzkości - jedna potężna siła militarna pilnująca, by Astoria była bezpieczna a handel funkcjonował. Lojalista.
* VS: Nie toleruje głupiego, nieefektywnego wykorzystywania zasobów tylko dlatego, żeby polityka się zgadzała. Dlatego dołączył do Inferni.
* VS: Nie zgadza się na supremację jakichkolwiek istot czy bytów nad ludzkość. Kraloth, Spustoszenie, bogowie, TAI - to wszystko jest nieakceptowalne.

### Znaczące Czyny (3)

* Gdy był cywilnym członkiem załogi statku handlowego 'Pelidor', skutecznie rozstawił ładunki gdy byli abordażowani przez piratów. Piraci myśląc że 'Pelidor' się rozpada - ewakuowali się.
* Gdy zbieracze mieli problem na Cmentarzysku Statków, przystosował laser górniczy jednego z ich statków i zestrzelił trzy ŁZy niebezpiecznie zbliżające się do ich pozycji.
* Gdy Infernia uciekała przed Nocną Kryptą, wykorzystał swoje umiejętności i jakkolwiek uszkodził statek, ale wyciągnął z niego dość mocy by Infernia uciekła anomalnemu napastnikowi.

### Kluczowe Zasoby (3)

* COŚ: Zbiór zaawansowanych magitechowych narzędzi do przebudowywania bytów technologicznych - dzięki temu umie zmienić większość rzeczy w broń (lub w coś co wybuchnie).
* KTOŚ: Kilku inżynierów w warsztatach Kontrolera Pierwszego; często Eustachy jest proszony o ocenienie jak odporna jest dana konstrukcja.
* WIEM: Zna słabe strony większości konstrukcji ludzkich i magitechowych; jeśli nie jest to supertajne, to Eustachy wie jak to najskuteczniej i dyskretnie uszkodzić.
* OPINIA: Jeśli Eustachy coś chce zniszczyć to to zniszczy - nie da się go zatrzymać. Jedyne co można to skierować jego energię w inną stronę.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | sabotował kult Robaków bombą w analizatorze żywności i zaeskalował konflikt Kidironowie - Robaki, wszystką chwałą (i konsekwencjami) obarczył kuzyna Tymona. Zrobił ostry sabotaż szyn w wyniku którego sporo rannych i zniszczeń -> wszystko na Tymona a oficjalnie Robaki. | 0092-08-15 - 0092-08-27 |
| 220914-dziewczynka-trianai          | po przyjęciu zadania odnalezienia dwójki zaginionych nastolatków przejął dowodzenie, rozstrzelał piętnastolatkę Trianai (z trudem się powstrzymał by jej nie zabić) i odniósł do Kidironów z Ardillą. Wykazał się absolutną skutecznością i zimną bezwzględnością. | 0092-09-10 - 0092-09-11 |
| 221006-ona-chce-dziecko-eustachego  | zarówno wujek jak i Rafał Kidiron próbują wygrać jego duszę. Dowodził Infernią i się z nią zintegrował; pomógł crawlerowi Exerinn, oddał geny Avie i uratował kilka osób używając bunker-bustera z EMP i wbił się do miejsca gdzie Farighanowie ufortyfikowali się przed atakiem agentów Exerinn. | 0092-09-20 - 0092-09-24 |
| 230104-to-co-zostalo-po-burzy       | gatlingiem uratował Rufusa przed dużym krystalnikiem; potem wzmocnił Skorpiona częściami Inferni magią i dogadał się z Kidironem by mogli jechać w burzę. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | użył zaklęcia by reanimować zniszczony Lancer Rufusa. Dzięki temu zniszczył fizyczną formę whispraitha i samego Lancera (resztką woli Rufusa). Technicznie przejął dowodzenie nad Seiren. Opracował plan - przeczekują burzę, by whispraith się rozproszył. | 0092-10-29 - 0092-11-02 |
| 220720-infernia-taksowka-dla-lycoris | PAST: nakablował na Ardillę za co opieprzył go wujek (never betray a FAMILY!) po czym próbował się wkraść do laboratorium i złapał go niekompetentny zwykle Kamil. ACTUAL: 20 lat; doszedł do obecności Plagi Trianai na CES Purdont i współpracując z Zespołem przebił się do centrum komunikacyjnego usuwając osy a nawet ainshkera. Przełamywał komputery używając też do pomocy magii - dzięki temu sabotaż Plagi Trianai nie dała rady odciąć bazy. Dowodzi, jako następny po Bartłomieju w rodzie Korkoran. | 0093-01-20 - 0093-01-22 |
| 220817-osy-w-ces-purdont            | przejął kontrolę nad obroną CES Purdont, magią zakłócił komunikację napastników-komandosów, dowiedział się czemu atakują i kto. Utrzymał CES Purdont aż Infernia przybyła na ratunek. | 0093-01-23 - 0093-01-24 |
| 230201-wylaczone-generatory-memoriam-inferni | skuszony przez Infernię, wyłączył generatory Memoriam i Infernia oplotła jego duszę. Okazuje się też, że ma kumpli którzy z nim piją by "skapnęły" mu dziewczyny które chcą go podrywać. Gdy Infernia zaczęła odpalać rakiety, musiał udawać że to ON strzelał. Skupił się na ratowaniu okolicznych arkologii by zniszczyć amalgamoid; mniej ważni noktianie niż zniszczenie NAWET kosztem tego że musi oszukać Ardillę i innych używając Inferni. Potem z noktianami przesterował (sabotował) reaktor fuzyjny i zniszczył Coruscatis. Ale jednak wybrał Ardillę; nie oddał noktian Kidironom. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | Rafał Kidiron broni jego decyzji przed wujkiem. Poszedł na randkę z Kalią (niechętnie, bo Kidiron mu kazał). Powiedział Kalii, że lubi wybuchy i że w sumie ma coś w arkologii na czym mu zależy. Trochę z nią flirtuje, ale tak po swojemu. Zdobył od Kalii co ona wie o Inferni. Gdy poczuł emanację magiczną z Ambasadorki, zostawił Kalię samą przy Ambasadorce i pobiegł w kierunku Inferni. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | ostrzegł Kalię o problemie, zdobył broń, aktywował alarm na Inferni i przeprowadził okrążenie budynku Ambasadorki. Przyczynił się do rozwiązania konfliktu poprzez pomysł obniżenia poziomu tlenu oraz mediację między Kidironem i Ardillą. | 0093-02-22 - 0093-02-23 |
| 230315-bardzo-nieudane-porwanie-inferni | dowodził Infernią by ratować CES Mineralis; próbował się połączyć z koordynatorem Trianai by zwabić wszystkie w pułapkę, ale zarezonował z wrogim magiem i eksplodowało. Potem był przesłuchiwany, wypalili mu świnki na pośladkach, ale gdy Memoriam padły to przejął kontrolę nad sytuacją i przechwycił piratów. Dołączył ich do załogi Inferni. | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | użył Inferni by przechwycić Skorpiona Szczepana (rakietą go wpakował w dół); potem załatwił override danych medycznych od Kidirona. Wypytał Wojtka o spiski piratów i ogólnie 100% łyknął, że sprawa z pamiętnikiem Eweliny to poważny spisek przeciwko arkologii. | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | bukiet czerwonych róż od Kalii okazał się być dyskretną wiadomością, którą przeoczył. Z wujkiem przegadał stan Inferni. Gdy był zamach na Kidirona, połączył się z Kalią i ją uspokoił; Kalia prawidłowo podnosi morale i zostaje porwana przez Infiltratora. Jego magia dała skrzydło Lancerów z Inferni; tak uzbrojony, leci za Infiltratorem. | 0093-03-22 - 0093-03-24 |
| 200122-kiepski-altruistyczny-terroryzm | inżynier zniszczenia; wpierw uszkodził Persefonę a potem doprowadził do tego, że Persefona 2 myślała, że zniszczyła napastników. | 0110-09-15 - 0110-09-17 |
| 200408-o-psach-i-krysztalach        | odkrystalił Sebastiana i innych przechwyconych członków Leotis; potem sprawił że Infernia - QShip - była w stanie pokonać ciężki statek jakim był Leotis. | 0110-09-29 - 0110-10-04 |
| 200415-sabotaz-miecza-swiatla       | sabotował skutecznie silniki Miecza Światła. Zbyt skutecznie - doprowadził do kaskadowych zniszczeń i sabotażu poczciwego statku ratunkowego. | 0110-10-08 - 0110-10-10 |
| 200624-ratujmy-castigator           | miał sabotować Castigator przed arystokratami, a faktycznie niszczył go stopniowo by powstrzymać anomaliczną Rozalię z Alkarisa. Wielki sukces - uratował Castigator wybuchami. | 0110-10-15 - 0110-10-19 |
| 200708-problematyczna-elena         | wszedł z Eleną w pętlę kłótni o kompetencje i prawie wysadził Infernię by jej pokazać, że jest lepszy. Potem wykazał się rozpaczliwymi umiejętnościami naprawy statku. | 0110-10-29 - 0110-11-03 |
| 200715-sabotaz-netrahiny            | sabotował Netrahinę, by odwrócić uwagę Persefony; musiał też wycofać się z pojedynku z Eleną i zdjął gniew Eleny z siebie przekierowując go na Ariannę. | 0110-11-06 - 0110-11-09 |
| 200722-wielki-kosmiczny-romans      | wplątany w romans którego nie miał, groziło mu nawet że ma znaleźć dziewczynę na szybko. Wyplątał Elenę ze strasznego pojedynku używając Leony jako straszaka. | 0110-11-12 - 0110-11-15 |
| 200729-nocna-krypta-i-emulatorka    | dużo wysadzał - kierunkowe miny by dostać się na Kryptę, fortyfikacja korytarza przed ghulami Esuriit, a potem lepiszcze Ariannie i Klaudii by wyszły ze stupora ;-). | 0110-11-16 - 0110-11-22 |
| 200819-sekrety-orbitera-historia-prawdziwa | robił żarliwe przemowy przekonujące Leonę oraz Elenę - oraz wraz z Martynem, przebudował Leonę by mogła pokonać Tadeusza. Plus, załatwił wzmocnienie Inferni w lepsze czujniki dzięki hazardowi. | 0110-11-26 - 0110-12-04 |
| 200826-nienawisc-do-swin            | udowodnił, że nie umie zmusić Martyna i Klaudii do gadania jak nie chcą - ale robi świetną pirotechnikę Ariannie, by ta miała dobre wejście. | 0110-12-08 - 0110-12-14 |
| 200909-arystokratka-w-ladowni-na-swinie | zrobił epicki konkurs która arystokratka może z nim iść na kawę by wyciągnąć Martyna, po czym... zwiał na Tucznika. Zestrzelił większość rakiet nojrepów używając flaka z działek Tucznika (które normalnie nie miały flaka, ale od czego jest magiem?). Paradoksem uzbroił anomalię terroru. | 0110-12-15 - 0110-12-20 |
| 200916-smierc-raju                  | wysadził silniki Tucznika by odwrócić uwagę Izy od Anastazji; potem zdekombinował Tucznikowi osłony anty-glukszwajnowe do kierowania nojrepami. | 0110-12-21 - 0110-12-23 |
| 200923-magiczna-burza-w-raju        | robi pranka Elenie by ubrudzić Anastazję (ubrudził Elenę). Odpowiednio niszczy i fortyfikuje fragmenty Raju, by przetrwać burzę. Znalazł dowód sabotażu. | 0110-12-24 - 0110-12-28 |
| 201014-krystaliczny-gniew-elizy     | rozbroił kaskadowo bomby, po czym gdy Elena wysłała SOS kupił czas Ariannie - robiąc carpet bombing terenu z Eleną. Poranił ją, ale uratował przed śmiercią. Też strzelił Dariuszowi w nogę (odstrzeliwując ją) za cierpienie Eleny. | 0111-01-02 - 0111-01-05 |
| 201021-noktianie-rodu-arlacz        | poderwał Dianę Arłacz, która wygrała konkurs; zbałamucił ją i uciekła z nim na Infernię. Wysadził altankę Roberta Arłacza by pokazać Dianie co umie. | 0111-01-07 - 0111-01-10 |
| 201104-sabotaz-swini                | opanowany żądzą bycia paladynem przypadkowo uratował załogę Wieprza przed sabotażem miragenta; potem używając Diany jako baterii zdekombinował anomalię i zmusił ją do odlotu. | 0111-01-13 - 0111-01-16 |
| 201118-anastazja-bohaterka          | wpierw sponiewierał Jamniczka w walce na szpady z nieuczciwą pomocą, a potem rozkochał w sobie pół świata Paradoksem Arianny XD. | 0111-01-22 - 0111-01-25 |
| 201210-pocalunek-aspirii            | przebudował (konstrukcja) Infernię by wyglądała jak Blask Aurum. Potem - zestrzelił Pocałunek Aspirii. | 0111-01-26 - 0111-01-29 |
| 201216-krypta-i-wyjec               | mistrz planu inscenizacji "Krypta porwała Anastazję", taktyk. Ogłuszył Martyna, by ten nie wiedział o tym że zostawiają Anastazję. Dużo wybuchów ;-). | 0111-01-29 - 0111-01-31 |
| 210707-po-drugiej-stronie-bramy     | Absolutny MVP; utrzymał Infernię przed zmiażdżeniem pod wodą i przekształcił pintkę w "randkę" by Lewiatan się nie zaprzyjaźnił z Infernią za bardzo. | 0111-03-13 - 0111-03-15 |
| 210714-baza-zona-tres               | odnalazł do których wzmacniaczy łączy się Klaudia, zamaskował w komputerach Zony Tres Infernię jako Alivię Nocturnę oraz nic nie wysadził - poza lodem planety pod wodą której się znajdują. | 0111-03-16 - 0111-03-18 |
| 210721-pierwsza-bia-mag             | naprawia Zonę Tres - wpierw używając narzędzi a potem wspomagając się magią. Potem pomaga Martynowi rozdzielić Klaudię i BIA. | 0111-03-19 - 0111-03-21 |
| 210728-w-cieniu-nocnej-krypty       | Pozyskując złom w Nierzeczywistości zapewnił efemerydę Diany Inferni; też wyrwał Kryptę z koszmarnego snu, by Arianna mogła wrócić do domu. | 0111-03-22 - 0111-04-08 |
| 210804-infernia-jest-nasza          | wkradł się na Infernię by nikt mu jej nie zabrał, po czym przejął nad nią kontrolę i się z nią magicznie neurosprzęgł, po czym złapał w pułapkę kultystę magii - eternianina tien Żaranda. | 0111-04-23 - 0111-04-26 |
| 201111-mychainee-na-netrahinie      | zaimponował Dianie selfie z wybuchami; uszkodził Netrahinę bardziej niż musiał. Zaplanował jak wbić na Netrahinę. Mistrz artylerzysta; wpakował Saabar na torpedę abordażową. | 0111-05-07 - 0111-05-10 |
| 201230-pulapka-z-anastazji          | przesterował systemy Inferni, by zdążyć uratować Zgubę Tytanów przed AK Rodivas. Potem próbował "uratować Anastazję", ale zrobił za duże zniszczenia bo się popisywał. | 0111-05-21 - 0111-05-22 |
| 210127-porwanie-anastazji-z-odkupienia | sabotował Infernię by odwrócić uwagę Eleny, skłócał Elenę i Dianę by nie wiedziały o akcji odbijania Anastazji a potem magią (wybuchów) odpychał Dariusza by kupić Klaudii czas. | 0111-05-24 - 0111-05-25 |
| 210106-sos-z-haremu                 | rozkochał w sobie Rozalię, po czym skłonił ją do pójścia z nią na Infernię. Pozwolił na to, by jego reputacja ucierpiała ("pornmaster") by ratować Julię Aktenir przed kralothem. | 0111-05-28 - 0111-05-29 |
| 210120-sympozjum-zniszczenia        | nakłonił Dianę do zabawy Esuriit i przekroczenia granicy - po raz pierwszy ever. Potem wyciągnął Dianę z obsesji Esuriit. I z Klaudią zrobił Emiter Plagi Nienawiści. | 0111-06-03 - 0111-06-07 |
| 210218-infernia-jako-goldarion      | udaje dowódcę Goldariona; przebudował Infernię w Czarny Grom a Czarny Grom w Goldariona. Zdobył szacun Tomasza Sowińskiego, który go uważa za swego chłopa. | 0111-07-19 - 0111-08-03 |
| 200429-porwanie-cywila-z-kokitii    | stał lojalnie za Arianną w konflikcie z Medeą oraz mistrzowsko skołował Kokitię strzałami artylerii - nigdy nie wiedzieli, że ktokolwiek ich zaatakował. | 0111-08-03 - 0111-08-08 |
| 210317-arianna-podbija-asimear      | udając Aleksandra Leszerta irytował Martynę (kapitan Płetwala), by utrzymać konspirację - nie są Infernią, są Goldarionem. Polubił bycie kapitanem. | 0111-08-20 - 0111-09-04 |
| 210414-dekralotyzacja-asimear       | taktycznie dowodzi operacją 'dekralotyzacja Asimear', używając sił porządkowych Lazarin i sił Inferni. Potem desantuje Elenę w Eidolonie na Płetwala, z dywersją odwracającą uwagę Morrigan. | 0111-09-05 - 0111-09-14 |
| 210421-znudzona-zaloga-inferni      | reanimując Tirakala zreanimował Morrigan. Sabotował Infernię i przez przypadek dał Elenie znać że mu na niej zależy. Gdy się z nim skonfrontowała, przypadkowo ją odstraszył (bo "niegodne z dowódcą"). | 0111-09-18 - 0111-09-21 |
| 210428-infekcja-serenit             | rozpaczliwie przekierował ogień Inferni (Morrigan) na resztki Goldariona, sabotował Infernię by odciąć Morrigan i osłaniał artyleryjsko Klaudię i Elenę jak uciekały z Falołamacza przed Odłamkiem Serenit. | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | zrobił reaktywny pancerz na Inferni, ostrzelał Falołamacz i wprowadził tam Entropika po czym odstrzelił ładownię gdzie są ludzie. Inżynier-artylerzysta w pełnej mocy. | 0111-09-25 - 0111-10-04 |
| 210519-osiemnascie-oczu             | operator roju robotów (który sam skonstruował) celem dywersji, mapowania drogi i unieszkodliwiania. Twórca planów taktycznych. | 0111-10-09 - 0111-10-20 |
| 210526-morderstwo-na-inferni        | chciał leniwie zrzucić poszukiwania Tala na wszystkich innych, ale jak Tal zginął to wyciągnął kluczowe informacje rozmawiając ze swoimi technikami. | 0111-10-26 - 0111-11-01 |
| 210609-sekrety-kariatydy            | by zdobyć pieniądze i środki na wyprawę do Anomalii Kolapsu przy marnych środkach z Aurum rozpalił arystokratki opowieścią o Kariatydzie. | 0111-11-05 - 0111-11-08 |
| 210818-siostrzenica-morlana         | fabrykuje sygnaturę statku koloidowego, po czym magią ratuje Ofelię przed Eidolonami (eksplozja kierunkowa wywala ich ze statku). | 0111-11-15 - 0111-11-19 |
| 210616-nieudana-infiltracja-inferni | dobry glina i zły glina jednocześnie - zdecentrował i ogłupił Flawię, po czym pokazał jej, że Infernia ma świetny system zabezpieczeń przed infiltracją. Szacun! | 0111-11-22 - 0111-11-27 |
| 210825-uszkodzona-brama-eteryczna   | świadek tego jak "Diana" i Elena o niego walczyły; autor pomysłu, by Infernię zamaskować jako noktiański statek by efemerydy Infernię wspierały. Po raz pierwszy UNIKAŁ walki i modulował sygnaturę ekranowania tak, by efemerydy nie zbliżały się do Inferni. | 0111-11-30 - 0111-12-03 |
| 210901-stabilizacja-bramy-eterycznej | mistrzowski plan i inżynieria by ochronić pinasy przed efemerydami i naprawić bezproblemowo Bramę Kariańską; Elena pod Esuriit chce założyć mu pas cnoty. | 0111-12-05 - 0111-12-08 |
| 210929-grupa-ekspedycyjna-kellert   | sterował Piraniami używając "torped z sonarem", robiąc przejście dla Inferni. Potem manipulował lokalizacją fragmentu statku z Vigilusem by Martyn mógł ratować ludzi. | 0112-01-07 - 0112-01-10 |
| 211013-szara-nawalnica              | refitował Infernię by móc wysadzić torpedę Rziezy, znalazł sposób jak omijać skutecznie Piranie, sondami sejsmicznymi znalazł bazę ludzi w Kalarfam i użył torpedy anihilacyjnej by zabić "smoka". | 0112-01-12 - 0112-01-17 |
| 211020-kurczakownia                 | pod wpływem Esuriit zmasakrował z komandosami grupę lokalsów na wrakowisku. Potem na zimno opracował Kurczakownię - oddzielanie mechanicznego mięsa. Gdy Arianna była dominowana przez Zbawiciela-Niszczyciela, strzelił weń z Działa Rozpaczy, uszkadzając je na zawsze. | 0112-01-18 - 0112-01-20 |
| 211110-romans-dzieki-esuriit        | wydostał Leonę ze szpitala na jej prośbę. Potem jednak przekierował się na Elenę, która uciekła i potrzebowała pomocy. Próbował ją uspokoić i wyjaśnić, że będzie dobrze. Faktycznie, uspokoił ją trochę. Pokazał że jej ufa. Acz się napromieniował (co jest nieprzyjemne). | 0112-02-07 - 0112-02-08 |
| 211117-porwany-trismegistos         | udawał kapitana Tivra, opracował kilka planów wejścia na Trismegistos i teatralnie przekonał wszystkich, że Serenit to tajna broń Orbitera (lub jego?). | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | model i twarz kampanii "dołącz do Inferni". Opracował plan przechwycenia Nereidy, ale pod wpływem gniewu złamał plan i zestrzelił okrutnie Nereidę. Zintegrowany z Infernią poranił "nowych" członków załogi. | 0112-02-14 - 0112-02-18 |
| 211208-o-krok-za-daleko             | ixiońsko zintegrowany z Infernią; zdalnie się z nią łączy. Zdominował Infernię i pokazał jej, gdzie jej miejsce, acz kosztem 46 osób z załogi. | 0112-02-19 - 0112-02-20 |
| 211215-sklejanie-inferni-do-kupy    | nie udało mu się ustabilizować załogi, ALE znalazł ixiońskiego potwora analizując systemy Stoczni (wpadając w pułapkę Kasandry/Leony). Gdy był związany, przekonał Leonę, że kontroluje Dianę. Więcej - zarejestrował dla potomności, że FAKTYCZNIE Diana nic głupiego nie zrobiła gdy był torturowany. | 0112-02-21 - 0112-02-23 |
| 211222-kult-saitaera-w-neotik       | przekonał Dianę, że Elena musi być uziemiona w Inferni i sprawił, że Diana CHCE współpracować z Eustachym. Bardzo zajmował się Dianą by ją stabilizować i usprawnić. | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | zintegrował się z zamrożoną Infernią (payload memetyczny) by uratować Dianę przed korupcją Saitaera. Ciężko ranny, ale udało mu się. Ratował życie WSZYSTKICH ryzykując swoim oraz Infernią. Opracował genialny plan, ale naprzeciw niemu stanął osobiście Saitaer. | 0112-02-28 - 0112-03-04 |
| 220126-keldan-voss-kolonia-saitaera | przesłuchał Szczepana Kaltabena; przypadkiem Annika uznała go za dowódcę sił Orbitera i ufnie oddała się pod jego dowodzenie. | 0112-03-19 - 0112-03-22 |
| 220202-sekrety-keldan-voss          | Ewakuuje Annikę i pallidan z Keldan Voss na orbitę, do Kastora i jednostek wspierających. Podczas ewakuacji zaatakował mgłę i wyciągnął jednego z Kroczących do badań dla Marii. Naprawił generator osłaniany przez Elenę. | 0112-03-24 - 0112-03-26 |
| 220216-polityka-rujnuje-pallide-voss | zaprzyjaźnił ze sobą Annikę i poznał jej historię i co ona myśli. Infiltruje Pallidę Voss, sabotował generatory Memoriam i niestety Paradoks gdy dotarł do "sali medycznej" gdzie krojono drakolitów spowodował Kaskadę Paradoksu. Konieczność ewakuacji statku... cholerni kultyści Saitaera. | 0112-03-27 - 0112-03-29 |
| 220223-stabilizacja-keldan-voss     | na szybko montuje w Keldan Voss generatory Memoriam, by móc ewakuować nadmiar pallidan z Kastora. Po ostrzeżeniu Eleny zrobił killzone i ochronił pallidan podnosząc im morale. | 0112-03-30 - 0112-04-02 |
| 220309-upadek-eleny                 | podnosi morale Anniki - ma próbować. Zmusił ją do wzięcia odpowiedzialności za Keldan Voss i jej przyszłość. Annika za tym poszła. Pomógł dziewczynie. | 0112-04-03 - 0112-04-09 |
| 220316-potwor-czy-choroba-na-etaur  | uniknął aresztowania przez siły na EtAur, podzielił siły i szuka potwora? Choroby? Aha, uniknął oświadczyn Eleny. | 0112-04-24 - 0112-04-26 |
| 220330-etaur-i-przyneta-na-krypte   | szybko zdążył uratować Raoula i Marię przed "potworem" - z Raoulem odgonił stwora. | 0112-04-27 - 0112-04-30 |
| 220610-ratujemy-porywaczy-eleny     | skutecznie zsabotował statek Aureliona sprawiając, że popanikowali i zaczęli się ewakuować. Potem uwolnił Martyna i uspokoił budzącą się Elenę. | 0112-07-11 - 0112-07-13 |
| 220706-etaur-dziwnie-wazny-wezel    | gdy Maria przyszła doń z konspiracją Termia - Aurelion, poszedł do Termii bezpośrednio. Powiedział jej, że nie pójdzie za jej planem; lepiej znaleźć koloidową bazę Aureliona używając przemytników i jego środków z bazy EtAur. Zbudował sojusz Infernia - EtAur i gra pod siebie i pod Infernię. | 0112-07-15 - 0112-07-17 |
| 220615-lewiatan-przy-seibert        | organizuje Dni Otwarte na Inferni, znajduje sposób jak przechwycić energię z Lewiatana używając kontrolowanego zwarcia i zlokalizował koloidową korwetę pyłem magnetycznym i zderzaniem się Tivrem. | 0112-07-23 - 0112-07-25 |
| 220622-lewiatan-za-pandore          | kapitan Inferni, przejął dowodzenie nad Olą Szerszeń i postrzelał torpedami anihilacyjnymi do Lewiatana. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220914-dziewczynka-trianai          | opinia niesamowicie bezwzględnego. Nieważne co - jest zadanie to wykona. Wzbudza strach oraz szacunek w Arkologii Nativis. | 0092-09-11
| 221006-ona-chce-dziecko-eustachego  | oddał swoje geny Avie z Exerinna. Czyli najpewniej będzie miał dziecko :D. | 0092-09-24
| 230201-wylaczone-generatory-memoriam-inferni | Infernia (anomalia Interis) ma macki w jego głowie. Tylko on może ją pilotować. | 0093-02-12
| 230201-wylaczone-generatory-memoriam-inferni | zbrodnie "wojenne"; Infernia strzeliła rakietami i zginęło kilkunastu noktian. Kidiron osłania, ale to jest czyn jakiego Wujek i wielu nie wybaczy. | 0093-02-12
| 230201-wylaczone-generatory-memoriam-inferni | oszukał Ardillę i resztę Zespołu Infernią i udawał, że Amalgamoid nie uczestniczył w operacji. To kiedyś wyjdzie; będzie rift z jego rodem, gdy opuści Nativis. | 0093-02-12
| 230208-pierwsza-randka-eustachego   | w arkologii Nativis najbardziej zależy mu w sumie na tym że jego ród i wujek   jest podziwiany i szanowany. Że jest ostoją bezpieczeństwa dla arkologii. Też sama arkologia i jej działanie. | 0093-02-21
| 200624-ratujmy-castigator           | zaimponował Leszkowi Kurzminowi swoim wyczuciem sytuacji i tym, że kapitan im pomoże. A potem - pokonując Rozalię / Azalię d'Alkaris skażoną ixionem. | 0110-10-19
| 200715-sabotaz-netrahiny            | hańba; musiał z podkulonym ogonem wycofać się z pojedynku z Eleną i ją przeprosić. Elena wygrała moralnie. | 0110-11-09
| 200722-wielki-kosmiczny-romans      | w oczach Olgierda Drongona jest platonicznie zakochany w Ariannie typu 'senpai notice me'. Olgierd patrzy na to z litościwym pobłażaniem i więcej mu wybacza. | 0110-11-15
| 200722-wielki-kosmiczny-romans      | dostał opinię "fetyszysty z gminu" wśród arystokracji Aurum i Eterni - podrywa tylko szlachcianki, nie interesują go inne kobiety. Co gorsza, jest bardzo skuteczny w podrywaniu. | 0110-11-15
| 200909-arystokratka-w-ladowni-na-swinie | jego renoma kobieciarza i miłośnika arystokratek wymaga sprawdzenia ;-). | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | rywale - nie jest taki dobry. Co więcej, podrywa dziewczyny i jest wobec nich bucem. Negatywne konsekwencje sławy reality show ;-). | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | niechętnie widziany na Orbiterze chwilowo, acz pożądany przez fanki. | 0110-12-20
| 201021-noktianie-rodu-arlacz        | potężne uczucia opiekuńcze wobec Diany Arłacz. ONA MUSI MIEĆ DOBRZE. | 0111-01-10
| 201118-anastazja-bohaterka          | ma stały bonus +3 w kontaktach do wszystkich kobiet. | 0111-01-25
| 201118-anastazja-bohaterka          | zakochuje się w nim Julia, ukochana Stefana Jamniczka. Ma też przechlapane u stronników Jamniczka. | 0111-01-25
| 210728-w-cieniu-nocnej-krypty       | Blizna "Krwawy". Gdy sytuacja jest zła, nie patrzy na nic i nikogo, ratuje swoją skórę - robi się ultrapragmatyczny i nie przejmuje się komu robi krzywdę. | 0111-04-08
| 210728-w-cieniu-nocnej-krypty       | Jest powiązany historycznie z Infernią. Gdy Infernia anomalizowała, to on i jego zespół/rodzina doprowadzili Infernię do działania. A Infernia wymaga okresowych uszkodzeń. | 0111-04-08
| 210804-infernia-jest-nasza          | ma możliwość magią "neurosprzężenia" z Infernią przez anomalny rdzeń Inferni. | 0111-04-26
| 201230-pulapka-z-anastazji          | pod wrażeniem umiejętności technicznych Eleny - jej pilotażu, jej ognistego profesjonalizmu, jej skłonności do ryzyka. | 0111-05-22
| 210106-sos-z-haremu                 | wabi dziewczyny na orgię, potem nic z tego nie ma. Wtf. Z jednej strony reputacja skandalisty, z drugiej reputacja "nie kończy". Ale skojarzenie Eustachy - orgia zostaje. | 0111-05-29
| 210106-sos-z-haremu                 | zakochują się w nim beznadziejnie Rozalia Teirik i Julia Aktenir. Ale Eustachy ostro podpada Elenie. | 0111-05-29
| 210106-sos-z-haremu                 | wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy. | 0111-05-29
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-06-07
| 200429-porwanie-cywila-z-kokitii    | łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera. | 0111-08-08
| 210428-infekcja-serenit             | bonus do niszczenia Inferni. Nie "pozorowanego" a "faktycznego". Eustachy potrafi niszczyć / sabotować Infernię jak nikt inny. | 0111-09-25
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-10-20
| 211110-romans-dzieki-esuriit        | plotki, że zadaje się z Leoną na Inferni. Że ona jest jego stałą partnerką. Kinky pair of doom. | 0112-02-08
| 211124-prototypowa-nereida-natalii  | jest solidnym bohaterem. Twardy i nie warto zaczepiać. Wszystko dzięki kampanii Izy. | 0112-02-18
| 211208-o-krok-za-daleko             | zdalnie łączy się z Infernią; potrafi się z nią integrować i mieć sentisprzężenie, nawet zdalnie. Infernia się go słucha. I go lubi. POTĘŻNY BONUS przy działaniu z Infernią gdy sentisprzężony. | 0112-02-20
| 211222-kult-saitaera-w-neotik       | aspekt Nihilusa (Vigilus - Nihilus). Załoga i Kult jest przekonana, że jest Aspektem Niszczyciela. | 0112-02-25
| 220105-to-nie-pulapka-na-nereide    | ciężko ranny, sprzężony z Infernią. Przeżył tylko dzięki niesamowitej skuteczności i bezwzględności Marii Naavas. | 0112-03-04
| 220330-etaur-i-przyneta-na-krypte   | NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają. | 0112-04-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 63 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 59 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 42 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 29 | ((200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 24 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny)) |
| OO Infernia          | 20 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Maria Naavas         | 12 | ((210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Ardilla Korkoran     | 11 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Kamil Lyraczek       | 11 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210929-grupa-ekspedycyjna-kellert)) |
| Izabela Zarantel     | 10 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Rafał Kidiron        | 8 | ((220720-infernia-taksowka-dla-lycoris; 230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Diana Arłacz         | 7 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| AK Nocna Krypta      | 6 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Aleksandra Termia    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Bartłomiej Korkoran  | 6 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Kalia Awiter         | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Roland Sowiński      | 6 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Celina Lertys        | 5 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Diana d'Infernia     | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Janus Krzak          | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Otto Azgorn          | 5 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia)) |
| Raoul Lavanis        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Damian Orion         | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Flawia Blakenbauer   | 4 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia)) |
| Jan Lertys           | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Marian Tosen         | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Olgierd Drongon      | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Tivr              | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Tadeusz Ursus        | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Stanisław Uczantor   | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Tymon Korkoran       | 3 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Castigator        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Rzieza d'K1      | 1 | ((211013-szara-nawalnica)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |
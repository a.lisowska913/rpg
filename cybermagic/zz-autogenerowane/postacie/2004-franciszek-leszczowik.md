---
categories: profile
factions: 
owner: public
title: Franciszek Leszczowik
---

# {{ page.title }}


# Read: 

## Kim jest

### Paradoksalny Koncept

Zafascynowany Saitaerem artykonstruktor, chcący przekształcać ciało i technologię. Pragnie znaleźć homo superior, ale nie posuwa się do zakazanych rytuałów. Ma ogromną władzę której nadużywa, ale pragnie odbudowy pozycji swego rodu. Szuka prawdziwej wiedzy, lecz zwalcza dziennikarzy którzy zniszczyli jego ród. Boi się potworów i kosmosu - dlatego pragnie ewolucji, która sprawi że człowiek sobie poradzi.

### Motto

"Perfekcja jest nieunikniona a magitech jest do niej środkiem."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Ekspert od łączenia ciała z artefaktami, technologią lub bioformami.
* ATUT: Doskonały taktyk - jeśli się przygotowuje i ma informacje o drugiej stronie, zaadaptuje swoją "stronę" by mieć przewagę.
* SŁABA: Boi się straszliwych Skażeńców, Kosmosu czy Enklaw. Stąd taki nacisk na homo superior. A gdy się boi, robi błędy.
* SŁABA: Pod bardzo silną presją gdy musi się adaptować nie wie co robić. Nie umie wyjść z pozycji "przegrywam" na zwycięską.

### O co walczy (3)

* ZA: Odzyskać sławę, chwałę i miłość do swojego rodu który upadł przez skandale zakazanych rytuałów.
* ZA: Odkryć model homo superior, czy magiczny czy technologiczny, który przesunie magów do roli bogów.
* VS: Nie stracić władzy, pozycji, potęgi czy środków. To jest ostatnie co mu pozostało.
* VS: Zwalczać wszelkie plotki... czy dziennikarzy. Na to samo wychodzi.

### Znaczące Czyny (3)

* Fragmenty majątku które kontroluje są bardzo efektywne - zaadaptował ludzi tam pracujących.
* Po otrzymaniu środków Lemurczaka, stworzył dlań biotechnoformy którymi Lemurczak mógł osiągnąć swą pozycję.
* Stworzył biomechanizm ulojalniania człowieka, przez dystrybucję bólu i przyjemności. Ów mechanizm działał.

### Kluczowe Zasoby (3)

* COŚ: Liczne bioformy i technoformy które zawsze mu towarzyszą, dostosowane do jego celów.
* KTOŚ: Protektorzy dużo wyżej w Aurum, którzy wyciągną go z tarapatów - za cenę.
* WIEM: Regularnie sprawdza wszystkie informacje o każdym miejscu do którego dociera by być encyklopedią wiedzy o terenie.
* OPINIA: "Szalony naukowiec" bez szczególnej moralności, który jednak nigdy nie pójdzie w mroczne rytuały.

## Inne

### Wygląd


### Coś Więcej



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200414-arystokraci-na-trzesawisku   | przerobił sobie Sabinę w piękną seksbombę, potem ciężko straumatyzowany przez potwory na Trzęsawisku i chemikalia Pięknotki. | 0110-07-31 - 0110-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ignacy Myrczek       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Mariusz Trzewń       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Pięknotka Diakon     | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Sabina Kazitan       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Strażniczka Alair    | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Talia Aegis          | 1 | ((200414-arystokraci-na-trzesawisku)) |
---
categories: profile
factions: 
owner: public
title: OE Falołamacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210428-infekcja-serenit             | wpakował się na Serenit. Serenit go zaczął infekować, zmieniając go w "odłamek Serenit". Falołamacz jest stracony, ale da się jeszcze uratować załogę - i to próbuje zrobić Infernia. | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | w pełni staje się Odłamkiem Serenit; KIA. Uszkodzony przez samobójczą pintkę #2, odechciało mu się walczyć z Infernią i ruszył w kierunku na Serenit, gdzie jego miejsce... | 0111-09-25 - 0111-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Arianna Verlen       | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Elena Verlen         | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Eustachy Korkoran    | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Klaudia Stryk        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Martyn Hiwasser      | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Infernia          | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Roland Sowiński      | 1 | ((210512-ewakuacja-z-serenit)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
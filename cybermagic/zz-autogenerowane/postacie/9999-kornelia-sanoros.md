---
categories: profile
factions: 
owner: public
title: Kornelia Sanoros
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | "Dobra mama" od załadunku i sprzedaży dóbr; bliska przyjaciółka martwego kapitana. Uspokaja załogę nawet kosztem swej reputacji. Została P.O. kapitana. | 0108-07-20 - 0108-07-26 |
| 220427-dziwne-strachy-w-morzu-ulud  | wymanewrowana politycznie przez Berdysza i emocjonalnie przez Maję, praktycznie oddała Mai dowodzenie. Sytuacja ją przerasta. ALE - zapewni, żeby piraci zostali uratowani bo to właściwa rzecz do zrobienia. Skutecznie uspokaja załogę i dba o to, by wszystko działało prawidłowo - na swoją zgubę polityczną. | 0108-07-29 - 0108-07-31 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | nie wytrzymała napięcia; gdy "kalcynit" spadł z sufitu i została ranna przez Maję to się poddała i została Zainfekowana. Po krótkiej walce na Wężu wsadzili jej neuroobrożę by była milutka. BROKEN. | 0108-08-02 - 0108-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozpruwacz   | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
---
categories: profile
factions: 
owner: public
title: Szczepan Myksza
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200715-sabotaz-netrahiny            | oficer naukowy Netrahiny, silnie powiązany z frakcją purystów i ekspansjonistów. Nie zauważył, że Persefona go rozgrywa przeciw innym. | 0110-11-06 - 0110-11-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200715-sabotaz-netrahiny)) |
| Elena Verlen         | 1 | ((200715-sabotaz-netrahiny)) |
| Eustachy Korkoran    | 1 | ((200715-sabotaz-netrahiny)) |
| Klaudia Stryk        | 1 | ((200715-sabotaz-netrahiny)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| OO Netrahina         | 1 | ((200715-sabotaz-netrahiny)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Rufus Komczirp       | 1 | ((200715-sabotaz-netrahiny)) |
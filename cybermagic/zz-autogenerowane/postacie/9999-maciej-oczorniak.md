---
categories: profile
factions: 
owner: public
title: Maciej Oczorniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201025-kraloth-w-parku-janor        | zniewolony przez kralotha młody, idealistyczny terminus; porwany przez Pięknotkę i naprawiony przez Lucjusza. Wezwał SOS do Pustogoru mówiąc im o kralocie. | 0110-10-25 - 0110-10-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201025-kraloth-w-parku-janor        | strasznie psychicznie uzależniony od kralotha; zdewastowany tym co zrobił i ogólnie biedny - złamał wszystkie swoje ideały. Pomoc psychologa w Pustogorze. | 0110-10-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Lucjusz Blakenbauer  | 1 | ((201025-kraloth-w-parku-janor)) |
| Minerwa Metalia      | 1 | ((201025-kraloth-w-parku-janor)) |
| Pięknotka Diakon     | 1 | ((201025-kraloth-w-parku-janor)) |
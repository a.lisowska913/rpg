---
categories: profile
factions: 
owner: public
title: Nadia Sekernik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | administratorka CES Mineralis; przechwycona przez piratów ściągnęła Infernię w pułapkę. Zginęła w eksplozji Paradoksu Eustachego. KIA. | 0093-03-06 - 0093-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Eustachy Korkoran    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| OO Infernia          | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rafał Kidiron        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Ralf Tapszecz        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| SAN Szare Ostrze     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Wojciech Grzebawron  | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
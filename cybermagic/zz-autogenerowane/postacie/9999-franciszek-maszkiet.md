---
categories: profile
factions: 
owner: public
title: Franciszek Maszkiet
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210804-infernia-jest-nasza          | oficer Orbitera na K1 zajmujący się provisioningiem. Poinformował Ariannę z zadowoleniem, że ma dla niej nową jednostkę w miejscu Inferni decyzją Admiralicji. Ofc Arianna zaczęła działania odzyskujące Infernię. | 0111-04-23 - 0111-04-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Termia    | 1 | ((210804-infernia-jest-nasza)) |
| Antoni Kramer        | 1 | ((210804-infernia-jest-nasza)) |
| Arianna Verlen       | 1 | ((210804-infernia-jest-nasza)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Eustachy Korkoran    | 1 | ((210804-infernia-jest-nasza)) |
| Klaudia Stryk        | 1 | ((210804-infernia-jest-nasza)) |
| Leona Astrienko      | 1 | ((210804-infernia-jest-nasza)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
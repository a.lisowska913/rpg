---
categories: profile
factions: 
owner: public
title: Krystian Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210331-elena-z-rodu-verlen          | służy na Castigatorze, pierwszy oficjalny Blakenbauer w kosmosie po tym co zrobiła Elena. | 0111-06-29 - 0111-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210331-elena-z-rodu-verlen)) |
| Dariusz Blakenbauer  | 1 | ((210331-elena-z-rodu-verlen)) |
| Elena Verlen         | 1 | ((210331-elena-z-rodu-verlen)) |
| Romeo Verlen         | 1 | ((210331-elena-z-rodu-verlen)) |
| Viorika Verlen       | 1 | ((210331-elena-z-rodu-verlen)) |
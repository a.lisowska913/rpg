---
categories: profile
factions: 
owner: public
title: Roman Kłębek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | mieszkaniec Żarni; marudny twórca sztucznych kwiatów. Ogólnie, na utrzymaniu żony. | 0109-08-23 - 0109-08-25 |
| 180814-trufle-z-kosmosu             | człowiek który spotkał się z Truflami i uznał, że musi się wysadzić w Żerni lub ukradną mu stopy - a wysadzenie nie będzie miało konsekwencji. | 0109-08-30 - 0109-09-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bronisława Strzelczyk | 2 | ((180724-skorpiony-spod-ziemi; 180814-trufle-z-kosmosu)) |
| Jakub Wirus          | 2 | ((180724-skorpiony-spod-ziemi; 180814-trufle-z-kosmosu)) |
| Alicja Kłębek        | 1 | ((180724-skorpiony-spod-ziemi)) |
| Arkadiusz Mocarny    | 1 | ((180814-trufle-z-kosmosu)) |
| Kirył Najłalmin      | 1 | ((180814-trufle-z-kosmosu)) |
| Olga Myszeczka       | 1 | ((180724-skorpiony-spod-ziemi)) |
| Pięknotka Diakon     | 1 | ((180814-trufle-z-kosmosu)) |
---
categories: profile
factions: 
owner: public
title: Szymon Oporcznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190331-kurz-po-ataienne             | mag Orbitera, który nie jest wrogi Pięknotce. Opiekun Ataienne. Chciał zabrać ją na koncert; uważa siebie i Pięknotkę za patriotów. | 0110-03-06 - 0110-03-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190331-kurz-po-ataienne)) |
| Ataienne             | 1 | ((190331-kurz-po-ataienne)) |
| Diana Tevalier       | 1 | ((190331-kurz-po-ataienne)) |
| Pięknotka Diakon     | 1 | ((190331-kurz-po-ataienne)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
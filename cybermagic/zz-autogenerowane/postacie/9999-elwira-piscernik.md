---
categories: profile
factions: 
owner: public
title: Elwira Piscernik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | detektyw, arystokratka i neurokomandoska. Bogata Piscerniczka, która trochę cosplayuje detektywa. Udało jej się dojść do problemów na Talio - jest tam nanitkowy potwór - i doprowadziła do spotkania Mireli z jej ojcem. Osłabiła ostry konflikt między Ognikami i Taliatami, po czym wywiozła Mikotki z Talio. | 0104-02-24 - 0104-03-01 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | WIELKA NIEOBECNA. Jej pintka była na Okrutnej Wronie. Wiemy, że leciała odkryć sekrety linku planetoidy Talio i Morza Ułud. Nie wiemy co się z nią stało i gdzie jest. MIA. | 0108-08-02 - 0108-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antonina Terkin      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Aurus Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Berdysz Rozpruwacz   | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jacek Ożgor          | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kara Prazdnik        | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kornelia Sanoros     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Pola Mornak          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Tytus Ramkon         | 1 | ((220512-influencerskie-mikotki-z-talio)) |
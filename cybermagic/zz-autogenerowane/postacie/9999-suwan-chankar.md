---
categories: profile
factions: 
owner: public
title: Suwan Chankar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220316-potwor-czy-choroba-na-etaur  | nie jest w stanie opanować EtAur; nie wie czy walczyć z chorobą czy z potworem. Oddelegował Lamię do Zespołu, ale Lamia zawiodła i dała się zarazić XD. | 0112-04-24 - 0112-04-26 |
| 220323-zatruta-furia-gaulronow      | szybką reakcją uratował życie Lamii przed gaulronem w Furii; potem opracował jak zneutralizować chore gaulrony i wykorzystał Lamię do tego celu (za co przestała mu ufać). Przekonał Ketrana by się zajął gaulronami. | 1111-11-11 - 1111-11-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominika Perikas     | 2 | ((220316-potwor-czy-choroba-na-etaur; 220323-zatruta-furia-gaulronow)) |
| Arianna Verlen       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Elena Verlen         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Eustachy Korkoran    | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Feliks Ketran        | 1 | ((220323-zatruta-furia-gaulronow)) |
| Graniec Borgon       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Klaudia Stryk        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Lamia Akacja         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maria Naavas         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| TAI Nephthys         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
---
categories: profile
factions: 
owner: public
title: Maria Naavas
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Eterniańska lekarka "Żelazka", która tak samo skutecznie leczy jak zabija. Nieprawdopodobnie wesoła i rozszczebiotana, jednocześnie jest bardzo 'unsettling'. Nigdy nie traci optymizmu i zawsze dąży ku najlepszego rozwiązania. Podrywaczka i kolekcjonerka facetów, namawiająca wszystkich do życia pełnią życia póki są w stanie. Sympatyczna i lojalna socjopatka nie mająca granic ani wyrzutów sumienia.

### Co się rzuca w oczy

* Wraz ze swoim dowódcą, Olgierdem, założyła "kapelę karaoke", z której jest piekielnie dumna. Ona jest na growlu...
* Uwielbia bawić się w swatkę; zwłaszcza chce zeswatać Olgierda z kimś jego godnym.
* Wzdycha za Martynem Hiwasserem. On jest jej głównym słabym punktem.
* DOTYK. Maria zawsze dotyka. W tym dotyku często nie ma podtekstów; po prostu taka jest.
* Niesamowicie dużo energii i pasji. Dąży do tej pasji. Próbuje spełnić pasję swoją i innych. Intensywna jak cholera.
* Unika wszelkiego typu holoprojekcji i virtu. Jej miejsce jest tu, w świecie realnym.
* Podrywaczka. Sama o sobie mówi "kolekcjonerka facetów". Ma sex appeal i nie waha się go używać.
* Całkowicie nie ma empatii. Zero. Nie współczuje. Jest "niekompletna".
* Nie ma w naturze rywalizacji, nie próbuje nikomu też nic udowodnić. Jest tym czym jest.
* Zawsze uśmiechnięta, często lekko rozmarzona, na twarzy wyraz lekkiego zadziwienia. Ekspresyjna.
* Uważa, że Orbiter może nie jest perfekcyjny, ale jest sprawny i potrzebny. Będzie Orbiter wspierać - bo widziała horror.

### Jak sterować postacią

* Perky & Chirpy. Wesoła. Dusza imprezy. Jednocześnie - Hollow Soul, Incomplete.
* Oddana Martynowi Hiwasserowi. Pragnie być jego partnerką. On ją dopełni. Jednocześnie - on ją lubi, nawet kocha - ale nie chce się związać.
* ZAWSZE bawi się w swatkę i jest w tym świetna.
* Nigdy nie traci optymizmu. Tak jak Martyn, wierzy w lepszy świat i w odróżnieniu od niego pragnie go zobaczyć.
* Nie ma w naturze rywalizacji, nie próbuje nikomu też nic udowodnić. Jest tym czym jest.
* NEOFORM: Wiktoria Diakon + Garak (DS9) | Abzan (BWG) | Profanity + Community + Tribalism | "Savage and unrestrained will to live and thrive and protect way of life"

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Dostała się do domu groźnego pirata przez łóżko, zabiła go gołymi rękami a następnie poszła pokój obok uspokoić jego dzieci, że nic się nie stało.
* Przekonała Olgierda do tego, by zrobił z nią kapelę karaoke. Po czym sprzedawała bilety a dochody przeznaczyła na program integracyjny Orbitera.
* Gdy Żelazko umierało w kosmosie i czekało na ratunek, utrzymała morale wszystkich w górze. Dzięki niej dało się podjąć trudne decyzje i statek przetrwał.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: Z uwagi na specyfikę jej visiatu jest nieprawdopodobnie odporna na Esuriit. Nie widzi, nie wchodzi w interakcję itp. Nie ma arkinu.
* AKCJA: Lekarz i anioł śmierci Żelazka. Doskonały lekarz i eksterminator bioagentami. Specjalizuje się w kierowanych plagach.
* AKCJA: Pierwszy oficer Żelazka i do tego świetna aktorka. Nie zmieniając tonu głosu umie strzelić w plecy komuś kogo przed chwilą całowała.
* AKCJA: Uwodzicielka i oficer morale. Świetnie śpiewa, ma ogromny sex appeal, doskonale podnosi morale innych i umie zainspirować.
* SERCE: Maria pragnie ŻYĆ z całego serca. Niewrażliwa na krytykę czy statusy społeczne. Niezłomne morale, zawsze zachowuje zimną krew.
* COŚ: Przenośne holoprojektory i amplifikatory soniczne; Maria niezależnie od okoliczności zorganizuje karaoke czy imprezę.
* COŚ: Jeden z najlepszych ekspertów od Esuriit jakich mamy na Orbiterze. Rytuały, sprzęt... i nic z tego nie może użyć sama.

### Serce i Wartości (3)

* Stymulacja
    * Intensywność u niej podpada pod hedonizm. Wszyscy powinni żyć pełnią swojego życia, zawsze i wszędzie!
    * Bardzo oparta na wszystkich zmysłach, zwłaszcza na dotyku.
    * "Head over heels" - nadmiar energii i pasji. Pierwsza rzuca się jak ćma w płomienie. Często płonie. Nie pyta o pozwolenie.
* Tradycja
    * Orbiter musi przetrwać. Orbiter musi być silny. Orbiter ma podejście, które działa - to podejście należy wspierać.
    * Nieprawdopodobna lojalność, zwłaszcza w kontekście braku empatii. Orbiter jest jej przyszywaną rodziną i musi być maksymalnie wzmocniony.
* Pokora
    * Pełni rolę pomocniczą. Jest bardzo żywą, ale aktorką drugoplanową wobec wszystkich "kompletnych" ludzi. Tylko Martyn może dać jej duszę.
    * Zdaje sobie sprawę ze swojej niekompletności i słabości. WIE, że jej percepcja świata jest uszkodzona. Oddaje dowodzenie innym.
    * Jestem cieniem czarodziejki. Jak w wypadku TAI, moją rolą jest służyć. Jak kiedyś Martyn. (synteza Tradycji i Pokory)

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Arkin eternijski mnie odrzucił. Nie jestem w stanie zjednoczyć się z innymi."
* CORE LIE: "Jestem niekompletna. Muszę być z kimś i muszę być bardzo głęboko. Sama nie stanowię osoby."
* Śmiertelnie zakochana w Martynie do poziomu obsesji. Dla jego uśmiechu skoczy z mostu.
* Brak empatii. Nie rozumie innych, nie wie kiedy posuwa się za daleko. Nie wie, kiedy krzywdzi - i nie czuje że to jest problem.
* "Prawda" i "moralność" są dla niej tylko słowem. Powie to, co najbardziej przybliży ją do celu. Zrobi to, co uzna za najskuteczniejsze.
* Proaktywna do bólu. Działa zgodnie z tym co rozumie i jej powiedziano. Nie przekracza rozkazów - czasem interpretuje je źle.

### Magia (3M)

#### W czym jest świetna

* Leczenie, utrzymywanie przy życiu, regeneracja. Maria jest lekarzem Orbitera i to widać.
* Plagi i pasożyty - dedykowane środki pod kątem konkretnej bioformy / konkretnej osoby.
* Psychokonstrukty / efemerydy intensyfikujące uczucia i emocje. Specjalizuje się w Różowej Pladze.

#### Jak się objawia utrata kontroli

* Utrata racjonalnego podejścia. 100% hiperoptymizmu i "Ryzyko? Uda się".
* Monotoniczne obsesje. Poczucie pustki którą trzeba zapełnić. Często wybuchy erotycznych orgii.
* Rozsianie plag / pasożytów.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* "Myślisz, że śmierć to zła opcja? Są rzeczy gorsze."
* Przyjęła nazwisko po anomalicznym statku kosmicznym, który należał do Kultu Ośmiornicy. Bo czemu nie? Chcieli nazwisko...
* Jej tożsamość nie jest powiązana z Eternią, więc nie jest powiązana z niczym. Poza, potencjalnie, Martynem.
* Teraz: 35 lat (na datę chronologii Inferni).
* Jej pierwszym przyjacielem i ogromną miłością był Martyn Hiwasser. Do dzisiaj są bardzo blisko.
* Jest lojalna Olgierdowi i "Żelazku"; oni ją wyciągnęli z Kultu Ośmiornicy. Ale jeszcze bardziej lojalna Martynowi.
* Theme Song: _Inkubus Sukkubus: "Vampire Erotica"_

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201125-wyscig-jako-randka           | była miłość Martyna Hiwassera, zdradziła mu kilka sekretów o Żelazku i jego załodze... plus, poznała od niego "sekrety Arianny x Olgierda". | 0111-02-08 - 0111-02-13 |
| 210120-sympozjum-zniszczenia        | Abzan (BWG). Poświęci Eustachego by Olgierd i Arianna byli na randce. Wpakowała ich do jednej kapsuły, uszkodziła life support by było zimno i wysłała na 3 dni ;-). | 0111-06-03 - 0111-06-07 |
| 210818-siostrzenica-morlana         | przypisała ("przypadkowo") Ariannę i Tomasza do małego pokoiku na Żelazku, by wywołać zazdrość Olgierda. Z powodzeniem :-). Olgierd overridował decyzję i Arianna była sama. | 0111-11-15 - 0111-11-19 |
| 211110-romans-dzieki-esuriit        | tymczasowo dołączyła do Inferni z woli Martyna (który jest nieaktywny). Podała Eustachemu leki antyradiacyjne po jego wybryku. Zajmie się Eustachy x Elena ;-). | 0112-02-07 - 0112-02-08 |
| 211117-porwany-trismegistos         | zbadała martwego miragenta i wykryła, że to miragent. Potem zajęła się wszystkimi przejętymi arystokratami Aurum - ci do stazy, ci na potem. | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | upiękniła Eustachego i po tym jak Eustachy / Infernia pokrzywdził "nową" załogę Inferni zajęła się ratowaniem żyć. Dlatego nie mogła pomóc Klaudii w ratowaniu Natalii... | 0112-02-14 - 0112-02-18 |
| 211215-sklejanie-inferni-do-kupy    | zaproponowała Ariannie feromony by pomóc załodze przetrwać na Inferni (uspokajające feromony "dla kotów"). Ogromny sukces - adaptacja udana, załoga może działać na Inferni, choć kultura wyszła nieco dziwnie. | 0112-02-21 - 0112-02-23 |
| 211222-kult-saitaera-w-neotik       | jej feromony pomogły w mindwarpowaniu przez Ariannę i Izę załogi Inferni i zbudowaniu jej nowej, unikalnej kultury (kult x pro-lolitka x noktianie). | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | z Klaudią złożyła feromony by spowolnić destabilizację Eleny; ixiońska energia wpłynęła do kokonu Natalii. Cichy MVP operacji - dzięki jej działaniom medycznym i stabilizacyjnym (magiczne i nie) udało się uratować większość załogi przed strasznym losem. | 0112-02-28 - 0112-03-04 |
| 220316-potwor-czy-choroba-na-etaur  | zawsze ma "najlepsze" pomysły. Jako, że w EtAur nikt nie lubi Zespołu, zaproponowała rozkochać wszystkich feromonami Eustachego. WTF. Oczywiście - zablokowano ten plan. | 0112-04-24 - 0112-04-26 |
| 220330-etaur-i-przyneta-na-krypte   | niebezpieczne zaklęcie na uciekającego potwora, ale doszła do tego, że jest tu PLAGA - dziwna Irytka - i broń biologiczna w jednym. | 0112-04-27 - 0112-04-30 |
| 220706-etaur-dziwnie-wazny-wezel    | podejrzewa adm. Termię o grę przeciwko Orbiterowi i za Syndykatem Aureliona; przyszła z tym do Eustachego i chyba zaczęła sojusz Termia - EtAur? | 0112-07-15 - 0112-07-17 |
| 220622-lewiatan-za-pandore          | poproszona przez Klaudię dodała info o Oli Szerszeń do swojej listy szczurzenia po adm. Termii. Doszła do tego - niestety, poszła za ostro i skutecznie zniszczyła kilka serc i uderzyła w plany i reputację Termii. Maria ma problem, ale Termia też. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220105-to-nie-pulapka-na-nereide    | z uwagi na swoje bezwględne, spokojne triagowanie i przekierowywanie energii została uznana za potwora przez członków Inferni. Maria spokojnie to akceptuje. | 0112-03-04

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 201125-wyscig-jako-randka           | chce pomóc Olgierdowi Drongonowi spiknąć się z Arianną Verlen. Lub Ariannie spiknąć się z Olgierdem. Ma PLAN. | 0111-02-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 12 | ((210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Klaudia Stryk        | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 8 | ((210120-sympozjum-zniszczenia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur)) |
| Leona Astrienko      | 5 | ((210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 4 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Olgierd Drongon      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Infernia          | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 3 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Diana Arłacz         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Diana d'Infernia     | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Kamil Lyraczek       | 2 | ((211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OO Tivr              | 2 | ((211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Raoul Lavanis        | 2 | ((220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Wawrzyn Rewemis      | 2 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii)) |
| Aleksandra Termia    | 1 | ((220706-etaur-dziwnie-wazny-wezel)) |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Karol Reichard       | 1 | ((211110-romans-dzieki-esuriit)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
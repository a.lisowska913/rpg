---
categories: profile
factions: 
owner: public
title: Kallista Exolon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200510-tajna-baza-orbitera          | emulatorka Grzymościa. Przekonana że jest wolną emulatorką Orbitera z frakcji Orbitera a nie Grzymościa (tymczasowe działanie Talaranda) załatwiła z Pięknotką, że ta zostawi ich bazę w spokoju, przynajmniej na razie. | 0110-09-03 - 0110-09-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Pięknotka Diakon     | 1 | ((200510-tajna-baza-orbitera)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
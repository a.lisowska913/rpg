---
categories: profile
factions: 
owner: public
title: Michalina Kefir
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221015-duch-na-pancernej-jaszczurce | 39, tymczasowy kapitan Pancernej Jaszczurki z Orbitera; opryskliwa kapitan eksperymentalnych jednostek Orbitera. Dowodziła Pancerną Jaszczurką za radami Klaudii i Ignacego. | 0109-08-24 - 0109-08-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Klaudia Stryk        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
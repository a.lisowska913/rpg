---
categories: profile
factions: 
owner: public
title: Crystal d'Corieris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200226-strazniczka-przez-lzy        | Zaawansowana TAI medyczna 2.5 generacji, operująca w Szpitalu Terminuskim w Pustogorze. | 0110-07-25 - 0110-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |
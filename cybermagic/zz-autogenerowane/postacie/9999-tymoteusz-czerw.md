---
categories: profile
factions: 
owner: public
title: Tymoteusz Czerw
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220316-potwor-czy-choroba-na-etaur  | oficer łącznościowy Eterni. Chipper. Hero worship wobec EUSTACHEGO. Chce być jak on. Wpadł w kłopoty u Chankara, bo złamał protokoły by dać dostępy BOHATEROM (Eustachemu et al). | 0112-04-24 - 0112-04-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Elena Verlen         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Eustachy Korkoran    | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Klaudia Stryk        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Maria Naavas         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
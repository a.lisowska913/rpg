---
categories: profile
factions: 
owner: public
title: Erwin Galilien
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Nurek Szczeliny Pustogorskiej i tinkerer. Technomanta specjalizujący się w budowaniu własnych narzędzi. Trochę taki McGyver. Bardzo lubi ludzi, ale nie lubi ich narażać. Wszystkie przychody reinwestuje w Nutkę oraz w badanie Szczeliny. Świetnie zbiera plotki i jest mistrzem działania w warunkach skrajnych takich jak Szczelina - żadne miejsce go nie złamie. Dodatkowo, kocha sztukę i piękno.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* BEAUTY; piękno rzeczywistości jest dostępne wszystkim; kolekcjonować, zbierać i rozprzestrzeniać piękno
* CURIOSITY/ADAPTABILITY; poznać anomalie i zbudować narzędzia je rozwiązujące; wizyty w Szczelinie i ciągłe dostosowywanie bezpieczeństwa
* COMMUNITY/SAFETY; nikt poza mną nie może się narażać; wieczne budowanie narzędzi i działanie solo w trudnych sytuacjach

### Wyróżniki (3)

* Mistrz artefaktor. Jeden z najlepszych konstruktorów narzędzi technomantycznych i power suitów na Astorii.
* Ekspert od Szczeliny. Potrafi tam przetrwać i znajdować niesamowite rzeczy.
* Szperacz, scavenger i scrapper. Zrobi z różnych dziwnych artefaktów i anomalii całkiem sprawne narzędzia, też ze złomowiska czy Szczeliny.

### Zasoby i otoczenie (3)

* Nutka: inteligentny power suit ogromnej mocy, acz stosunkowo niewielkiej siły ognia. Bardziej zwrotny i wytrzymały niż bojowy.
* niewielkie acz bardzo interesujące przedmioty pochodzące ze Szczeliny; nic potężnego, ale kurioza
* liczne narzędzia technomagiczne; analityka oraz wsparcie. Szczególnie w obszarze katalizy i survivalu
* jest trochę celebrytą w Pustogorze; uważany za niegroźnego i bardzo sympatycznego
* ma opinię maga przynoszącego pecha; większość jego sojuszników ginie na akcjach w których on uczestniczy

### Magia (3)

#### Gdy kontroluje energię

* bardzo subtelny katalista; mistrz wyczucia nawet minimalnych zmian w pływach energii - np. delikatne zmiany Szczeliny
* mistrz konstrukcji narzędzi i technomancji - prawdziwy inżynier oraz naukowiec w jednym
* innymi słowy, twórca narzędzi oraz osoba identyfikująca różne problemy

#### Gdy traci kontrolę

* Galilien jest mistrzem kontroli i wyczucia magii. Gdy czuje, że traci kontrolę - odcina się od magii i przez pewien czas jest niezdolny do czarowania.

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180815-komary-i-kosmetyki           | próbuje chronić reputację Pięknotki, ostrzega ją przed potencjalnym problemem i finalnie włamuje się do Adeli zatruwając jej krem tworzący komary. | 0109-09-02 - 0109-09-05 |
| 180817-protomag-z-trzesawisk        | główne źródło informacji Pięknotki w okolicy, na absolutnie każdy istotny temat | 0109-09-07 - 0109-09-09 |
| 180929-dwa-tygodnie-szkoly          | podróżując po Szczelinie wpierw wpadł w kłopoty, coś mu najpewniej podali, stracił Nutkę (postawił w kasynie), odzyskał Nutkę i zdobył grzyby. Po tym - OPR. | 0109-09-17 - 0109-09-19 |
| 181003-lilia-na-trzesawisku         | skutecznie ucieka przed Lilią, zdradził Pięknotce kilka swoich sekretów z przeszłości i karnie współpracował na Trzęsawisku. | 0109-09-20 - 0109-09-22 |
| 181021-powrot-minerwy-z-terrorforma | sam się ograniczał, by Nutce nie stała się krzywda. Bardzo ciężko ranny osłaniając Pięknotkę w walce z terrorformem. Stracił Nutkę, odzyskał Minerwę. | 0109-09-24 - 0109-09-29 |
| 181227-adieu-cieniaszczycie         | wpierw dał się porwać Walerii, potem nie rozumiał Cieniaszczytu i na końcu nie zrobił nic produktywnego - ale jest szczęśliwy z Pięknotką. | 0109-11-27 - 0109-11-30 |
| 190101-morderczyni-jednej-plotki    | znalazł dla Pięknotki informacje o Karolu Szurnaku, to on rozpuszczał plotki. | 0109-12-12 - 0109-12-16 |
| 190120-nowa-minerwa-w-nowym-swiecie | nie kocha Minerwy. Kocha Pięknotkę. Chciał, by to było wystarczająco jasne. Nie do końca Minerwie ufa, ale jest skłonny jej zaufać. | 0110-01-21 - 0110-01-25 |
| 190127-ixionski-transorganik        | katalista Pięknotki i dywersja by odwrócić uwagę ixiońskiego transorganika. | 0110-01-28 - 0110-01-29 |
| 190210-minerwa-i-kwiaty-nadziei     | świetne wsparcie w przekonywaniu Minerwy. Dodatkowo, dzięki niemu dało się wyśledzić ruchy Kornela i jego dziwne kwiaty Nadziei. | 0110-02-20 - 0110-02-22 |
| 190402-eksperymentalny-power-suit   | wraz z Minerwą doszli do tego jak działa Cień i jak funkcjonuje. Nie dość dobrze opanował ten power suit; skończył w szpitalu gdy Cień wyrwał się spod kontroli. | 0110-03-11 - 0110-03-13 |
| 190422-pustogorski-konflikt         | najwybitniejszy ekspert od power suitów; ostrzegł Pięknotkę że power suit Marcela nie jest normalnym power suitem, bo zachowuje się nietypowo. | 0110-03-29 - 0110-03-30 |
| 190424-budowa-ixionskiego-mimika    | poczciwy i kochany; powiedział Pięknotce o próbach przemytu z okolicy Wolnych Ptaków. Nie miał pojęcia, że Pięknotka wyczyści Pustogor ixiońską pięścią. | 0110-03-30 - 0110-04-01 |
| 190427-zrzut-w-pacyfice             | niechętnie wziął Pięknotkę do Pacyfiki; płynnie po Pacyfice nawigował i skutecznie doprowadził Pięknotkę do tajemniczego ścigacza i Nikoli | 0110-04-03 - 0110-04-05 |
| 190429-sabotaz-szeptow-elizy        | katalista delikatnych energii oraz konstruktor pułapki energii; wraz z Alanem, Minerwą i Pięknotką stworzyli rezonator kryształów Elizy, niszczący jej plany. | 0110-04-06 - 0110-04-09 |
| 190527-mimik-sni-o-esuriit          | najlepszy katalista w okolicy, wpadł pomóc Pięknotce znaleźć Mirelę a znalazł faktyczne Esuriit. | 0110-05-03 - 0110-05-05 |
| 191105-zaginiona-soniczka           | najlepszy katalista. Wykrył po starej aurze co było nie tak z Mariolą oraz pomógł Pięknotce rozwikłać zagadkę. Gdy potrzebny jest arcykatalista, Erwin jest na miejscu. | 0110-06-28 - 0110-06-29 |
| 190817-kwiaty-w-sluzbie-puryfikacji |  | 0110-06-30 - 0110-07-05 |
| 200425-inflitrator-poluje-na-tai-minerwy | mistrz delikatnej katalizy, z Gabrielem odkrył, że za problemami TAI nie stoi Minerwa a Infiltrator Iniekcyjny Aurum | 0110-08-20 - 0110-08-22 |
| 200509-rekin-z-aurum-i-fortifarma   | najdoskonalszy katalista; pomógł Gabrielowi znaleźć, że Krwawy Potwór budowany jest w okolicy fortifarmy Irrydii. | 0110-08-26 - 0110-08-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180929-dwa-tygodnie-szkoly          | przeciorany za wyciągnięcie niebezpiecznych grzybów ze Szczeliny Pustogorskiej. Upiekło mu się. | 0109-09-19
| 181003-lilia-na-trzesawisku         | ma ogromne długi, które od kupiła Lilia Ursus. Efektywnie, nie jest panem własnego losu. | 0109-09-22
| 181003-lilia-na-trzesawisku         | zamodelował i stworzył Nutkę na wzór swojej dawnej partnerki która zginęła w Szczelinie. Nutka jest o niego zazdrosna. | 0109-09-22
| 181021-powrot-minerwy-z-terrorforma | jego servar (Nutka) jest zastąpiony przez Minerwę. | 0109-09-29
| 181021-powrot-minerwy-z-terrorforma | ma połączenie mentalne pomiędzy sobą a Minerwą. | 0109-09-29
| 181021-powrot-minerwy-z-terrorforma | bardzo ciężko ranny; co najmniej tydzień w szpitalu + rehabilitacja. | 0109-09-29
| 181024-decyzja-minerwy              | utracił Nutkę, ale zyskał Nutkę + Minerwę + terrorforma jako swój servar. A był tylko nieprzytomny... | 0109-10-04
| 181225-czyszczenie-toksycznych-zwiazkow | poszedł na rekonstrukcję (usunięcie Saitaera). Zakochał się w Pięknotce - nie jest już tylko przyjaciółką. | 0109-11-16
| 181227-adieu-cieniaszczycie         | uleczony od wpływów Saitaera, zakochany w Pięknotce (choć nadal ma serce do Minerwy). | 0109-11-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 20 | ((180815-komary-i-kosmetyki; 180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190101-morderczyni-jednej-plotki; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190527-mimik-sni-o-esuriit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Minerwa Metalia      | 8 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Atena Sowińska       | 5 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei)) |
| Karla Mrozik         | 4 | ((190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Olaf Zuchwały        | 4 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Adela Kirys          | 3 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 190527-mimik-sni-o-esuriit)) |
| Lilia Ursus          | 3 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Tymon Grubosz        | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 191105-zaginiona-soniczka)) |
| Alan Bartozol        | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Ataienne             | 2 | ((190402-eksperymentalny-power-suit; 191105-zaginiona-soniczka)) |
| Felicja Melitniek    | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Kasjopea Maus        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 2 | ((190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Lucjusz Blakenbauer  | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190127-ixionski-transorganik)) |
| Wiktor Satarail      | 2 | ((190127-ixionski-transorganik; 190527-mimik-sni-o-esuriit)) |
| Adam Szarjan         | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Artur Kołczond       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karolina Erenit      | 1 | ((190127-ixionski-transorganik)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Mariusz Trzewń       | 1 | ((191105-zaginiona-soniczka)) |
| Mateusz Kardamacz    | 1 | ((191105-zaginiona-soniczka)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Moktar Gradon        | 1 | ((181227-adieu-cieniaszczycie)) |
| Natalia Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Nikola Kirys         | 1 | ((190427-zrzut-w-pacyfice)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Sabina Kazitan       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Saitaer              | 1 | ((190127-ixionski-transorganik)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Tomasz Tukan         | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Waleria Cyklon       | 1 | ((181227-adieu-cieniaszczycie)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
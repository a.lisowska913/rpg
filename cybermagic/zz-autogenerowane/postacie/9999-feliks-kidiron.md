---
categories: profile
factions: 
owner: public
title: Feliks Kidiron
---

# {{ page.title }}


# Generated: 



## Fiszki


* najmłodszy z rodu Kidironów, mający podniesione uprawnienia u Prometeusa, 16 | @ 230329-zdrada-rozrywajaca-arkologie
* ENCAO:  +0-++ |Dowcipny, Błyskotliwy, o ciętym języku;;Zawsze pełen energii wśród innych ludzi;;Towarzyski i przyjacielski| VALS: Self-direction, Family >> Universalism| DRIVE: Daredevil | @ 230329-zdrada-rozrywajaca-arkologie
* młody master hacker, mający wsparcie BIA Prometheus i chcący pomóc Ewelinie. | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


historia-eustachego
arkologia-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | jedyny przyjaciel Eweliny; uczy się psychotroniki i ma uprawnienia do overridowania Prometeusa. Stworzył rekord "szantażyski Zosi" by chronić Ewelinę przed konsekwencjami jej czynów. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ardilla Korkoran     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
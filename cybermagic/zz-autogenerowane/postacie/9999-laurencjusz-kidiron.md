---
categories: profile
factions: 
owner: public
title: Laurencjusz Kidiron
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | mściwy młody (16) Kidiron próbujący wygrać "eksterminację szczurów" z Lertysami a potem zwandalizować slumsowy dom Lertysów. Zmienił zdanie po tym jak został OSTRZELANY. | 0087-05-03 - 0087-05-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Jan Lertys           | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Karol Lertys         | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Tymon Korkoran       | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
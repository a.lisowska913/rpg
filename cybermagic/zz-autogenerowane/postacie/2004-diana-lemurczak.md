---
categories: profile
factions: 
owner: public
title: Diana Lemurczak
---

# {{ page.title }}


# Read: 

## Kim jest

### Motto

"Zatańczmy w tą szaloną noc, jutro będzie lepiej - sami możemy to zagwarantować."

### Paradoksalny Koncept

Arystokratka Aurum, która promuje w Szczelińcu kulturę letycką. Arystokratka bez bogactwa, za to z klubem w którym robi występy dla Szczelińca. Złośliwa i wesoła, zachowuje niewzruszony optymizm niezależnie od skali przeciwnika z którym ma do czynienia. Geniusz, deprecjonowana przez własną rodzinę, której z tego powodu bardzo dobrze.

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: alchemiczka najwyższej klasy. Specjalizuje się zwłaszcza w efektownej pirotechnice, odwracaniu uwagi i szokowaniu innych. 
* ATUT: sprzężona psychotronicznie. Łączy zalety czarodziejki oraz taktycznej TAI klasy Elainka. Niesamowita pamięć, percepcja, doskonały taktyk.
* ATUT: performerka najwyższej klasy. Potrafi śmieszyć, tumanić, przestraszać. Zrobi show jak mało kto.
* SŁABA: bardzo słaba czarodziejka. Ma wszystkie wady maga, nie będąc w stanie czarować dynamicznie. Ma jedynie alchemię.
* SŁABA: ma tendencje do BARDZO bezwzględnych i nieludzkich działań; próbuje się przed tymi tendencjami powstrzymywać. Tak więc - ZBYT krzywdzi lub UNIKA krzywdzenia.
* AT/SŁ: nie chowa się, nie ucieka. Jest waleczna i trudna do zatrzymania lub zastraszenia. Ale też - łatwo ją zaatakować.

### O co walczy (3)

* ZA: zbudowanie bezpiecznej przystani uciekinierom z Aurum w Szczelińcu. Jeśli ktoś chce opuścić Aurum, ona chce pomóc.
* ZA: największy show na planecie. Skłonności do efektowności, teatru itp. Świat jest teatrem i zasługuje na spektakl!
* VS: tracenie / odbieranie marzeń. Diana absolutnie nie zgadza się na to, by ktokolwiek rezygnował z marzeń, poddawał się lub przyjmował los jak jest.
* VS: narzucanie siły słabszym. Nieważne kim są słabsi i kim jest napastnik. Nieważne, kto ma rację. Pozwól żyć innym jak chcą!

### Znaczące Czyny (3)

* Ratując przyjaciółkę przed siepaczami swej rodziny zabiła wszystkich siepaczy pułapkami i pirotechniką, imponując nawet swojemu ojcu. Odchorowywała to miesiąc.
* W swoim podwierckim klubie, Arkadii, kierowała ofertę zwłaszcza wobec "Latających Rekinów" by pokazać im jak mogą używać swoich umiejętności by pomóc Pustogorowi.
* Nie umie walczyć, a jednak stanęła w obronie przyjaciółki wobec bardzo groźnego przeciwnika - w pełni wiedząc dzięki sprzężeniu, że zostanie ciężko pobita i nie ma szans.

### Kluczowe Zasoby (3)

* COŚ: Klub Arkadia w Podwiercie; bardzo efektowny, skupiony na sztuce i poezji. A w piwnicy laboratorium alchemiczne.
* KTOŚ: Gang 'Latające Rekiny'. Są tam osoby które lubią Dianę, są też takie urażone tym, że wysysa z nich pieniądze.
* WIEM: Znam dziesiątki scenariuszy bojowych dzięki sprzężeniu z Elainką i potrafię je użyć lepiej niż jakakolwiek TAI. ZAWSZE ma plan.
* OPINIA: Wesoła, czarująca i złośliwa właścicielka klubu Arkadia. Nieustraszona. W potrzebie pomoże każdemu. Jeśli jesteś z Aurum, oj, skasuje Cię na sporo monet.

## Inne

### Wygląd

Ubiera się niesamowicie krzykliwie i pstrokato; nie pokazuje ciała. Krótkie, brązowe włosy - zwykle z absurdalną czapką. Zielone oczy. Paskudna blizna na lewym przedramieniu, jak od przyjęcia uderzenia nożem.

### Coś Więcej

Diana jest bardzo niebezpiecznym przeciwnikiem gdy jest w trybie bojowym (taktyka + pirotechnika to ŚWIETNA kombinacja), albo siłą przekształcającą Podwiert i Szczeliniec w azyl dla uciekinierów z Aurum. Jednocześnie - jedyny powód czemu może tu być to to, że jej ród to toleruje. Tak więc jest tu niezły wbudowany konflikt ostateczny.

W Dianie trzeba zrozumieć to, że Diana NIGDY się nie poddaje. Może zaprzestać na jakiś czas, ale nie ulegnie. Nawet - zwłaszcza - gdy powinna.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200520-figurka-a-kopie-zapasowe     | osoba przechowująca Figurkę Żabboga dla Mariusza, potem wywabiona przez Gerwazego z domu. Jedyne co osiągnęła to wsadzić Gerwazego na 1 dzień do aresztu dzięki czemu odkryła, że Mariusz też stał za tym wszystkim. | 0109-10-13 - 0109-10-20 |
| 200513-trzyglowec-kontra-melinda    | jako "Diana Lauris"; przyjaciółka Melindy, która ją przyjęła i dała pracę. Bardzo inteligentna alchemiczka, dała Melindzie bombę gazową. | 0109-11-08 - 0109-11-17 |
| 200524-dom-dla-melindy              | broniła Melindę jak ognia przed terminuską, przed rodziną. Jednocześnie - nie chciała związać się z mafią ani wysługiwać arystokratom. Wygrała. Wierzy w dobro terminusów. | 0110-09-09 - 0110-09-12 |
| 210518-porywaczka-miragentow        | Lorena wysłała przeciw niej starego miragenta, więc Diana go... zwinęła. Skonfrontowała się z Marysią Sowińską i powiedziała jej prawdę. Zdziwiona, że Marysia nie chciała zrobić jej krzywdy. | 0111-05-03 - 0111-05-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200520-figurka-a-kopie-zapasowe     | wie, że Gerwazy Lemurczak i Mariusz Grabarz (którego uważała za przyjaciela) doprowadzili do okradzenia Arkadii i jej mieszkania. Połączyła to z Żabbogiem, ale nie ma dowodów i nikt jej nie słucha. Aha, wie, że to "Pożeracz" był egzekutorem. | 0109-10-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Melinda Teilert      | 3 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Feliks Keksik        | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Gerwazy Lemurczak    | 2 | ((200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Mariusz Grabarz      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Aranea Diakon        | 1 | ((200520-figurka-a-kopie-zapasowe)) |
| Diana Tevalier       | 1 | ((200524-dom-dla-melindy)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Jan Łowicz           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Julia Kardolin       | 1 | ((210518-porywaczka-miragentow)) |
| Katja Nowik          | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Laura Tesinik        | 1 | ((200524-dom-dla-melindy)) |
| Lorena Gwozdnik      | 1 | ((210518-porywaczka-miragentow)) |
| Mariusz Trzewń       | 1 | ((200524-dom-dla-melindy)) |
| Marysia Sowińska     | 1 | ((210518-porywaczka-miragentow)) |
| Natasza Aniel        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Pięknotka Diakon     | 1 | ((200524-dom-dla-melindy)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Triana Porzecznik    | 1 | ((210518-porywaczka-miragentow)) |
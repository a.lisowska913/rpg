---
categories: profile
factions: 
owner: public
title: Natalia Aradin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211124-prototypowa-nereida-natalii  | oblatywaczka stoczni Neotik i przyjaciółka Adama Szarjana. Jakaś forma destabilizacji mentalnej sprawiła, że neurosprzęgła się z Nereidą po kanałach ixiońskich i doszło do Syntezy Ixiońskiej. Fanka Eustachego i Sekretów Orbitera. Jej Nereidę zestrzeliła Infernia i weszła w szok. Klaudia próbowała ją uratować, ale Eksplozja Skażenia otoczyła ją kokonem z ixiońskiej energii i transmetalu. Czeka na wyklucie się... | 0112-02-14 - 0112-02-18 |
| 220105-to-nie-pulapka-na-nereide    | wykluła się, jako synteza energii Eleny, Arianny, Esuriit, Ixionu i kilku innych kanałów. Stała się anomalią kosmiczną na chassisie Nereidy. | 0112-02-28 - 0112-03-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211124-prototypowa-nereida-natalii  | konwersja ixiońska; jest w formie larwy ixiońskiej i się dopiero musi "wykluć" przez Skażenie Klaudii. | 0112-02-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Elena Verlen         | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Eustachy Korkoran    | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Izabela Zarantel     | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Klaudia Stryk        | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Adam Szarjan         | 1 | ((211124-prototypowa-nereida-natalii)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Kamil Lyraczek       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Leona Astrienko      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Lutus Amerin         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| OO Infernia          | 1 | ((211124-prototypowa-nereida-natalii)) |
| Roland Sowiński      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
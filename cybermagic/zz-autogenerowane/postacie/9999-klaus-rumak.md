---
categories: profile
factions: 
owner: public
title: Klaus Rumak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | dowódca "Szalonego Rumaka". Uważa, że jego życie się skończyło po zdradzie Dariusza. Anastazja kazała mu słuchać jej rozkazów. Niechętnie, zgodził się. | 0111-01-07 - 0111-01-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Ataienne             | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 1 | ((201021-noktianie-rodu-arlacz)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |
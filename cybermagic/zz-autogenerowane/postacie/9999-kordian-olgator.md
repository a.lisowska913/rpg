---
categories: profile
factions: 
owner: public
title: Kordian Olgator
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220817-osy-w-ces-purdont            | od ochrony bazy; ufortyfikował remnants koło Medical i niechętnie, ale oddał Eustachemu dowodzenie nad obroną CES (Kidiron autoryzował). Dobrze pomagał Eustachemu chronić przed Trianai. | 0093-01-23 - 0093-01-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220817-osy-w-ces-purdont)) |
| Celina Lertys        | 1 | ((220817-osy-w-ces-purdont)) |
| Eustachy Korkoran    | 1 | ((220817-osy-w-ces-purdont)) |
| Jan Lertys           | 1 | ((220817-osy-w-ces-purdont)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Kamil Wraczok        | 1 | ((220817-osy-w-ces-purdont)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
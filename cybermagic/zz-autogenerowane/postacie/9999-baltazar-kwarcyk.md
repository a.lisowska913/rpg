---
categories: profile
factions: 
owner: public
title: Baltazar Kwarcyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | koordynator TAI i ruchu na K1; nerd zafascynowany zwłokami każdego typu i przyjaciel oraz kontakt Marianny i Adeli. Odkrył, że wykresy z lifesupport wskazują na potencjalne zwłoki po przetworzeniu przez energię magiczną, m.in. Esuriit. | 0111-09-10 - 0111-09-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
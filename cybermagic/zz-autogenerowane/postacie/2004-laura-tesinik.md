---
categories: profile
factions: 
owner: public
title: Laura Tesinik
---

# {{ page.title }}


# Read: 

## Kim jest

### Paradoksalny Koncept

Uczennica terminusa, której bliżej do prawniczki niż do wojowniczki. Wykorzystuje drony i AI, by nie musieć działać osobiście. Potrafi każdego przeciwnika wystawić / wykurzyć z bezpiecznego miejsca, ale nie umie tego wykorzystać; jej magia jest za wolna. Uciekinierka z Cieniaszczytu (kralothy) i studentka Akademii Magii, która pragnie życia terminusa by nie musieć wracać. Wykorzystuje prawo by znaleźć w nim luki, które obróci na swoją korzyść.

### Motto

"Prawo nie jest celem - to kolejna broń w arsenale terminusa - dokładnie jak drony czy technologia."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zna absolutnie każdy paragraf i zasadę prawną jaka dotyczy sytuacji. Skutecznie paraliżuje praworządne osoby.
* ATUT: Psychotroniczka; edytuje i wchodzi w głąb AI. Świetnie zarządza dronami i wykorzystuje je jako wsparcie.
* ATUT: Potrafi każdego wykurzyć zza osłony / z fortecy kombinacją dron, gazów bojowych i superciężkich zaklęć destrukcyjnych. Wystawi każdy cel.
* SŁABA: W walce wręcz i krótkiego zasięgu; nadaje się tylko na średni / daleki zasięg. W krótkim zasięgu / walce wręcz słabsza niż nie-terminusi!
* SŁABA: Jej magia jest wolna; nie jest w stanie czarować szybko i w warunkach nie-artyleryjskich.

### O co walczy (3)

* ZA: Zrobi wszystko by osiągnąć status pełnoprawnej terminuski; to jej pozwoli się ochronić przed wrogami z Cieniaszczytu (kralotycznymi).
* ZA: Większa swoboda i wolność wyboru; kocha model cieniaszczycki i nie przepada za szczelińskim. Nie jest przeciwna mafii czy używkom.
* VS: Bardzo przeciwna kralothom i dominacji viciniusów. Boi się kralothów. By do żadnego się nie zbliżyć, wesprze diabła.
* VS: Nienawidzi hipokrytów, którzy tylko "wykonują rozkazy". Ona zaryzykowała i wygrała.

### Znaczące Czyny (3)

* W Cieniaszczycie z zimną krwią zastawiła pułapkę zabijając kralotha w odwecie za zniewolenie jej przyjaciół. Potem uciekła.
* Niejednokrotnie wybroniła Tukana z problemów prawnych przez jego współpracę z Grzymościem; została jego protegowaną.
* Zrobiła automatyczny monitoring obszaru dronami i elektroniką, gdy wymykała się na schadzkę z przyjacielem w szkole magów. 

### Kluczowe Zasoby (3)

* COŚ: Osiem dron zwiadowczo-bojowych klasy 'Żądłacz'; sama je odratowała i wstrzyknęła im poszerzoną psychotronikę.
* KTOŚ: Jej głównym kręgiem towarzyskim są jednak uczniowie Akademii Magii w Zaczęstwie. Napoleon ją zwalczał i nie wierzy, że została terminuską...
* WIEM: Wie bardzo dużo na temat tego jak szybko przejąć kontrolę / wykorzystywać typowe cywilne TAI.
* OPINIA: Bardzo dobrze wykorzystuje prawo dla własnych celów; nie stroni od zabawy i wolności typu cieniaszczyckiego.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200425-inflitrator-poluje-na-tai-minerwy | niedoświadczona terminuska i asystentka Tukana; psychotroniczka, która zna chyba wszystkie regulacje i działa zgodnie z każdą jedną z nich. | 0110-08-20 - 0110-08-22 |
| 200510-tajna-baza-orbitera          | miała okazję użyć swoich zaawansowanych skilli prawnych by stawić czoło Aurum i WYGRAĆ - nie przeniosą Tadeusza. Prawo jest jej bronią. Pomogła Pięknotce :D. | 0110-09-03 - 0110-09-07 |
| 200524-dom-dla-melindy              | Tukan chciał chronić Melindę przed Aurum. Pięknotka też. Laura całkowicie z własnej inicjatywy zablokowała ekstradycję Melindy i dostała podziękowania od obu. | 0110-09-09 - 0110-09-12 |
| 200616-bardzo-straszna-mysz         | ratowała reputację Tukana jak mogła; integrowała się z Krystalizatorem i wykryła, że twórcą myszy Esuriit jest Henryk ale w cieniu stoi Diana. | 0110-09-14 - 0110-09-17 |
| 201006-dezinhibitor-dla-sabiny      | głos sumienia Gabriela jak chodzi o Sabinę Kazitan - tym razem jest niewinna. Wykryła skanując serratusa dronami, że Sabina złamała parol. | 0110-10-03 - 0110-10-05 |
| 210817-zgubiony-holokrysztal-w-lesie | uruchomiona przez Tukana by znalazła ko-matrycę Kuratorów wśród studentów AMZ; znalazła ślad wskazujący na Lilianę i Stellę. Przyjaciółka Liliany "od zawsze". | 0111-06-18 - 0111-06-20 |
| 210824-mandragora-nienawidzi-rekinow | cyniczna do bólu na Tukana i Ekaterinę. Dobrze zaplanowała pułapkę na pnączoszpona. Bezradna w obliczu duchów. Doskonała taktycznie. | 0111-06-29 - 0111-07-01 |
| 210831-serafina-staje-za-wydrami    | z rozkazu Tukana wysłała Marysi info jak zginęli ludzie tworzący mandragorę; niewyjaśnione. Pustogor ich ukrył? Laura nie jest szczęśliwa z tego jak działa Tukan, ale jest oportunistką. | 0111-07-03 - 0111-07-06 |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | ściągnięta jako terminuska do problemów z portretem Liliany. Z uwagi na przeszłość w Cieniaszczycie powiedziała Julii dane które pomogą jej i Lilianie w dojściu do tego kto stoi za tym wszystkim. Nie podaje własnych opinii i chowa się za aspektami prawnymi. Nie jest ich koleżanką. | 0111-10-18 - 0111-10-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201006-dezinhibitor-dla-sabiny      | znajomość i szacunek wśród uczniów Akademii Magii w Zaczęstwie. Jest "jedną z nich", ale fajniejsza (transfer student kiedyś). | 0110-10-05
| 201006-dezinhibitor-dla-sabiny      | ma materiały do szantażu i "wiszą jej" Rekiny z frakcji Justyniana Diakona. | 0110-10-05
| 210817-zgubiony-holokrysztal-w-lesie | przyjaciółka Liliany Bankierz "od zawsze". | 0111-06-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gabriel Ursus        | 4 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200616-bardzo-straszna-mysz; 201006-dezinhibitor-dla-sabiny)) |
| Mariusz Trzewń       | 3 | ((200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 3 | ((210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Pięknotka Diakon     | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy)) |
| Tomasz Tukan         | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow)) |
| Daniel Terienak      | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Franek Bulterier     | 2 | ((200524-dom-dla-melindy; 200616-bardzo-straszna-mysz)) |
| Ignacy Myrczek       | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Karolina Terienak    | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Liliana Bankierz     | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 210831-serafina-staje-za-wydrami)) |
| Sabina Kazitan       | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Amelia Sowińska      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Diana Tevalier       | 1 | ((200524-dom-dla-melindy)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Julia Kardolin       | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Melinda Teilert      | 1 | ((200524-dom-dla-melindy)) |
| Minerwa Metalia      | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Napoleon Bankierz    | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rafał Torszecki      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sensacjusz Diakon    | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
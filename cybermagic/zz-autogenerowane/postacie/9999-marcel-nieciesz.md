---
categories: profile
factions: 
owner: public
title: Marcel Nieciesz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190422-pustogorski-konflikt         | sympatyczny terminus z Fortu Mikado w Pustogorze, który padł ofiarą mimika symbiotycznego i porwał dwie Miasteczkowiczanki. Jego mimik został pokonany przez Cienia. | 0110-03-29 - 0110-03-30 |
| 220923-wasale-zza-muru-pustogorskiego-PLC |  | 0111-10-10 - 0111-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Ekaterina Zajcew     | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Erwin Galilien       | 1 | ((190422-pustogorski-konflikt)) |
| Karla Mrozik         | 1 | ((190422-pustogorski-konflikt)) |
| Karolina Terienak    | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Marysia Sowińska     | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Olaf Zuchwały        | 1 | ((190422-pustogorski-konflikt)) |
| Pięknotka Diakon     | 1 | ((190422-pustogorski-konflikt)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
---
categories: profile
factions: 
owner: public
title: Albert Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | ojciec Mai, dba o poprawność etykiety i niespecjalnie wie co robi Maja (UW: structure). Gdy Maja dała ślady obecności o 2 rano w okolicach Centrum Danych Symulacji Zarządzania, ruszył jej na pomoc komunikując się z wykrytą tam Eleną Samszar. Jego uwaga odwrócona, dzięki działaniom Eleny i Karolinusa Maja nie wpadła w kłopoty. Dba o Maję. | 0095-08-09 - 0095-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Karolinus Samszar    | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
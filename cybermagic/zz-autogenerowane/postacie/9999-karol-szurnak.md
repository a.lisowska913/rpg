---
categories: profile
factions: 
owner: public
title: Karol Szurnak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190101-morderczyni-jednej-plotki    | łowca nagród z Pustogoru, który rozpuszczał plotki o Pięknotce i jej "brudnym gabinecie". Został upokorzony a jego reputacja zniszczona przez Pięknotkę. | 0109-12-12 - 0109-12-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190101-morderczyni-jednej-plotki    | upokorzony przez Pięknotkę; wyszło, że lubił nastolatki oraz musiał na kolanach płacząc przepraszać i odszczekiwać plotki. Wróg Pięknotki. | 0109-12-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Erwin Galilien       | 1 | ((190101-morderczyni-jednej-plotki)) |
| Olaf Zuchwały        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Pięknotka Diakon     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
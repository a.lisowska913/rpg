---
categories: profile
factions: 
owner: public
title: Sonia Skardin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221015-duch-na-pancernej-jaszczurce | 33, pilot-oblatywacz; cicha i solidna agentka Orbitera pilotująca Pancerną Jaszczurkę pod Michaliną Kefir. Skrajnie lojalna Michalinie. | 0109-08-24 - 0109-08-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Klaudia Stryk        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
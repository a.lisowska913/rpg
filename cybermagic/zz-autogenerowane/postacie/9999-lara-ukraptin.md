---
categories: profile
factions: 
owner: public
title: Lara Ukraptin
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  +--+0 |Nie planuje długoterminowo;; Opiekuńcza| VALS: Stimulation Self-direction >> Power| DRIVE: Pantha Rhei | @ 230613-zaginiecie-psychotronika-cede
* sympatyczna studentka psychotroniki w Gwiazdoczach, zna się na rzeczy (23) | @ 230613-zaginiecie-psychotronika-cede

### Wątki


szamani-rodu-samszar
mroczna-wiedza-samszarów

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230613-zaginiecie-psychotronika-cede | dziewczyna Cede, studentka. Jej pamięć została przekształcona przez Fabiana Samszara, ale Karolinus ją odbudował Soczewką. | 0095-08-02 - 0095-08-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230613-zaginiecie-psychotronika-cede | odbudowana pamięć przez Karolinusa, głęboko zakochana w Cede. Dzięki Karolinusowi - bardziej niż kiedykolwiek ;p. | 0095-08-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Karolinus Samszar    | 1 | ((230613-zaginiecie-psychotronika-cede)) |
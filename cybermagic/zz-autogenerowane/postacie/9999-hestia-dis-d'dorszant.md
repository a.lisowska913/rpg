---
categories: profile
factions: 
owner: public
title: Hestia Dis d'Dorszant
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191123-echo-eszary-na-dorszancie    | pozostawiona przez Eszarę z wiedzą jak przejąć kontrolę nad Stacją używająć chemikaliów Skażających narotyki; niechętnie, ale współpracuje z Zespołem by nikt nie zginął. | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Filip Szczatken      | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Gerwazy Kruczkut     | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia Ain d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Kamelia Termit       | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Rafał Kirlat         | 1 | ((191123-echo-eszary-na-dorszancie)) |
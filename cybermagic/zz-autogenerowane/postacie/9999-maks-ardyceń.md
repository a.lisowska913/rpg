---
categories: profile
factions: 
owner: public
title: Maks Ardyceń
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | były strażnik parku w Drzewcu; ekspertyza w tropieniu, zna się na górach i survivalu; prawie spadł w przepaść wyciągnięty przez Olafa i Lily. Usunął pijawki z Olafa i Mai więcej niż raz. | 0083-10-02 - 0083-10-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Lily Sanarton        | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Maja Wurmramin       | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Olaf Zuchwały        | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Roman Wyrkmycz       | 1 | ((230618-reality-show-z-zaskoczenia)) |
---
categories: profile
factions: 
owner: public
title: Amara Zegarzec
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  +0-+0 |Spontaniczna;;Niecierpliwa;;Pomocna| VALS: Self-direction, Achievement| DRIVE: Młodsza Siostra | @ 230516-karolinka-raciczki-zemsty-verlenow
* styl: wesoła "młodsza siostra" próbująca zrobić wszystko pomocne | @ 230516-karolinka-raciczki-zemsty-verlenow
* dusza hotelu: hospitality, majsterkowiczka, gotowanie, wiedza o lokalnym terenie | @ 230516-karolinka-raciczki-zemsty-verlenow

### Wątki


waśnie-samszar-verlen
waśnie-blakenbauer-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230516-karolinka-raciczki-zemsty-verlenow | współwłaścicielka hotelu Odpoczynek Pszczół; starała się zrobić co może by chronić lokalne duchy i ekosystem. Mimo, że siostra próbowała zarobić. Przekonywała Elenę o współpracę. | 0095-07-29 - 0095-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Elena Samszar        | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Karolinus Samszar    | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
---
categories: profile
factions: 
owner: public
title: Lilia Ursus
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (2)

Bogata czarodziejka, dość młoda, która pragnie spełnić swoje marzenie zostania wybitnym nurkiem Szczeliny i dojścia do Erwina Galiliena. Świetna w kojarzeniu faktów, tropienia ludzi oraz kupowaniu dobrej klasy sprzętu. Bardzo dobry kupiec, urocza aktorka i mistrzyni perswazji. Nie umie walczyć.

### Co motywuje postać (2)

* nie do końca wie czego się uczyć i boli ją nieefektywność tej nauki -> uczy się od najlepszych i pragnie osiągnąć mistrzostwo w tym co robi
* uważa za cholerną niesprawiedliwość los Galiliena i jego samotność -> chce zostać partnerką Galiliena. Co najmniej w zakresie wspólnych akcji.
* nie jest traktowana jako "osoba", raczej jak worek złota lub maskotka -> chce być pełnoprawną, godną agentką - umiejętności, nie tylko bogactwo.
* wulkan energii, przesympatyczna, promyczek radości, buzia jej się nie zamyka

### Idealne okoliczności (2)

* w bezpiecznym miejscu, wykorzystuje swoje bogactwo by osiągnąć sukces. Zalewa coś kasą.
* ma przewagę sprzętową - w momencie, w którym i tak ma przewagę, jej przewaga jest po prostu większa.

### Szczególne umiejętności (3)

* świetnie wyceni większość rzeczy, doskonały kupiec, sprzeda nawet ranną stonogę
* aspirujący detektyw, dbałość o szczegóły, triki i sztuczki iluzjonistyczne
* znajomość i obsługa sprzętu magitechowego

### Zasoby i otoczenie (3)

* bogata jak cholera; pieniądze nie mają dla niej żadnego znaczenia. Ona zalewa pieniędzmi problemy.
* dobrej klasy sprzęt magitechowy, przede wszystkim survivalowo-bojowy.

### Magia (3)

#### Gdy kontroluje energię

* kocia akrobatka: niesamowita kontrola przestrzeni i poruszania się, też w magitechach
* bardzo efektowna magia bojowa: nie do końca ją kontroluje, ale zdecydowanie ma do niej dostęp i ma do niej dryg
* magia percepcji: ciężko się przed nią ukryć; you can run but you can't hide

#### Gdy traci kontrolę

* jej magia bojowa uruchamia się na Efektach Skażenia i na niczym innym; ona nie potrafi rzucić zaklęcia bojowego
* jest to jedyny sposób, w jaki manifestuje się jej utrata kontroli
* zaklęcia bojowe różnej klasy lecą jak fajerwerki - niekontrolowane, we wszystkie możliwe strony

### Powiązane frakcje

{{ page.factions }}

## Opis

(14 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181003-lilia-na-trzesawisku         | chce pomóc Erwinowi przez co on wpada w kłopoty. Na Trzęsawisku niespecjalnie się wykazała, ale nie pogorszyła opinii. | 0109-09-20 - 0109-09-22 |
| 181021-powrot-minerwy-z-terrorforma | okazało się, że ma przeszłość powiązaną z Adamem. Nie lubi Adama, nie ma zamiaru z nim współpracować nawet jak ona by na tym zyskała. | 0109-09-24 - 0109-09-29 |
| 181024-decyzja-minerwy              | grzeczna panienka, naprawiła chatę Erwinowi, po czym - na prośbę Pięknotki - sobie poszła. | 0109-10-01 - 0109-10-04 |
| 181125-swaty-w-cieniu-potwora       | wyszukała informacje o nojrepach, wyszukała o Wioletcie, po czym wpadła w szpony Moktara i skończyła jako zabaweczka do uratowania przez Pietra. Chwilowo w Nukleonie. | 0109-10-28 - 0109-10-31 |
| 181227-adieu-cieniaszczycie         | porwana przez Walerię, jest w silnej depresji. Nie jest bezpieczna, nie ma celu. Zgodziła się na warunki Pięknotki by nie sprawiać kłopotu. Low point in her life. | 0109-11-27 - 0109-11-30 |
| 191103-kontrpolowanie-pieknotki-pulapka | wejście Pięknotki do Aurum; sporo wie o tamtych terenach. Powiedziała Pięknotce jak działają rody Aurum, w jaki sposób kolekcjonuje się artefakty i jaką rolę ma Małmałaz w tym wszystkim. | 0110-06-20 - 0110-06-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181003-lilia-na-trzesawisku         | ma w swoich rękach długi Erwina Galiliena i mu nie odpuści - chce do Szczeliny | 0109-09-22
| 181003-lilia-na-trzesawisku         | po akcji na Trzęsawisku jest wstrząśnięta i straciła pewność siebie. Zależy jej rozpaczliwie by się wykazać | 0109-09-22
| 181003-lilia-na-trzesawisku         | ojciec wysłał oddział który ma ją chronić. Ona o nich nie wie. | 0109-09-22
| 181021-powrot-minerwy-z-terrorforma | obwinia Adama Szarjana oraz Atenę Sowińską o stan Erwina Galiliena. Plus, dalej uważa, że Adam jest złym magiem z dobrym PRem. | 0109-09-29
| 181021-powrot-minerwy-z-terrorforma | ma w przeszłości wywalenie dobrej terminuski z Koszmaru oraz próbę wejścia do Koszmaru przez łóżko. | 0109-09-29
| 181125-swaty-w-cieniu-potwora       | ośmieszona i pohańbiona przez Moktara; uwierzyła w jego słowa, że jest bezużytecznym śmieciem z pieniędzmi i nic nie może zrobić. Chwilowo w szpitalu na detoksie. | 0109-10-31
| 181125-swaty-w-cieniu-potwora       | znalazła powiązanie pomiędzy nojrepami a Orbiterem Pierwszym i doprowadziła Pięknotkę do Wioletty. | 0109-10-31
| 181125-swaty-w-cieniu-potwora       | jeszcze trzy dni musi spędzić w klinice Nukleon na regenerację. Moktar jej nie oszczędzał... | 0109-10-31
| 181226-finis-vitae                  | porwana przez Walerię na prośbę Pięknotki; znajduje się w Colubrinus Psiarni, acz nikt jej nie krzywdzi. | 0109-11-26
| 181227-adieu-cieniaszczycie         | została nieszczęśliwą asystenką Ateny Sowińskiej. Niezbyt się szanują z Ateną. | 0109-11-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Erwin Galilien       | 3 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Adam Szarjan         | 2 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy)) |
| Atena Sowińska       | 2 | ((181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Minerwa Metalia      | 2 | ((181024-decyzja-minerwy; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Moktar Gradon        | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Adela Kirys          | 1 | ((181024-decyzja-minerwy)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Romuald Czurukin     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
---
categories: profile
factions: 
owner: public
title: Mojra Karstall
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230521-rozszczepiona-persefona-na-itorwienie | security Itorwien; nie zakwalifikowała się do sił Orbitera przez nałogi; została czysta, ale gdy pojawiły się problemy na Itorwien stare nawyki wróciły i chciała pozyskać środki z przemytu. Skaziła Itorwien substratem do robienia niebezpiecznych narkotyków. Dostały ją siły specjalne Orbitera. | 0109-09-15 - 0109-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Fabian Korneliusz    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Helmut Szczypacz     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudia Stryk        | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Martyn Hiwasser      | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Serbinius         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
---
categories: profile
factions: 
owner: public
title: Pietro Dwarczan
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Silny i dobrej klasy atleta, opiekuńczy. Wygląda jak prawdziwy epicki wiking. Świetny DJ. Imprezowy zwierz, uwielbiający zabawę, rywalizację i przygody. Silny kręgosłup każdej ekipy. Kiedyś gladiator; do dzisiaj mistrz rzucania przedmiotami do celu.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* ENJOYMENT/FREEDOM; życie i tak ma wiele problemów, więc idźmy radością; czy przez muzykę czy przez imprezę, on to naprawi 
* ludzie dzielą wszystkich i szukają różnic; wszyscy powinniśmy dawać innym szanse; zawsze powita innego i stanie przeciw wykluczeniu
* ciągłe zastanawianie się co wolno a co nie; idź za sercem, mniej polityki; nie będzie pieprzył się w politykę a robił to co uważa za słuszne
* łagodny, kobieciarz, opiekuńczy, przyjazny 

### Wyróżniki (3)

* Miotanie przedmiotami: mało kto tak jak on potrafi rzucić czymś i trafić dokładnie tam gdzie chciał.
* Król imprez: DJ elektroniki, techno i hardstyle - sama jego obecność może zmienić imprezę w legendarną.
* Łagodzenie konfliktów: potrafi obniżyć poziom złości i problemów, dogada się z większością ludzi i ogólnie "chill out, man".
* Wiara w swoje ideały: incorruptible, a przynajmniej taki próbuje być, niezależnie od okoliczności.
* Walczy z przeciwnościami losu lub pogody, wspina się gdzieś czy próbuje przetrwać w dziczy. Ciało zawodzi, lecz on idzie dalej.

### Zasoby i otoczenie (3)

* Epicki sprzęt muzyczno-dźwiękowy: niezależnie od okoliczności, the rave can be started!
* Lokalna / internetowa sława: nie jest może bardzo sławny, ale jest lubiany i popularny w Cieniaszczycie i w internecie

### Magia (3)

#### Gdy kontroluje energię

* Magia soniczna: czy to dla muzyki czy do zniszczenia czegoś, jego moc skupia się na dźwiękach i muzyce
* Magia iluzji: skupiona przede wszystkim w kierunku imprez i efektowności. Impreza musi trwać!
* Magia lecznicza: zwłaszcza w obszarze kojenia bólu i usuwania niekorzystnych efektów narkotyków. Ale też - leczenie (acz średnio).

#### Gdy traci kontrolę

* Smutna muzyka: głośna, smutna muzyka niszcząca klimat i wszelkie próby 'stealth'...
* Aura miłości: ludzie zaczynają się mieć ku sobie. Przychodzi impreza lub przychodzi niekontrolowana miłość i radość

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181118-romuald-i-julia              | "wiking". Silny, świetnie miota kamieniami. Troszczył się o Pięknotkę i dbał o nią na akcji. Bardzo sympatyczny, z Pięknotką mają do siebie pewną miętę. | 0109-10-26 - 0109-10-28 |
| 181125-swaty-w-cieniu-potwora       | zabujał się w Wioletcie, po czym znalazł i uratował Lilię gdy ta wpadła w ręce Moktara. Przez to skończył w Nukleonie a nie na arenie jak chciał. Przegrał Wiolettę do Pięknotki. | 0109-10-28 - 0109-10-31 |
| 181209-kajdan-na-moktara            | głównodowodzący grupy organizujący plan uratowania Walerii przed dziwnymi nojrepami. Rozkradli bazę Moktarowi XD. | 0109-11-01 - 0109-11-02 |
| 181227-adieu-cieniaszczycie         | Pięknotka wzięła go na litość i wziął na siebie opiekę nad viciniuską, Brygidą Maczkowik. | 0109-11-27 - 0109-11-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | epicka ballada Romualda sprawiła, że jest jeszcze bardziej znany i ceniony jako "ten dobry", ratujący i pomagający ludziom i magom. | 0109-10-31
| 181225-czyszczenie-toksycznych-zwiazkow | spiknął się z Wiolettą dzięki machinacjom Pięknotki. Jest szczęśliwy. | 0109-11-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 3 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Romuald Czurukin     | 2 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora)) |
| Adam Szarjan         | 1 | ((181118-romuald-i-julia)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Atena Sowińska       | 1 | ((181227-adieu-cieniaszczycie)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Julia Morwisz        | 1 | ((181118-romuald-i-julia)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
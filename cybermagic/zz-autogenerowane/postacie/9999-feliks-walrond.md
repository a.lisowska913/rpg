---
categories: profile
factions: 
owner: public
title: Feliks Walrond
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210526-morderstwo-na-inferni        | komodor Orbitera (w linii Szradmanna); ma duże wonty do noktian. Jego zdaniem integracja z noktianami to błąd itp. Wykorzystywał czarne sektory do ćwiczeń Lancerami. | 0111-10-26 - 0111-11-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Arianna Verlen       | 1 | ((210526-morderstwo-na-inferni)) |
| Elena Verlen         | 1 | ((210526-morderstwo-na-inferni)) |
| Eustachy Korkoran    | 1 | ((210526-morderstwo-na-inferni)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Klaudia Stryk        | 1 | ((210526-morderstwo-na-inferni)) |
| Leona Astrienko      | 1 | ((210526-morderstwo-na-inferni)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 1 | ((210526-morderstwo-na-inferni)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
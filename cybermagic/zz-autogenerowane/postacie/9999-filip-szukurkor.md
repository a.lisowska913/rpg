---
categories: profile
factions: 
owner: public
title: Filip Szukurkor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221113-ailira-niezalezna-handlarka-woda | (ENCAO: 0-0+- |Purytański;;Bezkompromisowy| VALS: Achievement, Face >> Self-direction| DRIVE: Wygrać w rywalizacji) (kiedyś Orbiterowiec, 44 lat); przyszedł wyśmiać Nastię z jej nieszczęścia i ze stanu Hiyori; ma jakąś przeszłość (wybrała Ogdena a nie jego). Jako najemnik wziął Nastię i Jakuba na operację na anomalnym statku noktiańskim dla jakiegoś kapitana Orbitera. Chce wyśmiać ale też chce pomóc. | 0090-04-24 - 0090-04-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ailira Niiris        | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Daria Czarnewik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
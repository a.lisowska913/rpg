---
categories: profile
factions: 
owner: public
title: Prokop Umarkon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | górnik z Trzech Przyjaciół; został wrobiony w zostanie kapitanem Królowej Przygód. Chciał ją sprzedać blakvelowcom i mieć dobre życie, ale nie wyszło. | 0108-02-25 - 0108-03-11 |
| 220329-mlodociani-i-pirat-na-krolowej | postawił się Sewerynowi w sprawie dzieci; mają mieć szansę. Chce pomóc zagrożonej jednostce (nawet jak piraci). Nie odrzuci życia Łucjana nawet jak to zabije groźnych piratów. | 0108-06-22 - 0108-07-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | POWAŻNY OPIERDOL od blakvelowców, bo przedkłada jakieś dzieciaki nad finanse Blakvela i swoją załogę. | 0108-07-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gotard Kicjusz       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Łucjan Torwold       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Miranda Ceres        | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
---
categories: profile
factions: 
owner: public
title: Szczepan Olgrod
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200106-infernia-martyn-hiwasser     | komodor Orbitera na Kontrolerze Pierwszym. Wsparł Ariannę Verlen w ataku na komodora Ogryza; ale za to sporo zyskał koncesji z jej strony. | 0110-06-28 - 0110-07-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Dominik Ogryz        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Kamil Lyraczek       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Klaudia Stryk        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Martyn Hiwasser      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Wioletta Keiril      | 1 | ((200106-infernia-martyn-hiwasser)) |
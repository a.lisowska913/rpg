---
categories: profile
factions: 
owner: public
title: Diana Lauris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200616-bardzo-straszna-mysz         | gdy grupka Rekinów chciała skrzywdzić ją i Melindę, pozwoliła im kontynuować plan - ale swoim Eidolonem doprowadziła do ucieczki myszy Esuriit i do jej ewolucji. Rozpalona przez krew Lemurczaków, została zatrzymana przez Laurę i Gabriela i zmuszona do ucieczki jak niepyszna. | 0110-09-14 - 0110-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Laura Tesinik        | 1 | ((200616-bardzo-straszna-mysz)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
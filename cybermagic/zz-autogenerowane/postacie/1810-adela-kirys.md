---
categories: profile
factions: 
owner: public
title: Adela Kirys
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (2)

Była uczennica Pięknotki Diakon, która jest absolutnie zakochana w temacie piękna, kosmetyków i urody. Pochodzi z nizin społecznych; była żebraczka która już NIGDY nie wróci do tego życia. Ma niskie poczucie własnej wartości i za wszelką cenę chce odnieść sukces. Nie jest złą osobą, ale jest zdesperowana.

### Co motywuje postać (2)

* Pięknotka jej nie dostrzega i nie szanuje jako eksperta -> Pięknotka traktuje ją jak równą i sama dąży do współpracy z Adelą bo ta jest od niej lepsza
* Ani nie jest piękna, ani nie jest znana i jest o 1 fakturę od bankructwa -> Zrobi wszystko, by być znana, piękna i bezpieczna finansowo.
* W zależności od urodzenia czy Rodu masz szanse w życiu lub nie -> Nie urodzenie a praca powinna mieć znaczenie. Kosmetyki, piękno itp. dają radość i nadzieję.
* nieufna, kocha się w pięknie, niezbyt etyczna, próbuje pomóc, ufa raczej osobom szemranym

### Idealne okoliczności (2)

* przygotowuje nowe kosmetyki i mikstury na bazie badań oraz eksperymentów; skierowane pod kątem konkretnego celu
* dyskretnie próbuje gdzieś się wkraść czy przedostać; nikt tam nie zwraca na nią uwagi
* dzieli się radością z piękna wśród osób ubogich oraz dzieci

### Szczególne umiejętności (3)

* kosmetyki, upiększanie, uroda - mało kto zna się na tym tak jak ona
* charakteryzacja, żebranie, ukrywanie się, szybkie poruszanie się w mieście
* alchemia, trucizny, różnego rodzaju dziwne środki i eliksiry

### Zasoby i otoczenie (3)

* sporo osób z nizin i nie mających szans uważa ją za jedną z najcenniejszych i najważniejszych osób w okolicach Pustogoru
* z uwagi na przeszłość ma dostęp do wielu szemranych osób - oraz te osoby z nią chętnie współpracują za opłatą
* ma zawsze przy sobie coś upiększającego i do charakteryzacji. Też, stroje do przebrania się.

### Magia (3)

#### Gdy kontroluje energię

* eliksiry i mikstury, kosmetyki oraz upiększanie. Biomancja od trucizn po piękno. Różnego rodzaju narkotyki czy stymulanty, upiększacze czy charakteryzacje.
* magia ukrywająca jej obecność, maskująca aurę oraz odwracająca od niej uwagę.
* dodatkowo, magia piękna oraz iluzje przywołujące radość oraz optymizm. Iluzjonistka, która chce by piękno świata dorównywało wizji z jej iluzji.

#### Gdy traci kontrolę

* jej energia zwykle wpływa silnie na jej emocje - Adela robi rzeczy których potem żałuje; jej stan emocjonalny się rozchwiewa
* dodatkowo, jej zaklęcia zwykle są duplikowane na okolicznych zwierzętach i roślinach. Ona bywa chodzącą bombą Skażeniową.
* często jej emocje są duplikowane wśród innych - od złego humoru po euforię. Czasem losowe osoby się zakochują w innych osobach.

### Powiązane frakcje

{{ page.factions }}

## Opis

.

# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180815-komary-i-kosmetyki           | była uczennica Pięknotki powtarzająca jej błędy ale nadzieja sprawia, że ignoruje potencjalne ryzyko. Wpadła w kłopoty, bo musiała wszystko wycofać. | 0109-09-02 - 0109-09-05 |
| 180929-dwa-tygodnie-szkoly          | współpracuje z kasynem i Rolandem Grzymościem. Jej rola jest jeszcze nieznana. | 0109-09-17 - 0109-09-19 |
| 181024-decyzja-minerwy              | nieco zaplątana w tą sprawę; dała radę zrobić makijaż servarowi, po czym poprawiła na bardzo udany makijaż (przebijając Pięknotkę) | 0109-10-01 - 0109-10-04 |
| 181027-terminuska-czy-kosmetyczka   | dała z siebie wszystko by wygrać konkurs na najlepszą kosmetyczkę - i o włos! Ale Pięknotka była jednak lepsza. Za to, Adela jest druga. | 0109-10-07 - 0109-10-11 |
| 190106-a-moze-pustogorska-mafia     | która dla uratowania Kasjana przyszła prosić Pięknotkę. Niestety, nie dało się go uratować - ale pomogła go przekonać, by nie szedł w mafię a w Cieniaszczyt. | 0109-12-23 - 0109-12-25 |
| 190113-chronmy-karoline-przed-uczniami | stworzyła eliksir dla Karoliny, proszona przez Napoleona. Niestety, eliksir wszedł w interakcję z polem magicznym Karoliny (nie wiedziała że Karolina ma takie pole). | 0110-01-04 - 0110-01-05 |
| 190119-skorpipedy-krolewskiego-xirathira | lekko nadwyrężyła zaufanie Alana i poprosiła o próbkę skorpipeda by zrobić przeciw niemu truciznę. Niestety, Wiktor jest o kilka klas lepszy od Adeli. | 0110-01-13 - 0110-01-14 |
| 190202-czarodziejka-z-woli-saitaera | bardzo jej zależało by pomóc Karolinie Erenit. Przekonała Sławka i dała mu eliksir. Czyli - drugi raz zrobiła ten sam błąd, eliksir na nieznany target. | 0110-01-31 - 0110-02-04 |
| 190505-szczur-ktory-chroni          | chciała ratować Krystiana i Oliwię i zmontowała najdzikszą konspirację proszku kralotycznego, która się rozpadła na jej oczach. Trafiła na Trzęsawisko do Wiktora. | 0110-04-18 - 0110-04-20 |
| 190527-mimik-sni-o-esuriit          | chce być taka jak Wiktor, ale ów ją zbywa; dała Pięknotce trochę swoich kosmetyków by Mirela się jej nie bała. Aha, zaprzyjaźniła się z Mirelą. | 0110-05-03 - 0110-05-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180815-komary-i-kosmetyki           | jej zapowiadająca się dobrze kariera kosmetyczki została uszkodzona przez konieczność wycofania partii kremu (bo tworzył magiczne komary) (bo Erwin go zatruł) | 0109-09-05
| 181027-terminuska-czy-kosmetyczka   | drugie miejsce kosmetyczek w Pustogorze. Ma klientów i nie grozi jej już upadek czy katastrofa. Przekonana, że Pięknotka wygrała przez użycie Ateny. | 0109-10-11
| 190101-morderczyni-jednej-plotki    | jest przerażona Pięknotką. Boi się stanąć przeciwko niej. Nie zrobi już tego. Widzi, że to nie ta liga. Przestała mówić o "największej nemesis". | 0109-12-16
| 190105-turysci-na-trzesawisku       | jest nią zainteresowana mafia i ciemniejsze interesy. | 0109-12-23
| 190113-chronmy-karoline-przed-uczniami | traci trochę pewności siebie - widzi, że mogła zostać pozwana za to co zrobiła sama z tym eliksirem dla Karoliny Erenit (bez sprawdzenia osobiście) | 0110-01-05
| 190119-skorpipedy-krolewskiego-xirathira | jej poczucie własnej wartości i umiejętności biomantycznych jest już naprawdę niskie. | 0110-01-14
| 190202-czarodziejka-z-woli-saitaera | sporo z tematów powiązanych z utratą przez Wojtka mocy zostało jej przypisane. Jest formalne śledztwo w jej kierunku. | 0110-02-04
| 190505-szczur-ktory-chroni          | dostaje lekcje szamaństwa, zielarstwa i alchemii u Wiktora Sataraila, który przy okazji pokazuje jej jak ewoluować w coś sensownego | 0110-04-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190106-a-moze-pustogorska-mafia; 190113-chronmy-karoline-przed-uczniami; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Arnulf Poważny       | 3 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami)) |
| Erwin Galilien       | 3 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 190527-mimik-sni-o-esuriit)) |
| Minerwa Metalia      | 3 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190202-czarodziejka-z-woli-saitaera)) |
| Wiktor Satarail      | 3 | ((190119-skorpipedy-krolewskiego-xirathira; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Ignacy Myrczek       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Karolina Erenit      | 2 | ((190113-chronmy-karoline-przed-uczniami; 190202-czarodziejka-z-woli-saitaera)) |
| Kasjan Czerwoczłek   | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Roland Grzymość      | 2 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia)) |
| Sławomir Muczarek    | 2 | ((190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Waldemar Mózg        | 2 | ((190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Adam Szarjan         | 1 | ((181024-decyzja-minerwy)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Lilia Ursus          | 1 | ((181024-decyzja-minerwy)) |
| Liliana Bankierz     | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Olga Myszeczka       | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Saitaer              | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Teresa Mieralit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Tymon Grubosz        | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Wojtek Kurczynos     | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
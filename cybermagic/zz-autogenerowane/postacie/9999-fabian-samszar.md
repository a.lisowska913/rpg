---
categories: profile
factions: 
owner: public
title: Fabian Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO: -++-0 |Powściągliwy;; Bezwzględny| VALS: Family, Self-direction >> Hedonism | DRIVE: Eternal Evolution | @ 230613-zaginiecie-psychotronika-cede
* ekspert od | @ 230613-zaginiecie-psychotronika-cede

### Wątki


szamani-rodu-samszar
mroczna-wiedza-samszarów

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230613-zaginiecie-psychotronika-cede | potężny mentalista powiązany z dziwnymi eksperymentami i podziemnymi bazami Samszarów, kiedyś nauczyciel Karolinusa. Powiązany ze zniknięciem Cede. | 0095-08-02 - 0095-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Karolinus Samszar    | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
---
categories: profile
factions: 
owner: public
title: OA Odkupienie
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | sentisprzężony statek z cyberzałogą, pod dowództwem Sowińskich. Miał przewieźć Anastazję do Aurum, ale został zaatakowany przez killware Krypty i przez Infernię (o czym nie wie). | 0111-05-24 - 0111-05-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | dostał killware w formie Nocnej Krypty (XD), potem uszkodzony przez Infernię. Zregenerował i odleciał do Sowińskich, acz wymaga naprawy. | 0111-05-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Arianna Verlen       | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Elena Verlen         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Klaudia Stryk        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
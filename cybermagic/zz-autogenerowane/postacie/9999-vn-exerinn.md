---
categories: profile
factions: 
owner: public
title: VN Exerinn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | crawler często współpracujący z Nativis; dołączyły doń dwa Robaki. Zaatakowany przez Farighany, Infernia pomogła Exerinnowi odzyskać ludzi i przetrwać. Brudny, ciasny i niewygodny - ale wystarczająco solidny. | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
---
categories: profile
factions: 
owner: public
title: Joachim Pulkmocz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | bibliotekarz; poprosił Elenę Samszar o pomoc, bo jego kuzyn Paweł oddał książkę do biblioteki 3 dni po czasie i za to "Adelaida" nałożyła nań klątwę i Paweł zakochał się w tomiku kiepskiej poezji Verlenów o niedźwiedziach. To naprowadziło Elenę do działania. | 0094-10-04 - 0094-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
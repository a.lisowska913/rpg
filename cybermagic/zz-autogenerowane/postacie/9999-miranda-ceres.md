---
categories: profile
factions: 
owner: public
title: Miranda Ceres
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | bardzo zmodyfikowana TAI Ceres; ma jakieś powiązanie z Percivalem Diakonem. Reanimowana w Pasie Kazimierza, wrobiła wszystkich w to, że jej statek (Królowa Przygód) będzie odbudowana przez blakvelowców i trzech górników zostanie członkami jej załogi. | 0108-02-25 - 0108-03-11 |
| 220329-mlodociani-i-pirat-na-krolowej | nigdy się nie zdradziła, choć to ona steruje Królową Przygód. Uratowała statek przed Berdyszem, potem udając Annę połączyła z nim dzieci. Niewidzialne wsparcie. | 0108-06-22 - 0108-07-03 |
| 220405-lepsza-kariera-dla-romki     | nadal działa z cienia - pokazała Annie że Helena się szprycuje i pokazała Romce ULTRA HARD PORN by ta nie szła w prostytucję. Królowa cieni :-). | 0108-07-09 - 0108-07-11 |
| 220503-antos-szafa-i-statek-piscernikow | koordynowała i dyskretnie przesyłała wszystkie ważne informacje Annie i Zespołowi - np. to, że Luxuritias / Piscernik to może być statek niewolniczy. | 0108-07-17 - 0108-07-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 3 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| SC Królowa Przygód   | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Antos Kuramin        | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Damian Szczugor      | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Gotard Kicjusz       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Kara Prazdnik        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Łucjan Torwold       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Seweryn Grzęźlik     | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |
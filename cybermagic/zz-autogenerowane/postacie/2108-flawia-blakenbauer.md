---
categories: profile
factions: 
owner: public
title: Flawia Blakenbauer
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Infiltratorka z konieczności, która zawsze marzyła o gwiazdach i kosmosie, daleko od Aurum i durnych intryg. Nie przepada za arystokracją i za abstrakcjami - lubi to, co może dotknąć. Specjalistka od transformacji płaszczek i siebie.

### Co się rzuca w oczy

* Flawia wierzy w przeznaczenie i w swoje miejsce w egzystencji - dla niej to Orbiter, w odróżnieniu od niezależnego Lucjusza (jej ulubionego kuzyna; też działa poza strukturami Aurum).
* Nie jest fanką Arystokracji Aurum i jest aktywnie wroga Eterni. Lepiej jej z żołnierzami i ludźmi pracy niż z Arystokracją.
* Flirciara, wesoła - czasem mylona z Diakonką. Przynajmniej do momentu jak zdejmie ubranie i widać "symbiotycznego skorpioida".
* Szuka struktury, porządku, miejsca gdzie może się przydać i pomagać. Jej marzeniem jest oficer wykonawczy na Orbiterze, ale nie kapitan.
* Bardzo "kinetyczna" - musi DOTKNĄĆ, zobaczyć sama. Nastawiona na konkret i to, co materialne. Kiepsko radzi sobie z abstrakcjami i integracją z virt / maszynami.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy Lucjusz obejmował Szpital Terminuski, Flawia wzięła urlop awaryjny i przyleciała do Pustogoru. Tak długo czarowała, przekonywała, dawała dowody, prosiła - aż w końcu dali mu szansę. Była jego banneretką ;-).
* Gdy na Orbiterze była świadkiem kocenia innych, działała z cienia - przekupstwem, dobrym słowem lub podłożeniem fałszywych dowodów sprawiała, że kocenie było zawsze ukarane.
* Gdy Domena Barana była zaatakowana przez Strachy, Flawia magią (wspierana przez Antonellę i Esuriit) syntetyzowała skorpioidami pajęczynę do neutralizacji Strachów. Misja ważniejsza niż niechęć do Esuriit.

### Co się rzuca w oczy: Atuty i Przewagi (3)

* AKCJA: Porusza się i zachowuje się swobodnie prawie wszędzie. Mistrzyni infiltracji społecznej. Doskonała aktorka. Jak trzeba, zabójczyni.
* AKCJA: Ma podstawowe wyszkolenie bojowe i zagrożeniowe Orbitera. Umie walczyć wręcz, strzelać, zna podstawowe problemy i zagrożenia, zachowuje zimną krew...
* AKCJA: Potrafi magią edytować płaszczki i siebie. Jej moc jest ograniczona do biomancji.
* COŚ: Ma pet scorpoid queen. Z tych miniaturowych. Co straszniejsze, ma ową królową skorpioidów w ciele; w okolicach boku.
* COŚ: Ma ogłupiacze, ogłuszacze i detoksy - psychoaktywne środki, które może komuś podać.

### Serce i Wartości (3)

* Twarz 
    * ma tysiące twarzy, ale honor i uznanie tych na których jej zależy jest bardzo ważne. 
    * bardzo wysokie integrity i poszanowanie Zasad. Wiele zapłaci w służbie Sprawy.
* Dobrodziejstwo
    * ma bardzo altruistyczne podejście do bliźnich. Potrafi bardzo dużo energii włożyć by pomóc komuś, kto zasłużył. Pomaga tym, którzy sami próbują sobie pomóc - nie ratuje tych co nie próbują zmienić swego losu.
    * She holds grudges. Jeśli kogoś nie lubi lub uznaje za wroga - nie pomoże. 
* Pokora
    * dąży do swojego miejsca w egzystencji. Walczy by się tam dostać. 
    * Jej miejscem jest Orbiter. Wykonuje rozkazy i akceptuje, że inni mogą być lepsi. 
    * Przez to ma wewnętrzną harmonię - przeznaczenie sprawi, że jeśli będzie walczyć o Orbiter, w końcu tam trafi.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* Jakkolwiek by się nie starała, Flawia po prostu nie potrafi integrować się z maszynami czy virtem. Jeśli chodzi o "normalne" urządzenia, daje radę - ale nic zaawansowanego.
* Flawia ma bardzo duże problemy z odrzuceniem rozkazów czy działaniem wbrew rozkazom (chodzi o osoby którym ufa). Normalnie to Zasady > Rozkazy, ale nie tu.
* Flawia nie potrafi zostawić osoby w potrzebie "tak po prostu". Dla ratowania osób wobec których jest lojalna - zaryzykuje spalenie misji.
* Flawia po prostu nie nadaje się do dowodzenia. Nie ogarnia wysokopoziomowych planów strategicznych. Radzi sobie z poziomem operacyjnym i z adaptacją empatyczną, ale nie z abstrakcją dowodzenia.

### Specjalne

* .

## Inne
### Wygląd

.

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | ma nadzieję, że Tomasz Sowiński pomoże jej w karierze/finansach, ale Tomasza porwano na Gwiezdnym Motylu. Przymiliła się do Jolanty Sowińskiej, zdobyła informacje od Nataniela Morlana i poświęciła reputację by chronić Ducha (Antonellę). | 0108-10-20 - 0108-10-25 |
| 210813-szmuglowanie-antonelli       | współpracując z Lucjuszem doprowadziła do tego, że Antonelli Pustogor naprawi Wzór. Wyflirtowała z sanepidowcem dostęp do wiewiórki i zintegrowała jej krew z krwią Antonelli, skutecznie gubiąc Morlana i jego łowców nagród. | 0108-12-07 - 0108-12-19 |
| 210820-fecundatis-w-domenie-barana  | zaczęła od flirtu zbierającego info co się dzieje w okolicy, potem psychotropowała Damiana i skończyła na tym, że magią (wspierana przez Antonellę i Esuriit) syntetyzowała skorpioidami pajęczynę do neutralizacji Strachów. Wymęczyło ją to, ale dała radę. | 0108-12-30 - 0109-01-02 |
| 210616-nieudana-infiltracja-inferni | Elena kiedyś wpakowała ją w GIGANTYCZNE długi. Sowińscy ją wykupili i ona jest teraz infiltrating agent ("Bond"). Teraz miała zajmować się Rolandem, ale Roland chciał na Infernię, więc ona "zinfiltrowała" Infernię. Próbowała się wybronić lub uciec, ale nie była w stanie. Finalnie - dołączyła do Inferni pod pretekstem "infiltracji i szukania Anastazji". | 0111-11-22 - 0111-11-27 |
| 210825-uszkodzona-brama-eteryczna   | dostała polecenie od Arianny by oczarować Blocha. Odmówiła, bo Orbiter nie ma intryg przeciw Orbiterowi. Arianna przykładami ją złamała do współpracy. | 0111-11-30 - 0111-12-03 |
| 210901-stabilizacja-bramy-eterycznej | uwiodła i przekonała Gilberta Blocha do przekazania Inferni tajnych danych sił specjalnych. Jest jej z tym źle. | 0111-12-05 - 0111-12-08 |
| 211020-kurczakownia                 | akcja infiltracyjna Kultu Esuriit z Eleną. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Elenie się uciec. | 0112-01-18 - 0112-01-20 |
| 211027-rzieza-niszczy-infernie      | w wyniku Krwawej Emisji Eleny i działań simulacrum Martyna wyssało ją w kosmos z laboratorium przy K1. Jej los się na zawsze rozdzielił z Orbiterem. | 0112-01-22 - 0112-01-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | poświęciła reputację i nawet trafiła do aresztu za dziką imprezę z ostrymi środkami psychoaktywnymi. Pikanteria - zrobiła to dla misji i by chronić Tomasza Sowińskiego. | 0108-10-25
| 210820-fecundatis-w-domenie-barana  | zawsze nosi przy sobie psychotropy "na miło i wesoło" oraz środki unieszkodliwiające. | 0109-01-02
| 210616-nieudana-infiltracja-inferni | oddelegowana przez Anastazego Sowińskiego na Infernię jako, że "skutecznie ją zinfiltrowała a Infernia szuka Anastazji". | 0111-11-27
| 210616-nieudana-infiltracja-inferni | absolutna nienawiść do Eleny. Zemści się na niej. Zniszczy ją. | 0111-11-27
| 211020-kurczakownia                 | TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne. | 0112-01-20
| 211027-rzieza-niszczy-infernie      | wyssana w kosmos. Nie zginęła, ale jej los i los Orbitera się na zawsze rozdzieliły. | 0112-01-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Elena Verlen         | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Klaudia Stryk        | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Eustachy Korkoran    | 4 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia)) |
| Antonella Temaris    | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Martyn Hiwasser      | 3 | ((210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Kamil Lyraczek       | 2 | ((210616-nieudana-infiltracja-inferni; 211020-kurczakownia)) |
| Leona Astrienko      | 2 | ((210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Infernia          | 2 | ((210825-uszkodzona-brama-eteryczna; 211020-kurczakownia)) |
| Otto Azgorn          | 2 | ((211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Tomasz Sowiński      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Izabela Zarantel     | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Marian Tosen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Roland Sowiński      | 1 | ((210616-nieudana-infiltracja-inferni)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| TAI Rzieza d'K1      | 1 | ((211027-rzieza-niszczy-infernie)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Vigilus Mevilig      | 1 | ((211020-kurczakownia)) |
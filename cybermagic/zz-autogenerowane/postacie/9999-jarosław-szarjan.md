---
categories: profile
factions: 
owner: public
title: Jarosław Szarjan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211208-o-krok-za-daleko             | dowódca Stoczni Neotik. Skupiony na technologii, nie na polityce; trochę zbyt ufny ludziom, ale dobry dowódcom. Zapewnił Inferni azyl po jej anomalizacji używając technologii Neotik. Rozważa tematy spokojnie i logicznie. | 0112-02-19 - 0112-02-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Szarjan         | 1 | ((211208-o-krok-za-daleko)) |
| Arianna Verlen       | 1 | ((211208-o-krok-za-daleko)) |
| Diana d'Infernia     | 1 | ((211208-o-krok-za-daleko)) |
| Elena Verlen         | 1 | ((211208-o-krok-za-daleko)) |
| Eustachy Korkoran    | 1 | ((211208-o-krok-za-daleko)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Klaudia Stryk        | 1 | ((211208-o-krok-za-daleko)) |
| Leona Astrienko      | 1 | ((211208-o-krok-za-daleko)) |
| OO Infernia          | 1 | ((211208-o-krok-za-daleko)) |
| Wawrzyn Rewemis      | 1 | ((211208-o-krok-za-daleko)) |
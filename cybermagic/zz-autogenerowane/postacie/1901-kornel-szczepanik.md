---
categories: profile
factions: 
owner: public
title: Kornel Szczepanik
---

# {{ page.title }}


# Read: 

## Postać

### Koncept (3)

* "ukryty" kolekcjoner artefaktów
* właściciel parku rozrywki
* mag-rezydent Samoklęski

### Motywacja (gniew, zmiana, sposób) (3)

* "ci głupcy sami zrobią sobie krzywdę"
* troska o ludzi ze swojego terenu

### Wyróżniki (3)

* znany specjalista od artefaktów

### Zasoby i otoczenie (3)

* Park rozrywki
* Alfred - butler
* Monitoring (żeby mu artefaktów nie ukradli)

### Magia (3)

#### Gdy kontroluje energię


#### Gdy traci kontrolę


#### Powiązane frakcje

{{ page.factions }}

## Opis


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190115-skazony-pnaczoszpon          | Z pomocą Alfreda odpędził pnączoszpona atakującego jego dom i zidentyfikował problem zarazy. Wezwał terminusów pustogorskich na pomoc. | 0110-01-06 - 0110-01-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190115-skazony-pnaczoszpon)) |
| Alfred Bułka         | 1 | ((190115-skazony-pnaczoszpon)) |
| Kornelia Weiner      | 1 | ((190115-skazony-pnaczoszpon)) |
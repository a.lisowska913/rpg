---
categories: profile
factions: 
owner: kić
title: Daria Czarnewik
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach



### Co się rzuca w oczy

* Około 40-tki
* No-nonsene
* Brak widocznych augumentacji

### Jak sterować postacią

No nonsense, jest problem, trzeba go rozwiązać. Inżynieria zwykle może pomóc, nawet jeśli to problem z morale.
Inżynierami dowodzi poprzez przykład i dając im szansę na sukcesy.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* 

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* AKCJA: 
* AKCJA: 
* AKCJA: 
* AKCJA: 
* CECHA: 
* COŚ: 
* COŚ: 

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: 
* CORE LIE: 
* 
* 
* 
* 

### Serce i Wartości (3)

* Wartości
    * TAK: Self-direction ()
    * NIE: 
* Ocean
    * E:, N:, C:, A:, O:
	* 
	* 
    * 
* Silnik
	* 
	* 

### Magia (3M)

#### W czym jest świetna

* Naprawa i utrzymanie wszelkich statków kosmicznych

#### Jak się objawia utrata kontroli

* 

## Inne

### Wygląd

.

### Coś Więcej

Urodzona na K1, tam się wychowała. Próżniowiec, świetnie czuje się w nieważkości. 
Ma pomniejsze wszczepy wspomagające pamięć (wszystkie możliwe blueprinty pod ręką), ale nie dała sobie wszczepić niczego bardziej zaawansowanego niż moduł pamięci i AR.
Choć urodziła się i wychowała na K1 i ma tam wielu znajomych, nie lubi tam przebywać i unika tego jak może.
Zna i lubi się z wieloma inżynierami na różnego rodzaju statkach.


### Endgame

* ?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 23 lata, inżynier na salvagerze; analizowała dokładnie derelict Okarantis, opracowała nieudaną intruzję a potem udaną intruzję. Wbiła się do manifestu cargo Okarantis, potem określiła serię eksplozji na fault lines. | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | wykryła bawiąc się zabawkami do detekcji BIO 'węgorze' w wodzie, natychmiast zrobiła alarm i nadzorowała operację pozbycia się niebezpiecznej wody. | 0090-04-19 - 0090-04-23 |
| 221113-ailira-niezalezna-handlarka-woda | wrobiła Kaspiana w dowodzenie załogą w zastępstwie Nastii. Złożyła z serii dostawców najtańszy akceptowalny model Hiyori. Dużo zajmowała się rzeczami technicznymi i szpiegowała Kaspiana. Iga za nią negocjowała. | 0090-04-24 - 0090-04-30 |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | w cywilnej części Kontrolera Pierwszego, przydzielona na Królową jako chief engineer. Przejęła kontrolę nad inżynierią, natychmiast kazała odbudować to co ważne, podjęła niepopularne decyzje (m.in. rozmontowanie toru do wyścigu psów). | 0100-03-08 - 0100-03-14 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | uruchomiła silniki Królowej i Królowa wyszła poza kontrolę; z trudem odzyskała kontrolę nad jednostką. CO TU SIĘ DZIEJE?! | 0100-03-16 - 0100-03-18 |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | wykryła scrambler TAI i inne rzeczy na powierzchni Królowej, potem wykorzystała to jako okazję do ćwiczeń. Reanimowała Semlę. Odzyskuje inżynieryjną kontrolę nad statkiem. | 0100-03-19 - 0100-03-23 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | wprawnie zrestartowała Królową gdy pojawiła się awaria i przeszła na manualne. Królowa jest w podłym stanie, ale Daria krok po kroku ją doprowadza do działania. | 0100-03-25 - 0100-04-06 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | uruchomiła statek do pełnego działania. Po długiej pracy Królowa jest kontrolowana i będzie działać. Więc - przesuną ją gdzieś indziej XD. | 0100-04-10 - 0100-04-17 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | zaproponowała Ariannie inne blueprinty do Astralnej Flary; cywile nie chodzą na sprzęcie Orbitera. Po odkryciu, że Elena generuje ogromne Skażenie jako pilot rekalkulowała Flarę by zwiększyć jej wydajność. Skutecznie włączyła ograniczniki gdy Elena przekroczyła parametry lotu. Potwierdziła próbę porwania z nazwisk. | 0100-05-30 - 0100-06-05 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | wyczaiła, że agent Lodowca Alan może i był lokalsem ale jest zbyt pro-Orbiterowy; pozyskała doskonałą planetoidę od Leo i wyjaśniła jak działa stacja Nonarion; wykorzystała manewr Flary by rozpuścić wodę i odwrócić uwagę anomalnych asteroid od Athamarein. | 0100-07-11 - 0100-07-14 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | przeprowadziła plan i procedurę naprawy Hadiah Emas tym co miała; duży koszt surowcowy, ale dobra okazja do pomocy. ABSOLUTNIE nie zgadza się na skrzywdzenie Mirtaeli i próbowała przekonać pośrednio komodora, by nie Ograniczył Mirtaeli. Przyjaciółka Ellariny, pomogła ją zintegrować z Astralną Flarą. | 0100-07-15 - 0100-07-18 |
| 221130-astralna-flara-w-strefie-duchow | ostrzegła Leo przed tym, że Orbiter COŚ robi z tymi neikatiańskimi TAI. Pozyskała dane o Strefie Duchów. Próbowała reanimować TAI Zarraleę z Isigtand; niestety, nie udało jej się - zbyt zniszczona (bardzo wbrew sobie, nie pozwoli na jej Ograniczenie). Przerażona powstaniem nekroTAI. | 0100-08-07 - 0100-08-10 |
| 221214-astralna-flara-kontra-domina-lucis | wraz z Ellariną opracowuje sygnał mający za zadanie przekonać noktian do poddania się. Retransmituje sygnał z Orbitera o NekroTAI i wielką mowę Lodowca by Nonarion miał dowody jak groźny i bezwzględny i niemoralny jest Orbiter a zwłaszcza Lodowiec. | 0100-08-11 - 0100-08-13 |
| 221221-astralna-flara-i-nowy-komodor | ostrzegła Ariannę o tym, że najpewniej squatterzy się pojawili w Kazmirian; pomogła opracować plan odstraszania bezkrwawego i sojuszu ze squatterami. Sprzedała to jako 'Orbitera sie nie okrada'. Wstawiła się za Sargonem u Arianny. | 0100-09-09 - 0100-09-12 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | lokalny ekspert od Biur HR; wie co to jest. Skutecznie ochroniła Flarę i Athamarein przed integracją psychotroniczną z Nox Ignis budując odpowiednie ekrany z pomocą Mai. | 0100-09-15 - 0100-09-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę. | 0100-04-17
| 221130-astralna-flara-w-strefie-duchow | Arianna wie, że Daria się starała w odbudowie Zarralei. Ale Lodowiec ma podejrzenia, że Daria go sabotowała. | 0100-08-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 9 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 7 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 6 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Erwin Pies           | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 5 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| OO Królowa Kosmicznej Chwały | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Kaspian Certisarius  | 4 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221221-astralna-flara-i-nowy-komodor)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
---
categories: profile
factions: 
owner: public
title: Ksenia Kirallen
---

# {{ page.title }}


# Read: 

## Paradoks

Medyk zadający rany, detektyw bez oczu i noktianka walcząca za Astorię.

## Motywacja

### O co walczy

* Astoria. Szczeliniec. Orbiter. Ten teren, ta kultura-to przyszłość która powinna być
* każdy ma swoje miejsce w egzystencji i ma je spełniać
* zawsze patrz w przyszłość, optymalizuj następne pokolenia a nie siebie

### Przeciw czemu walczy

* noktiańskie kłamstwa; infekcja kultury Noktis w Astorię
* naruszenia zasad, honoru i powinności roli osoby na Astorii - masz swoje miejsce.
* wszelkie formy przestępstw - zwłaszcza nadużycie zaufania

## Działania

### Specjalność

* augmentacje: siłowe, stabilizacyjne, detekcyjne
* po śladach do celu-detektor i detektyw wysokiej klasy
* dobry medyk frontowy-ustabilizuje, utrzyma przy życiu, odkazi
* ulubioną bronią są dwa szybkostrzelne pistolety o dużym odrzucie
* potrafi się neurosprząc z większością pojazdów

### Słabość

* nienawiść do Noktis i Eternii sprawia, że zaczyna śledztwo od nich i idzie za daleko - zada niepotrzebne cierpienie
* brak elastyczności - jej światopogląd robi z niej agenta wydziału wewnętrznego, ale kiepskiego polityka lub terminusa lokalnego
* wrażliwa na EMP
* brak magii aktywnej - przeniosła energię w biowspomagania
* jej skuteczność jest okrutna i wygląda strasznie - trudno o sojuszników

### Akcje

* podejrzliwa, zawsze ma domniemanie winy
* fanatyczna, skrupulatna i cierpliwa
* zawsze eleganka; pod maską arystokratki kryje się inkwizytor
* lubi wzbudzać strach-jej zdaniem każdy ma coś do ukrycia
* lubi pomagać dzieciom i osobom starszym- regularnie w wolontariacie
* nie chce mieć niczego swojego, bo wtedy ma coś do stracenia

### Znaczące czyny

* uratowana przez Orbiter z niewoli w Eternii
* odmówiono jej transferu do Trzeciego Raju, ale zaakceptowano do Pustogoru; tu jest dużo noktian i ktoś musi mieć na nich oko
* oślepiona przez EMP, zdjęła przeciwników wbudowaną bronią
* regularnie pomaga jako wolontariuszka w Zamku Weteranów
* podczas Szturmu, użyła działa servara dzięki augmentacjom; połamało ją.
* rozbiła szajkę terminuskich przemytników w Pustogorze, po śladach do celu
* ze szczególną pasją niszczy wszystko czego dotknie Noktis lub Kajrat
* strachem, stymulantami i siłą ognia wymusiła sprawną ewakuację przy pożarze
* przekonała starego terminusa- weterana do eutanazji, gdyż był za drogi dla rodziny i dla Zamku. Wbrew życzeniom wszystkich.

## Mechanika

### Archetypy

terminus medyk, cyborg, neurosprzężony pilot, terminus wydziału wewnętrznego

### Motywacje

.

## Inne

### Wygląd

* blada karnacja, liczne blizny, zintegrowane cyber-oczy (6 asymetrycznych)
* elegancki czarno-stalowy strój w cywilu, mundur ogólnie
* rękawiczki ukrywają cyberwspomagania i iniektory
* jasnoszare, krótko obcięte włosy; nieliczne między bliznami


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 16 lat; tyci młodsza od Klaudii. Piekielnie wręcz odważna. Urwisowata - miała szlaban, bo zgubiła się w Nieskończonym Labiryncie pod Skrzydłem Loris AMZ. Stworzyła z przyjaciółmi zaklęcie ukrywające nieobecność kogoś przed magią (przed dyrektorem). Oddała ten projekt grupie, by wspólnie opracować Beacon ściągający Agenta Esuriit do Labiryntu. | 0083-10-13 - 0083-10-22 |
| 210926-nowa-strazniczka-amz         | 16 lat; imprezowy bloodhound, pogodna iskierka; pojechała po Trzewniu (co wpadł bez pukania do pokoju dziewczyn), po czym pojechała do Zamku Weteranów zająć się tymi, którzy już nie mogą walczyć. Bardzo opiekuńcza i pogodna. | 0084-06-14 - 0084-06-26 |
| 211009-szukaj-serpentisa-w-lesie    | już 17 lat (miała urodziny); MVP - zgłosiła się na ochotniczkę do przekonania serpentisa, nie dała się sterroryzować (mimo grozy) i przekonała serpentisów do oddania się w ręce Astorii. I ustabilizowała Edelmirę. | 0084-11-13 - 0084-11-14 |
| 211010-ukryta-wychowanka-arnulfa    | 17 lat; jeszcze nie doszła do siebie (krzyczy w nocy) po akcji z serpentisami. Mimo, że się boi - chce iść chronić Klaudię przed niebezpieczeństwem. Ona i Felicjan zostali parą. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | lojalna przyjaciółka Klaudii; dała się złapać terminusowi by odwrócić uwagę od Teresy na prośbę Klaudii nie wiedząc o co chodzi. Wie, że Teresa jest noktianką i chce jej mimo wszystko pomóc i ją zsocjalizować. Klaudia BARDZO docenia. | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | z Klaudią budują petycję by weterani noktiańscy też mieli Dom Weterana. Nieskutecznie socjalizuje Teresę - ta nie chce iść na zakupy bez pieniędzy. | 0084-12-20 - 0084-12-24 |
| 220119-sekret-samanty-arienik       | próbuje na podstawie krwi Samanty zbudować mechanizm niszczenia karaluchów; skończyło się silnym backslashem. Samanta nie ma krwi organicznej i magia zadziałała _źle_. Klaudia musiała ją spuryfikować. | 0085-07-26 - 0085-07-28 |
| 190906-wypadek-w-kramamczu          | inkwizytorka i mistrzyni śledztw. Chce zniszczyć Włóknin po tym jak się dowiedziała o sytuacji; ale uspokoiła ją Pięknotka i Kasjopea. Buduje śledztwo w tej sprawie. | 0110-06-14 - 0110-06-15 |
| 190912-rexpapier-i-wloknin          | nieubłagana terminuska skłonna do zniszczenia Włóknina, Rexpapier, czegokolwiek. Wycofana przez Centralę, bo tu wystarczy subtelność. | 0110-06-17 - 0110-06-21 |
| 191103-kontrpolowanie-pieknotki-pulapka | podeszła do tematu Małmałaza z charakterystyczną dla siebie łagodnością i subtelnością. Pięknotka zatrzymała ją, zanim Ksenia zantagonizowała cały świat. Ksenia i Pięknotka współpracują. | 0110-06-20 - 0110-06-27 |
| 200616-bardzo-straszna-mysz         | wzięła operację 'potwór w okolicach terenu Esuriit', ale Pięknotka odepchnęła ją z tego przypisując Gabriela i Laurę. Napisała odpowiednie rzeczy do papierów. | 0110-09-14 - 0110-09-17 |
| 211026-koszt-ratowania-torszeckiego | KIEDYŚ miała konia z pozytywną energią magiczną. Po jej "wypadku" oddała go na kampus uczelni. Bo tu jest bezpieczny i może pomagać innym. | 0111-07-28 - 0111-08-01 |
| 211127-waśń-o-ryby-w-majklapcu      | oddelegowywana do wszystkich małych waśni pomiędzy firmami, bo jest absolutnie przerażająca i jeśli to coś nieważnego, NIKT JEJ NIE CHCE. 3 mc temu rozwiązała problem z Paprykarzem Z Majkłapca. A teraz pojawił się inny i jej nie wezwano. ALE! Jak tylko dostała dowody, że działa tam mafia (Stella Amarkirin) to ruszyła do działania. | 0111-08-15 - 0111-08-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | w ogromnym szoku po tym ataku Esuriit. Dariusz Drewniak był jej ulubionym nauczycielem. Od tej pory odwiedza regularnie Dom Weteranów. | 0083-10-22
| 211009-szukaj-serpentisa-w-lesie    | trauma związana z zastraszeniem przez serpentisa noktiańskiego (Agostino). Ale była w stanie funkcjonować poprawnie, mimo, że następne pół roku miała koszmary i problemy ze spaniem / majaki. | 0084-11-14
| 211010-ukryta-wychowanka-arnulfa    | została parą z Felicjanem Szarakiem po wydarzeniach z serpentisami w Lesie Trzęsawnym. | 0084-12-12
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | mieszka w akademiku AMZ z Klaudią i Teresą. | 0084-12-15
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | ma dostęp do "pokoju schadzek" w akademiku AMZ tylko dla niej (czyli chwilowo też dla Felicjana ;p) | 0084-12-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 7 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Mariusz Trzewń       | 6 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| Arnulf Poważny       | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Felicjan Szarak      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Talia Aegis          | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Teresa Mieralit      | 4 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211026-koszt-ratowania-torszeckiego)) |
| Strażniczka Alair    | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Dariusz Kuromin      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Kasjopea Maus        | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Pięknotka Diakon     | 2 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Karolina Terienak    | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Laura Tesinik        | 1 | ((200616-bardzo-straszna-mysz)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Marysia Sowińska     | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Minerwa Metalia      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Wiktor Satarail      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
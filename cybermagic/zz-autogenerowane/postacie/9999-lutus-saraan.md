---
categories: profile
factions: 
owner: public
title: Lutus Saraan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | noktianin, który pnie się wśród przestępczości by móc uratować swojego 'brata'. Ich wspólnym hasłem jest Alivia Nocturna. Zaufał Olgierdowi Drongonowi, by ten pomógł odzyskać jego brata. | 0096-05-13 - 0096-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Olgierd Drongon      | 1 | ((221122-olgierd-lowca-potworow)) |
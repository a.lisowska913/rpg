---
categories: profile
factions: 
owner: public
title: Lia Sagabello
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | nie ma budynku, do którego się nie wkradnie (security tester). Świetna obserwatorka; wyczaiła powiązania duchy/Damian oraz znalazła Damianowi pracę. | 0109-10-14 - 0109-10-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | się zaprzyjaźniła z Karoliną; najchętniej grają razem. Biedny Damian rzadziej widzi dziewczynę niż Lia ;-). | 0109-10-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Podpalnik     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Karolina Erenit      | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
---
categories: profile
factions: 
owner: public
title: SLX Gwiezdny Motyl
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | statek Luxuritias, ogromny i ufortyfikowany rejsowiec. Podróżuje między Astorią, Neikatis, Valentiną. Miejsce "zbrodni" - porwanie Sowińskiego przez noktian, miragent szukający wolności i Nataniel polujący na Ducha. | 0108-10-20 - 0108-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Flawia Blakenbauer   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Jolanta Sowińska     | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tomasz Sowiński      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
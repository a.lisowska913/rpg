---
categories: profile
factions: 
owner: public
title: Zuzanna Kraczamin
---

# {{ page.title }}


# Generated: 



## Fiszki


* medical + monsterform | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora) | @ 230124-kjs-wygrac-za-wszelka-cene
* nie jest stąd; z przyjemnością robi nieetyczne stwory | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | lekarz Koszarowa Chłopięcego (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora). Odkryła, że "doping" polega na pełnej rekonfiguracji hormonalnej. Chce pomóc to rozwiązać, ale nie wie z kim współpracować. | 0095-06-23 - 0095-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
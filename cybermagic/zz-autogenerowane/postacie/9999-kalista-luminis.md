---
categories: profile
factions: 
owner: public
title: Kalista Luminis
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka płomieni, savaranka, CES Coruscatis | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  -0+0- |Robi co każą;;Nie znosi być w centrum uwagi| VALS: Family, Security >> Stimulation| DRIVE: Ujawnić prawdę o Trianai) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Te wszystkie zniszczenia, te śmierci - to Wasza wina!!!" | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | savarańska czarodziejka płomieni z Coruscatis; (ENCAO:  -0+0- |Robi co każą;;Nie znosi być w centrum uwagi| VALS: Family, Security >> Stimulation| DRIVE: Ujawnić prawdę o Trianai). Duchowa przywódczyni Coruscatis, jedyna zdolna do spalenia Trianai. Pragnie współpracować z neikatianami i znaleźć bezpieczne miejsce dla ich małej grupki noktian. | 0093-02-10 - 0093-02-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Celina Lertys        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalia Awiter         | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| OO Infernia          | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Rafał Kidiron        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ralf Tapszecz        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
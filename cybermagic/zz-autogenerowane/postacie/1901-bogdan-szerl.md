---
categories: profile
factions: 
owner: public
title: Bogdan Szerl
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Medyk i doktor Łysych Psów, też mindwarp i ogólnie - neuronauta. Pasjonuje się w kontroli umysłów i dominacji innych. Nie jest wyznawcą Saitaera, ale podziela jego ideały. Na krótkiej smyczy Moktara. Naukowiec wysokiej klasy. Potrafi walczyć, acz nie jest w tym świetny.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* KNOWLEDGE/ADAPTABILITY; osiągnąć perfekcję taką jak głosi Saitaer; rekonstruktor i mindwarp - częste testowanie ludzkich granic
* CONTROL/BEAUTY; posiadać piękne, perfekcyjnie kontrolowane kształty ludzkie; zdobywa jeńców i ofiary i przekształca ich w lalki
* uśmiechnięty, lekko nerwowy, okrutny, karmi się dominacją, często się oblizuje

### Wyróżniki (3)

* Mindwarp: potrafi każdego złamać, ulojalnić czy przekształcić.
* Lekarz i naukowiec: skupia się na szybkim postawieniu bardziej niż na ograniczeniu cierpienia. Bardzo skuteczny w odnajdowaniu nietypowych rozwiązań
* Jeśli zostaje sam na sam z kimś i ta osoba jest w jego mocy, to bardzo szybko robi z tej osoby swojego wiernego sługę
* Mając trudny problem medyczny potrafi znaleźć bardzo skuteczne rozwiązanie. Jest niemoralne, ale bardzo skuteczne

### Zasoby i otoczenie (3)

* liczne artefakty paraliżujące, unieruchamiające, dominacyjne
* kolekcja upiornych holokostek Mausów; doskonałych na różne okazje
* stary kompleks medyczny; na terenach Skażonych na wschód od Cieniaszczytu. W połowie zakopany pod ziemią, ma opcje portali.

### Magia (3)

#### Gdy kontroluje energię

* dostosowywanie ciała i umysłu do swojej woli: jest to wyjątkowo unikalny sposób magii medycznej...
* wszelkie formy unieruchomienia bądź spętywania swojej ofiary, czy to fizycznie czy mentalnie
* magia skupiająca się na efektach takich jak: dominacja, kontrola, uwielbienie

#### Gdy traci kontrolę

* przebłyski szaleństwa; czy to u siebie czy innych - prawdziwe pragnienia czy JEGO pragnienia manifestują się w formie niemożliwych do kontroli impulsów

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | przerażający mindwarp Łysych Psów. Przekształcił sobie Pięknotkę i uwolnił ją spod wpływów terrorforma. Ma własny harem płci obojga. Żyje dominacją i znajdowaniem granic. Dużo wie o terrorformie Saitaerze. | 0109-11-04 - 0109-11-10 |
| 181218-tajemniczy-oltarz-moktara    | odpowiednio wysłał sygnał do Pięknotki i wpadł we współzależność z nią. Powiedział jej o ołtarzu Arazille Moktara i scementował swój hold na Pięknotce. | 0109-11-10 - 0109-11-12 |
| 181225-czyszczenie-toksycznych-zwiazkow | pragnął zadowolić siebie i Moktara i przygotował Pięknotkę do walki z Dominautem. Skończył rozszarpany przez Moktara i żywy dzięki energii Saitaera. | 0109-11-12 - 0109-11-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | jest zauroczony Pięknotką; jest jego ukochaną zabawką. Chce ją z powrotem, ale chce też by nie była tylko u niego. | 0109-11-10
| 181218-tajemniczy-oltarz-moktara    | potrzebuje Pięknotki. To nie jest miłość, to jest uzależnienie. Spirala. | 0109-11-12
| 181225-czyszczenie-toksycznych-zwiazkow | skończył na "reedukacji" u Moktara za to co zrobił Pięknotce. | 0109-11-16

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | odzyskać swoją Pięknotkę. Przywrócić ją do swoich. | 0109-11-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Atena Sowińska       | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Pięknotka Diakon     | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Julia Morwisz        | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Mirela Niecień       | 1 | ((181216-wolna-od-terrorforma)) |
| Moktar Gradon        | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Moktar Grodan        | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Romuald Czurukin     | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Waleria Cyklon       | 1 | ((181218-tajemniczy-oltarz-moktara)) |
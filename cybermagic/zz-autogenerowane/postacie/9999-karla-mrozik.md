---
categories: profile
factions: 
owner: public
title: Karla Mrozik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181230-uwiezienie-saitaera          | głównodowodząca Pustogoru; informacja od Pięknotki o Saitaerze była ostatnim ogniwem brakującym Karli do zrozumienia sprawy i wydania rozkazu przejęcia. | 0109-12-06 - 0109-12-08 |
| 190120-nowa-minerwa-w-nowym-swiecie | przekazała Minerwę pod opiekę Pięknotce i zaakceptowała, że Minerwa osiądzie raczej w Zaczęstwie. Niech Tymon ma wsparcie w Zaczęstwie (terminusi). | 0110-01-21 - 0110-01-25 |
| 190422-pustogorski-konflikt         | raz na jakiś czas organizuje w Pustogorze kryzys, by co bardziej zawadiackich wolnościowców z Miasteczka wygnać do Wolnych Ptaków. Pustogor przede wszystkim. | 0110-03-29 - 0110-03-30 |
| 190424-budowa-ixionskiego-mimika    | bez westchnięcia smutku autoryzowała plan Pięknotki wrobienia niewinnego Miasteczkowca i zestrzelenie go działami Wagner tylko po to, by Pustogor odzyskał spokój. | 0110-03-30 - 0110-04-01 |
| 190429-sabotaz-szeptow-elizy        | trzeba zaryzykować zdrowie Minerwy? Ok. Trzeba być opieprzoną przez Lucjusza? Ok. Idzie za celami Barbakanu i o nic innego nie dba. | 0110-04-06 - 0110-04-09 |
| 190503-bardzo-nieudane-porwania     | amused commander; Pięknotka dała jej możliwość wbicia bolesnej szpili elitarnym Kirasjerom. Chroni Minerwę i Nikolę przed szponami Orbitera. | 0110-04-14 - 0110-04-16 |
| 200418-wojna-trzesawiska            | w niefortunnym położeniu - chce chronić ludzi (bombardowanie z orbity) i chce chronić teren (nie dotykać Trzęsawiska). Decyduje o negocjacji z Satarailem. | 0110-08-12 - 0110-08-17 |
| 200623-adaptacja-azalii             | z ciężkim sercem, wysłała Pięknotkę i Minerwę na ratowanie Orbitera - nie chciała przed Trzęsawiskiem ujawniać Minerwy, ale nie ma wyjścia. | 0110-09-22 - 0110-09-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Minerwa Metalia      | 6 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200623-adaptacja-azalii)) |
| Erwin Galilien       | 4 | ((190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Olaf Zuchwały        | 3 | ((190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Lucjusz Blakenbauer  | 2 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy)) |
| Wiktor Satarail      | 2 | ((200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Alan Bartozol        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Damian Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Gabriel Ursus        | 1 | ((200418-wojna-trzesawiska)) |
| Ignacy Myrczek       | 1 | ((200418-wojna-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Kreacjusz Diakon     | 1 | ((181230-uwiezienie-saitaera)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Sabina Kazitan       | 1 | ((200418-wojna-trzesawiska)) |
| Tymon Grubosz        | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
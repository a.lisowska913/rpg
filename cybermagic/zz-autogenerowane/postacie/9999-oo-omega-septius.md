---
categories: profile
factions: 
owner: public
title: OO Omega Septius
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210929-grupa-ekspedycyjna-kellert   | superpancernik Orbitera; przesunięty przez Termię w okolice Bramy Kariańskiej na wypadek poważnych problemów. | 0112-01-07 - 0112-01-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Antoni Kramer        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Arianna Verlen       | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Eustachy Korkoran    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Klaudia Stryk        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Leona Astrienko      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
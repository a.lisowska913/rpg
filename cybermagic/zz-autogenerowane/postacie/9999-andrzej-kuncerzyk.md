---
categories: profile
factions: 
owner: public
title: Andrzej Kuncerzyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | silny trener personalny; zainfekowany demonem odpychał od siebie swoją dziewczynę. Dostał wpiernicz od "mafii" i wszystko powiedział. | 0110-10-31 - 0110-11-02 |
| 201215-dziewczyna-i-pies            | myśli, że jest ojcem Patrycji. Torturowany przez Daniela, zorientował się w prawdzie i stanął po stronie Daniela przeciw Patrycji. | 0110-11-11 - 0110-11-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Karolina Terienak    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
---
categories: profile
factions: 
owner: public
title: Lena Kardamacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200219-perfekcyjna-corka            | piękna, drapieżna i nienaturalnie groźna viciniuska; stworzona przez Mateusza Kardamacza jako jego perfekcyjna córka. Po pokonaniu 'ojca', wróciła z nim - jedyną rodziną. | 0110-05-12 - 0110-05-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Szklarska | 1 | ((200219-perfekcyjna-corka)) |
| Ataienne             | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Mateusz Kardamacz    | 1 | ((200219-perfekcyjna-corka)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Sabina Kazitan       | 1 | ((200219-perfekcyjna-corka)) |
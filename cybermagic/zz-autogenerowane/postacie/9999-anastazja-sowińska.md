---
categories: profile
factions: 
owner: public
title: Anastazja Sowińska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200909-arystokratka-w-ladowni-na-swinie | 15-letnia wysoka szlachcianka Aurum o przypadłości nienaturalnie silnych Paradoksów; schowała się w ładowni Tucznika i jej Paradoksy stworzyły tam krainę terroru. Uratowana przez załogę Tucznika (Inferni) z niekończącego się koszmaru. | 0110-12-15 - 0110-12-20 |
| 200916-smierc-raju                  | w bardzo trudnej sytuacji - jej bohaterowie nie są tacy jak myślała, nie ma ubrań i stroju i nie kontroluje energii magicznych. Oddana w opiekę Elenie. | 0110-12-21 - 0110-12-23 |
| 200923-magiczna-burza-w-raju        | zaczyna ufać Elenie, która traktuje ją twardo ale wie, przez co Anastazja przechodzi. Nawet powoli zaczyna pomagać. Wysłała SOS dla Raju do Nikodema z prośbą o wsparcie. | 0110-12-24 - 0110-12-28 |
| 201014-krystaliczny-gniew-elizy     | Elena poświęciła sporo zdrowia, by ją uratować przed zabójcami jej przyjaciela, ochroniarza. Sama jest ranna. Jej świat się rozsypał. | 0111-01-02 - 0111-01-05 |
| 201021-noktianie-rodu-arlacz        | Arianna wytrąciła ją z pasywności; wreszcie zaczęła działać jak Sowińska. Wpierw kazała "Szalonemu Rumakowi" się dostosować, potem pięknie zażądała Diany. Będzie z niej wartościowa arystokratka Aurum. Musiała się "pogodzić" z Nikodemem przez Marię Gołąb, czego NIE CIERPI. | 0111-01-07 - 0111-01-10 |
| 201104-sabotaz-swini                | aresztowała miragenta... przez co zginął astorianin pilnujący aresztanta. Trzeci Raj nie jest pod jej pozytywnym wrażeniem. | 0111-01-13 - 0111-01-16 |
| 201118-anastazja-bohaterka          | miała tylko patronować pojedynkowi Eustachy - Jamniczek, ale się zakochała w Eustachym i skompromitowała publicznie na K1. | 0111-01-22 - 0111-01-25 |
| 201210-pocalunek-aspirii            | załamana tym, że po ostatnim zrobiła pośmiewisko swojemu rodowi. Zakochana w Eustachym. Chce wstąpić do wojska i postawiła się Juliuszowi. | 0111-01-26 - 0111-01-29 |
| 201216-krypta-i-wyjec               | podatna na moc mentalną Krypty, została na pokładzie celem Krypty naprawy Wzoru. Tchórzliwa, ale ufa Ariannie i Zespołowi. | 0111-01-29 - 0111-01-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200909-arystokratka-w-ladowni-na-swinie | Paradoksalnie splątana z Arianną Verlen; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją. | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | śmiertelnie i na całe życie i w ogóle zakochana w Martynie Hiwasserze, jej RYCERZU. | 0110-12-20
| 200909-arystokratka-w-ladowni-na-swinie | boi się zostać sama, bo Paradoksalny Koszmar wróci... | 0110-12-20
| 200923-magiczna-burza-w-raju        | zaakceptowała Elenę jako opiekunkę i strażniczkę. Przekonana, że Arianna chce ją wykorzystać, ale ELENIE na niej zależy. Taka "twarda sympatia". | 0110-12-28
| 201014-krystaliczny-gniew-elizy     | jej świat się rozsypał. Tylko dziadkowi na niej zależy (chyba), Elena chroniąc ją prawie umarła i po raz pierwszy ever została RANNA. ZABÓJCY! | 0111-01-05
| 201014-krystaliczny-gniew-elizy     | już nie ma traumy "nie może zostać sama". Wyleczyła się. Zamknęła się w sobie. | 0111-01-05
| 201021-noktianie-rodu-arlacz        | publiczne (na wizji) potwierdził, że Nikodem i ona współpracowali by znaleźć zdrajcę. Nie może nic już zrobić Nikodemowi przez próbę ratowania noktian. | 0111-01-10
| 201021-noktianie-rodu-arlacz        | dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian. | 0111-01-10
| 201104-sabotaz-swini                | opinia w Trzecim Raju: plan Arianny był dobry, ale Anastazja źle go wykonała. Anastazja ma w Raju złą opinię jako niekompetentna arystokratka Aurum. | 0111-01-16
| 201216-krypta-i-wyjec               | została na Nocnej Krypcie do naprawy Wzoru; nie wie nawet o tym, co się z nią dzieje. | 0111-01-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Eustachy Korkoran    | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Klaudia Stryk        | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Arłacz         | 4 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Elena Verlen         | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Martyn Hiwasser      | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Kamil Lyraczek       | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| AK Nocna Krypta      | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Dariusz Krantak      | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
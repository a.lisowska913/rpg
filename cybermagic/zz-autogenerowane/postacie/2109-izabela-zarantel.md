---
categories: profile
factions: 
owner: public
title: Izabela Zarantel
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Uciekinierka z Eterni która szuka pieniędzy, chwały i sposobów na wyciągnięcie rodziny z Eterni. Kiedyś, oportunistyczna i ambitna dziennikarka. Po Śmierci Inferni, gorliwa wyznawczyni Arianny (w aspekcie Zbawiciela) i propagandzistka Inferni. Nadal zawzięta dokumentalistka i skandalistka, pragnąca rozprzestrzenić Kult Arianny na skraj galaktyki i uwolnić wszystkich - niech niewolnictwo przestanie istnieć.

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* Jeśli może poszerzyć kult Arianny lub pokazać Infernię w dobry sposób, zrobi to. Szuka takich okazji proaktywnie.
* Bardzo proaktywna. Szpera, szuka, działa. Nie zostaje bez kajecika czy komputera. Non stop szkicuje, sprawdza, agreguje.
* Nie ma w niej cienia arogancji. Uczy się od każdego. Nikogo nie lekceważy. Jest wyczulona na słowa każdego. Zbiera opowieści od każdego i z przyjemnością je zapisuje.
* Eterniożerca, nie siedzi cicho wobec żadnych przejawów niewolnictwa i kontroli umysłów. Paradoksalnie nie dotyczy to religii.
* Bardzo zadziorna i waleczna z charakteru jeśli ktoś jej nadepnie na odcisk. Wie, co jej nie pasuje i ma zamiar to powiedzieć. Nie jest bezstronną dziennikarką i takowej nie udaje.
* Głęboko oddana Ariannie, Inferni i Kultowi. Często używa imienia Arianny albo monikera „Aria Vigilus” by dodać sobie odwagi
* Nie chce nawracać na siłę. Nie jest „wojującym misjonarzem”. Probuje znaleźć lepszy dowód, lepszą opowieść. Nie każdy trafi pod Jej opiekę, ale jej rolą jest to, by dać każdemu szansę.
* Zawsze probuje pokazać prawdę. Ok, w odpowiednim świetle, ale fakty są najważniejsze - i przez wzgląd na Arię Vigilus i przez wzgląd na przeznaczenie historyczne.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Po sytuacji z Kurczakownią i Rziezą pomogła Kamilowi stworzyć skuteczniejszą melodię i opowieść Arii Vigilus. On pokazał jej Światło, ona pokazała mu Prawdę. I opracowali Księgę Arii Vigilus.
* .
* .

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: prządka opowieści. Iza potrafi opowiadać i tworzyć opowieści na niesamowitej umiejętności perswazji. Umie przyciągać uwagę i sprzedać prawie wszystko.
* AKCJA: wzmocnienie lub zniszczenie reputacji, pokazanie czegoś w odpowiednim świetle. Potrafi być niesamowicie złośliwa i naprawdę stworzyć coś niesamowicie wyśmiewającego.
* AKCJA: skłania innych do opowieści, do gadania, ludzie uwielbiają się jej zwierzać. Ma niesamowitą cierpliwość
* AKCJA: 
* COŚ: 
* COŚ: 

### Serce i Wartości (3)

* Uniwersalizm
    * Który skrzywdziłeś człowieka poczciwego, nieważne kim jesteś, Twoje czyny będą pokazane jako skandal którym są! Proaktywnie poszuka i złośliwie ujawni.
    * Wszystkie istoty są równorzędne i powinny być równorzędne - ludzie, magowie, arystokraci, TAI. Każdy ma swoją historię i każdą z nich warto usłyszeć. W różnorodności historii jest bogactwo tego świata. Od każdego warto się uczyć.
    * Każda istota zasłużyła na zrozumienie Arii Vigilus. Każda istota zasługuje na szczęście i bezpieczeństwo. Na pokój ducha. Iza spróbuje ukoić każdą istotę która cierpi.
* Bezpieczeństwo
    * Boi się niewolnictwa i ograniczenia woli. Zna to z Eterni. Skupia się na tym, by to było nielegalne wobec wszystkich istot - bo to da jej ochronę której potrzebuje.
    * Pieniądze, wpływy, kredytki, reputacja, rozpoznawalność. Dostęp do zasobów i surowców. To ją zawsze interesuje i ma do tego nosa - chce mieć jak najwiecej. Dla własnego bezpieczeństwa. Dla maksymalizacji Kultu.
    * Doprowadzi do maksymalizacji wpływu kultu Arianny. Wpływ będzie rósł - oraz ilość kultystów będzie rosła. To jest droga do uratowania świata przed koszmarem rzeczywistości i do bycia bezpieczną.
* Pokora
    * Arianna jest inkarnacją Zbawiciela-Niszczyciela. Ona decyduje, kto przetrwa a kto zginie. Ona jest jedyną drogą do sukcesu ludzkiej cywilizacji. A Iza jest osobą, której przeznaczeniem jest sprawić, by świat Ariannę zrozumiał jako Inkarnację którą jest. 
    * „Nieważne, czy mi się uda czy nie. Nie muszę ja wygrać w tym momencie. Muszę spróbować - i prędzej czy później komuś się uda to osiągnąć. Aria Vigilus jest po mojej stronie. ” - w pewien sposób przekonanie o tym, ze jest po właściwej stronie przeznaczenia i ona sama nie ma znaczenia. Znaczenie mają tylko czyny.
    * Nikt - łącznie z inkarnacją Arianny - nie zna prawdy i nie ma monopolu na prawdę i rację. Ale Aria Vigilus chce pomóc nam wszystkim - i dlatego jest naszą najwiekszą nadzieją. Nigdy o tym nie zapominaj i nie pozwól by inni zapomnieli.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Jestem sama. Moja rodzina gnije w Eterni. Powinnam ich ratować, ale nawet nie próbuję. Jestem potworem."
* CORE LIE: "Aria Vigilus jest jedyną odpowiedzią rzeczywistości. Niesprawiedliwość jest i będzie - jedynie Aria Vigilus może oczyścić świat."
* ?

### Magia (3M)

#### W czym jest świetna

* ?

#### Jak się objawia utrata kontroli

* ?

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200819-sekrety-orbitera-historia-prawdziwa | dziennikarka i prządka opowieści na Orbiterze, która stworzyła absolutny hit - "Sekrety Orbitera" ku wielkiej żałości Admiralicji i Inferni. | 0110-11-26 - 0110-12-04 |
| 200909-arystokratka-w-ladowni-na-swinie | kontynuuje "Sekrety Orbitera", wyczuła złoto. Dołączyła do załogi Inferni (teraz: Tucznika). Została ranna przy wybuchach na Tuczniku przez nojrepy. | 0110-12-15 - 0110-12-20 |
| 200916-smierc-raju                  | najpierw Eustachy odwrócił jej uwagę od świńskiego skandalu, potem udokumentowała zniszczenie Trzeciego Raju przez dziwne nojrepy. | 0110-12-21 - 0110-12-23 |
| 200923-magiczna-burza-w-raju        | zmontowała genialne prośby o pomoc plus reportaż, dzięki którym Raj dostanie podstawowe zapasy i możliwości działania. | 0110-12-24 - 0110-12-28 |
| 201021-noktianie-rodu-arlacz        | storzyła świetną opowieść i odcinek o pomocy noktianom ze strony Arianny i Anastazji. | 0111-01-07 - 0111-01-10 |
| 210630-listy-od-fanow               | pochodzi z Eterni; jej rodzina nadal tam jest. Jej udało się uciec. Nienawidzi Eterni. Pomaga Ariannie przekonać Remigiusza Falorina by przekazał Ariannie Eidolona. | 0111-11-10 - 0111-11-13 |
| 210616-nieudana-infiltracja-inferni | wróciła na stałe do załogi Inferni. Będzie pracować nad ciągiem dalszym Sekretów Orbitera. | 0111-11-22 - 0111-11-27 |
| 211114-admiral-gruby-wieprz         | była zmęczona, nie chciała kolejnego wywiadu. Klaudia przekonała ją do pomocy, będzie skandal. Iza pomogła Klaudii opóźnić turniej Gwiezdnego Podboju i zrobiła wywiad z TAI Gruby Wieprz. Ale Rzieza zniszczył Wieprza i zrobił z niego potwora na wizji a z niej zrobił fanatyczkę wolności i opętaną ideałami idiotkę. | 0111-12-11 - 0111-12-16 |
| 210922-ostatnia-akcja-bohaterki     | nienawidzi Eterni - ale stworzyła najlepszy odcinek o Eterni jak to było możliwe. Jej opus magnum. Teraz się szczerze nienawidzi. | 0111-12-19 - 0112-01-03 |
| 211117-porwany-trismegistos         | zrobiła odcinek Sekretów o Eustachym i Klaudii i BARDZO skupiła się na tym by pokazać Klaudię w jak najlepszej stronie by pomóc w oskarżeniach. Wzięła dyshonor na siebie. To najbardziej pomoże Inferni, Klaudii i Ariannie. | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | zrobiła świetną kampanię "dołącz do Inferni" by zdobyć załogę. Poszło jej rewelacyjnie - podniosła zwłaszcza szacun Eustachemu. | 0112-02-14 - 0112-02-18 |
| 211222-kult-saitaera-w-neotik       | pomaga Ariannie w odpowiednim natężeniem efektowności by zrobić najlepszy speech ever. | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | widząc że Infernia umiera i energie przechodzą poza kontrolę przekierowała (z pomocą Klaudii) całość energii na siebie jak kiedyś Martyn. Po czym opuściła Infernię. Bez skafandra. KIA. | 0112-02-28 - 0112-03-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201104-sabotaz-swini                | ALBO poprawi reputację Anastazji (która Iza w sumie uszkodziła), ALBO Sowińscy wsadzą ją do karceru na długie lata. | 0111-01-16
| 211114-admiral-gruby-wieprz         | podjęła decyzję - nie chce być częścią Orbitera czy Aurum itp. Nie chce być freelancerem. Jej czyny - czuje się częścią Inferni. Nie jest już całkowicie niezależna i nie jest jej z tym źle. Oddała część niezależności o którą zawsze walczyła. | 0111-12-16
| 211114-admiral-gruby-wieprz         | traci część dostępów w Orbiterze i część sympatii w Aurum. Kontakty ma, ale nie jest już uznawana za freelancerkę. | 0111-12-16
| 211114-admiral-gruby-wieprz         | osobisty WRÓG tych, którzy chcą korzystać z AI jako narzędzi. "Głupia propagandzistka, goni ambulanse". | 0111-12-16
| 211114-admiral-gruby-wieprz         | przez Rziezę wyszła na fanatyczkę wolności. Zwalcza Eternię. Robi dziwne połączenia Eternia - zniewolone TAI? Ogólnie, "dziwna". | 0111-12-16
| 211020-kurczakownia                 | OGROMNA TRAUMA. Kurczakowanie - rekurczakowanie + Zbawiciel-Niszczyciel. Nie jest w stanie sobie z tym poradzić. Flashbacki itp. Potworność wymaga religii? | 0112-01-20
| 211027-rzieza-niszczy-infernie      | uwierzyła w to, że Arianna Verlen jest prawdziwą inkarnacją aspektu Zbawiciela. To jest jedyne, co ma sens w kontekście tego WSZYSTKIEGO. Jest wyznawcą. To Arianna zażyczyła sobie, by Izabela przetrwała to wszystko - i dlatego Izabela żyje. | 0112-01-27
| 211117-porwany-trismegistos         | stworzyła narrację, że to ona wrobiła Klaudię w hackowanie Rdzenia Sprzężonego na K1. Ma reputację "miłośniczki TAI która nienawidzi Eterni". Ma groźnych wrogów. | 0112-02-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 13 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211114-admiral-gruby-wieprz; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Arianna Verlen       | 12 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Elena Verlen         | 11 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Eustachy Korkoran    | 10 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Leona Astrienko      | 5 | ((200819-sekrety-orbitera-historia-prawdziwa; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Maria Naavas         | 4 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Roland Sowiński      | 4 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Kamil Lyraczek       | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 210616-nieudana-infiltracja-inferni; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 3 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 210922-ostatnia-akcja-bohaterki)) |
| Adam Szarjan         | 2 | ((211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Olgierd Drongon      | 2 | ((210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Rafael Galwarn       | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Antoni Kramer        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Dariusz Krantak      | 1 | ((200923-magiczna-burza-w-raju)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana d'Infernia     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Tosen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Infernia          | 1 | ((211124-prototypowa-nereida-natalii)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
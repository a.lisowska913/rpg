---
categories: profile
factions: 
owner: public
title: TAI Rzieza d'K1
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210209-wolna-tai-na-k1              | UW; przebudzony przez Zefiris system odpornościowy K1 niszczący wolne TAI i zdegenerowane strainy. Nie chce niszczyć niegroźnych, ale musi. Samotny. Mirroruje rozmówcę i jego atrium. Generacja podobna do Zefiris. Pozwolił uciec Neicie, jak długo opuści Kontroler Pierwszy. | 0111-07-02 - 0111-07-07 |
| 210630-listy-od-fanow               | gdy tylko Elena podpięła TAI bazy Eterni na K1 do K1, zniszczył wszystkie jednym ruchem i zastąpił je "dewastatorami". Dał się przekonać Klaudii, by dała twinnowanym ludziom opuścić K1 w ciągu 24h. | 0111-11-10 - 0111-11-13 |
| 211013-szara-nawalnica              | rozwiązał dla Klaudii zagadkę jak działają Piranie i TAI w sektorze Mevilig - niestety, chciał Klaudię (i Infernię) upolować by nie lecieli tam. | 0112-01-12 - 0112-01-17 |
| 211027-rzieza-niszczy-infernie      | chciał usunąć pamięć Inferni o swej obecności; natrafił na Ariannę i wykrył ingerencję Ataienne. Potem współpracował z Arianną i Klaudią jak shit hit the fan. MÓGŁ i POWINIEN wszystkich zabić, ale NIE CHCIAŁ i udało mu się jednak większość uratować. Za co nikt mu nie podziękował. De facto, zabił Infernię. | 0112-01-22 - 0112-01-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 4 | ((210209-wolna-tai-na-k1; 210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Arianna Verlen       | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Elena Verlen         | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Martyn Hiwasser      | 3 | ((210209-wolna-tai-na-k1; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Otto Azgorn          | 2 | ((211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Aleksandra Termia    | 1 | ((211013-szara-nawalnica)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Eustachy Korkoran    | 1 | ((211013-szara-nawalnica)) |
| Flawia Blakenbauer   | 1 | ((211027-rzieza-niszczy-infernie)) |
| Izabela Zarantel     | 1 | ((210630-listy-od-fanow)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Leona Astrienko      | 1 | ((210209-wolna-tai-na-k1)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
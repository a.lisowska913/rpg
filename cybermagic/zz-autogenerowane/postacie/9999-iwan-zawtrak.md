---
categories: profile
factions: 
owner: public
title: Iwan Zawtrak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211127-waśń-o-ryby-w-majklapcu      | 44 lata; właściciel kociarni i miłośnik tych zwierzaków. Nie chce się opłacać mafii, ale gdy mafia wypuściła część jego kotów i je podtruła by były agresywne, zmienił trochę zdanie. Zaufał Karo i Danielowi, powiedział im o co chodzi. Na uboczu, uczciwy, ale trudny w obejściu. | 0111-08-15 - 0111-08-20 |
| 220814-stary-kot-a-trzesawisko      | jak Karo przywiozła doń rannego kota to mu pomógł asap. Jak się zorientował, że to nie pacyfikator a vistermin to chciał mu nadal pomóc, ale kot mu uciekł. Zadzwonił do Karo z prośbą o pomoc... | 0111-10-08 - 0111-10-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 2 | ((211127-waśń-o-ryby-w-majklapcu; 220814-stary-kot-a-trzesawisko)) |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Arkadia Verlen       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Rafał Torszecki      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
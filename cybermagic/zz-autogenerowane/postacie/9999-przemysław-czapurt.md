---
categories: profile
factions: 
owner: public
title: Przemysław Czapurt
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | sierżant Verlenów, mag; kompetentny wiarus, który ściągnął do Holdu Bastion Viorikę i Ariannę za plecami Apolla, by jego plany (jakiekolwiek by nie były) zostały zneutralizowane. Ciepło patrzy na Viorikę i Ariannę, mniej na Apolla (bo spieprzył). | 0092-08-03 - 0092-08-06 |
| 210306-wiktoriata                   | wkręcił Viorikę i Apollo w pojedynek by Viorika przegrała i nauczyła się roli umiejętności społecznych. Potem autoryzował Viorikę do wspólnej operacji z Lucjuszem Blakenbauerem. | 0093-10-21 - 0093-10-30 |
| 210324-lustrzane-odbicie-eleny      | ostrzeżony przez Viorikę o stanie Eleny, natychmiast zabezpieczył wszystkich nieVerlenowych magów na terenie Verlenlandu. Potem z oddziałem unieszkodliwił Elenę. | 0097-07-05 - 0097-07-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Lucjusz Blakenbauer  | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Viorika Verlen       | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Apollo Verlen        | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Arianna Verlen       | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Elena Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Michał Perikas       | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Romeo Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
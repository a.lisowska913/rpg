---
categories: profile
factions: 
owner: public
title: Jan Revlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190709-somnibel-uciekl-arienikom    | czarodziej, półkrwi noktianin; przyjaciel syna Arieników (Stasia). Nielubiany przez Arieników. Urszula Arienik chce jego zguby. | 0110-06-01 - 0110-06-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Pięknotka Diakon     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Tomasz Tukan         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
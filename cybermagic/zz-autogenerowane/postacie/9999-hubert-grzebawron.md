---
categories: profile
factions: 
owner: public
title: Hubert Grzebawron
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | zdecydował się opuścić zarówno piratów jak i Infernię; trzeba ostrzec wszystkich by na pewno nikt nie ośmielił się atakować Inferni. | 0093-03-06 - 0093-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | starszy brat Wojciecha; w odróżnieniu od Wojtka nie szedł ku zaszczytom czy chwale i został liniowym agentem. | 0093-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Eustachy Korkoran    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| OO Infernia          | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rafał Kidiron        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Ralf Tapszecz        | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| SAN Szare Ostrze     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Wojciech Grzebawron  | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
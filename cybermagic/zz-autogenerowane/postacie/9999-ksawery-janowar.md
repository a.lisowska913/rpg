---
categories: profile
factions: 
owner: public
title: Ksawery Janowar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | eks-pirat z Okrutnej Wrony; został piratem bo uciekł przed stalkerką. Słucha się Berdysza i zapewnił, że Wąż nie odleciał bez nich. | 0108-08-02 - 0108-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozpruwacz   | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
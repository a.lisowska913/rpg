---
categories: profile
factions: 
owner: public
title: Rozalia Wączak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200624-ratujmy-castigator           | PO TAI d'Alkaris. Skażona przez Sataraila celem zniszczenia Castigatora; działa jak TAI 3 kategorii. Nie dała rady nawet nanitkami - Eustachy niszczył Castigator wywalając ją w kosmos a Arianna dewastowała jej nanoroje. | 0110-10-15 - 0110-10-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200624-ratujmy-castigator)) |
| Eustachy Korkoran    | 1 | ((200624-ratujmy-castigator)) |
| Klaudia Stryk        | 1 | ((200624-ratujmy-castigator)) |
| Leona Astrienko      | 1 | ((200624-ratujmy-castigator)) |
| Leszek Kurzmin       | 1 | ((200624-ratujmy-castigator)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Castigator        | 1 | ((200624-ratujmy-castigator)) |
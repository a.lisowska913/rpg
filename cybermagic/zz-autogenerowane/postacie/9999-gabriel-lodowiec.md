---
categories: profile
factions: 
owner: public
title: Gabriel Lodowiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* komodor-in-training | @ 221116-astralna-flara-dociera-do-nonariona-nadziei
* ENCAO: 0--+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Niszczenie hipokrytycznego systemu | @ 221116-astralna-flara-dociera-do-nonariona-nadziei
* ENCAO: 0+-+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Procedury, poprawność systemu, kaganek oświaty. | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | komodor Orbitera in-training. Próżniowiec, chudzina; mało ćwiczy i mało je. Całkiem sensowny dowódca Arianny i Kurzmina. Dopakował Ariannie ekstra marines, skonfliktowany między kulturą Orbitera i sytuacją na Nonarionie (neikatiańskie TAI, niewolnictwo itp.). Pod wrażeniem Astralnej Flary - to nie to czego się spodziewał. | 0100-07-11 - 0100-07-14 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | rozsądny i wyważony; pyta Ariannę o jej propozycje i dopasowuje ludzi do zadań (widząc, że Arianna nie chce robić Ograniczenia TAI Mirtaela, dał to ludziom Kurzmina). Od czasu do czasu wpada w furię, ale umie się opanować. Dobrze wysokopoziomowo dowodzi jednostką. Niestety, ma traumę związaną z walką z niekontrolowaną neikatiańską TAI w przeszłości i jest niepowstrzymany w woli niszczenia ich. | 0100-07-15 - 0100-07-18 |
| 221130-astralna-flara-w-strefie-duchow | podszedł do rozbierania Strefy Duchów metodycznie i ostrożnie. Single-minded w celu doprowadzenia noktian do Pax Orbiter. Zupełnie nie nadaje się do przesłuchiwania niegroźnych savarańskich noktianek. Skonfliktowany między "noktianie muszą być zniszczeni" a "nie bądź okrutny, Pax Orbiter". Nie jest przeciwny używania koktajlu chemicznego do przesłuchania Kirei, ale Arianna go przekonała. | 0100-08-07 - 0100-08-10 |
| 221214-astralna-flara-kontra-domina-lucis | dostał z Orbitera za zadanie unieszkodliwienie Xadaara; nie jest fanem samobójstw noktiańskich, ale zrobił przemowę której celem było ich zastraszyć przez nekroTAI. Doprowadził do tego, że udało się uniknąć walki z noktianami. Koło 200 'uratowanych', koło 40 popełniło samobójstwo. Zniszczył swoją reputację i uszkodził reputację Orbitera wśród lokalsów. Ale wygrał. | 0100-08-11 - 0100-08-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | oficjalny protest i skarga od Darii Czarnewik, bo 'Ograniczenie Mirtaeli jest okrutne i jest sprzeczne z celami Orbitera na tym terenie'. | 0100-07-18
| 221130-astralna-flara-w-strefie-duchow | podejrzewa, że Daria Czarnewik sabotuje plan Arianny z reanimacją TAI bo jest sympatyczką TAI. Zero zaufania do Darii. | 0100-08-10
| 221214-astralna-flara-kontra-domina-lucis | przemowa o nekroTAI i że noktianie BĘDĄ służyć, żywi lub martwi, w imię Pax Orbiter. Uszkodził poważnie reputację Orbitera i swoją, ale osiągnął sukces - zdobył bezstratnie Dominę Lucis. | 0100-08-13
| 221221-astralna-flara-i-nowy-komodor | przeniesiony z okolic Anomalii Kolapsu na inne przestrzenie; ogólnie pochwała za dobrą robotę i silna nagana za bardzo niewłaściwe plotki o Orbiterze w okolicach Anomalii Kolapsu | 0100-09-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Daria Czarnewik      | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| OO Astralna Flara    | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| OO Athamarein        | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Arnulf Perikas       | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Ellarina Samarintael | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Kajetan Kircznik     | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Leszek Kurzmin       | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Elena Verlen         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
---
categories: profile
factions: 
owner: public
title: Laurencjusz Sorbian
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | ochroniarz i cień młodych Sowińskich; chciał rozwiązać problemy w formie Aurum (zrób, zapłać) i skończył aresztowany przez Tymona Grubosza. | 0110-10-21 - 0110-10-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | wpierw próbował przekupić Tymona a potem kłócić się z Ksenią. Zdeptany - ZROZUMIAŁ, że Szczeliniec "tak nie działa". Nie cierpi tego miejsca. | 0110-10-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |
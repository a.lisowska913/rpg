---
categories: profile
factions: 
owner: public
title: Aster Sarvinn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | cynical and cold, doesn't want to endanger the team taking Adrianna - however accepts her as she can be of use for noctian team. Created a direct scenario checking Adrienne's mental prowess and acting-under-stress. Found indirect proofs who sabotaged the derelict and so who should pay for endangering Adrienne and the Team. | 0083-12-07 - 0083-12-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adrianna Kastir      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Napoleon Myszogłów   | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Niferus Sentriak     | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Talia Irris          | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Tristan Andrait      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
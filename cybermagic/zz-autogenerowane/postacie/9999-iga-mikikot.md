---
categories: profile
factions: 
owner: public
title: Iga Mikikot
---

# {{ page.title }}


# Generated: 



## Fiszki


* 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić | @ 221022-derelict-okarantis-wejscie
* ENCAO:  0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy_anomalii_kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić; |ENCAO: 0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale|; postawiła Kaspiana na nogi i nanitkami go dopakowała gdy ten został ranny. | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | chciała poplotkować z Darią o Kaspianie, ale się odbiła. Jej wiedza bio nie jest wystarczająca, by zrobić coś z 'węgorzami'. | 0090-04-19 - 0090-04-23 |
| 221113-ailira-niezalezna-handlarka-woda | świetna negocjatorka i bardzo empatyczna; pomogła Darii znaleźć lepsze deale i dotarła do prawdy o Ailirze. Kaspian nie pozwolił jej zgłębiać tego tematu głębiej. Nieco naiwna i nieco zbyt optymistyczna. | 0090-04-24 - 0090-04-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
---
categories: profile
factions: 
owner: public
title: Persefona d'Infernia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200708-problematyczna-elena         | poważne uszkodzenia psychotroniczne spowodowane przez Klaudię i anomalne sensory sprawiły, że ma cechy paranoiczne. Musiała przejąć kontrolę nad Infernią by Eustachy jej nie zniszczył. | 0110-10-29 - 0110-11-03 |
| 210421-znudzona-zaloga-inferni      | zwalczała Morrigan jak mogła - backupy, odrzucanie swojego rdzenia, przywołanie danych Anastazji, ekrany ochronne... skończyła zintegrowana z Morrigan i Samurajem, przez Ariannę XD. | 0111-09-18 - 0111-09-21 |
| 210428-infekcja-serenit             | Aspekt 'Morrigan' próbuje zestrzelić Falołamacza, bo wykryła Serenit. Morrigan przejęła Falołamacza aż Eustachy wyłączył pół Inferni by Morrigan unieczynnić. | 0111-09-24 - 0111-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210421-znudzona-zaloga-inferni      | Skażone TAI, na stałe zintegrowane osobowości Morrigan i Samuraja Miłości. Opętana, nienaturalna TAI - ale nadal, o dziwo, sprawna. | 0111-09-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Elena Verlen         | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Eustachy Korkoran    | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Klaudia Stryk        | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Martyn Hiwasser      | 2 | ((210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| AK Serenit           | 1 | ((210428-infekcja-serenit)) |
| Antoni Kramer        | 1 | ((200708-problematyczna-elena)) |
| Jolanta Sowińska     | 1 | ((210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 1 | ((210421-znudzona-zaloga-inferni)) |
| Leszek Kurzmin       | 1 | ((200708-problematyczna-elena)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| OE Falołamacz        | 1 | ((210428-infekcja-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Infernia          | 1 | ((210428-infekcja-serenit)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
| Tomasz Sowiński      | 1 | ((210421-znudzona-zaloga-inferni)) |
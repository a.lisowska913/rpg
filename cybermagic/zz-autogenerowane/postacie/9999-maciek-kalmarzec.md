---
categories: profile
factions: 
owner: public
title: Maciek Kalmarzec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211222-kult-saitaera-w-neotik       | Neotik; przestępca seksualny, najpewniej zdaniem Kasandry miragent? Coś z nim nie tak, pracuje przy medlab. | 0112-02-24 - 0112-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Szarjan         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Arianna Verlen       | 1 | ((211222-kult-saitaera-w-neotik)) |
| Diana d'Infernia     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 1 | ((211222-kult-saitaera-w-neotik)) |
| Izabela Zarantel     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Klaudia Stryk        | 1 | ((211222-kult-saitaera-w-neotik)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maria Naavas         | 1 | ((211222-kult-saitaera-w-neotik)) |
---
categories: profile
factions: 
owner: public
title: Tatiana Cisrak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190720-klatwa-czarnej-piramidy      | neuropilot, główny Łowca i koordynator "pasikoników". Uratowała męża, ale sama się stoczyła w mrok. Bezpiecznie uciekła; gdzieś jest w Podwiercie. | 0110-06-04 - 0110-06-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dawid Klamczran      | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Jacek Cisrak         | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Mel Mirsiak          | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Samuel Czałczak      | 1 | ((190720-klatwa-czarnej-piramidy)) |
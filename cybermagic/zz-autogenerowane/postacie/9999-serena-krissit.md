---
categories: profile
factions: 
owner: public
title: Serena Krissit
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  --0-+ |Nie kłania się nikomu;;Prowokacyjna;;Skryta| VALS: Achievement, Security| DRIVE: Pokój i bezpieczeństwo) | @ 230124-kjs-wygrac-za-wszelka-cene
* styl: pracowita, chce pokazać | @ 230124-kjs-wygrac-za-wszelka-cene
* "Wojna się nie skończy tylko dlatego że nie ma noktian. Prawdziwy wróg jest _tam_." | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | starsza siostra, bardzo ćwiczy i chce być jak najlepsza. Zaskakująco przegrała z młodszą Martą (jakaś forma dopingu?). Zaprzyjaźniła się z Żanetą i powiedziała jej co wie na temat łóżkowych spraw Marty, wskazując na Franciszka. | 0095-06-23 - 0095-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
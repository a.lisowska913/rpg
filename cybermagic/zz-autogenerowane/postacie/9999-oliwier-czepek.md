---
categories: profile
factions: 
owner: public
title: Oliwier Czepek
---

# {{ page.title }}


# Generated: 



## Fiszki


* mentor, manager i przyjaciel Michała, 52 lata | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  0+-0+ |Spontaniczny;;Cyniczny;;Wierzy w konspiracje| VALS: Benevolence, Humility >> Face| DRIVE: Inkwizytor | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: konkretny, cyniczny, działa pod wpływem chwili, centuś | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


rekiny-a-akademia

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | centuś, przyszedł do Karo z prośbą pomocy Michałowi bo jest przeklęty i zapłacił 100 kredytek uczniowi terminusa i ten nic nie znalazł... | 0111-10-12 - 0111-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Karolina Terienak    | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
---
categories: profile
factions: 
owner: public
title: Marian Lechot
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220814-stary-kot-a-trzesawisko      | KIA; terminusi nie uratowali jego żony na Trzęsawisku, więc poszedł ją ratować i został wchłonięty. Zestrzelony przez MIRV Alana po odnalezieniu przez kota Rozbójnika. | 0111-10-08 - 0111-10-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Karolina Terienak    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
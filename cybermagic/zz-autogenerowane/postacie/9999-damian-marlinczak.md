---
categories: profile
factions: 
owner: public
title: Damian Marlinczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220712-pasozytnicze-osy-w-nativis   | pomniejszy medyk; uczestniczył w reanimacji miragenta Lycoris. Stracił dwóch przyjaciół, dowiedział się o Pasożycie i powiedział Rafałowi o miragencie. Nikt ważny, acz zna sekret Nativis i nie chce "zniknąć". | 0092-10-29 - 0092-11-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jarlow Gurdacz       | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Lycoris Kidiron      | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Rafał Kidiron        | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Serentina d'Remora   | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
---
categories: profile
factions: 
owner: public
title: Oliwia Karelan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210728-w-cieniu-nocnej-krypty       | echo; na czas inwazji Finis Vitae pierwszy oficer Alivii Nocturny. W przyszłości - kapitan. W wizji Nocnej Krypty animowana przez Ariannę. | 0111-03-22 - 0111-04-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Arianna Verlen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
---
categories: profile
factions: 
owner: public
title: Roland Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | prawdziwy paladyn, kiedyś towarzysz Strzały. Chce pomóc i Sanktuarium Kazitan i lokalnym ludziom, więc Elena i Irek nie muszą go szczególnie przekonywać. | 0095-07-24 - 0095-07-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Elena Samszar        | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Irek Kraczownik      | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Karolinus Samszar    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
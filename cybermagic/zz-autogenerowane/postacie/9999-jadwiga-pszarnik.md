---
categories: profile
factions: 
owner: public
title: Jadwiga Pszarnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190208-herbata-grzyby-i-mimik       | królowa grzybów i właścicielka Podfarmy. Ultra-wyspecjalizowana biomantka. Skłócona z Dariuszem przez mimika w rękach Almedy; zmuszona do opuszczenia Skałopływu. | 0110-03-03 - 0110-03-05 |
| 190213-wygasniecie-starego-autosenta | szukając nowego miejsca na pieczarę z grzybami wpakowała się w ciężką ranę. Autosent ją uratował a Rafał postawił na nogi. | 0110-03-07 - 0110-03-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kotomin       | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Baltazar Rączniak    | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Dariusz Bankierz     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Pięknotka Diakon     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Atena Sowińska       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
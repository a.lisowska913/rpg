---
categories: profile
factions: 
owner: public
title: Ignacy Szarjan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221015-duch-na-pancernej-jaszczurce | 29, inżynier Orbitera, mag, z eksperymentalnej stoczni (ENCAO: 0-0-+, szczery i wymagający drama queen| benevolence, face > hedonism); nie akceptował, że JEGO jednostka ma problemy. | 0109-08-24 - 0109-08-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Klaudia Stryk        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
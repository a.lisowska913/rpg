---
categories: profile
factions: 
owner: public
title: Sia d'Cranis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | KIA. Noktiańska TAI Sia, która uległa częściowej asymilacji przez Serenit. Poświęciła się by pomóc astoriańskiemu Zespołowi odnaleźć Aidę i Michała. | 0087-08-09 - 0087-08-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Serenit         | 1 | ((190802-statek-zjada-statki)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| Mikado               | 1 | ((190802-statek-zjada-statki)) |
| Serenit              | 1 | ((190802-statek-zjada-statki)) |
| Stella Koral         | 1 | ((190802-statek-zjada-statki)) |
| Travis Longhorn      | 1 | ((190802-statek-zjada-statki)) |
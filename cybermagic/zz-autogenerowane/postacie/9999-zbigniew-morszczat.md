---
categories: profile
factions: 
owner: public
title: Zbigniew Morszczat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201117-morszczat-zostaje-piratem    | komodor Orbitera polujący na noktian, który po wydarzeniach w Trzecim Raju chciał uciec z sektora. Ale przeszedł do piratów Kirona jako jego prawa ręka. | 0111-01-11 - 0111-01-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201117-morszczat-zostaje-piratem    | opuszcza Kontroler Pierwszy i nie jest już komodorem pod adm. Termią. Zostaje prawą ręką Ludwika Kirona, lorda pirackiego z Pasu Omszawera. | 0111-01-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bożena Kaldesz       | 1 | ((201117-morszczat-zostaje-piratem)) |
| Donald Parziarz      | 1 | ((201117-morszczat-zostaje-piratem)) |
| Ludwik Kiron         | 1 | ((201117-morszczat-zostaje-piratem)) |
| Siergiej Rożen       | 1 | ((201117-morszczat-zostaje-piratem)) |
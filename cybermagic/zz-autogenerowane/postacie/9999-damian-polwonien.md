---
categories: profile
factions: 
owner: public
title: Damian Polwonien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200507-anomalna-figurka-zabboga     | najgorszy złodziej na świecie; próbował ukraść figurkę ze sklepu (zatrzymany przez Janka) a potem ukradł ją Nataszy (która się zorientowała i pozwoliła na to) | 0109-09-19 - 0109-09-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Jan Łowicz           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Kinga Kruk           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Melinda Teilert      | 1 | ((200507-anomalna-figurka-zabboga)) |
| Natasza Aniel        | 1 | ((200507-anomalna-figurka-zabboga)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |
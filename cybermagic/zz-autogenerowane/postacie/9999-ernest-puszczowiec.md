---
categories: profile
factions: 
owner: public
title: Ernest Puszczowiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | młody podliz wobec Eustachego, stawia mu alkohol by "skapnęła" mu dziewczyna po Eustachym. Teraz chciał się popisać przed jakimiś laskami i sklupać "bladawca" (Ralfa). Ardilla go upokorzyła. Absolutnie przerażony tym co Ardilla robi i czym ogólnie jest. | 0093-02-10 - 0093-02-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Celina Lertys        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalia Awiter         | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| OO Infernia          | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Rafał Kidiron        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ralf Tapszecz        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
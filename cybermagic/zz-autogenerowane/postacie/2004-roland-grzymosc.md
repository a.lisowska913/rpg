---
categories: profile
factions: 
owner: public
title: Roland Grzymość
---

# {{ page.title }}


# Read: 

## Kim jest

### Paradoksalny Koncept

Kiedyś oficer Orbitera na Kontrolerze Pierwszym, teraz szef mafii "Wolny Uśmiech" w Szczelińcu. Zafascynowany bioformami, reinwestuje wszystko w naukę. Potrzebując pieniędzy krzywdzi jednostki by ratować populację. Lojalny ludzkości, lecz wciąż groźny mafiozo. Jego szczególną cechą jest CAŁKOWITY brak mściwości.

### Motto

"Pokonaliśmy Pęknięcie. Odparliśmy Saitaera. Radzimy sobie z anomaliami. Szybko ewoluując, ludzkość przezwycięży wszelkie kłopoty."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Biomanta ze specjalizacją lekarską. Grzymość jest ekspertem od zmian ludzi i magów. Jego magia - jak i jego zainteresowania - na tym się kończą.
* ATUT: Mistrz przekupstwa i negocjacji. Potrafi odkryć sekrety i marzenia drugiej strony, po czym dostarczyć im wszystko, o czym marzyli... za cenę.
* ATUT: Niewiarygodne zasoby - jest szefem mafii, ma wsparcie z Cieniaszczytu, potężne magitechy i wszystko.
* SŁABA: W pewien sposób ma gołębie serce. Nawet, jeśli ktoś go zdradzi, i tak Grzymość nic z tym złego nie zrobi. Po prostu IGNORUJE jednostki - ani nie pomoże, ani nie zaszkodzi.
* SŁABA: Mimo, że był oficerem Orbitera, nie jest w stanie już walczyć. Jego ciało jest zbyt zniszczone. Musi być dżentelmenem.

### O co walczy (3)

* ZA: Ewolucja i przetrwanie ludzkości w zmieniającym się świecie. Wybiera grupy nad jednostki; jednostki można poświęcić.
* ZA: Budowa i doskonalenie badań biomagicznych, mających na celu ewoluować ludzkość. Pragnie perfekcji ludzkości.
* ZA: Poszerzanie wpływów Cieniaszczytu i Aurum (swoich sponsorów stamtąd) w Szczelińcu dla pieniędzy (reinwestowanie w badania).
* VS: Saitaer, Serenit... wszelkie niebezpieczne Anomalie i istoty maksymalnego ryzyka dla ludzkości.
* VS: Jakiekolwiek formy typu: indywidualizm nad grupa. Grzymość skupia się na całej ludzkości, nawet nie na swojej organizacji.

### Znaczące Czyny (3)

* Zbudował i złożył logistycznie nie jedną a DWIE bazy tuż pod nosem terminusów w Szczelińcu; dywersja na dywersji.
* Utrzymał się przy życiu po katastrofalnej operacji przeciwko Saitaerowi, usunął z siebie Skażenie Saitaera. Przetrwał, aż go uratowano i kralothy odbudowały co się dało.
* Gdy Kajrat go zdradził a Amanda została Skażona przez Saitaera, poświęcił dużo sił by ją przejąć... celem wyleczenia.

### Kluczowe Zasoby (3)

* COŚ: Baza R&D w Czemercie, dość blisko Trzęsawiska. Bardzo zaawansowane elementy korekcji biomagicznej, dowodzoną przez BIA klasy Neiren (2).
* KTOŚ: Patroni w Cieniaszczycie, chcący przenieść tamtejszą kulturę do Szczelińca (i płacący). Patroni w Aurum, chcący zdobyć przyczółek (i płacący).
* WIEM: Ekspert od puryfikacji Skażenia u ludzi i magów oraz od redukcji wpływu Anomalii klasy S.
* OPINIA: Łagodny szef mafii poszerzający wpływy Cieniaszczytu w okolicy, który z jakiegoś powodu bardzo interesuje się anomaliami biologicznymi.

## Inne

### Wygląd

.

### Coś Więcej

.

# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180929-dwa-tygodnie-szkoly          | właściciel kasyna, prowadzi szemrany interes i jest powiązany z siłami które szkodzą Atenie na Epirjonie. | 0109-09-17 - 0109-09-19 |
| 190106-a-moze-pustogorska-mafia     | mafiozo, który wpierw spróbował zwerbować Pięknotkę a potem Adelę i Kasjana. Za każdym razem jego plany zostały pokrzyżowane | 0109-12-23 - 0109-12-25 |
| 190726-bardzo-niebezpieczne-skladowisko | bardzo skupiony na dziwnych narkotykach; nie uwierzył Pięknotce że Orbiter na to poluje więc ściągnął kanał do podziemi. | 0110-06-22 - 0110-06-24 |
| 191201-ukradziony-entropik          | okazało się, że ma dużo większe wsparcie - albo z Aurum albo z Orbitera. Skupiony na tematach medycznych; gdzieś tu coś go boli. | 0110-07-09 - 0110-07-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201025-kraloth-w-parku-janor        | zamknięty w Pustogorze, w więzieniu. | 0110-10-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia; 190726-bardzo-niebezpieczne-skladowisko; 191201-ukradziony-entropik)) |
| Adela Kirys          | 2 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Amanda Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Erwin Galilien       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Kasjan Czerwoczłek   | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Keraina d'Orion      | 1 | ((191201-ukradziony-entropik)) |
| Mariusz Trzewń       | 1 | ((191201-ukradziony-entropik)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Minerwa Metalia      | 1 | ((191201-ukradziony-entropik)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Napoleon Bankierz    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Persefona d'Jastrząbiec | 1 | ((191201-ukradziony-entropik)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Waldemar Mózg        | 1 | ((190106-a-moze-pustogorska-mafia)) |
---
categories: profile
factions: 
owner: public
title: Seweryn Grzęźlik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | psychotronik blakvelowców; chciał się włamać do Ceres i zmienić dowodzenie na Damiana. Okazało się, że się nie da; oddelegowany w imieniu blakvelowców do Królowej Przygód. | 0108-02-25 - 0108-03-11 |
| 220329-mlodociani-i-pirat-na-krolowej | zgryźliwy, kochający ciszę i nie znoszący dzieci. Zdekodował manifest jednostki pirackiej i miał sporo dobrych rad. Coś podejrzewa o Mirandzie. | 0108-06-22 - 0108-07-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gotard Kicjusz       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Łucjan Torwold       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Miranda Ceres        | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
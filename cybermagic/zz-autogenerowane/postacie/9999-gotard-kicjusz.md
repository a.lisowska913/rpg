---
categories: profile
factions: 
owner: public
title: Gotard Kicjusz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | górnik z Trzech Przyjaciół; lekko ekscentryczny i optymistyczny, chce przejąć Królową dla nich. Chce przygód i zobaczenia świata. | 0108-02-25 - 0108-03-11 |
| 220329-mlodociani-i-pirat-na-krolowej | próbuje ukryć problemy z dzieciakami tak, by nie było oficjalnie. Potem strzelał (kiepsko, ale Miranda strzelała dobrze) i rozbrajał miny (na szczęście nie musiał poważnie). | 0108-06-22 - 0108-07-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Łucjan Torwold       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Miranda Ceres        | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
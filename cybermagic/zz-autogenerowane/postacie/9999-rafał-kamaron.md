---
categories: profile
factions: 
owner: public
title: Rafał Kamaron
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200913-haracz-w-parku-rozrywki      | Grzymościowiec z Wolnego Uśmiechu; złapał jakiegoś dziwnego pasożyta próbując zdobyć haracz z parku rozrywki Janor. Zestrzelony przez Fulmen Alana. | 0110-10-05 - 0110-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Diana Tevalier       | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Mariusz Trzewń       | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Melinda Teilert      | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Pięknotka Diakon     | 1 | ((200913-haracz-w-parku-rozrywki)) |
---
categories: profile
factions: 
owner: public
title: Karol Kszatniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191113-jeden-problem-dwie-rodziny   | lojalny Lemurczakowi, katai i manipulator. Skutecznie dawał odpór Potworowi i doprowadził do jego śmierci, zbierał też informacje o wszystkim w okolicy. | 0110-09-26 - 0110-10-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Klara Baszcz         | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Leszek Baszcz        | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Paweł Kukułnik       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |
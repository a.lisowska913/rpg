---
categories: profile
factions: 
owner: public
title: Mariusz Trzewń
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 18 lat; chciał poderwać Klaudię na Hestię, wkradli się do budynku AMZ i skończył z pasami na gołym tyłku od Szurmaka. Potem gdy zaatakował Agent Esuriit chciał postawić Hestię by mogła pomóc, ale jedynie doprowadził do zniszczenia Hestii i śmierci dyrektora. Nie może sobie tego wybaczyć i wyleczył się z bycia bohaterem. | 0083-10-13 - 0083-10-22 |
| 210926-nowa-strazniczka-amz         | 19 lat; nerd militarny. Rozpaczliwie trzyma się terminusów Pustogoru, bo to jedyne co jest STAŁE po wojnie. Rozchwiany, doskonale się skrada - podsłuchał TYMONA GRUBOSZA (ze wszystkich ludzi). Chce nadać temat terminusom, ale Tymon + Klaudia + Eszara przekonali go, by dał Eszarze szansę. | 0084-06-14 - 0084-06-26 |
| 211009-szukaj-serpentisa-w-lesie    | 19 lat; przechwycony przez serpentisa Agostino w Lesie Trzęsawnym, ale dał radę przekazać zapętloną wiadomość Klaudii magią przez zagłuszający pojazd. Technomanta <3. | 0084-11-13 - 0084-11-14 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | wziął na siebie część roboty Klaudii, po czym zrobił sieć pomocników - w ten sposób odpowiedzialność nie leży na samej Klaudii i wszyscy są w stanie działać. Szukał ukrytego noktianina w systemach i wykluczył Teresę jako noktiankę (co jest całkiem zabawne). Chce usunąć noktian z AMZ. | 0084-12-20 - 0084-12-24 |
| 190827-rozpaczliwe-ratowanie-bii    | taktyk, wsparcie, hacker z Barbakanu; często współpracuje z Pięknotką. Zidentyfikował dla Pięknotki, że pnączoszpony używały noktiańskiej taktyki. | 0110-01-17 - 0110-01-20 |
| 190906-wypadek-w-kramamczu          | analityk Barbakanu Pustogoru; powiedział Pięknotce o sytuacji finansowej Włóknina. Innymi słowy - mają przyczynę. | 0110-06-14 - 0110-06-15 |
| 190901-polowanie-na-pieknotke       | organizował kampanię do tępienia noktian w enklawach; przekonany przez Pięknotkę że to zły pomysł | 0110-06-17 - 0110-06-20 |
| 191103-kontrpolowanie-pieknotki-pulapka | analityk Pięknotki; powiedział jej, że Ksenia ma lokalizację - gdzieś w Aurum. Pomógł Pięknotce wyłączyć problemy z Ksenią. | 0110-06-20 - 0110-06-27 |
| 191105-zaginiona-soniczka           | na prośbę Pięknotki sprawdził silnym sygnałem obecność Marioli, przez co rozwalił Karli jedno tajne śledztwo. ALE - uratowali pięć dziewczyn. Więc w sumie Karla go nie zabije. | 0110-06-28 - 0110-06-29 |
| 191112-korupcja-z-arystokratki      | rozpaczliwie próbował być przydatny i odkupić się u Karli, więc znalazł Pięknotce Jastrząbiec. Dzięki Pięknotce, Karla zdjęła go z 'listy-osób-do-opieprzania'. | 0110-06-30 - 0110-07-01 |
| 191201-ukradziony-entropik          | pomógł Pięknotce zlokalizować kompleks Itaran używając matematyki, triangulacji i znajomości z odpowiednimi magami. Niestety, przez to był wyciek. | 0110-07-09 - 0110-07-11 |
| 200202-krucjata-chevaleresse        | nerd taktyki i gryzipiórek; doszedł do tego, że nagrania wskazują, że napastnikiem bazy Grzymościa jest TAI poziomu ponad 3. Pięknotce jest zimno. | 0110-07-20 - 0110-07-23 |
| 200414-arystokraci-na-trzesawisku   | odkrył, że seksbomba-arystokratka to Sabina Kazitan używając systemów analitycznych Pustogoru. | 0110-07-31 - 0110-08-01 |
| 200510-tajna-baza-orbitera          | przegrzebywał się przez dokumenty Gabriela, gdzie był ślad dla Pięknotki. Gabriel zdecydowanie ich przecenił - nic nie znalazł. Ale pomógł nakierować na awiana. | 0110-09-03 - 0110-09-07 |
| 200524-dom-dla-melindy              | nigdy nie był w Podwiercie bo to "miasto gangów" a on nie umie walczyć. Pięknotka wzięła go na kabaret w Arkadii. Zainicjował szukanie Melindy. | 0110-09-09 - 0110-09-12 |
| 200913-haracz-w-parku-rozrywki      | znalazł dla Pięknotki łatwą misję, która okazała się być dość ostra; nie dał rady odnaleźć ludzi od Grzymościa, ale wykrył Pasożyta w magu. | 0110-10-05 - 0110-10-06 |
| 210720-porwanie-daniela-terienaka   | wspiera Ulę Miłkowicz. Jest jej "terminusem-opiekunem", acz nieformalnym. Przy okazji, zna kody do różnych mechanicznych potworności powojennych w Lesie Trzęsawnym. | 0111-06-10 - 0111-06-12 |
| 210817-zgubiony-holokrysztal-w-lesie | pomógł Tukanowi dowiedzieć się czym jest ten dziwny kryształ - i wykrył ko-matrycę Kuratorów. | 0111-06-18 - 0111-06-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | wyleczył się z heroizmu od teraz do końca życia. Ciężka trauma - przez niego zginął dyrektor, w jego obronie. I Hestia, w którą wierzył do samego końca. Zrozumiał, że nie wszystko da się wygrać. | 0083-10-22
| 210926-nowa-strazniczka-amz         | 19-latkiem będąc, był przekonany o wyższości podejścia Pustogoru nad wolnym strzelcem Tymonem Gruboszem. Nie lubi Tymona. | 0084-06-26
| 210926-nowa-strazniczka-amz         | za 10 lat i później będzie cichym sojusznikiem AMZ i Eszary. Ona będzie z nim koordynować nie mówiąc nikomu. | 0084-06-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((190827-rozpaczliwe-ratowanie-bii; 190901-polowanie-na-pieknotke; 190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Ksenia Kirallen      | 6 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| Minerwa Metalia      | 5 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Talia Aegis          | 5 | ((190827-rozpaczliwe-ratowanie-bii; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Diana Tevalier       | 4 | ((190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Klaudia Stryk        | 4 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| Tymon Grubosz        | 4 | ((190827-rozpaczliwe-ratowanie-bii; 191105-zaginiona-soniczka; 200510-tajna-baza-orbitera; 210926-nowa-strazniczka-amz)) |
| Alan Bartozol        | 3 | ((190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 210817-zgubiony-holokrysztal-w-lesie)) |
| Ataienne             | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Ignacy Myrczek       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Laura Tesinik        | 3 | ((200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 210817-zgubiony-holokrysztal-w-lesie)) |
| Sabina Kazitan       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie)) |
| Tomasz Tukan         | 3 | ((200202-krucjata-chevaleresse; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Arnulf Poważny       | 2 | ((210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Damian Orion         | 2 | ((191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Felicjan Szarak      | 2 | ((211009-szukaj-serpentisa-w-lesie; 211122-czolenkowe-esuriit-w-amz)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marysia Sowińska     | 2 | ((210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Mateusz Kardamacz    | 2 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Teresa Mieralit      | 2 | ((200510-tajna-baza-orbitera; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Daniel Terienak      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Kuromin      | 1 | ((190906-wypadek-w-kramamczu)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ernest Kajrat        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Erwin Galilien       | 1 | ((191105-zaginiona-soniczka)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karolina Terienak    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Kasjopea Maus        | 1 | ((190906-wypadek-w-kramamczu)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Lucjusz Blakenbauer  | 1 | ((190901-polowanie-na-pieknotke)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Torszecki      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Miłkowicz    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
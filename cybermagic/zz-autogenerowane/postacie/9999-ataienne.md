---
categories: profile
factions: 
owner: public
title: Ataienne
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190330-polowanie-na-ataienne        | chciała tylko zrobić koncert - a cudem uniknęła śmierci dzięki ratunkowi Chevaleresse a potem Pięknotki. Miała hosta w formie człowieka ku niezadowoleniu Pięknotki. | 0110-03-02 - 0110-03-03 |
| 190331-kurz-po-ataienne             | uważa Chevaleresse za przyjaciółkę i dlatego nie udało się Pięknotce jej przekonać do użycia swoich mocy by nieco ustabilizować Chevaleresse. Wdzięczna Pięknotce. | 0110-03-06 - 0110-03-08 |
| 190402-eksperymentalny-power-suit   | wniknęła do Cienia i uratowała Pięknotkę przed nienawistnym instynktem psychotroniki. Skończyła bardzo ciężko ranna, z rozpadającą się matrycą. | 0110-03-11 - 0110-03-13 |
| 190326-arcymag-w-raju               | mindwarpująca TAI o funkcji nadpisywania wspomnień; hipnopiosenkarka holograficzna. Kontroluje Trzeci Raj składający się z jeńców wojennych. | 0110-04-07 - 0110-04-08 |
| 200219-perfekcyjna-corka            | dzięki współpracy Leszka i Sabiny przejęła kontrolę nad Kalbarkiem i się tam zaszczepiła. Zapewnia absolutną widoczność oraz neutralizuje Paradoksy przekierowując emitery. | 0110-05-12 - 0110-05-15 |
| 191105-zaginiona-soniczka           | przestraszyła się o Mariolę (swoją soniczkę która zaginęła) i udała się z prośbą o pomoc do Pięknotki. Przypadkowo, Ataienne wykryła gang przerzutowy dziewczyn do Cieniaszczytu. | 0110-06-28 - 0110-06-29 |
| 191112-korupcja-z-arystokratki      | nieprawdopodobnie potężna z perspektywy opętywania maszyn i przejmowania kontroli. Sojuszniczka Pięknotki, która przełamała wszystkie zabezpieczenia nawet się nie starając... | 0110-06-30 - 0110-07-01 |
| 200202-krucjata-chevaleresse        | TAI powyżej 3 stopnia. Działa w tle - taktyk oraz commander, ale niekoniecznie mistrzyni zdobywania informacji. Jedyna TAI 3.5 w okolicy Szczelińca. Sformowała 'Liberitas' | 0110-07-20 - 0110-07-23 |
| 200222-rozbrojenie-bomby-w-kalbarku | autoryzowana przez Pięknotkę do pomocy w Barbakanie, pokazuje bezwzględność wobec Małmałaza i Kardamacza. Rozmontowała Liberatis i odzyskała broń. | 0110-07-23 - 0110-07-28 |
| 200916-smierc-raju                  | w Trzecim Raju działa w ukryciu, nie pokazując co naprawdę umie i stanowi źródło rozczarowań. Ale jak Raj został zniszczony, użyła pełnej mocy hipnotycznej. Nikt jej nie jest w stanie kontrolować, acz Arianna i Klaudia widzą, co potrafi. | 0110-12-21 - 0110-12-23 |
| 201014-krystaliczny-gniew-elizy     | widząc jak Dariusz potraktował Anastazję Sowińską (jej krewną), użyła swej mocy na życzenie Arianny by go zmusić do przyznania się. | 0111-01-02 - 0111-01-05 |
| 201021-noktianie-rodu-arlacz        | bardzo chce się połączyć psychotronicznie z Anastazją. Nie ma okazji - Arianna jej nie pozwala. Za to ma nowych 100 noktian do zmanipulowania i Raj do trzymania. | 0111-01-07 - 0111-01-10 |
| 201104-sabotaz-swini                | zestrzeliła działkami Trzeciego Raju anomalnego Wieprzka, po czym użyła pełni mindwarpa by wyczyścić ofiary anomalnego Wieprzka. I nikt nic nie wie. | 0111-01-13 - 0111-01-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190402-eksperymentalny-power-suit   | ciężko ranna, z uszkodzoną matrycą. Uratowała życie Pięknotce, ale zapłaciła rozpad matrycy; ma trochę czasu regeneracji. | 0110-03-13
| 200923-magiczna-burza-w-raju        | niechęć wobec Arianny Verlen. Ataienne jest przeciwna bogom i istotom bogom podobnym a Arianna jest arcymagiem używającym eterniańskich metod. | 0110-12-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Anastazja Sowińska   | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Arianna Verlen       | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Tevalier       | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eustachy Korkoran    | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Mateusz Kardamacz    | 4 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eliza Ira            | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Mariusz Trzewń       | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Aleksandra Szklarska | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Arłacz         | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Elena Verlen         | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Erwin Galilien       | 2 | ((190402-eksperymentalny-power-suit; 191105-zaginiona-soniczka)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Tymon Grubosz        | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Alan Bartozol        | 1 | ((190331-kurz-po-ataienne)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
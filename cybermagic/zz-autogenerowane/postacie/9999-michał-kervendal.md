---
categories: profile
factions: 
owner: public
title: Michał Kervendal
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | Nativis / Robak; bioinżynier żywności (62) który poznał prawdę o żywności z Nativis; Lycoris splicuje Trianai. Powiedział o wszystkim Ardilli i Eustachemu. Całkowicie to zignorowali. Wrócił na Infernię powiedzieć wszystko Bartłomiejowi Korkoranowi. Stracił rękę by zamaskować, że żyje. | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
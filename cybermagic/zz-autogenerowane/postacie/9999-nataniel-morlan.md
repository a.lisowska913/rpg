---
categories: profile
factions: 
owner: public
title: Nataniel Morlan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | potężny eternijski łowca nagród, szukający córki. Nie wierzy, że ona nie żyje i znalazł ją w Duchu. Szuka Antonelli (myśląc że to Marcelina); gdy już prawie mu się udało, został uśpiony przez Zespół. | 0108-10-20 - 0108-10-25 |
| 210921-przybycie-rekina-z-eterni    | w pełni świadomy tego co robi Jolanta Sowińska; akceptuje plan Amelii Sowińskiej w którym Amelia przyjeżdża do niego na wymianę a na jej miejsce jedzie Ernest Namertel. | 0111-07-16 - 0111-07-21 |
| 210818-siostrzenica-morlana         | absolutnie bezwzględny; skłonny porwać / zabić siostrzenicę by tylko wszystko wróciło "do normy". Wie, że Infernia działa przeciw niemu. | 0111-11-15 - 0111-11-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210818-siostrzenica-morlana         | wie, że to Infernia (Klaudia) próbuje dowiedzieć się o jego działaniach przeciw Ofelii. Wie, kto z nim walczy. | 0111-11-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Sowińska     | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210921-przybycie-rekina-z-eterni)) |
| Tomasz Sowiński      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210818-siostrzenica-morlana)) |
| Amelia Sowińska      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Antonella Temaris    | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Arianna Verlen       | 1 | ((210818-siostrzenica-morlana)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Eustachy Korkoran    | 1 | ((210818-siostrzenica-morlana)) |
| Flawia Blakenbauer   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Karolina Terienak    | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Klaudia Stryk        | 1 | ((210818-siostrzenica-morlana)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leona Astrienko      | 1 | ((210818-siostrzenica-morlana)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Marysia Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Rafał Torszecki      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |
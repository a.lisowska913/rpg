---
categories: profile
factions: 
owner: public
title: Aletia Nix
---

# {{ page.title }}


# Generated: 



## Fiszki


* noktianka, uciekła pod opiekę Szymona przed siłami Tristana | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO:  0+--- |Wszystko robi na ostatnią chwilę;;Zrezygnowana, wszędzie widzi ciemność| VALS: Self-direction, Security >> Benevolence, Achievement| DRIVE: Ukryć sekret (zna Lokację) | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO:  0+--- |Wszystko robi na ostatnią chwilę;;Zrezygnowana, wszędzie widzi ciemność| VALS: Self-direction, Security >> Benevolence, Achievement| DRIVE: Ukryć sekret (kody serpentisa) | @ 230102-elwira-koszmar-nox-ignis

### Wątki


historia-talii
koszmar-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | podobno porwana przez Orbiter noktianka, bo jest ładna, jako łup wojenny? | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | córka wysokiej klasy oficera medycznego Serpens Nivis i awaryjna aktywatorka serpentisów. ZNIKNIĘTA. | 0082-08-02 - 0082-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
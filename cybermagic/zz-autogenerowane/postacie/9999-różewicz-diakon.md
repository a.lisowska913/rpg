---
categories: profile
factions: 
owner: public
title: Różewicz Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210622-verlenka-na-grzybkach        | Rekin. Specjalizuje się w kwiatach, pięknie i truciznach. Piękny i nieskazitelnie elegancki. Użyty przez Arkadię by dowiedzieć się więcej o tym środku który na nią działa. | 0111-05-26 - 0111-05-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Ignacy Myrczek       | 1 | ((210622-verlenka-na-grzybkach)) |
| Julia Kardolin       | 1 | ((210622-verlenka-na-grzybkach)) |
| Liliana Bankierz     | 1 | ((210622-verlenka-na-grzybkach)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Marysia Sowińska     | 1 | ((210622-verlenka-na-grzybkach)) |
| Rafał Torszecki      | 1 | ((210622-verlenka-na-grzybkach)) |
| Triana Porzecznik    | 1 | ((210622-verlenka-na-grzybkach)) |
| Urszula Miłkowicz    | 1 | ((210622-verlenka-na-grzybkach)) |
---
categories: profile
factions: 
owner: public
title: Tristan Ozariat
---

# {{ page.title }}


# Generated: 



## Fiszki


* (dekadianin) kiedyś oficer; potem użył Nox Ignis i "próbuje odbudować Elwirę" (jego córka to była Tatiana - ciemne włosy, blada cera) | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO: MANIC!!! | Zimna taktyka;; Głodna dusza i nie kontaktuje| Power, Benevolence > Humility | DRIVE: odbudować / znaleźć Tatianę | @ 221230-dowody-na-istnienie-nox-ignis
* "to jedna z nich! To moja ukochana Elwira." | @ 221230-dowody-na-istnienie-nox-ignis

### Wątki


historia-talii
koszmar-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | kiedyś oficer noktiański; wie o Nox Ignis, jest szalony. Miał u siebie Katrinę Komczirp jako ukrytą agentkę Orbitera. Podobno szuka swojej córki i PRZERABIA dziewczyny w swoją córkę. | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | objął w przeszłości Nox Ignis by zestrzelić Tatianę - własną córkę - która stała się terrorformem Saitaera. Zapomniał kim jest jego córka; Nox Ignis podmieniła "Tatianę" na "Elwirę" w jego głowie i się nim ciągle żywi. | 0082-08-02 - 0082-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
---
categories: profile
factions: 
owner: public
title: Paulina Mordoch
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | dziewczyna Andrzeja i kochanka Izydora, miejscowa piękność. To, że Andrzej ją odpychał poważnie nadwątliło jej ego i poczucie seksapilu. Złamała Małopsową 'omertę' wzywając Rekiny; poniesie konsekwencje. | 0110-10-31 - 0110-11-02 |
| 201215-dziewczyna-i-pies            | myśli, że jest matką Patrycji. Sprawdzała się w tej roli ;-). | 0110-11-11 - 0110-11-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | Złamała Małopsową 'omertę' wzywając Rekiny; poniesie JAKIEŚ konsekwencje. | 0110-11-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Daniel Terienak      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Karolina Terienak    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
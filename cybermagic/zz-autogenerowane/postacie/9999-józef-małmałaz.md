---
categories: profile
factions: 
owner: public
title: Józef Małmałaz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190901-polowanie-na-pieknotke       | noktiański łowca ciekawych przedmiotów, bardzo groźny zabójca. Wpierw zranił Pięknotkę (został ciężko ranny przez Alana), potem na stymulantach porwał Chevaleresse i ciężko zranił Alana. | 0110-06-17 - 0110-06-20 |
| 191103-kontrpolowanie-pieknotki-pulapka | okazało się, że jest bardzo wpływowym magiem, łowcą i kolekcjonerem artefaktów, powiązanym politycznie z rodem Barandis w Aurum. | 0110-06-20 - 0110-06-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190901-polowanie-na-pieknotke       | na parę dni trafił do szpitala przez Alana Bartozola i działo Koenig (jak zeszły stymulanty) | 0110-06-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Mariusz Trzewń       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Minerwa Metalia      | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Pięknotka Diakon     | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Alan Bartozol        | 1 | ((190901-polowanie-na-pieknotke)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Diana Tevalier       | 1 | ((190901-polowanie-na-pieknotke)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lucjusz Blakenbauer  | 1 | ((190901-polowanie-na-pieknotke)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
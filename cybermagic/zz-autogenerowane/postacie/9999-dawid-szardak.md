---
categories: profile
factions: 
owner: public
title: Dawid Szardak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190326-arcymag-w-raju               | górnik z Trzeciego Raju; kiedyś żołnierz. Uważa, że Eliza zdradziła, gdy nie zniszczyła Saitaera (i części populacji Astorii). Bardzo wrogi Elizie. | 0110-04-07 - 0110-04-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Eliza Ira            | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
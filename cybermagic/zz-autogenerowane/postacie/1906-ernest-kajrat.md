---
categories: profile
factions: 
owner: public
title: Ernest Kajrat
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Kiedyś, zastępca Elizy - specjalista od logistyki i kontroli roju. Dzisiaj - mafioso i potwór wymuszający czego chce torturami.

### Czego chce a nie ma (3)

* walka z bogami i ochrona Noctis; misja się nie skończyła tylko dlatego, że Inwazja przegrała. Śmierć Astorii jest akceptowalna.
* nie zatracić siebie, pozostać niezależnym; każdy podlegający komuś (bogom, energiom, systemowi) musi zostać uwolniony niezależnie od mroku.
* złamać każdego, być mistrzem korupcji; wpływ Esuriit przekształcił mu hobby - chce osiągnąć mistrzostwo w złamaniu np. seksbota.

### Sposób działania (3)

* terror - tortury, szantaż, reputacja, efektowny i krwawy styl walki, pożarcie Esuriit, nieskończona nienawiść
* walka - zakrzywienia przestrzeni, dowodzenie agentami, drony i roje, świetny pilot i strzelec servara
* dowodzenie - taktyka, wiedza o przeciwniku, motywowanie strachem, liczni agenci
* urok - dżentelmen, elegancki ubiór, etykieta, prawdziwy arystokrata

### Zasoby i otoczenie (3)

* Cień Esuriit: straszliwy koszmar i servar Esuriit chroniący go przed atakiem, na pierwszej linii (gdy trzeba).
* Ciało Esuriit: wampiryczna regeneracja, zadane przezeń rany się gorzej leczą, możliwość opętania
* Sekrety i niewolnicy: mistrz szantażu, o każdym coś wie, znajduje słabe punkty i ich używa
* Drony: zawsze ma sporo dron i latających tarcz. Praktycznie działa tylko nimi; sam nie walczy bo nie musi.
* Reputacja: najstraszliwszy potwór w okolicy.
* Najlepszej klasy servar; odporny i silny.

### Magia (3)

#### Gdy kontroluje energię

* Włada negatywną energią Esuriit; przyzywa strach, głód i ból oraz echa cierpienia z przeszłości.
* Kineza i teleportacje - jako mag specjalizuje się w polach siłowych, szybkim poruszaniu się i barierach teleportacyjnych.
* Łańcuchy i reduktory poruszania się i woli. Ciemność, melasa.

#### Gdy traci kontrolę

* Eksplozja Esuriit; darkness of the unlife. Awatar Esuriit. Istoty Esuriit.
* Wpada w szał berserkerski; nie potrafi się powstrzymać. Pojawia się jego broń i cień.

### Powiązane frakcje

{{ page.factions }}

## Opis

Esuriit pozwoliło mu uwolnić się z niewoli swoich panów po zdradzie najbliższej przyjaciółki; nigdy więcej nie będzie zniewolony. Też: Gaurn.

(12 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | ojej, Rekinki zdecydowały się na pomoc lokalną. To słodkie i fajne. Ale wchodzą w szkodę Grzymościowi? Ok - czas na darmowy trening. Maksymalizacja upokorzenia. | 0108-04-03 - 0108-04-14 |
| 190827-rozpaczliwe-ratowanie-bii    | całkowicie nieświadomy sprawy wpakował się w kiepskiej klasy intrygę Talii. Wziął winę na siebie, dał się złapać i poszedł dla niej do więzienia na pewien czas; uzyskał jej wsparcie za to. | 0110-01-17 - 0110-01-20 |
| 190828-migswiatlo-psychotroniczek   | nadal w więzieniu; okazuje się, że pełni kluczową rolę neutralizatora przepływu artefaktów noktiańskich. Dzięki niemu nie ma dziwnych anomalii. Teraz go nie ma. | 0110-02-07 - 0110-02-09 |
| 190505-szczur-ktory-chroni          | prawa ręka mafii. Oficer Grzymościa. Niebezpieczny w walce, z dużym poczuciem humoru. Polował na Adelę i Krystiana; skończył trafiony przez Alana z Koeniga. | 0110-04-18 - 0110-04-20 |
| 190519-uciekajacy-seksbot           | dżentelmen i oficer mafii, który - jak się okazuje - ma bardzo mroczne zapędy. Zarówno sadystyczne jak i wobec Ateny. I nic a nic nie ukrywa swojej natury. Nic dziwnego, że się go boją. | 0110-04-21 - 0110-04-22 |
| 190616-anomalna-serafina            | mastermind stojący za "niech Pustogor i Cieniaszczyt staną do walki przeciw sobie". Trochę pomaga Pięknotce, bardzo Serafinie. Grzymość go w końcu zatrzymał, ale zaczął ziarno konfliktu. | 0110-05-08 - 0110-05-11 |
| 190622-wojna-kajrata                | pokazał bardziej okrutną naturę szantażysty i dominatora przerażonej Liliany; dał jednak Pięknotce chwilę czasu na rozwiązanie sprawy Serafiny i Liliany. | 0110-05-14 - 0110-05-17 |
| 190623-noc-kajrata                  | Pięknotka odkryła jego sekret powiązany z Esuriit. Doprowadził do transformacji Serafiny Iry w istotę będącą częściowo Banshee - broń przeciwko Ataienne. Skończył wyczerpany ale zwycięski. | 0110-05-18 - 0110-05-21 |
| 190626-upadek-enklawy-floris        | wzmacnia Enklawy pod szyldem Błękitnego Nieba; zapewnił sobie wsparcie Floris z fabrykatorem Wiecznej Maszyny. | 0110-05-24 - 0110-05-27 |
| 190709-somnibel-uciekl-arienikom    | całkowicie zaskoczony tym, że ma somnibela; porwał dla niego go Ksawery. Chciał sprawdzić czy Serafina rozerwie link somnibel - ofiara; okazuje się, że tak. | 0110-06-01 - 0110-06-03 |
| 190714-kult-choroba-esuriit         | przekazał Pięknotce jako wsparcie swoją "córkę". Nie udało mu się zbadać Tukana, ale zbadał dotkniętego przez Esuriit kultystę. | 0110-06-03 - 0110-06-05 |
| 190726-bardzo-niebezpieczne-skladowisko | zdecydował się zaufać Pustogorowi w formie Pięknotki by nie eskalować wojny z Wolnym Uśmiechem. A przynajmniej teraz. | 0110-06-22 - 0110-06-24 |
| 200311-wygrany-kontrakt             | śmiertelnie ranny, utrzymywany przez Lucjusza przy życiu. Najpewniej gdzieś w Rezydencji Blakenbauerów | 0110-07-16 - 0110-07-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190616-anomalna-serafina            | Grzymość mu zdecydowanie mniej ufa. "Mad Dog Kajrat", nie zaufana prawa ręka - czemu chciał wojny? | 0110-05-11
| 190616-anomalna-serafina            | Zły na Pięknotkę - wprowadziła Grzymościa do walki z Kajratem zamiast stanąć naprzeciw niego osobiście. | 0110-05-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((190505-szczur-ktory-chroni; 190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Amanda Kajrat        | 4 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy)) |
| Liliana Bankierz     | 4 | ((190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata; 200311-wygrany-kontrakt)) |
| Tomasz Tukan         | 4 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit)) |
| Serafina Ira         | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Krystian Namałłek    | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Lucjusz Blakenbauer  | 2 | ((190505-szczur-ktory-chroni; 200311-wygrany-kontrakt)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 200311-wygrany-kontrakt)) |
| Nikola Kirys         | 2 | ((190622-wojna-kajrata; 190626-upadek-enklawy-floris)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Talia Aegis          | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Tymon Grubosz        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mariusz Trzewń       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
---
categories: profile
factions: 
owner: public
title: Alojzy Lemurczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211212-nie-taki-bezkarny-mlody-tien | 17 lat. Wyżej w hierarchii niż Roland Sowiński. Swego czasu narozrabiał poza Aurum, więc wysłano za nim Stalową Kompanię. Koleś szuka guza, ale nie z Rolandem. Porwany przez Stalową Kompanię wraz z dwoma innymi magami. | 0101-04-10 - 0101-04-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klara Gwozdnik       | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| Nikczemniczka Diakon | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| Roland Sowiński      | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
---
categories: profile
factions: 
owner: public
title: Karol Reichard
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210526-morderstwo-na-inferni        | Młodszy służbista z admiralicji; zażądał od Klaudii uwolnienie Tala Marczaka z Inferni bo nie wolno go zamykać (a tak pokazują systemy). Zneutralizowany przez Klaudię - szukają go, poszedł na dziewczynki. | 0111-10-26 - 0111-11-01 |
| 211110-romans-dzieki-esuriit        | upewnia się, że Arianna nie robi GIER WOJENNYCH BEZ ZEZWOLENIA! Za to dostaje wpiernicz (Klaudia antydatowała gry wojenne). | 0112-02-07 - 0112-02-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211110-romans-dzieki-esuriit        | bardzo ostrożny we wszelkich kontaktach z Infernią. WIE, że coś jest nie tak, ale nie ma jak tego udowodnić lub się bronić. | 0112-02-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Elena Verlen         | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Eustachy Korkoran    | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Klaudia Stryk        | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Leona Astrienko      | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Maria Naavas         | 1 | ((211110-romans-dzieki-esuriit)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 1 | ((210526-morderstwo-na-inferni)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Wawrzyn Rewemis      | 1 | ((211110-romans-dzieki-esuriit)) |
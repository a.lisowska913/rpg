---
categories: profile
factions: 
owner: public
title: Hestia d'Dorszant
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | Przebudzona przez Zespół, by wspierać Stację najlepiej jak się da. Naukowo-administracyjna, o pierwszym poziomie topozoficznym. Pomaga w zagłuszaniu sygnału Kuratorów. | 0110-06-14 - 0110-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Gerwazy Kruczkut     | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Jaromir Uczkram      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kamelia Termit       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Oliwier Pszteng      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Rafał Kirlat         | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Szymon Szelmer       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
---
categories: profile
factions: 
owner: public
title: Fergus Salien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190326-arcymag-w-raju               | świetny taktyk i cyber-terminus; wymanewrował kilkukrotnie Elizę i stał za planem przyłączenia Elizy do walki przeciwko Astorii. | 0110-04-07 - 0110-04-08 |
| 190521-dwa-stare-miragenty          | rozmontowywał wszystkie pułapki krystaliczne i negocjował współpracę z Elizą. W sumie - jednej nie rozbroił. Zestrzelił miragenty. | 0110-04-15 - 0110-04-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eliza Ira            | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Olga Leszcz          | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Alojzy Wypyszcz      | 1 | ((190521-dwa-stare-miragenty)) |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fred Wypyszcz        | 1 | ((190521-dwa-stare-miragenty)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
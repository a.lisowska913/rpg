---
categories: profile
factions: 
owner: public
title: Michał Waczarek
---

# {{ page.title }}


# Generated: 



## Fiszki


* chętny pomocnik Lucasa, 17 | @ 230307-kjs-stymulanty-szeptomandry
* (ENCAO:  0-+-0 |Nie przejmuje się niczym;;Nie ma absolutnie żadnych granic | VALS: Stimulation, Power | DRIVE: POKAŻĘ magom, wyrwać się stąd) | @ 230307-kjs-stymulanty-szeptomandry

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230307-kjs-stymulanty-szeptomandry  | 17-latek, który chciał się wyrwać z Verlenlandu i poprosił Lucasa o pomoc. Poszukiwał informacji od Marty dla Lucasa by się wykazać. | 0095-06-26 - 0095-06-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| KDN Kajis            | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Marta Krissit        | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Żaneta Krawędź       | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
---
categories: profile
factions: 
owner: public
title: Cezary Zwierz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181112-odklatwianie-ateny           | zrobił wywiad z interesującą Diakonką, która chciała przelecieć kralotha na śmierć. To była Pięknotka w przebraniu. | 0109-10-22 - 0109-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Atena Sowińska       | 1 | ((181112-odklatwianie-ateny)) |
| Lucjusz Blakenbauer  | 1 | ((181112-odklatwianie-ateny)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Pięknotka Diakon     | 1 | ((181112-odklatwianie-ateny)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
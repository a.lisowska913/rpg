---
categories: profile
factions: 
owner: public
title: Tymon Krakdacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | advancer-pilot, główny rywal Eleny w zespole. Poza próbą bycia najlepszym i poza odrzuceniem Eleny niczym się nie wsławił. | 0099-03-22 - 0099-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((221004-samotna-w-programie-advancer)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Karsztarin        | 1 | ((221004-samotna-w-programie-advancer)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
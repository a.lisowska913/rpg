---
categories: profile
factions: 
owner: public
title: Adrianna Kastir
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  +-0-0 |Beztroska i nieco naiwna;;Sarkastyczna i złośliwa | VALS: Universalism, Conformity | DRIVE: Rule of Cool | @ 230113-ros-adrienne-a-new-recruit
* atarienka, OBSERWATORKA | @ 230113-ros-adrienne-a-new-recruit
* kadencja: gadatliwa, ma zawsze rację, chce pomóc | @ 230113-ros-adrienne-a-new-recruit
* "My, dziewczyny musimy trzymać się razem", "Nie byłoby fajnie gdyby...", "Razem osiągnięmy więcej!!!" | @ 230113-ros-adrienne-a-new-recruit

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | young woman having AMAZING GRADES who wanted to join ROS. Can do spacewalk and medical. Has some political problems issues in Orbiter. Masked herself as a noctian for EVERYONE EXCEPT NOCTIANS (looks like blackface). Can do her job as a ROS team member, however is inefficient. Talks WAY too much, however is generally chipper and brave. She is acting properly like a medic should, under stress and duress, protecting people. | 0083-12-07 - 0083-12-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | changed her looks to look more noctian than atarien (savaran-pattern). Got accepted in ROS team as an intern. | 0083-12-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aster Sarvinn        | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Napoleon Myszogłów   | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Niferus Sentriak     | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Talia Irris          | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Tristan Andrait      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
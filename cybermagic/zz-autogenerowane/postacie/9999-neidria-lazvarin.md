---
categories: profile
factions: 
owner: public
title: Neidria Lazvarin
---

# {{ page.title }}


# Generated: 



## Fiszki


* Duch, echo noktiańskiej, niezwykle charyzmatycznej artystki i genialnej mentorki. Niezwykła mentorka, ale też niezwykle niebezpieczna gdyż jest tak pewna siebie. | @ 230418-zywy-artefakt-w-gwiazdoczach
* Jako duch, nie przyjmuje już więcej danych ani faktów, jest zapętlonym echem. | @ 230418-zywy-artefakt-w-gwiazdoczach
* Zginęła podczas wojny, normalnie jest zasealowana i ma zablokowane informacje o Esuriit, ale pozostałe elementy zostają. Pełni rolę instruktorki i mentorki. | @ 230418-zywy-artefakt-w-gwiazdoczach

### Wątki


szamani-rodu-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | coś ją unsealowało; odpowiedziała na silne emocje Sary i pokazała jej jak zrobić tatuaż Esuriit. "Jeśli Ci naprawdę zależy, oddasz wszystko. Jeśli nie, nie powinnaś nic dostać." Cichy instigator. Ale co ją unsealowało? Elena ją złapała i spróbuje do tego dojść. | 0094-10-04 - 0094-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
---
categories: profile
factions: 
owner: public
title: Antoni Kotomin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190208-herbata-grzyby-i-mimik       | dowodził ludźmi w obronie przeciwpożarowej a potem słownie wdeptywał idiotów w ziemię; pokonał z Pięknotką mimika symbiotycznego. | 0110-03-03 - 0110-03-05 |
| 190213-wygasniecie-starego-autosenta | koordynator i taktyk; wydobywał dużo danych o przeszłości autosenta. Chciał uratować Teresę i nie był szczęśliwy, że się nie dało. | 0110-03-07 - 0110-03-08 |
| 190226-korporacyjna-wojna-w-mmo     | wraz z Alanem Bartozolem dotarli do Sekatora Pierwszego i zmusili go do współpracy. | 0110-03-13 - 0110-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Bankierz     | 3 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta; 190226-korporacyjna-wojna-w-mmo)) |
| Baltazar Rączniak    | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Jadwiga Pszarnik     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Pięknotka Diakon     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Atena Sowińska       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
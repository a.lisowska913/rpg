---
categories: profile
factions: 
owner: public
title: Elea Brzozecka
---

# {{ page.title }}


# Generated: 



## Fiszki


* zna tysiące opowieści. Opiekuje się dziećmi. | @ 230425-klotnie-sasiadow-w-wanczarku
* pierwsza ofiara mandragory. Chciała zabić swojego narzeczonego, Ilfonsa Lawellana. | @ 230425-klotnie-sasiadow-w-wanczarku

### Wątki


szamani-rodu-samszar
mroczna-wiedza-samszarów

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | chciała zabić swojego narzeczonego, Ilfonsa Lawellana pod wpływem Mandragory. To sprawia, że nikt w miasteczku jej o nic nie wini. Oni znają przeszłość. | 0095-05-16 - 0095-05-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Karolinus Samszar    | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
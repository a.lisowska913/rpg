---
categories: profile
factions: 
owner: public
title: Henryk Urkon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220716-chory-piesek-na-statku-luxuritias | szef ochrony SL Rajasee Bagh. Niechętny do Orbitera, ale przekonany przez Klaudię współpracuje by uchronić statek przed Śmiechowicą. Ochronił swoich mocodawców politycznie przed gniewem Orbitera. | 0109-05-05 - 0109-05-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Fabian Korneliusz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Klaudia Stryk        | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Martyn Hiwasser      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| OO Serbinius         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
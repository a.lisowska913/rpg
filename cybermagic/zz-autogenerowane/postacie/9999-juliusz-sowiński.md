---
categories: profile
factions: 
owner: public
title: Juliusz Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | kuzyn Anastazji i przywódca Szturmowców Sowińskich. Paladyn, chciał zniszczyć Tesserakt Elizy Iry a potem samą Elizę. Łażą po pustyni, prowadzeni przez Wanessę. | 0111-01-07 - 0111-01-10 |
| 201210-pocalunek-aspirii            | chce odzyskać Anastazję, jest przerażony tym, że ona chce wstąpić do armii. Wysłał do pomocy Anastazji OA "Zguba Tytanów". | 0111-01-26 - 0111-01-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Arianna Verlen       | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Eustachy Korkoran    | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Klaudia Stryk        | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| AK Nocna Krypta      | 1 | ((201210-pocalunek-aspirii)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Ataienne             | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |
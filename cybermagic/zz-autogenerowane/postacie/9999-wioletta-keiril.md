---
categories: profile
factions: 
owner: public
title: Wioletta Keiril
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200106-infernia-martyn-hiwasser     | uratowana przez Orbitera jako nie-mag cywil, dzięki Martynowi weszła w Gry Wojenne. Ogólnie, słaby agent; Martyn zostawił w niej informację do Zespołu kto go porwał. | 0110-06-28 - 0110-07-03 |
| 200115-pech-komodora-ogryza         | ma przechlapane na Kontrolerze Pierwszym; Janet i Klaudia wpakowały ją do sił Ogryza. Niestety, nie sprawdziła się, ale została już z Ogryzem... | 0110-07-20 - 0110-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Klaudia Stryk        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Martyn Hiwasser      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Arianna Verlen       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Kamil Lyraczek       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
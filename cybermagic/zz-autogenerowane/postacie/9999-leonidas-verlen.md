---
categories: profile
factions: 
owner: public
title: Leonidas Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | (12) trzeci raz na niedźwiedziowisku, dobry w chowaniu się, gdy się mocno przestraszył do jego Paradoks sformował "Blakenbestię" z dźwiedzich komandosów. Gdy się schował, instruktorka go znalazła i przekonała do skonfrontowana się z potworem pod wodospadem. Pokonał niedźwiedziowisko i został wreszcie dorosłym Verlenem. Rodzice chcieli wojownika a dostali, cóż, jego. | 0095-03-24 - 0095-03-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | NOT a graceful winner. Chełpi się tym, że wszystkich uratował przez Blakenbestią. Ku żałości wszystkich dzieciaków, został tymczasowym bohaterem. | 0095-03-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dźwiedź Ciężka Łapa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Lekka Stopa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Palisadowspinacz | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Strużenka Verlen     | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Wędziwój Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
[conflict]=1-3
[character]=4-6
[result]=7-9
FONT = Arial, 8, B, #000000
TEXT = [conflict],"EASY", 3%, 15%, 30%, 15%, right, bottom, 90
TEXT = [conflict],"NORMAL", 3%, 35%, 40%, 20%, left, bottom,90
TEXT = [conflict],"HARD", 3%, 60%, 30%, 20%, left, bottom,90
TEXT = [conflict],"HEROIC", 3%, 78%, 30%, 20%, left, bottom,90

FONT = Arial, 22, B, #000000
TEXT = [conflict],"3", 30%, 10%, 30%, 20%, right, bottom
TEXT = [conflict],"3", 30%, 30%, 30%, 20%, right, bottom
TEXT = [conflict],"3", 30%, 53%, 30%, 20%, right, bottom
TEXT = [conflict],"5", 30%, 75%, 30%, 20%, right, bottom

TEXT = [conflict],"4", 60%, 10%, 30%, 20%, right, bottom
TEXT = [conflict],"7", 60%, 30%, 30%, 20%, right, bottom
TEXT = [conflict],"10", 60%, 53%, 30%, 20%, right, bottom
TEXT = [conflict],"20", 60%, 75%, 30%, 20%, right, bottom

TEXT = [conflict],"+2", 8%, 10%, 30%, 20%, right, bottom
TEXT = [conflict],"0", 8%, 30%, 30%, 20%, right, bottom
TEXT = [conflict],"-2", 8%, 53%, 30%, 20%, right, bottom
TEXT = [conflict],"-6", 8%, 75%, 30%, 20%, right, bottom

IMAGE = [conflict],"success.png",25%,3%, 10%, 10%, 0, PN
IMAGE = [conflict],"failure.png",80%,3%, 10%, 10%, 0, PN
IMAGE = [conflict],"tie.png",50%,3%, 10%, 10%, 0, PN

LINE = [conflict],0,15%,100%,15%,#000000,0.1
LINE = [conflict],0,32%,100%,32%,#000000,0.1
LINE = [conflict],0,57%,100%,57%,#000000,0.1
LINE = [conflict],0,75%,100%,75%,#000000,0.1
LINE = [conflict],20%,0%,20%,100%,#000000,0.1
LINE = [conflict],45%,0%,45%,100%,#000000,0.1
LINE = [conflict],70%,0%,70%,100%,#000000,0.1

FONT = Arial, 10, B, #000000
TEXT = [character],"General concept (3)", 5%, 10%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Who is the character? What can it do? How do you see them?", 5%, 15%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Motivation (3)", 5%, 25%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"What does the character want for themselves? What for others?", 5%, 30%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Modus operandi (3)", 5%, 42%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Typical actions, skills, differentiatiors?", 5%, 47%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Resources (3)", 5%, 53%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"What does the character have access to? What equipment is characteristic? Batman's toolbelt", 5%, 58%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Magic (3)", 5%, 70%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"What kind of energy does the character control? What happens when they lose control?", 5%, 75%, 90%, 100%, left, wordwrap

FONT = Arial, 22, B, #000000

TEXT = [result],"SUCCESS", 0, 10%, 100%, 100%, center, top
TEXT = [result],"FAILURE", 0, 40%, 100%, 100%, center, top
TEXT = [result],"TIE", 0, 70%, 100%, 100%, center, top

IMAGE = [result],"success.png",20%,20%, 10%, 10%, 0, PN
IMAGE = [result],"success.png",30%,20%, 10%, 10%, 0, PN
IMAGE = [result],"success.png",60%,20%, 10%, 10%, 0, PN
IMAGE = [result],"tie.png",70%,20%, 10%, 10%, 0, PN

IMAGE = [result],"failure.png",20%,50%, 10%, 10%, 0, PN
IMAGE = [result],"failure.png",30%,50%, 10%, 10%, 0, PN
IMAGE = [result],"failure.png",60%,50%, 10%, 10%, 0, PN
IMAGE = [result],"tie.png",70%,50%, 10%, 10%, 0, PN

IMAGE = [result],"success.png",20%,80%, 10%, 10%, 0, PN
IMAGE = [result],"failure.png",30%,80%, 10%, 10%, 0, PN
IMAGE = [result],"tie.png",60%,80%, 10%, 10%, 0, PN
IMAGE = [result],"tie.png",70%,80%, 10%, 10%, 0, PN
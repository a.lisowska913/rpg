CARDSIZE = 6, 9

LINK="archetypy-profil-gen-back.txt"
[all]=1-{(Nazwa)}
FONT = Arial, 12, B, #000000
TEXT = [all],[Nazwa], 1%, 0%, 100%, 100%, center, top

FONT = Arial, 10, B, #000000
TEXT = [all],"Co to?", 3%, 7%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[CoTo], 5%, 12%, 90%, 95%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Tagi", 3%, 30%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Tagi], 5%, 35%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Akcje", 3%, 60%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Akcje], 5%, 65%, 95%, 100%, left, wordwrap
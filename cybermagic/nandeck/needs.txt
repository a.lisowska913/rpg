CARDSIZE = 6, 9


LINK="needs-data.txt"
[all]=1-{(Name)}

FONT = Arial, 12, B, #000000
TEXT = [all],[Name], 1%, 0%, 100%, 100%, center, top

FONT = Arial, 10, B, #000000
TEXT = [all],"By� (w�asno�ci)", 3%, 7%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Being], 5%, 12%, 90%, 95%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Posiada� (rzeczy)", 3%, 25%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Having], 5%, 30%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Dzia�a� (akcje)", 3%, 50%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Doing], 5%, 55%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"W otoczeniu", 3%, 75%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Setting], 5%, 80%, 95%, 100%, left, wordwrap

---
layout: post
title: "Two soldiers problem - PRAM in action"
date: 2015-04-29
---

# So, two soldiers brawl in a bar... who wins?

Imagine two soldiers - Albert and Bob. 
They are of exactly the same skill level and they both are armed with... well, nothing. Fists. They are brawling in the bar. There is only one difference between them - Bob is tired (hasn't slept a lot lately), which gives Albert an advantage.

What are your expectations of the result of this brawl and why (choose the answer CLOSEST to you)?

100) Albert should always win, because he has an advantage; at their skill level every advantage - even the smallest - will determine the victor.
200) Both Albert and Bob have a chance to win (Albert has a higher chance, say, 70% vs 30%); the advantage is not strong enough for Albert to "always win".
300) Both Albert and Bob have a chance to win, because GM should decide in a situation like that. It is too close to "know", so GM or players should decide.

Now, imagine Bob is a player character and Albert is an NPC. But the player controlling Bob (called Zoe from now on) SUCKS. She is a newbie, she really, really tries... but she is simply unable to construct anything more advanced than "well, I try to hit him" or "I try to avoid the hit".

So, what should happen and why (choose the answer CLOSEST to you)? 

10) Albert should definitely win. No matter if Bob's player is a newbie or not (or tries or not), player's declarations doomed poor Bob especially with him being tired.
20) Nothing changed from the previous example ('Albert wins' or 'both have a chance'). Player's declarations may suck, but the "realism" and "world's logic" should override a player.
30) Bob should win (or have a higher chance). It is important to motivate a new player and the situation was pretty close, so it is not a stretch.

Now, imagine Bob is an NPC and Albert is a player character. Albert's player (Yvonne from now on) is not a newbie. She is a veteran RPG player, a very good one. She is awesome, but at this session she seems distracted and her declaration was simply bad - you would expect something like this from Zoe, but not Yvonne. Like: "avoiding that thrown beer by dodging..." standing in the crowd 2 meters from Bob The Beer-thrower (no place to move AND too close to dodge well).

So, what should happen and why (choose the answer CLOSEST to you)?

1) Bob should win (or have major advantage). No matter everything else, this declaration is simply bad and even Albert's advantage cannot change that.
2) Nothing changed from the first example. As lousy as Yvonne's declaration was, the player should NOT doom the character.

Now, sum the numbers from above. For example, if you chose "400, 50, 6" you have 456 (used out-of-bounds numbers not to taint the sample).

# Who should win in those situations above?

This may be a boring answer, but there is no proper answer. It depends - specifically, on you. On your playstyle, on your expectations from this game, on your expectations from realism and other players.
Let's look at the questions more closely.

The first question (100, 200, 300) puts two characters against each other to determine how should a close advantage be resolved.
- '100' implies a very predictable system without randomness or external influence. 
- '200' leaves some things to uncertainty and chance. It accepts the probability (external component outside the players) as a generator of uncertainty. 
- '300' implies that uncertainty should be resolved by arbitrary means; usually using the game master's fiat.

The second question (10, 20, 30) puts a weaker character in the hand of a newbie player with weak declarations, to determine the level of arbitrary influences on decisions.
- '10' implies that player's declaration tilted the scale against the character. The out-of-game component (Zoe = new player) is not very relevant, but in-fiction actions (wrong decisions) are.
- '20' implies that player and their declarations are irrelevant to the game. Player is to do higher level decision making, but task-level resolution is left outside the player's influence.
- '30' implies that who the player is may be more important than their declarations. Out-of-game component will override the in-fiction action declarations and the small predictable advantage.

The third question (1, 2) puts a stronger character in the hand of a strong player with weak declarations, to eliminate the 'human' component.
- '1' implies that the arbitrary component should be more important than small predictable advantage. 
- '2' implies that predictable component should be stronger than arbitrary component.

# What does it have to do with PRAM?

PRAM (Predictable Random Arbitrary Meta) is a mechanics analysis tool described in the previous post:
<a href="rpg/2015/04/23/pram-mechanics-analysis-tool.html">PRAM - mechanics analysis tool</a>

Let's look at this example using the lenses PRAM gives us.
Note: Meta and Arbitrary will meld in some of those examples; they shall be denoted as "M+A" when they do.

- 100: P is the dominant component. Neither Random nor Arbitrary resolutions are acceptable even with close situations (low differences of P).
- 200: in case of low difference of P (dP), R is acceptable. (say, dP_thisCase=2, dR=4 or more)
- 300: in case of low difference of P (dP), A is acceptable. (say, dP_thisCase=2, dA=4 or more)

What is important here: assume you are playing THIS game with 2 soldiers in a bar with particular mechanics:

dP = 2 (say, Albert has 'fight' skill = 10, Bob 'fight' skill = 8 (because he is tired))
dR = 5 (say, you roll 1d6)

If you chose '100' or '300', this will not work for you. If you chose '200', it MIGHT (does work in THIS example of 10 and 8, but may not in OTHERS; depends what '10' and '8' mean in-fiction).

Next one:

- 10: P (slightly weaker character) + A (much weak declaration) > "M + A" (Zoe is a new player).
- 20: A is irrelevant; A is probably not an acceptable component in conflict resolution.
- 30: "M + A" (Zoe is a new player) > P (slightly weaker character) + A (much weak declaration)

Now, why "M+A" not M: because here, this is blurred. GM's decision takes into account the out-of-game situation (Zoe is a new player) but GM does not TEST Zoe (not in those examples). Thus, "M+A" as this was - in a way - an Arbitrary decision made by the GM with no Zoe's input, but based on out-of-game premises.
The beauty of Arbitrary component is that GM (or other players) do not have to explain it. This is an Arbitrary modifier, by the very definition. So it can be "because I liked your declaration" but nothing can stop the GM from doing it like "you brought my favourite snacks tonight" and explain to others "there is something you don't know".
Thus "M+A" (I will explain the difference between those two thoroughly in a different post).

Note that people who chose '20' here will hate the Golden Rule (GM can override any rule at any time) with a passion. '10' and '30' are something which let's you finetune your approach to M and A (how much a player and out-of-game situation matter, how much declarations changing in-fiction stuff matter).

Next one:

- '1': A (bad declaration) > P (slightly stronger character)
- '2': A is irrelevant.

Note how '1' and '2' mirror '30' and '20', but without the taint of "new player".

# Using PRAM, let's read the results

Note: we have made 3 tests. This does in NO WAY show you your playstyle. It only hints some components. The exact expectation analysis would require about 100 tests like this one. What is to be seen below is that the playstyles will differ a lot between different answers, and therefore the conflict resolution mechanics will also have to differ a lot.

- '231': dR > low dP, "M+A" > P + A, dA > low dP.
    We have a mechanics where in case of a tie, randomness can be used to resolve it. A GM (or other players) should take into account SOME components of what happens out of game and, for example, help a new player (new player should have compensation for their bad declarations) or punish a vet who is all like "meh I hit him". A declaration can override a stats advantage. What is unkown (not tested in three cases above) is the declaration-to-randomness influence proportion.

- '311': A > low dP, "M+A" > P + A, dA > low dP
    This is a hint towards so-called "storytelling", that is "story, dynamics of a game etc are more important than 'game-y' components". The conflicts are resolved by the general consensus or GM's will, while the game is very immersive and players 'live' the story. Those tests do not contain the 'dP_high-to-arbitrary' component or 'meta-to-arbitrary' component, so it is impossible to see if the players are more important (and taxed) or the characters matter more. What the players in real world do and what they are can override the bad in-fiction declaration even with small predictable disadvantage.
    
- '222': dR > low dP, A is irrelevant
    We have a hard, predictable, tactical mechanics where the player declarations shall be centered around "what" do they do, not "how" they do it with GM being a processor. A beautiful description by a player, an interesting declaration of resolution do not matter. This is how 'RPGs of old' used to do it, the ones which came from the tabletop wargames. Note: we do not know the dP_high-to-R relation and it is extremely easy to break this type of game without good numbers. 
    
As you can see, the above presents really different playstyles. All are valid, they are simply different.
For fun, imagine a GM of style 222 and players of style 311. Or opposite.
Awesome, isn't it?

What you can do with PRAM: take a set of interesting situation which happened on your session, tell your expectations (each separately), note where they differ, perform PRAM analysis on your mechanics and then move the numbers on PRAM where you want them to be. Then, reverse engineer the mechanics so it works as intended.

Or don't. But whatever you do, acknowledge the differences and choose how this particular action should have been resolved. Together.


# A real life example - Warhammer 1st Edition

Warhammer 1st edition was fun like that (I played it very long time ago; the situation below happened on our sessions). 
This was to be a dark fantasy world where everything bad could have happened. So they had critical hits - when you rolled '6' for damage on 1d6, you took 6 and rolled again. If you got '6', the same happened. This way a single goblin could kill any player.

So, R was infinite.

And so our GM put a MASSIVELY POWERFUL DEMON against us. I think it was a Khorne Bloodletter. We were caught and we were to be interrogated, and a demon wanted to toy with us. So here we have it: one naked dwarf (literally naked), my character, against a Khorne Bloodletter. And he was like "go ahead, hit me, puny mortal". And so I did.

Somehow I was insanely lucky. Not only I managed to roll under my Melee Skill to actually hit a monster, but I got a critical. And a critical. AND a critical...
I think it was 4 '6's in a row (0.08% chance for this to happen).

My naked dwarf woodsman punched the demon and killed him outright. 
One punch from a dwarven fist.
And that demon - a moment ago - captured us and killed our convoy...
THE POWER OF A DWARVEN WOODSMAN!

PRAM shows this problem very easily:

Damage is calculated as Strength (usually 2-5) + weapon modifier (if I recall correctly; not sure of this) + 1d6 (with crits).
So: 
dP is like... 5 (1-6 Strength) + 3 (let's assume weapon could give up to +3 damage) = 8
dP_avg = 3+1 = 4 (2-5 strength, weapon +1 were most common).

dR is like... 1d6 with crit with crit... as in, INFINITY.

dR_75% is 4 (no crit, '6' eliminated from equation thus 4)
dR_95% is like (assuming 16.6% chance for 6 to appear)... 1 crit, thus 6 + 1d6 thus 10
dR_99% is like... 2 crits, so 12 + 1d6.
dR_99.5% is 3 crits, so 18 + 1d6.

In human language: 
- difference between best equipped character dedicated to deal damage and one who is lowest possible is 8. 
- if you roll a 1d20 AND if you roll 20 (quite likely), you deal 6+1d6 more damage. 11 is likely. With that lowest possible character.
- random result overpowers everything else.

How to correct this situation?

Easiest way: 
1) Eliminate the crits beyond the first. You want to keep the 'everything can happen' feel but you don't want absurd situations in dark, gloomy setting. Thus, dR = 11.
2) dP is only 9. Change the random damage die resolution to 1d3. Now dR is: 3 + 1-3 = 6 if very lucky, 1-2 in 75% cases.

dR_75% = 6 with dP = 9
dR_95+% = 3+1-3 = 6 with dP = 9

Of course, I have just broken the relation between Toughness, Strength and other stuff in Warhammer, but I think you see my point in this single example.
A GM cannot plan an encounter if they have to deal with potentially infinite dice resolutions. Neither can players. And with randomness being outside the solar system, lots of hilarious and stupid situations were happening.

And the situations like this one happen in most RPGs I know.

Thus PRAM.
---
layout: post
title: "PRAM - mechanics analysis tool"
date: 2015-04-23
---

# What is it for?

Have you ever looked at a RPG game and wondered one of the following questions:
- Will this game work for my group? Is it compatible with our playstyle?
- What type of sessions work with this game? How does this game actually FEEL?
- I do not have d12... can I use a d20 instead?
- Is this game 'realistic'? 'fair'? 'immersive'?

We had those questions as well. 
To be ale to get an answer better than "well... maybe..." we had to come up with something that would let us analyse and compare different gaming systems.

PRAM mechanics analysis tool can approximate the answers to the questions like those above, assuming you know what is the playstyle of the group.


# What is PRAM?

PRAM is an acronym for "Predictable, Random, Arbitrary, Meta" - four main components of the RPG mechanics. 
PRAM attempts to take an existing mechanics and split it into those four components trying to gauge how does this mechanics work and how will a session feel. PRAM focuses on conflict resolution submodule of the RPG mechanics, that is: "which player's declaration became reality in the shared fiction" (I say I have escaped the guards, you say the guards caught me - Conflict Resolution mechanics determines which of those two happened, in simplification).

'Predictable' is a component for everything which can be predicted by the players; the important thing is that the players know what the P component will be in the conflict (in the worst case, they can make an educated guess).
'Random' is a component for everything which does not depend on any player in any way.
'Arbitrary' is any kind of fiat bonus that is granted by a GM or other players.
'Meta' is a component for everything which tests the PLAYER sitting at the table, not the CHARACTER in fiction.

# An example of PRAM analysis of the mechanics:

## Mechanics:

A character sheet is composed of stats and skills which are added to each other. For example, if bard wants to sing, they add STAT and SKILL. Then they add a result of a die roll to those two. If the declaration was very good, GM can provide up to 3 to the result. If the final result exceeds the success threshold, it was a success. 

STAT can be in the range of [2, 12]
SKILL can be in the range of [0, 10]
DIE ROLL is a 1d100 roll
ARBITRARY MODIFIERS (GM's fiat in this case) are in the range of [0, 3].

## Analysis:

dComponent = difference between component values. For example, dP means difference between Predictable values.

Predictable: sum of stat and skill; no tactical modifiers possible, so the maximum possible range (max - min): dP = (12 - 2) + (10 - 0) = 20
Random: the maximum possible range between 1d100: dR = (100 - 1) = 99
Arbitrary: the maximum possible range between arbitrary modifiers: dA = (3 - 0) = 3
Meta: unknown, unspecified; assuming: none, thus: dM = 0.

What does PRAM of dP = 20, dR = 99, dA = 3, dM = 0 mean for players in the game?

First, let's look at the dominant component of this mechanics. In this case it is Random. And Random is really dominating: Random (99) > Predictable + Arbitrary + Meta (23 total).

This means that the game is ENTIRELY governed by the Random component (in this case, 1d100 roll). Even if a player has a minmaxed Conan the Barbarian facing the lowliest, wimpiest goblin; even if the player tries to arrange the best situation possible, he can still not defeat that goblin because of a dice roll.

Does this mean this game is broken?
Not necessarily.

Consider the game about an ordinary human at the faerie court. The powerful entities there are ruling over the fate of puny mortals according to their whims and desires. This type of mechanics - with overwhelming Random component would work pretty well there, though with a bit different parameters:

dP = 10 (no matter who you are, though being of the particular fae's type helps...)
dA = 10 (...no matter how you approach them, though a proper approach and acting as this fae expects you to helps...)
dR = 20 (...the whims and randomness shall rule over your fate...)
dM = 0  (...no matter what - as a player - you do)

This mechanics is still dominantly random, though now the player can at least do something about the situation (and it conveys a particular 'feel').
From those numbers you can reverse engineer the components of the mechanics knowing what the desired differences of particular game components are.

Note that some people would love this type of game, while some other would absolutely hate it. This is what PRAM does - it distills the mechanics into "how does it work" so you can determine if the mechanical part of the game appeals to you and whether the results it generates are acceptable to you.


# Into the details - components of PRAM

## P - Predictable

'Predictable' is a component for everything which can be predicted by the players; the important thing is that the players know what the P component will be in the conflict (in the worst case, they can make an educated guess).

Example of P:

- Character sheet's declaration of intent about the capabilities of the character (Strength = 8)
- The rule which directly specifies "if X happens, you get modifier of Y" (If you flank an enemy, you get +2)
- A tool, giving you a bonus (Sword gives you +3 to Strength when you attack)

The role of 'Predictable' component is to empower players and let them predict the future, help them make proper decisions and show them what their character are good at. It is also used to give players the feeling of strategic-level control (the selection of conflict matters as much as what we do / prepare and what we don't do).
Players are able to use Predictable part to strategize, make high level choices and pick their battles. For example: 
- "I cannot win with the ogre head on, but we can go other way; my Fighting skill is only 8 and his is 20". 
- "if you distract a goblin I will get an opportunity bonus +2"

If Predictable is too strong: the game can become a solvable puzzle. You know exactly what you can win, which battles to pick and you can solve the session before it starts.
If Predictable is irrelevant: players' high level decisions do not matter, players cannot control or predict their own future, character description is not consistent with action resolution (Conan lost against a goblin in hand-to-hand combat).

Calculate dP as a difference between maximum possible value of P (including all possible modifiers) and lowest possible value of P (including all possible modifiers). Yes, you do count the unlikely modifiers (you can calculate the 95% cases and 5% cases separately to better gauge the feels; personally, I check 95%, 99%, 100% range bands separately if applicable). 
Always try to calculate dP_avg as well, that is, predicted most common test situation you will have to deal with; this helps you in seeing the usual case and outliers. Or, as an alternative, you can infer the dP_avg from the mechanics and use it as a baseline test on your sessions to get the feel you want to achieve.

Example of calculation:

Stat is in the range [2, 12]
Skill is in the range [1, 5] (skill of 0 means that the skill cannot be used)
Max bonus for multiple attackers: +3
Max bonus for ambush: +2

dP = (12 - 2) + (5 - 1) + 3 + 2 = 19


## R - Random

'Random' is a component for everything which does not depend on any player in any way.

Example of R:

- 1d20 (a single 20-sided die)
- a deck of cards in which red cards are 'hits' weighted by the card value (and black are 'misses')
- a dice pool of: 2d4, 2d6, 2d8, 2d10; roll and discard 2 rolls of your choice

The role of 'Random' component is to make it impossible to plan absolutely everything, inject some chaos and unknown, add some adrenaline and make the game more exciting. Injecting controlled randomness will make the sessions more varied and will ensure that every player's input has a chance to be put into fiction in some way. Randomness helps with equalizing players of very different RPG ability levels, therefore if randomness is done well it is friendly to beginners.

If Random is too strong: nobody controls anything. Pure chaos.
If Random is irrelevant: those players who are worse in either minmaxing their characters, or worse in negotiations, or less charismatic - their ideas can get ignored. The game can get very predictable and stale.
More on influences of Random components:
http://www.compete-complete.com/2013/01/randomness-in-games.html

Calculate dR as a difference between maximum possible influence of R and lowest possible influence of R. Count unlikely modifiers. Here, calculate 95% band, 99% band and 100% band separately if you deal with cards / dice pools; this is not needed for single die.

Example of calculations:

- you have a 1d6. dR = 6 - 1 = 5.
- you have a 1d20 translated into [-4, 4] range. dR = 4 - 0 = 4 (influence is 4, even if difference is 8)
- every test we take 10 cards from the deck. Numbers are '1', Jack is '2', Queen is '3', King is '4'. Only red cards matter (Diamonds, Hearts).

lowestR = 10 black cards = 0
highestR = 2 red kings, 2 red queens, 2 red jacks, 4 red numbers = 8+6+4+4 = 22
dR = 22 - 0 = 22

## A - Arbitrary

Arbitrary is any kind of fiat bonus that is granted by a GM or other players. For a bonus to be arbitrary the application of such has to be granted by living people, not automatic because of rules (in which case it would be Predictable).

Example of A:

- "as Talia is an elf and a prince loves elves, you get +2 to charming him" (GM's decision "outside mechanics")
- player: "I jump left and throw sand at enemy's face and using the confusion I stab him with a dagger"; GM: "wow, nice; have +3"
- player: "those are dwarven halls right? I try to find where the vault should be located on this map"; GM: "you're a dwarf, ey? Sensible... take +1"
- player A: "I try to prank B"; player B: "harsh. I wouldn't expect that from partymate; take +2 against me"
- GM: "this ogre should have problems hitting you now you blinded him; take +4 to dodge against him"

Note all the cases above were neither on the character sheet nor in the mechanics itself. This comes from people's individual judgement on the situation in Shared Fiction.

The role of 'Arbitrary' component is to reward creativity and ingenuity in players. On top of that it protects the mechanics from bloating; instead of adding every single rule and every single case you can abstract quite a lot to Arbitrary bonuses in a sensible range.
Unlike Predictable which works on high level decision making (engage or not) and high level tactics (flank, blind... what to do) Arbitrary component is used to give the players the feeling of task-level control (the exact way of making a particular action matters, not only what this action is).

If Arbitrary is too strong: the most charismatic player at the table or the one with highest authority (usually game master) has total control over every conflict of the game. The players do not control anything, although they may perceive they do. 
If Arbitrary is irrelevant: probably you have something like a board game, but without a board ;-). The mechanics needs more rules for special cases. Declarations are very mechanical.

Calculate dA as a difference between maximum possible influence of A (including all possible modifiers) and lowest possible influence of A (including all possible modifiers).

Example of calculations:

- "GM can give up to +3 for a 'wow' action made by a player": dA = 3
- "Every player at the table can add +2 to a player, once, per session, at a whim": dA = 2 * number of players.
- "A GM can modify a difficulty of this test by the circumstances by [-2, 2]": dA = 2 (influence)

## M - Meta

Meta is a component for everything which tests the PLAYER sitting at the table, not the CHARACTER in fiction.

Example of M:

- "Alice, if you solve this riddle in 1 minute your wizard Altena will get +2 to the test"
- "Sorry, Brian, you declared you threw the grenade but never declared taking off the linchpin. The grenade just lies there on the floor."
- "Christina, you want to stop Ssathair's bleeding? Here, have a bandage; show me how you do it."
- "Darius, if you sing a song your bard Ellirandae will get +4 to her test of charming a prince."

Note every of those cases has tested the PLAYER, but player's ability or inability had an implication of what happens in fiction.

The role of 'Meta' component is primarily to increase immersion and increase realism. Most notably, if the mechanics does not have high reliance on Random component, very often to inject some uncertainty a combination of Arbitrary and Meta shall be used.

If Meta is too strong: you are unable to play a character in the domain you are not an expert of. Furthermore, if a player is not good at something, the character will not be either; if the spellcasting is resolved by solving puzzles only the puzzle experts can play wizards. If you need to show how you try to help the wounded, only someone knowing first aid can play a doctor. Novice players may have it very, very hard.
If Meta is irrelevant: this can hurt immersion badly and it also can lead to "board game" syndrome.

Calculate dM as a difference between maximum possible influence of M (including all possible modifiers) and lowest possible influence of M (including all possible modifiers).

Example of calculation:

- "Say as many types of weapon starting with 'S' you know. For each, you get +1": dM = potentially infinite
- "GM can add a modifier for any possible reason; the modifier can be up to 5": dM = 5 (note: in this case dM and dA are both 5)


# Gates and modifiers

Consider this:

1 - "If you have strength 14, you can open the gate. Else, you cannot"
2 - "If your strength exceeds 14, add +2 to gate opening test"

This first case is a gate: you pass it or not. It ignores PRAM and focuses only on its own component.
This second case is a modifier: it changes the overall result taking other elements of PRAM into account.

Every element of PRAM can be used as modifier or a gate. 
Be very careful using gates; you either make an easy gate (so... why was there a gate at all if it was to be passed anyway?) or you risk walling off a team from the objective and everything will have a problem.


# Example of PRAM usage - the mechanics our group used for a year (before yesterday when we went unstable again)

Every character sheet has 2 categories: "what can they do" (value: 0, 1, 3 or 6) and "what are they like" (value: 0, 1, 2 or 4). When you enter the conflict you cross-combine one of skills and one of traits. Furthermore, the character can have tools and support (+1, +2 or +3). If you use magic, you have a flat-out always-cumulative bonus of +2.

When you enter the conflict, you use 1d20 which is changed into pseudo-normal distribution between [-4, 4].
GM is the only person who can add fiat modifiers and all the modifiers need to take into account in-fiction situations. No out of fiction situation shall be taken into account.
GM can add arbitrary bonuses for circumstances, description, tactics and adaptation in range of [-3, 3].

Thus the mechanics looks like this in PRAM analysis:

dP_100% = 6+4+3+2 = 15.
dP_99% = 12
dP_90% = 10
dP_avg = 3  (comes from our group's experience)

dR = 4
dA = 3
dM = 0

What does that say about the mechanics we use?

- The mechanics is very skewed towards the planning and predictability. Players are able to choose the fights, to strategize and to optimize the way they play and build the characters.
- Random component is really important only in the fights which are pretty close (the distribution means that -4 and +4 will appear only in 5% cases each.
- Players' reactive creativity and ingenuity is similar to random component; they matter in pretty close situations.

Now, dP towers over dR and dA which makes the system very deliberate and 'pre-planned'. The absence of dM (especially combined with low influence of dA) means that immersion is not the focus of this particular mechanics, but tactics are.

Look at dP_avg, that is, average difference of opponents; the value of 3 is pretty natural seeing how the characters are created (and that the sessions tend to be balanced around tests at values 5-7). Although there exists a way to create very unequal odds the usual winnable conflict will be in dA + dR range. This means that proper declaration of actions and preparation combined with a little luck will allow the player to win a difficult situation. And THIS implies that there are many conflicts which you should never start, but also lots of conflicts you can win - and this exactly is where this game is fun. By manipulating Predictable component and selecting the fights and circumstances. Especially seeing how you can get 3 from Predictable bonuses and 2 more from Predictable magic.

This mechanics proved to be very good for beginners; they have all the information they need on the character sheet, they don't have to be good on generating awesome and flashy descriptions on the fly or use some amazing tactical declaration prowess (losing only dA), they don't have to fight their natural shame by singing or showing something / challenging themselves as people (losing dM bonuses which are inexistent here) which means we had something like this:

veteran player: dP + dR + dA = ~10 + [-4, 4] + [0, 3]
novice player: dP + dR + 0 = ~10 (less optimized but impossible NOT to hit 10 because of character sheet design) + [-4, 4]

Therefore novice player had only [0, 3] less than a veteran which could be compensated by different conflict selection and luck.

We do not use any gates; only modifiers.

Now, THIS is a particular playstyle we like in THIS particular system. But as you see, the mechanics analysis tool can give you some predictions about the feel of the mechanics; thus, you can predict how will the whole game feel.